# -*- coding: utf-8 -*-
from django.utils.translation import ugettext as _
import logging

import pytest
from django.urls import reverse

logger = logging.getLogger(__name__)


@pytest.mark.xfail
def test_NotificationView_get(client):
    response = client.get(reverse('yandex_money_notification'))
    assert 405 == response.status_code


@pytest.mark.xfail
def test_NotificationView_post_bad(client):
    data = {}
    response = client.post(reverse('yandex_money_notification'), data)
    assert 400 == response.status_code


@pytest.mark.xfail
def test_NotificationView_post_good(client):
    data = {
        "id": "22e12f66-000f-5000-8000-18db351245c7",
        "status": "succeeded",
        "paid": True,
        "amount": {
            "value": "2.00",
            "currency": "RUB"
        },
        "captured_at": "2018-07-18T11:17:33.483Z",
        "created_at": "2018-07-18T10:51:18.139Z",
        "description": "Order #72",
        "metadata": {},
        "payment_method": {
            "type": "bank_card",
            "id": "22e12f66-000f-5000-8000-18db351245c7",
            "saved": False,
            "card": {
                "first6": "555555",
                "last4": "4444",
                "expiry_month": "07",
                "expiry_year": "2022",
                "card_type": "MasterCard"
            },
            "title": "Bank card *4444"
        },
        "refunded_amount": {
            "value": "0.00",
            "currency": "RUB"
        },
        "test": False
    }
    response = client.post(reverse('yandex_money_notification'), data)
    assert 200 == response.status_code
