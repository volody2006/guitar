# -*- coding: utf-8 -*-
import json

import pytest
from django.urls import reverse
from django.utils.translation import ugettext as _
import logging

from images.models import ContentImage

from manage import rel

logger = logging.getLogger(__name__)


@pytest.fixture
def image_file():
    import io

    image = rel('src/test_cases/test.jpg')

    with open(image, "rb", buffering=0) as fp:
        return io.BytesIO(fp.read())

@pytest.fixture
def text_test_file():
    import io

    image = rel('src/test_cases/text_test_file.txt')

    with open(image, "rb", buffering=0) as fp:
        return io.BytesIO(fp.read())


def test_upload_images(client_login, image_file):
    url = reverse('upload_images')
    data = {'file[]': [image_file, ]}

    response = client_login.post(url, data=data)
    # print(response.content)
    # print(json.loads(response.content)[0]['file']['id'])

    assert 200 == response.status_code
    assert 1 == ContentImage.objects.all().count()
    assert 1 == json.loads(response.content)[0]['file']['id']


def test_upload_images_not_image(client_login, text_test_file):
    url = reverse('upload_images')
    data = {'file[]': [text_test_file, ]}

    response = client_login.post(url, data=data)
    # print(response.content)
    # print(json.loads(response.content)[0]['file']['id'])

    assert 200 == response.status_code
    assert 0 == ContentImage.objects.all().count()
    assert [] == json.loads(response.content)
