# -*- coding: utf-8 -*-
import logging
from datetime import timedelta
from decimal import Decimal

import pytest
from django.utils.timezone import now

from lang.models import Language
from offer.models import Currency
from shop.models import Composer, ShopOffers, UserFile, Sale
from shop.tasks import set_daily_sale, task_sale_down
from users.models import User

from manage import rel

logger = logging.getLogger(__name__)


@pytest.fixture
def users_8():
    from users.models import User
    for i in range(8):
        User.objects.create_user(
            email='test_user{}@email.com'.format(i),
            password='Test12345',
            is_active=True,
            slug='test_user{}'.format(i)
        )


@pytest.fixture
def items_users(users_8, mixer, user_superuser):
    composer, cre = Composer.objects.update_or_create(
        slug='testcomposer2',
        defaults={
            'name': 'Test Composer 2',
            'accepted': True,
        }
    )
    for user in User.objects.filter(is_active=True):
        kwargs = {
            'name': 'Mane',
            'description': 'Distrip oil',
            'price': 100,
            'currency': mixer.blend(Currency),
            'composer': composer,
            'difficulty': 1,
            'user_created': user,
            'checked': True,
            'video_url': 'https://www.youtube.com/watch?v=EadZchN5_F8',
            'moderator': user_superuser
        }
        items = []
        for i in range(0, 10):
            kwargs.update({'name': '{0} {1} {2}'.format('Test name', i, user.pk)})
            item = ShopOffers.objects.create(**kwargs)
            filename = rel('src/test_cases/test.jpg')
            with open(filename, mode='rb') as f:
                uf = UserFile.objects.create(item=item, user_created=user)
                uf.file.save(filename, f)
            items.append(item)


def test_set_daily_sale_not_agree_promotion(users_8, items_users):
    # Если нет достойных пользователей, то нет и товаров
    users = User.objects.all()
    users.update(not_agree_promotion=True)
    set_daily_sale()
    assert 0 == ShopOffers.objects.filter(discount__gt=0).count()


def test_set_daily_sale(users_8, items_users):
    # все товары не имеют скидку.
    # users = set([offer.user_created for offer in
    #              ShopOffers.objects.published().order_by('user_created_id').distinct('user_created')])
    for i in ShopOffers.objects.all():
        assert 0 == i.discount

    set_daily_sale(test=True)

    assert 4 == ShopOffers.objects.filter(discount__gt=0).count()


def test_set_daily_sale_agree_sale_60(users_8, items_users):
    users = User.objects.all()
    users.update(agree_sale_60=False)

    # все товары не имеют скидку.
    assert 0 == ShopOffers.objects.filter(discount__gt=0).count()
    set_daily_sale(test=True)

    assert 3 == ShopOffers.objects.filter(discount__gt=0).count()
    for discount in [90, 45, 30]:
        offers = ShopOffers.objects.filter(discount=discount)
        assert 1 == offers.count()

    assert 0 == ShopOffers.objects.filter(discount=60).count()


def test_set_daily_sale_agree_sale_90(users_8, items_users):
    users = User.objects.all()
    users.update(agree_sale_90=False)

    # все товары не имеют скидку.
    assert 0 == ShopOffers.objects.filter(discount__gt=0).count()
    set_daily_sale(test=True)

    assert 3 == ShopOffers.objects.filter(discount__gt=0).count()
    for discount in [60, 45, 30]:
        offers = ShopOffers.objects.filter(discount=discount)
        assert 1 == offers.count()
    assert 0 == ShopOffers.objects.filter(discount=90).count()


def test_set_daily_sale_agree_sale_30_45(users_8, items_users):
    users = User.objects.all()
    users.update(agree_sale_30=False)

    # все товары не имеют скидку.
    assert 0 == ShopOffers.objects.filter(discount__gt=0).count()
    set_daily_sale(test=True)

    assert 3 == ShopOffers.objects.filter(discount__gt=0).count()
    for discount in [90, 60]:
        offers = ShopOffers.objects.filter(discount=discount)
        assert 1 == offers.count()

    assert 0 == ShopOffers.objects.filter(discount=30).count()
    assert 1 == ShopOffers.objects.filter(discount=45).count()


def test_set_daily_sale_2(users_8, items_users):
    # все товары не имеют скидку.
    assert 0 == ShopOffers.objects.filter(discount__gt=0).count()

    set_daily_sale(test=True)

    old_offer = ShopOffers.objects.filter(discount__gt=0)
    assert 4 == old_offer.count()

    old_offer_ids = list(old_offer.values_list('id', flat=True))
    old_offer_sellers_ids = list(old_offer.values_list('user_created_id', flat=True))

    set_daily_sale(test=True)
    old_offer = ShopOffers.objects.filter(discount__gt=0)
    new_offer_ids = list(old_offer.values_list('id', flat=True))
    new_offer_sellers_ids = list(old_offer.values_list('user_created_id', flat=True))

    assert 0 == len(set(old_offer_ids) & set(new_offer_ids))
    assert 0 == len(set(old_offer_sellers_ids) & set(new_offer_sellers_ids))


def test_set_daily_sale_not_agree_promotion_offers(users_8, items_users):
    # Если нет достойных товаров, то нет и товаров в акциях
    offers = ShopOffers.objects.all()
    offers.update(agree_promotion=False)
    set_daily_sale(test=True)
    assert 0 == ShopOffers.objects.filter(discount__gt=0).count()


def test_set_daily_sale_start_project(user_user, user_user_2, user_spam, user_staff, mixer):
    # начало проекта, есть три автора. У одного из авторов запрет на продвижение у каждой аранжировки
    composer, cre = Composer.objects.update_or_create(
        slug='testcomposer2',
        defaults={
            'name': 'Test Composer 2',
            'accepted': True,
        }
    )
    users = [user_user, user_user_2, user_spam]
    for user in users:
        kwargs = {
            'name': 'Mane',
            'description': 'Distrip oil',
            'price': 100,
            'currency': mixer.blend(Currency),
            'composer': composer,
            'difficulty': 1,
            'user_created': user,
            'checked': True,
            'video_url': 'https://www.youtube.com/watch?v=EadZchN5_F8',
            'moderator': user_staff
        }
        if user == user_spam:
            kwargs.update({'agree_promotion': False})
        for i in range(4):
            kwargs.update({'name': '{0} {1} {2}'.format('Test name', i, user.pk)})
            item = ShopOffers.objects.create(**kwargs)
            filename = rel('src/test_cases/test.jpg')
            with open(filename, mode='rb') as f:
                uf = UserFile.objects.create(item=item, user_created=user)
                uf.file.save(filename, f)

    assert 8 == ShopOffers.objects.filter(agree_promotion=True).count()
    assert 4 == ShopOffers.objects.filter(agree_promotion=False).count()
    for i in range(10):
        set_daily_sale(test=True)
        items = ShopOffers.objects.filter(discount__gt=0)
        assert 4 == items.count()
        assert items.filter(user_created=user_spam).exists() is False


def test_set_daily_sale_free_offers(users_8, items_users):
    # Бесплатные товары не могут учавствовать в акции
    offers = ShopOffers.objects.all()
    offers.update(price=Decimal('0.00'))
    set_daily_sale(test=True)
    assert 0 == ShopOffers.objects.filter(discount__gt=0).count()


@pytest.fixture()
def pub_sale(mixer, languages):
    sale =  mixer.blend(Sale, status=Sale.STATUS.run,
                       is_active=True,
                       date_pub=now() - timedelta(days=1),
                       date_start=now() + timedelta(hours=1),
                       date_end=now() + timedelta(days=2),
                       )
    sale.lang.set(Language.objects.all())
    return sale


@pytest.fixture()
def run_sale(mixer, languages):
    sale = mixer.blend(Sale, status=Sale.STATUS.run,
                       is_active=True,
                       date_pub=now() - timedelta(days=1),
                       date_start=now() - timedelta(days=1),
                       date_end=now() + timedelta(days=1),
                       )
    sale.lang.set(Language.objects.all())
    return sale

def test_sale_run_not_set_daily_sale(users_8, items_users, run_sale):
    # При запущенной распродаже, не запускаются скидки дня. Распродажа глобальная.
    set_daily_sale()
    assert ShopOffers.objects.filter(sale_day=True).exists() is False


def test_pub_sale_not_set_daily_sale(users_8, items_users, pub_sale):
    # Глобальная распродажа дня запланирована на сегодня, снимаем скидки дня.
    set_daily_sale(test=True)
    assert ShopOffers.objects.filter(sale_day=True).exists() is False
    assert ShopOffers.objects.filter(discount__gt=0).exists() is False


def test_run_sale_down_sale_day_no(users_8, items_users, run_sale):
    # Глобальная распродажа закончилась. Скидки дня не запустились.
    task_sale_down(run_sale.pk)
    assert ShopOffers.objects.filter(sale_day=True).exists() is False
    assert ShopOffers.objects.filter(discount__gt=0).exists() is False


def test_3():
    #
    pass


def test_4():
    #
    pass


def test_5():
    #
    pass
