# -*- coding: utf-8 -*-
import logging

import pytest
from django.utils.translation import ugettext_lazy as _

from offer.models import Currency
from project import rel
from shop.models import Composer, ShopOffers, UserFile

logger = logging.getLogger(__name__)


@pytest.fixture()
def item_user_1(user_user):
    composer, cre = Composer.objects.update_or_create(
        slug='testcomposer2',
        defaults={
            'name': 'Test Composer 2',
            'accepted': True,
        }
    )
    kwargs = {
        'name': 'Mane',
        'description': 'Distrip oil',
        'price': 4800,
        'currency': Currency.objects.get(pk=1),
        'composer': composer,
        'difficulty': 1,
        'user_created': user_user,
        'checked': True,
        'video_url': 'https://www.youtube.com/watch?v=EadZchN5_F8',
    }
    item = ShopOffers.objects.create(**kwargs)
    filename = rel('src/test_cases/test.jpg')
    with open(filename, mode='rb') as f:
        uf = UserFile.objects.create(item=item, user_created=user_user)
        uf.file.save(filename, f)
    return item


def test_shop_queryset_published_moder_chek(user_user, item_user_1, user_staff):
    assert ShopOffers.objects.published().exists() is False
    item_user_1.moderator = user_staff
    item_user_1.checked = True
    item_user_1.save()
    assert ShopOffers.objects.published().exists() is True


def test_shop_queryset_published_is_premoderation(user_user, item_user_1, user_staff):
    assert ShopOffers.objects.published().exists() is False
    user_user.is_premoderation = True
    user_user.save()
    assert ShopOffers.objects.published().exists() is True
