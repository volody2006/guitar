# -*- coding: utf-8 -*-
import logging
from decimal import Decimal

import pytest
from dateutil.relativedelta import relativedelta
from django.utils.timezone import now

from manage import rel
from offer.models import Currency
from shop.models import Composer, ShopOfferPrice, ShopOffers, UserFile

# import statsy
# from statsy.models import StatsyObject

# from shop.utils import get_popular_day_offers
from shop.utils import disable_all_discounts

logger = logging.getLogger(__name__)


@pytest.fixture
def item_user_1(user_user, mixer):
    composer, cre = Composer.objects.update_or_create(
        slug='testcomposer2',
        defaults={
            'name': 'Test Composer 2',
            'accepted': True,
        }
    )
    kwargs = {
        'name': 'Mane',
        'description': 'Distrip oil',
        'price': 4800,
        'currency': Currency.objects.get(pk=1),
        'composer': composer,
        'difficulty': 1,
        'user_created': user_user,
        'checked': True,
        'video_url': 'https://www.youtube.com/watch?v=EadZchN5_F8',
    }
    item = ShopOffers.objects.create(**kwargs)
    filename = rel('src/test_cases/test.jpg')
    with open(filename, mode='rb') as f:
        uf = UserFile.objects.create(item=item, user_created=user_user)
        uf.file.save(filename, f)
        # print('breke')
        # item.save()
    return item


def test_compression_file(item_user_1):
    from shop.utils import compression_file
    file_name_old = item_user_1.file.name
    compression_file(item_user_1.pk)
    item_user_1 = ShopOffers.objects.get(pk=item_user_1.pk)
    file_name_new = item_user_1.file.name
    assert file_name_new != file_name_old
    assert item_user_1.file._get_file().name == item_user_1.file.path

    file_name_old = file_name_new
    compression_file(item_user_1.pk)
    item_user_1 = ShopOffers.objects.get(pk=item_user_1.pk)
    file_name_new = item_user_1.file.name
    assert file_name_new != file_name_old
    assert item_user_1.file._get_file().name == item_user_1.file.path


@pytest.fixture
def currencies_rate():
    code_rate = (
        ('EUR', Decimal('80.00')),
        ('USD', Decimal('60.00')),

    )
    for code, rate in code_rate:
        cur = Currency.objects.get(iso_code=code)
        cur.rate_rub = rate
        cur.save()


@pytest.mark.parametrize("iso_code, price", [
    ("EUR", Decimal('60.00')),
    ("USD", Decimal('80.00')),
    ("RUB", Decimal('4800.00')),

])
def test_price_save_rub(item_user_1, currencies_rate, iso_code, price):
    item_user_1.price_save()
    assert price == item_user_1.get_price_currency(iso_code)[0]
    assert iso_code == item_user_1.get_price_currency(iso_code)[1].iso_code
    # assert getattr(item_user_1, test_input) == expected


@pytest.mark.parametrize("iso_code, price", [
    ("EUR", Decimal('60.00')),
    ("USD", Decimal('80.00')),
    ("RUB", Decimal('4800.00')),

])
def test_price_save_eur(item_user_1, currencies_rate, iso_code, price):
    item = ShopOffers.objects.get(pk=item_user_1.pk)
    item.price = Decimal('60')
    item.currency = Currency.objects.get(iso_code='EUR')
    item.save()
    item.price_save()
    # assert price == getattr(item, 'price_{}'.format(iso_code.lower()))
    assert Decimal('4800.00') == item.price_rub
    assert Decimal('80.00') == item.price_usd
    assert Decimal('60.00') == item.price_eur
    assert price == item_user_1.get_price_currency(iso_code)[0]
    assert iso_code == item_user_1.get_price_currency(iso_code)[1].iso_code


@pytest.mark.parametrize("iso_code, price", [
    ("EUR", Decimal('60.00')),
    ("USD", Decimal('80.00')),
    ("RUB", Decimal('4800.00')),

])
def test_price_save_usd(item_user_1, currencies_rate, iso_code, price):
    item = ShopOffers.objects.get(pk=item_user_1.pk)
    item.price = Decimal('80')
    item.currency = Currency.objects.get(iso_code='USD')
    item.save()
    item.price_save()
    assert price == item_user_1.get_price_currency(iso_code)[0]
    assert iso_code == item_user_1.get_price_currency(iso_code)[1].iso_code


def test_price_save_not_rate(item_user_1):
    item = ShopOffers.objects.get(pk=item_user_1.pk)
    for cur in Currency.objects.all():
        assert cur.rate_rub is None
    item.price_save()
    assert 1 == ShopOfferPrice.objects.all().count()
    assert 'RUB' == ShopOfferPrice.objects.all()[0].currency.iso_code


@pytest.fixture
def items_seller_rub(user_user, mixer):
    items = mixer.cycle(16).blend('shop.ShopOffers', currency=Currency.objects.get(pk=1), price=Decimal('100.00'),
                                  user_created=user_user, checked=True, moderator=user_user)

    # 4 аранжировки имеют скидки
    day = 0
    for i in items:
        i.date_approval_moderator = now() - relativedelta(days=day)
        if day in [4, 5, 6, 7]:
            i.discount = 50
        i.save()
        day += 1


@pytest.mark.parametrize("iso_code, price", [
    ("EUR", Decimal('0')),
    ("USD", Decimal('0')),
    ("RUB", Decimal('0')),

])
def test_item_price_save_free_item(item_user_1, currencies_rate, iso_code, price):
    item = item_user_1
    item.price = None
    item.save()
    item.price_save()
    assert Decimal('0') == item.price_rub
    assert Decimal('0') == item.price_usd
    assert Decimal('0') == item.price_eur
    assert price == item_user_1.get_price_currency(iso_code)[0]
    assert iso_code == item_user_1.get_price_currency(iso_code)[1].iso_code


def test_disable_all_discounts_sale_day_True(item_user_1):
    item = item_user_1
    item.discount = 50
    item.sale_day = True
    item.save()
    disable = disable_all_discounts(True)
    assert item.pk in disable['offers_ids']


def test_disable_all_discounts_sale_day_False(item_user_1):
    item = item_user_1
    item.discount = 50
    item.sale_day = False
    item.save()
    disable = disable_all_discounts(True)
    assert item.pk not in disable['offers_ids']
