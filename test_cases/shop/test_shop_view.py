# -*- coding: utf-8 -*-
import pytest
from copy import deepcopy

from decimal import Decimal

from django.conf import settings
from django.core import management
from django.urls import reverse
import logging

from django.utils import translation
from post_office.models import Email
from statsy.models import StatsyGroup, StatsyEvent, StatsyObject

from offer.models import Currency
from reversion.models import Version
from shop.views import views
from shop.models import Composer, ShopOffers, UserFile, TagOffers

from manage import rel

logger = logging.getLogger(__name__)


def setup():
    print("basic setup into module")
    StatsyObject.objects.all().delete()
    StatsyGroup.objects.all().delete()
    StatsyEvent.objects.all().delete()
    management.call_command('rebuild_index', interactive=False)


def teardown():
    # https://stackoverflow.com/questions/48831595/django-app-unit-tests-fails-because-of-django-db-utils-integrityerror/49003568#49003568
    print('teardown Statsy delete')
    StatsyObject.objects.all().delete()
    StatsyGroup.objects.all().delete()
    StatsyEvent.objects.all().delete()
    management.call_command('rebuild_index', interactive=False)


@pytest.fixture()
def image_file():
    import io

    image = rel('src/test_cases/test.jpg')

    with open(image, "rb", buffering=0) as fp:
        return io.BytesIO(fp.read())


@pytest.fixture()
def image_file_2():
    import io

    image = rel('src/test_cases/test_2.png')

    with open(image, "rb", buffering=0) as fp:
        return io.BytesIO(fp.read())


@pytest.fixture()
def valid_shop_item_data(currencies, image_file, mixer):
    image_file.seek(0)
    image_file_1 = deepcopy(image_file)
    image_file_2 = deepcopy(image_file)

    data = {
        'image': image_file_1,
        'name': 'Mane',
        'description': 'Distrip oil',
        'price': 4800.00,
        'composer_name': 'composer test',
        'difficulty': 1,
        'video_url': 'https://www.youtube.com/watch?v=EadZchN5_F8',
        'form-TOTAL_FORMS': 2,
        'form-INITIAL_FORMS': 0,
        'form-0-file': image_file_2,
        'tonality': 1,
        'tags': mixer.blend(TagOffers).pk
    }
    return data


@pytest.fixture()
def update_valid_shop_item(client_login, valid_shop_item_data):
    url = reverse('shopoffers_add')
    data = valid_shop_item_data
    client_login.post(url, data=data)
    item = ShopOffers.objects.all()[0]
    item.checked = True
    item.save()
    user_file = UserFile.objects.filter(item=item)[0]
    data.update(
        {'name': 'New name',
         'form-TOTAL_FORMS': '1',
         'form-INITIAL_FORMS': '1',
         'form-MAX_NUM_FORMS': '1000',
         'image': '',
         'form-0-file': '',
         'form-0-id': user_file.id,
         }
    )

    url = reverse('shopoffers_id_edit', kwargs={'pk': item.pk})
    client_login.post(url, data=data)
    item = ShopOffers.objects.get(pk=item.pk)
    return item


def _test_shop_main_page_status_200(client):
    response = client.get(reverse('shop-main'))
    assert response.status_code == 200
    assert response.resolver_match.func.__name__ == views.ShopMainPageView.as_view().__name__


def _test_shop_add_page_anonim(client):
    response = client.get(reverse('shopoffers_add'))
    assert response.status_code == 302
    assert response.url in '%s?next=%s' % (reverse('login'), reverse('shopoffers_add'))


def _test_shop_add_page_user(client_login, user_user):
    response = client_login.get(reverse('shopoffers_add'))
    assert response.status_code == 200
    assert response.resolver_match.func.__name__ == views.ShopCreateView.as_view().__name__


@pytest.fixture()
def currencies_rate():
    code_rate = (
        ('EUR', Decimal('80.00')),
        ('USD', Decimal('60.00')),

    )
    for code, rate in code_rate:
        cur = Currency.objects.get(iso_code=code)
        cur.rate_rub = rate
        cur.save()


@pytest.mark.parametrize('lang', settings.TEST_LANGUAGES)
def _test_shop_add_shop_item_user(valid_shop_item_data, client_login, currencies_rate, lang, mailoutbox):
    translation.activate(lang)
    url = reverse('shopoffers_add')
    data = valid_shop_item_data
    # страница открывается без проблем
    response = client_login.get(url)
    assert 200 == response.status_code
    assert 0 == ShopOffers.objects.all().count()

    # Форма отправляется удачно, ошибок нет
    response = client_login.post(url, data=data)
    # print(response.content.decode('utf-8'))
    assert 302 == response.status_code
    assert 1 == ShopOffers.objects.all().count()
    item = ShopOffers.objects.all()[0]
    # Валюта рублю
    assert item.currency.id == 1

    assert item.get_price_currency(item.currency)[0] == Decimal('4800.00')
    assert item.get_price_currency(item.currency)[1] == item.currency
    versions = Version.objects.get_for_object(item)
    assert 1 == len(versions)
    assert 0 == len(mailoutbox)
    assert Email.objects.all().exists() is True

    assert 'Новый ShopOffers' == Email.objects.filter(to=[settings.SUPPORT_EMAIL])[0].subject

    data['image'].seek(0)
    data['form-0-file'].seek(0)

    # Еще раз отправили, все отлично
    response = client_login.post(url, data=data)
    assert 302 == response.status_code
    assert 2 == ShopOffers.objects.all().count()
    item = ShopOffers.objects.get(pk=item.pk)
    assert item.checked is False
    assert item.file.__bool__() is True
    versions = Version.objects.get_for_object(item)
    assert 1 == len(versions)

    item.checked = True
    item.save()
    url = reverse('shopoffers_id_edit', kwargs={'pk': item.pk})
    # Изменяем товар, метка проверено слетает
    # image_file.seek(0)
    # file.seek(0)
    data['name'] = 'New name'
    user_file = UserFile.objects.filter(item=item)[0]
    data.update(
        {'form-TOTAL_FORMS': '1',
         'form-INITIAL_FORMS': '1',
         'form-MAX_NUM_FORMS': '1000',
         'image': '',
         'form-0-file': '',
         'form-0-id': user_file.id,
         }
    )

    # Форма отправляется удачно, ошибок нет
    response = client_login.post(url, data=data)
    # print(response.context['formset'].errors)

    assert 302 == response.status_code

    item = ShopOffers.objects.get(pk=item.pk)
    assert 'New name' == item.name
    assert item.file.__bool__() is True
    assert item.checked is False
    versions = Version.objects.get_for_object(item)
    assert 2 == len(versions)
    assert versions[1].field_dict["name"] == "Mane"
    assert versions[0].field_dict["name"] == "New name"

    assert 'Обновленный ShopOffers' == Email.objects.filter(to=[settings.SUPPORT_EMAIL])[2].subject


@pytest.mark.parametrize('lang', settings.TEST_LANGUAGES)
def _test_shop_item_add_resend_the_same_data(client_login, valid_shop_item_data, lang):
    translation.activate(lang)
    url = reverse('shopoffers_add')
    data = valid_shop_item_data

    # Форма отправляется удачно, ошибок нет
    client_login.post(url, data=data)
    assert 1 == ShopOffers.objects.all().count()

    data['image'].seek(0)
    data['form-0-file'].seek(0)

    # Еще раз отправили, все отлично
    client_login.post(url, data=data)
    assert 2 == ShopOffers.objects.all().count()


@pytest.fixture()
def item_user_1(user_user, mixer):
    kwargs = {
        'name': 'Mane',
        'description': 'Distrip oil',
        'price': 100.00,
        'currency': mixer.blend(Currency),
        'composer': mixer.blend(Composer),
        'difficulty': 1,
        'user_created': user_user,
        'checked': True,
        'video_url': 'https://www.youtube.com/watch?v=EadZchN5_F8',
    }
    item = ShopOffers.objects.create(**kwargs)
    return item


@pytest.fixture()
def item_name777_user_1(item_user_1, user_user, mixer):
    kwargs = {
        'name': str(item_user_1.pk),
        'description': 'Distrip 777',
        'price': 777.00,
        'currency': mixer.blend(Currency),
        'composer': mixer.blend(Composer),
        'difficulty': 1,
        'user_created': user_user,
        'checked': True,
        'video_url': 'https://www.youtube.com/watch?v=EadZchN5_F8',
    }
    item = ShopOffers.objects.create(**kwargs)
    return item


def _test_good_open_user_item(item_user_1, client_login, statsy_async_true):
    url = reverse('shopoffers_id_edit', kwargs={'pk': item_user_1.pk})
    response = client_login.get(url)
    assert response.status_code == 200
    assert response.resolver_match.func.__name__ == views.ShopUpdateView.as_view().__name__


def _test_good_get_user_item_slug(item_user_1, client_login, clear_statsy):
    url = reverse('shop-guitar-sheets-tabs-detail', kwargs={'slug': item_user_1.slug})
    response = client_login.get(url)
    assert response.status_code == 200
    assert response.resolver_match.func.__name__ == views.ShopDetailView.as_view().__name__


def _test_good_get_user_item_id(item_user_1, client_login, clear_statsy):
    # test off
    url = reverse('shopoffers_id_detail', kwargs={'pk': item_user_1.pk})
    response = client_login.get(url)
    assert response.status_code == 200
    assert response.resolver_match.func.__name__ == views.ShopDetailView.as_view().__name__


def _test_good_get_user_item_slug_777(item_user_1, item_name777_user_1, client_login):
    url = reverse('shop-guitar-sheets-tabs-detail', kwargs={'slug': item_user_1.pk})
    assert item_name777_user_1.slug == str(item_user_1.pk)

    response = client_login.get(url)
    assert response.status_code == 200
    assert response.resolver_match.func.__name__ == views.ShopDetailView.as_view().__name__
    assert response.context['object'] == item_name777_user_1


def _test_bad_get_user_item(item_user_1, client_login_2):
    url = reverse('shopoffers_id_edit', kwargs={'pk': item_user_1.pk})
    response = client_login_2.get(url)
    assert response.status_code == 403


def _test_moderator_can_edit_the_goods(item_user_1, client_login_moder):
    url = reverse('shopoffers_id_edit', kwargs={'pk': item_user_1.pk})
    response = client_login_moder.get(url)
    assert response.status_code == 200
    assert response.resolver_match.func.__name__ == views.ShopUpdateView.as_view().__name__


# После обновления товара пользователем,
# слетает проверка модератора (модератор должен проверить еще раз)
@pytest.mark.parametrize('lang', settings.TEST_LANGUAGES)
def _test_shop_item_update_mark_is_changed_to_false(update_valid_shop_item, lang):
    item = update_valid_shop_item
    assert 'New name' == item.name
    assert item.checked is False


@pytest.mark.parametrize('lang', settings.TEST_LANGUAGES)
def _test_shop_item_created_zip_file(client_login, valid_shop_item_data, lang):
    translation.activate(lang)
    url = reverse('shopoffers_add')
    data = valid_shop_item_data
    client_login.post(url, data=data)
    item = ShopOffers.objects.all()[0]
    assert item.file is not None


@pytest.mark.parametrize('lang', settings.TEST_LANGUAGES)
def _test_shop_item_update_zip_file(client_login, valid_shop_item_data, image_file_2, lang):
    translation.activate(lang)
    # Новый файл загружаем, пересобираем архив
    url = reverse('shopoffers_add')
    data = valid_shop_item_data
    response = client_login.post(url, data=data)
    # print(3, response.content.decode('utf-8'))
    item = ShopOffers.objects.get(name=response.context['item'])

    assert item.file.file is not None
    assert item.file.__bool__() is True
    zip_file_name_old = item.file.name
    url = reverse('shopoffers_id_edit', kwargs={'pk': item.pk})
    user_file = UserFile.objects.filter(item=item)[0]
    user_file_name_old = user_file.get_file_path()
    data.update(
        {'form-TOTAL_FORMS': '1',
         'form-INITIAL_FORMS': '1',
         'form-MAX_NUM_FORMS': '1000',
         'image': '',
         'form-0-file': image_file_2,
         'form-0-id': user_file.id,
         }
    )
    # print(UserFile.objects.all().count())
    response = client_login.post(url, data=data)
    # print(response.context['formset'].errors)

    assert 302 == response.status_code
    item = ShopOffers.objects.get(pk=item.pk)
    zip_file_name_new = item.file.name
    user_file = UserFile.objects.get(pk=user_file.pk)
    user_file_name_new = user_file.get_file_path()
    # print(UserFile.objects.all().count())
    assert user_file_name_new != user_file_name_old
    assert zip_file_name_new != zip_file_name_old
    assert item.file.__bool__() is True


@pytest.mark.parametrize('lang', settings.TEST_LANGUAGES)
def _test_shop_item_update_zip_file_2(client_login, valid_shop_item_data, lang):
    # Файл не менялся, архив НЕ меняется
    translation.activate(lang)
    url = reverse('shopoffers_add')
    data = valid_shop_item_data
    client_login.post(url, data=data)
    item = ShopOffers.objects.all()[0]
    zip_file_name_old = item.file.name
    url = reverse('shopoffers_id_edit', kwargs={'pk': item.pk})
    user_file = UserFile.objects.filter(item=item)[0]
    user_file_name_old = user_file.get_file_path()
    data.update(
        {'form-TOTAL_FORMS': '1',
         'form-INITIAL_FORMS': '1',
         'form-MAX_NUM_FORMS': '1000',
         'image': '',
         'form-0-file': '',
         'form-0-id': user_file.id,
         }
    )
    response = client_login.post(url, data=data)
    # print(response.context['formset'].errors)

    assert 302 == response.status_code
    item = ShopOffers.objects.get(pk=item.pk)
    zip_file_name_new = item.file.name
    user_file = UserFile.objects.get(pk=user_file.pk)
    user_file_name_new = user_file.get_file_path()
    assert user_file_name_new == user_file_name_old
    assert zip_file_name_new == zip_file_name_old
    assert item.file.__bool__() is True


@pytest.mark.parametrize('lang', settings.TEST_LANGUAGES)
def _test_shop_item_update_zip_file_3(client_login, valid_shop_item_data, image_file_2, lang):
    # Добавили новый файл, архив изменился
    translation.activate(lang)
    url = reverse('shopoffers_add')
    data = valid_shop_item_data
    client_login.post(url, data=data)
    item = ShopOffers.objects.all()[0]
    zip_file_name_old = item.file.name
    url = reverse('shopoffers_id_edit', kwargs={'pk': item.pk})
    user_file = UserFile.objects.filter(item=item)[0]
    user_file_name_old = user_file.get_file_path()
    data.update(
        {'form-TOTAL_FORMS': '2',
         'form-INITIAL_FORMS': '1',
         'form-MAX_NUM_FORMS': '1000',
         'image': '',
         'form-0-file': '',
         'form-0-id': user_file.id,
         'form-1-file': image_file_2,
         # 'form-1-id': user_file.id,
         }
    )
    response = client_login.post(url, data=data)
    # print(response.context['formset'].errors)

    assert 302 == response.status_code
    item = ShopOffers.objects.get(pk=item.pk)
    zip_file_name_new = item.file.name
    user_file = UserFile.objects.get(pk=user_file.pk)
    user_file_name_new = user_file.get_file_path()
    assert 2 == UserFile.objects.all().count()
    assert user_file_name_new == user_file_name_old
    assert zip_file_name_new != zip_file_name_old


def _test_delete_item_user(item_user_1, client_login):
    url = reverse('shopoffers_id_delete', kwargs={'pk': item_user_1.pk})
    client_login.get(url)
    response = client_login.post(url, {})
    assert response.status_code == 302
    assert ShopOffers.objects.get(pk=item_user_1.pk).is_delete is True


def _test_delete_is_not_the_owner(item_user_1, client_login_2):
    url = reverse('shopoffers_id_delete', kwargs={'pk': item_user_1.pk})
    client_login_2.get(url)
    response = client_login_2.post(url, {})
    assert response.status_code == 403
    assert ShopOffers.objects.get(pk=item_user_1.pk).is_delete is False


def _test_not_notify_moderator_when_price_changes_shop_item(client_login, valid_shop_item_data):
    url = reverse('shopoffers_add')
    data = valid_shop_item_data
    client_login.post(url, data=data)
    item = ShopOffers.objects.all()[0]
    item.checked = True
    item.save()
    data.update({
        'price': 2000,
    })

    url = reverse('shopoffers_id_edit', kwargs={'pk': item.pk})
    client_login.post(url, data=data)
    item = ShopOffers.objects.get(pk=item.pk)
    assert item.checked is True


def _test_shop_add_shop_item_user_serial(valid_shop_item_data, client_login, currencies_rate, ):
    """
    Когда добавляешь аранжировки с одним композитором
    (которые новые и падают на модерацию),
    после второй аранжировки композитор пропадает из первой и
    остаётся во второй. Когда добавляешь следующие аранжировки,
    он пропадает во второй, потом в третьей и т.д.
    Сейчас добавил 4 аранжировки, композитор есть только в четвертой,
    в предыдущих трёх пропали.
    """

    url = reverse('shopoffers_add')
    data = valid_shop_item_data
    # страница открывается без проблем
    response = client_login.get(url)
    assert 200 == response.status_code
    assert 0 == ShopOffers.objects.all().count()

    # Форма отправляется удачно, ошибок нет
    response = client_login.post(url, data=data)
    # print(response.content.decode('utf-8'))
    assert 302 == response.status_code
    assert 1 == ShopOffers.objects.all().count()
    for i in range(0, 4):
        data['image'].seek(0)
        data['form-0-file'].seek(0)
        data['name'] = 'Test-{}'.format(i)

        response = client_login.post(url, data=data)
        assert 302 == response.status_code
        assert i + 2 == ShopOffers.objects.all().count()

    for item in ShopOffers.objects.all():
        assert item.get_composer != ''
