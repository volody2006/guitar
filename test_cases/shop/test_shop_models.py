# -*- coding: utf-8 -*-
from decimal import Decimal

import pytest
import logging

from gis.models import Country
from offer.models import Currency
from shop.models import Composer, ShopOffers, UserFile

from manage import rel

logger = logging.getLogger(__name__)


@pytest.fixture()
def item_user_1(user_user, mixer):
    composer, cre = Composer.objects.update_or_create(
        slug='testcomposer2',
        defaults={
            'name': 'Test Composer 2',
            'accepted': True,
        }
    )
    kwargs = {
        'name': 'Mane',
        'description': 'Distrip oil',
        'price': 4800,
        'currency': Currency.objects.get(pk=1),
        'composer': composer,
        'difficulty': 1,
        'user_created': user_user,
        'checked': True,
        'video_url': 'https://www.youtube.com/watch?v=EadZchN5_F8',
    }
    item = ShopOffers.objects.create(**kwargs)
    filename = rel('src/test_cases/test.jpg')
    with open(filename, mode='rb') as f:
        uf = UserFile.objects.create(item=item, user_created=user_user)
        uf.file.save(filename, f)
    return item


@pytest.fixture()
def currencies_rate():
    code_rate = (
        ('EUR', Decimal('80.00')),
        ('USD', Decimal('60.00')),

    )
    for code, rate in code_rate:
        cur = Currency.objects.get(iso_code=code)
        cur.rate_rub = rate
        cur.save()


@pytest.fixture()
def country_rate(currencies_rate):
    # Азербайджан не имеет валюты
    cont_AZ = Country.objects.get(code='AZ')
    cont_AZ.ratio_price = 1
    cont_AZ.currency = None
    cont_AZ.save()

    cont_US = Country.objects.get(code='US')
    cont_US.ratio_price = 4
    cont_US.currency = Currency.objects.get(iso_code='USD')
    cont_US.save()

    cont_DE = Country.objects.get(code='DE')
    cont_DE.ratio_price = 2
    cont_DE.currency = Currency.objects.get(iso_code='EUR')
    cont_DE.save()

    cont_RU = Country.objects.get(code='RU')
    cont_RU.ratio_price = 1
    cont_RU.currency = Currency.objects.get(iso_code='RUB')
    cont_RU.save()


@pytest.fixture()
def user_user_ger(user_user, country_rate):
    user_user.reg_country = Country.objects.get(code='DE')
    user_user.save()
    return user_user


@pytest.mark.parametrize("iso_code, price", [
    ("EUR", Decimal('60.00')),
    ("USD", Decimal('80.00')),
    ("RUB", Decimal('4800.00')),

])
def test_item_get_price_currency(item_user_1, user_user, country_rate, iso_code, price):
    item_user_1.price_save()
    assert item_user_1.get_price_currency(iso_code)[0] == price
    assert item_user_1.get_price_currency(iso_code)[1] == Currency.objects.get(iso_code=iso_code)

    item_user_1.discount = 50
    item_user_1.save()

    assert item_user_1.get_discount_price_currency(iso_code)[0] == price / 2
    assert item_user_1.get_discount_price_currency(iso_code)[1] == Currency.objects.get(iso_code=iso_code)


@pytest.mark.parametrize("iso_code, price", [
    ("EUR", Decimal('60.00')),
    ("USD", Decimal('80.00')),
    ("RUB", Decimal('4800.00')),

])
def test_item_get_price_currency_user_ger(item_user_1, user_user_ger, country_rate, iso_code, price):
    item_user_1.price_save()
    assert item_user_1.get_price_currency(iso_code)[0] == price
    assert item_user_1.get_price_currency(iso_code)[1] == Currency.objects.get(iso_code=iso_code)

    item_user_1.discount = 50
    item_user_1.save()

    assert item_user_1.get_discount_price_currency(iso_code)[0] == price / 2
    assert item_user_1.get_discount_price_currency(iso_code)[1] == Currency.objects.get(iso_code=iso_code)


@pytest.mark.parametrize("country_code, iso_code, price, price_discount", [
    ('DE', "EUR", Decimal('60.00'), Decimal('30.00')),
    ('RU', "RUB", Decimal('2400.00'), Decimal('1200.00')),
    ('US', "USD", Decimal('160.00'), Decimal('80.00')),
    ('AZ', "USD", Decimal('40.00'), Decimal('20.00')),
])
def test_get_price_country_user_ger(item_user_1, user_user_ger, country_code, iso_code, price, price_discount):
    item_user_1.price_save()
    item_user_1.discount = 50
    item_user_1.save()

    assert item_user_1.get_price_country(country_code)[0] == Decimal(price)
    assert item_user_1.get_price_country(country_code)[1] == Currency.objects.get(iso_code=iso_code)
    assert item_user_1.get_discount_price_country(country_code)[0] == Decimal(price_discount)
    assert item_user_1.get_discount_price_country(country_code)[1] == Currency.objects.get(iso_code=iso_code)


@pytest.fixture
def user_user_ger_not_country_coefficient(user_user_ger, country_rate):
    user_user_ger.is_country_coefficient = False
    user_user_ger.save()
    return user_user_ger


@pytest.mark.parametrize("country_code, iso_code, price, price_discount", [
    ('DE', "EUR", Decimal('60.00'), Decimal('30.00')),
    ('RU', "RUB", Decimal('4800.00'), Decimal('2400.00')),
    ('US', "USD", Decimal('80.00'), Decimal('40.00')),
    ('AZ', "USD", Decimal('80.00'), Decimal('40.00')),
])
def test_get_price_country_user_ger_not_country_coefficient(item_user_1, user_user_ger_not_country_coefficient,
                                                            country_code, iso_code, price,
                                                            price_discount):
    item_user_1.price_save()
    item_user_1.discount = 50
    item_user_1.save()

    assert item_user_1.get_price_country(country_code)[0] == price
    assert item_user_1.get_price_country(country_code)[1] == Currency.objects.get(iso_code=iso_code)
    assert item_user_1.get_discount_price_country(country_code)[0] == price_discount
    assert item_user_1.get_discount_price_country(country_code)[1] == Currency.objects.get(iso_code=iso_code)


@pytest.mark.parametrize("country_code, iso_code, price, price_discount", [
    ('DE', "EUR", Decimal('60.00'), Decimal('30.00')),
    ('RU', "RUB", Decimal('4800.00'), Decimal('2400.00')),
    ('US', "USD", Decimal('80.00'), Decimal('40.00')),
    ('AZ', "USD", Decimal('80.00'), Decimal('40.00')),
])
def test_get_price_country_user_not_country_not_country_coefficient(item_user_1, user_user, country_rate,
                                                                    country_code, iso_code, price,
                                                                    price_discount):
    user_user.reg_country = None
    user_user.is_country_coefficient = False
    user_user.save()

    item_user_1.price_save()
    item_user_1.discount = 50
    item_user_1.save()
    if country_code != 'AZ':
        assert iso_code == Country.objects.get(code=country_code).currency.iso_code
    else:
        assert Country.objects.get(code=country_code).currency is None
    assert item_user_1.get_price_country(country_code)[0] == Decimal(price)
    assert item_user_1.get_price_country(country_code)[1] == Currency.objects.get(iso_code=iso_code)
    assert item_user_1.get_discount_price_country(country_code)[0] == Decimal(price_discount)
    assert item_user_1.get_discount_price_country(country_code)[1] == Currency.objects.get(iso_code=iso_code)


@pytest.mark.parametrize("country_code, iso_code, price, price_discount", [
    ('DE', "EUR", Decimal('120.00'), Decimal('60.00')),
    ('RU', "RUB", Decimal('4800.00'), Decimal('2400.00')),
    ('US', "USD", Decimal('320.00'), Decimal('160.00')),
    ('AZ', "USD", Decimal('80.00'), Decimal('40.00')),
])
def test_get_price_country_user_not_country(item_user_1, user_user, country_rate, country_code, iso_code, price,
                                            price_discount):
    user_user.reg_country = None
    user_user.save()

    item_user_1.price_save()
    item_user_1.discount = 50
    item_user_1.save()
    if country_code != 'AZ':
        assert iso_code == Country.objects.get(code=country_code).currency.iso_code
    else:
        assert Country.objects.get(code=country_code).currency is None
    assert item_user_1.get_price_country(country_code)[0] == Decimal(price)
    assert item_user_1.get_price_country(country_code)[1] == Currency.objects.get(iso_code=iso_code)
    assert item_user_1.get_discount_price_country(country_code)[0] == Decimal(price_discount)
    assert item_user_1.get_discount_price_country(country_code)[1] == Currency.objects.get(iso_code=iso_code)
