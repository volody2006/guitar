# -*- coding: utf-8 -*-
import pytest
from django.conf import settings
from elasticsearch import Elasticsearch
from haystack.query import SearchQuerySet

from manage import rel
from offer.models import Currency
from shop.models import Composer, ShopOffers, UserFile, TagOffers


@pytest.fixture()
def item_user(user_user):
    composer, cre = Composer.objects.update_or_create(
        slug='testcomposer2',
        defaults={
            'name': 'Test Composer 2',
            'accepted': True,
        }
    )
    kwargs = {
        'name': 'Mane',
        'description': 'Distrip oil',
        'price': 4800,
        'currency': Currency.objects.get(pk=1),
        'composer': composer,
        'difficulty': 1,
        'user_created': user_user,
        'checked': True,
        'video_url': 'https://www.youtube.com/watch?v=EadZchN5_F8',
        'is_available': True,
        'moderator': user_user,
    }
    item = ShopOffers.objects.create(**kwargs)
    filename = rel('src/test_cases/test.jpg')
    with open(filename, mode='rb') as f:
        uf = UserFile.objects.create(item=item, user_created=user_user)
        uf.file.save(filename, f)
    return item


def test_index_add_index(item_user, mixer):
    item = ShopOffers.objects.get(pk=item_user.pk)

    assert item.is_valid() is True
    tag = mixer.blend(TagOffers)
    item.tags.add(tag)
    item.save()
    es = Elasticsearch()
    index = settings.HAYSTACK_CONNECTIONS['default']['INDEX_NAME']
    res = es.get(index=index, id='shop.shopoffers.{}'.format(item.pk))
    assert tag.pk in res['_source']['tags_id']


def _test_get_object_index_queryset(item_user):
    sqs = SearchQuerySet()
    sqs = sqs.models(ShopOffers).using('default')
    sqs[0]._get_object()
    assert sqs[0].object is not None
