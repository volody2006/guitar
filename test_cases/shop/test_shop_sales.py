# -*- coding: utf-8 -*-
from datetime import timedelta

import pytest
from django.conf import settings
from django.urls import reverse
from django.utils import translation
from django.utils.timezone import now
from mixer.backend.django import mixer

from lang.models import Language
from manage import rel
from offer.models import Currency
from shop.models import Sale, Composer, ShopOffers, UserFile
from shop.views.sale import SaleDetailView
from users.models import User


@pytest.fixture()
def pub_sale(mixer, languages):
    sale =  mixer.blend(Sale, status=Sale.STATUS.run,
                       is_active=True,
                       date_pub=now() - timedelta(days=1),
                       date_start=now() + timedelta(days=1),
                       date_end=now() + timedelta(days=2),
                       )
    sale.lang.set(Language.objects.all())
    return sale


@pytest.fixture()
def run_sale(mixer, languages):
    sale = mixer.blend(Sale, status=Sale.STATUS.run,
                       is_active=True,
                       date_pub=now() - timedelta(days=1),
                       date_start=now() - timedelta(days=1),
                       date_end=now() + timedelta(days=1),
                       )
    sale.lang.set(Language.objects.all())
    return sale

@pytest.fixture()
def waiting_sale(mixer, languages):
    sale = mixer.blend(Sale, status=Sale.STATUS.waiting,
                       is_active=True,
                       date_start=now() + timedelta(days=1),
                       date_end=now() + timedelta(days=3),
                       )
    sale.lang.set(Language.objects.all())
    return sale

@pytest.fixture()
def draft_sale(mixer, languages):
    sale = mixer.blend(Sale, status=Sale.STATUS.draft,
                       date_start=now() + timedelta(days=10),
                       date_end=now() + timedelta(days=20),
                       )
    sale.lang.set(Language.objects.all())
    return sale

@pytest.fixture()
def finished_sale(mixer, languages):
    sale = mixer.blend(Sale, status=Sale.STATUS.finished,
                       date_start=now() - timedelta(days=10),
                       date_end=now() - timedelta(days=9),
                       )
    sale.lang.set(Language.objects.all())
    return sale

@pytest.fixture
def users_8():
    from users.models import User
    for i in range(8):
        User.objects.create_user(
            email='test_user{}@email.com'.format(i),
            password='Test12345',
            is_active=True,
            slug='test_user{}'.format(i)
        )


@pytest.fixture
def items_free_users(users_8, mixer, user_superuser):
    composer, cre = Composer.objects.update_or_create(
        slug='testcomposer2',
        defaults={
            'name': 'Test Composer 2',
            'accepted': True,
        }
    )
    for user in User.objects.filter(is_active=True):
        kwargs = {
            'name': 'Mane',
            'description': 'Distrip oil',
            'price': 0,
            'currency': mixer.blend(Currency),
            'composer': composer,
            'difficulty': 1,
            'user_created': user,
            'checked': True,
            'video_url': 'https://www.youtube.com/watch?v=EadZchN5_F8',
            'moderator': user_superuser
        }
        items = []
        for i in range(0, 10):
            kwargs.update({'name': '{0} {1} {2}'.format('Test name', i, user.pk)})
            item = ShopOffers.objects.create(**kwargs)
            filename = rel('src/test_cases/test.jpg')
            with open(filename, mode='rb') as f:
                uf = UserFile.objects.create(item=item, user_created=user)
                uf.file.save(filename, f)
            items.append(item)


class TestModelSale:

    def get_sale(self, status):
        return mixer.blend(Sale, status=getattr(Sale.STATUS, status), is_active=True)

    def test_up_waiting_sale_date_start_future(self, waiting_sale):
        assert waiting_sale.up() is False

    def test_up_waiting_sale_date_start_now(self, waiting_sale):
        sale = waiting_sale
        sale.date_start = now()
        sale.save()
        assert sale.up() is True
        assert sale.actual_start_date is not None
        assert sale.status == sale.STATUS.run

    def test_other_sales_status_up(self):
        for status in 'draft run finished expired adorted cancelled archive'.split(' '):
            sale = self.get_sale(status)
            # print('test sale up. Status {}'.format(status))
            assert sale.up() is False

    def test_up_waiting_sale_date_start_now_not_date_end(self, waiting_sale):
        sale = waiting_sale
        sale.date_start = now()
        sale.date_end = None
        sale.save()
        assert sale.up() is True
        assert sale.actual_start_date is not None
        assert sale.status == sale.STATUS.run

    def test_up_waiting_sale_date_start_in_past_not_date_in_past(self, waiting_sale):
        sale = waiting_sale
        sale.date_start = now() - timedelta(days=10)
        sale.date_end = now() - timedelta(days=9)
        sale.save()
        assert sale.up() is False
        assert sale.actual_start_date is None
        assert sale.status == sale.STATUS.expired

    def test_other_sale_down(self):
        for status in 'draft waiting finished expired adorted cancelled archive'.split(' '):
            sale = self.get_sale(status)
            print('test sale down. Status {}'.format(status))
            assert sale.down() is False

    def test_run_sale_down_not_end_date(self):
        sale = self.get_sale('run')
        assert sale.down() is True
        assert sale.actual_end_date is not None
        assert sale.status == sale.STATUS.finished

    def test_run_sale_down_past_end_date(self):
        sale = self.get_sale('run')
        sale.date_end = now() - timedelta(days=1)
        sale.save()
        assert sale.down() is True
        assert sale.actual_end_date is not None
        print(sale.actual_end_date, sale.date_end)
        assert sale.status == sale.STATUS.finished

    def test_run_sale_down_future_end_date(self):
        sale = self.get_sale('run')
        sale.date_end = now() + timedelta(days=1)
        sale.save()
        assert sale.down() is True
        assert sale.actual_end_date is not None
        print(sale.actual_end_date, sale.date_end)
        assert sale.status == sale.STATUS.adorted

    def test_deactivate_sale_run(self):
        sale = self.get_sale('run')
        sale.is_active = True
        sale.save()

        sale.deactivate()
        assert sale.is_active is False

    def test_deactivate_sale_other(self):
        for status in 'draft waiting finished expired adorted cancelled archive'.split(' '):
            sale = self.get_sale(status)
            print('test deactivate sale. Status {}'.format(status))

            sale.is_active = True
            sale.save()

            sale.deactivate()
            assert sale.is_active is False

    def test_set_offers_discount(self, items_free_users):
        # Аранжировки без цены, не могут участвовать в распродаже.
        sale = self.get_sale('run')
        sale.set_offers_discount()
        assert 0 == ShopOffers.objects.filter(discount__gt=0).count()


class TestManagerSale:

    # def get_sale(self, status):
    #     return mixer.blend(Sale, status=getattr(Sale.STATUS, status))

    def test_get_more_active_sale(self):
        mixer.cycle(5).blend(Sale, is_active=True)
        assert 5 == Sale.objects.active().count()

    def test_not_active_sale(self):
        mixer.cycle(5).blend(Sale, is_active=False)
        assert 0 == Sale.objects.active().count()

    @pytest.fixture()
    def fixture_sale(self):
        # Прошедшие распродажа
        sales = mixer.cycle(5).blend(Sale, is_active=False, status=Sale.STATUS.finished)
        for sale in sales:
            sale.date_pub = now() - timedelta(days=(sale.pk * 3 * 365))
            sale.date_start = now() - timedelta(days=(sale.pk * 2 * 365))
            sale.date_end = now() - timedelta(days=(sale.pk * 1 * 365))
            sale.save()

        # Распродажи в ожидание, даты все в будущем
        sales = mixer.cycle(4).blend(Sale, is_active=True, status=Sale.STATUS.waiting)
        for sale in sales:
            sale.date_pub = now() + timedelta(days=(sale.pk * 1 * 2))
            sale.date_start = now() + timedelta(days=(sale.pk * 2 * 2))
            sale.date_end = now() + timedelta(days=(sale.pk * 3 * 2))
            sale.save()

        # Распродажи опубликованы, между публикацией и стартом
        sales = mixer.cycle(3).blend(Sale, is_active=True, status=Sale.STATUS.waiting)
        for sale in sales:
            sale.date_pub = now() - timedelta(days=(sale.pk * 1 * 2))
            sale.date_start = now() + timedelta(days=(sale.pk * 2 * 2))
            sale.date_end = now() + timedelta(days=(sale.pk * 3 * 2))
            sale.save()

        # Распродажи активны, между стартом и концом
        sales = mixer.cycle(2).blend(Sale, is_active=True, status=Sale.STATUS.run)
        for sale in sales:
            sale.date_pub = now() - timedelta(days=(sale.pk * 3 * 2))
            sale.date_start = now() - timedelta(days=(sale.pk * 2 * 2))
            sale.date_end = now() + timedelta(days=(sale.pk * 1 * 2))
            sale.save()

        # Распродажи в черновиках
        mixer.blend(Sale, is_active=False, status=Sale.STATUS.draft)
        return True

    def test_more_published_sale(self, fixture_sale):
        assert 15 == Sale.objects.all().count()
        assert 5 == Sale.objects.published().count()

    def test_more_running_sale(self, fixture_sale):
        sales = Sale.objects.running()
        for sale in sales:
            assert sale.is_active is True
            assert Sale.STATUS.run == sale.status
        assert 2 == sales.count()

    def test_is_sale_false(self):
        assert Sale.objects.is_sale() is False

    def test_is_sale_true(self, fixture_sale):
        assert Sale.objects.is_sale() is True


class TestViewsOpenPageSale:
    # Черновики видят только администраторы, запущенную все, законченную только администраторы
    def test_open_page_draft_sale_client(self, draft_sale, client):
        response = client.get(draft_sale.gau())
        assert response.status_code == 404

    def test_open_page_draft_sale_client_login_moder(self, draft_sale, client_login_moder):
        response = client_login_moder.get(draft_sale.gau())
        assert response.status_code == 404

    def test_open_page_draft_sale_client_login_superuser(self, draft_sale, client_login_superuser):
        response = client_login_superuser.get(draft_sale.gau())
        assert response.status_code == 200
        assert response.resolver_match.func.__name__ == SaleDetailView.as_view().__name__

    def test_open_page_run_sale_client(self, run_sale, client):
        response = client.get(run_sale.gau())
        assert response.status_code == 200

    def test_open_page_run_sale_client_login_moder(self, run_sale, client_login_moder):
        response = client_login_moder.get(run_sale.gau())
        assert response.status_code == 200

    def test_open_page_run_sale_client_login_superuser(self, run_sale, client_login_superuser):
        response = client_login_superuser.get(run_sale.gau())
        assert response.status_code == 200

    def test_open_page_finished_sale_client(self, finished_sale, client):
        response = client.get(finished_sale.gau())
        assert response.status_code == 301

    def test_open_page_finished_sale_client_login_moder(self, finished_sale, client_login_moder):
        response = client_login_moder.get(finished_sale.gau())
        assert response.status_code == 301

    def test_open_page_finished_sale_client_login_superuser(self, finished_sale, client_login_superuser):
        response = client_login_superuser.get(finished_sale.gau())
        assert response.status_code == 200
        assert response.resolver_match.func.__name__ == SaleDetailView.as_view().__name__


class TestViewsContextSale:
    @pytest.mark.parametrize('lang', settings.TEST_LANGUAGES)
    def _test_SALE_not_in_context_draft(self, draft_sale, client, lang):
        translation.activate(lang)
        response = client.get(reverse('shop-guitar-sheets-tabs-main'))
        assert response.context.get('SALE') is None
        assert response.context['IS_SALE'] == False

    @pytest.mark.parametrize('lang', settings.TEST_LANGUAGES)
    def _test_SALE_in_context_run_sale(self, run_sale, client, lang):
        translation.activate(lang)
        response = client.get(reverse('shop-guitar-sheets-tabs-main'))
        print(reverse('shop-guitar-sheets-tabs-main'))
        assert Sale.objects.is_sale() is True
        assert response.context['SALE'] == run_sale
        assert response.context['IS_SALE'] == True

    @pytest.mark.parametrize('lang', settings.TEST_LANGUAGES)
    def _test_SALE_in_context_pub_sale(self, pub_sale, client, lang):
        translation.activate(lang)
        response = client.get(reverse('shop-guitar-sheets-tabs-main'))
        assert Sale.objects.is_sale() is True
        assert response.context['SALE'] == pub_sale
        assert response.context['IS_SALE'] == True
        # print(response.content.decode('utf-8'))
        # assert pub_sale.get_title() not in response.content.decode('utf-8')
        # assert True is False
