# -*- coding: utf-8 -*-
from django.conf import settings
from django.urls import reverse_lazy
import logging

logger = logging.getLogger(__name__)


def test_site_info(client):
    url = '/ru/'
    response = client.get(url)
    assert response.status_code == 200
    assert response.context['SITE_NAME'] == settings.SITE_NAME
    assert response.context['ip'] == '46.50.132.231'
    assert response.context['country'].code == 'RU'
