# -*- coding: utf-8 -*-
import pytest
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from mixer.backend.django import mixer

from base.templatetags.editlink_tags import has_permission_edit
from manage import rel
from offer.models import Currency
from shop.models import Composer, ShopOffers, UserFile


@pytest.fixture
def item_user_1(user_user):
    composer, cre = Composer.objects.update_or_create(
        slug='testcomposer',
        defaults={
            'name' : 'Test Composer',
            'accepted': True,
        }
    )
    # composer = mixer.blend(Composer,
    #                        accepted=True,
    #                        name='Test Composer',
    #                        slug='testcomposer')
    kwargs = {
        'name': 'Mane',
        'description': 'Distrip oil',
        'price': 4800,
        'currency': Currency.objects.get(pk=1),
        'composer': composer,
        'difficulty': 1,
        'user_created': user_user,
        'checked': True,
        'video_url': 'https://www.youtube.com/watch?v=EadZchN5_F8',
    }
    item = ShopOffers.objects.create(**kwargs)
    filename = rel('src/test_cases/test.jpg')
    with open(filename, mode='rb') as f:
        uf = UserFile.objects.create(item=item, user_created=user_user)
        uf.file.save(filename, f)
    return item


def test_has_permission_edit_superuser(item_user_1, user_superuser):
    assert has_permission_edit(item_user_1, user_superuser) is True


def test_has_permission_edit_user(item_user_1, user_user):
    assert has_permission_edit(item_user_1, user_user) is True


def test_has_permission_edit_user_staff_perm(item_user_1, user_staff):
    assert has_permission_edit(item_user_1, user_staff) is True


def test_has_permission_edit_user_not_perm(item_user_1, user_user_2):
    assert has_permission_edit(item_user_1, user_user_2) is False


def test_has_permission_edit_user_is_perm(item_user_1, user_user_2):
    content_type = ContentType.objects.get_for_model(ShopOffers)
    user_user_2.user_permissions.add(Permission.objects.get(content_type=content_type, codename='change_shopoffers'))
    assert has_permission_edit(item_user_1, user_user_2) is True
