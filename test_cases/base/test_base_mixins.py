# -*- coding: utf-8 -*-
import pytest

from base.mixins import ChangesBetweenModelsMix


@pytest.mark.parametrize('description1, description2, result', [
    ('123', ' 123', []),
    ('123', '12 3', []),
    ('123', '123 ', []),
    ('123', ' 12 3 ', []),
    ('111', ' 123 ', [['Описание', '111', ' 123 ']]),
])
def test_changes_between_models_mix(description1, description2, result, mixer):
    cbm = ChangesBetweenModelsMix()

    model1 = mixer.blend('shop.ShopOffers', description=description1)
    model2 = mixer.blend('shop.ShopOffers', description=description2)

    changes = cbm.get_changes_between_models(model1, model2, fields=['description'])
    assert changes == result
