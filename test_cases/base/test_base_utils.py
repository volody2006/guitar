# -*- coding: utf-8 -*-
import pytest
from base.utils import format_price
from decimal import Decimal


@pytest.mark.parametrize('variable, result', [
    ('123', '123'),
    ('123.', '123'),
    ('123.0000', '123'),
    ('123.0', '123'),
    ('123.500', '123.5'),
    (float(124), '124'),
    (float(124.5), '124.5'),
    (float(124.000), '124'),
    (Decimal('125'), '125'),
    (Decimal('125.'), '125'),
    (Decimal('125.0000'), '125'),
    (Decimal('125.5000'), '125.5'),

])
def test_format_price(variable, result):
    assert format_price(variable) == result


@pytest.mark.parametrize('variable', [
    ('ы123'),
    (''),
    ('sds'),
    ([]),
    ('1234e'),
    ('sds ddddd'),
])
def test_format_price_type_error(variable):
    with pytest.raises(ValueError):
        format_price(variable)
