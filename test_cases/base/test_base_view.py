# -*- coding: utf-8 -*-
import logging

from django.contrib.flatpages.models import FlatPage
from django.contrib.sites.models import Site
from django.urls import reverse_lazy
from robots.views import RuleList
from static_sitemaps.views import SitemapView

from base.views import IndexView

logger = logging.getLogger(__name__)


def test_main_page_status_200(client):
    response = client.get(reverse_lazy('index'))
    assert response.status_code == 200
    assert response.resolver_match.func.__name__ == IndexView.as_view().__name__


def test_robots_not_slash(client):
    url = '/robots.txt'
    response = client.get(url)
    assert response.status_code == 200
    # assert response.resolver_match.func.__name__ == RuleList.as_view().__name__


def test_robots_slash(client):
    url = '/robots.txt/'
    response = client.get(url)
    assert response.status_code == 302


def test_sitemap_not_slash(client):
    url = '/sitemap.xml'
    response = client.get(url)
    assert response.status_code == 200
    assert response.resolver_match.func.__name__ == SitemapView.as_view().__name__


def test_flatpages_not_slash(client):
    f = FlatPage.objects.create(url='/test_flatpages', title='test', content='test')
    f.sites.add(Site.objects.all()[0])
    response = client.get(f.url)
    assert response.status_code == 200


def test_flatpages_slash(client):
    f = FlatPage.objects.create(url='/test_flatpages/', title='test', content='test')
    f.sites.add(Site.objects.all()[0])
    response = client.get(f.url)
    assert response.status_code == 200


def test_user_slash(client, user_user):
    url = '/ru/{}/'.format(user_user.slug)
    response = client.get(url)
    assert response.status_code == 200


def test_user_not_slash(client, user_user):
    url = '/{}'.format(user_user.slug)
    response = client.get(url)
    assert response.status_code == 302
    assert response.url == '/ru{}/'.format(url)
