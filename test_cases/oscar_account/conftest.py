# -*- coding: utf-8 -*-

import pytest


# pytest_plugins = ['base_fuxtures', ]


@pytest.fixture
def source_rur(mixer, default_accounts):
    from offer.models import Currency
    return mixer.blend('oscar_accounts.Account', account_type__currency=Currency.objects.get(pk=1))


@pytest.fixture
def no_credit_limit_account_rur(mixer, default_accounts):
    from offer.models import Currency
    return mixer.blend('oscar_accounts.Account', account_type__currency=Currency.objects.get(pk=1), credit_limit=None)


@pytest.fixture
def destination_rur(mixer, default_accounts):
    from offer.models import Currency
    return mixer.blend('oscar_accounts.Account', account_type__currency=Currency.objects.get(pk=1))
