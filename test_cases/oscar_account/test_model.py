import pytest
import datetime
from decimal import Decimal as D

from django.utils import timezone

from oscar_accounts import exceptions
from oscar_accounts.models import Account, Transfer
from oscar_accounts.test_factories import TransactionFactory


@pytest.fixture
def account(mixer, default_accounts):
    from offer.models import Currency
    # return mixer.blend('oscar_accounts.Account', currency=Currency.objects.get(pk=1))
    return mixer.blend('oscar_accounts.Account')


class TestAnAccount:

    def setUp(self):
        self.account = Account()

    def test_is_open_by_default(self, account):
        assert Account.OPEN == account.status

    def test_can_be_closed(self, account):
        account.close()
        assert Account.CLOSED == account.status

    def test_always_saves_the_code_as_uppercase(self, account):
        account.code = 'abc'
        account.save()
        assert 'ABC' == account.code

    def test_can_be_authorised_when_no_user_passed(self, account):
        assert account.can_be_authorised_by() is False

    def test_can_be_authorised_by_anyone_by_default(self, account, user_user):
        account.save()
        user = user_user
        assert account.can_be_authorised_by(user) is False

    def test_can_only_be_authorised_by_primary_user_when_set(self, account, user_user, user_user_2):
        primary = user_user
        other = user_user_2
        account.primary_user = primary
        account.save()

        assert account.can_be_authorised_by(primary) is True
        assert account.can_be_authorised_by(other) is False

    def test_can_only_be_authorised_by_secondary_users_when_set(self, account, mixer, user_user):
        account.save()
        users = mixer.cycle(5).blend('users.User', is_active=True, phone_number='+79234102524')
        # users = [UserFactory(), UserFactory()]
        other = user_user
        for user in users:
            account.secondary_users.add(user)

        for user in users:
            assert account.can_be_authorised_by(user) is True
        assert account.can_be_authorised_by(other) is False

    def test_does_not_permit_an_allocation(self, account):
        amt = account.permitted_allocation(None, D('2.00'), D('12.00'))
        assert D('0.00') == amt


class TestAnAccountWithFunds:

    def test_cannot_be_closed(self, account):
        account.balance = D('2.00')
        with pytest.raises(exceptions.AccountNotEmpty):
            account.close()

    def test_allows_allocations_less_than_balance(self, account):
        account.balance = D('100.00')
        amt = account.permitted_allocation(
            None, D('2.00'), D('12.00'))
        assert D('12.00') == amt

    def test_doesnt_allow_allocations_greater_than_balance(self, account):
        account.balance = D('100.00')
        amt = account.permitted_allocation(
            None, D('2.00'), D('120.00'))
        assert D('100.00') == amt


class TestAnAccountWithFundsButOnlyForProducts:

    # def setUp(self):
    #     self.account = Account()
    #     self.account.can_be_used_for_non_products = False
    #     self.account.balance = D('100.00')

    def test_doesnt_allow_shipping_in_allocation(self, account):
        account.can_be_used_for_non_products = False
        account.balance = D('100.00')
        # account.save()
        amt = account.permitted_allocation(
            None, D('20.00'), D('40.00'))
        assert D('20.00') == amt


class TestANewZeroCreditLimitAccount:

    def test_defaults_to_zero_credit_limit(self, account):
        assert D('0.00') == account.credit_limit

    def test_does_not_permit_any_debits(self, account):
        assert account.is_debit_permitted(D('1.00')) is False

    def test_has_zero_balance(self, account):
        assert D('0.00') == account.balance

    def test_has_zero_transactions(self, account):
        assert 0 == account.num_transactions()


@pytest.fixture
def account_credit_limit_500(mixer, default_accounts):
    from offer.models import Currency
    return mixer.blend('oscar_accounts.Account', account_type__currency=Currency.objects.get(pk=1),
                       credit_limit=D('500'),
                       start_date=None, end_date=None)


class TestAFixedCreditLimitAccount:

    def test_permits_smaller_and_equal_debits(self, account_credit_limit_500):
        for amt in (D('0.00'), D('1.00'), D('500')):
            assert account_credit_limit_500.is_debit_permitted(amt) is True

    def test_does_not_permit_larger_amounts(self, account_credit_limit_500):
        for amt in (D('501'), D('1000')):
            assert account_credit_limit_500.is_debit_permitted(amt) is False


class TestAnUnlimitedCreditLimitAccount:

    def test_permits_any_debit(self, no_credit_limit_account_rur):
        for amt in (D('0.00'), D('1.00'), D('1000000')):
            assert no_credit_limit_account_rur.is_debit_permitted(amt) is True


class TestAccountExpiredManager:

    def test_includes_only_expired_accounts(self, mixer):
        now = timezone.now()
        mixer.blend('oscar_accounts.Account', end_date=now - datetime.timedelta(days=1))
        mixer.blend('oscar_accounts.Account', end_date=now + datetime.timedelta(days=1))
        accounts = Account.expired.all()
        assert 1 == accounts.count()


class TestAccountActiveManager:

    def test_includes_only_active_accounts(self, mixer):
        now = timezone.now()
        expired = mixer.blend('oscar_accounts.Account', end_date=now - datetime.timedelta(days=1))
        mixer.blend('oscar_accounts.Account', end_date=now + datetime.timedelta(days=1))
        mixer.blend('oscar_accounts.Account', start_date=now, end_date=now + datetime.timedelta(days=1))
        # AccountFactory(end_date=now + datetime.timedelta(days=1))
        # AccountFactory(start_date=now, end_date=now + datetime.timedelta(days=1))
        accounts = Account.active.all()
        assert expired not in accounts


class TestATransaction:

    # def test_cannot_be_deleted(self, mixer):
    #     txn = mixer.blend('oscar_accounts.Transaction')
    #     with pytest.raises(RuntimeError):
    #         txn.delete()

    def test_is_not_deleted_when_the_authorisor_is_deleted(self, user_user, no_credit_limit_account_rur,
                                                           destination_rur, system_user):
        user = user_user
        no_credit_limit_account_rur.primary_user = user
        no_credit_limit_account_rur.save()

        txn = Transfer.custom_objects.create(no_credit_limit_account_rur, destination_rur,
                                      D('20.00'), user=user)
        assert 2 == txn.transactions.all().count()
        user.delete()
        assert 2 == txn.transactions.all().count()

        # test_cannot_be_deleted

        transaction = txn.transactions.all()[0]
        with pytest.raises(RuntimeError):
            transaction.delete()


def test_user_have_account(user_user, default_accounts):
    from offer.models import Currency
    account_count = Account.objects.all().count()
    for cur in Currency.objects.all():
        account_count += 1
        account = user_user.get_account(currency=cur)
        assert account.balance == D('0.00')
        assert account.secondary_users.all().count() == 0
        assert account.primary_user == user_user
        assert account.credit_limit == D('0.00')


        assert account_count ==  Account.objects.all().count()
