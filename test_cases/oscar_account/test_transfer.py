# -*- coding: utf-8 -*-
import pytest
from decimal import Decimal as D
from django.utils import timezone
from oscar_accounts import exceptions
from oscar_accounts.models import Transfer


def test_creates_2_transactions(no_credit_limit_account_rur, destination_rur, system_user):
    transfer = Transfer.custom_objects.create(no_credit_limit_account_rur, destination_rur,
                                              D('10.00'), user=system_user)
    assert 2 == transfer.transactions.all().count()
    assert D('10.00') == transfer.amount
    assert -D('10.00') == transfer.source.balance
    assert D('10.00') == transfer.destination.balance


def test_cannot_be_deleted(no_credit_limit_account_rur, destination_rur, system_user):
    transfer = Transfer.custom_objects.create(no_credit_limit_account_rur, destination_rur,
                                              D('10.00'), user=system_user)
    with pytest.raises(RuntimeError):
        transfer.delete()


def test_records_static_user_information_in_case_user_is_deleted(no_credit_limit_account_rur, destination_rur,
                                                                 user_superuser, system_user):
    transfer = Transfer.custom_objects.create(no_credit_limit_account_rur, destination_rur,
                                              D('10.00'), user=user_superuser)
    username = user_superuser.email
    assert username == transfer.authorisor_username
    user_superuser.delete()
    transfer = Transfer.objects.get(id=transfer.id)
    assert username == transfer.authorisor_username


class TestATransferToAnInactiveAccount:

    def test_is_permitted(self, no_credit_limit_account_rur, destination_rur, user_user):
        self.user = user_user
        now = timezone.now()
        destination_rur.end_date = (now - timezone.timedelta(days=1))
        destination_rur.save()
        with pytest.raises(exceptions.AccountException):
            Transfer.custom_objects.create(no_credit_limit_account_rur, destination_rur,
                                           D('20.00'), user=self.user)


class TestATransferFromAnInactiveAccount:

    def test_is_permitted(self, no_credit_limit_account_rur, destination_rur, user_user):
        self.user = user_user
        now = timezone.now()
        no_credit_limit_account_rur.end_date = (now - timezone.timedelta(days=1))
        no_credit_limit_account_rur.save()
        with pytest.raises(exceptions.AccountException):
            Transfer.custom_objects.create(no_credit_limit_account_rur, destination_rur,
                                           D('20.00'), user=self.user)


class TestAnAttemptedTransfer:

    def test_raises_an_exception_when_trying_to_exceed_credit_limit_of_source(self, source_rur, destination_rur,
                                                                              user_user):
        with pytest.raises(exceptions.InsufficientFunds):
            source_rur.primary_user = user_user
            source_rur.save()
            Transfer.custom_objects.create(source_rur, destination_rur,
                                           D('20.00'), user=user_user)

    def test_raises_an_exception_when_trying_to_debit_negative_value(self, no_credit_limit_account_rur, destination_rur,
                                                                     user_user):
        # source = AccountFactory(credit_limit=None)
        # destination = AccountFactory()
        with pytest.raises(exceptions.InvalidAmount):
            Transfer.custom_objects.create(no_credit_limit_account_rur, destination_rur,
                                           D('-20.00'), user=user_user)

    def test_raises_an_exception_when_trying_to_use_closed_source(self, no_credit_limit_account_rur, destination_rur,
                                                                  user_user):
        source = no_credit_limit_account_rur
        source.close()
        destination = destination_rur
        with pytest.raises(exceptions.ClosedAccount):
            Transfer.custom_objects.create(source, destination,
                                           D('20.00'), user=user_user)

    def test_raises_an_exception_when_trying_to_use_closed_destination(self, no_credit_limit_account_rur,
                                                                       destination_rur, user_superuser):
        source = no_credit_limit_account_rur
        destination = destination_rur
        destination.close()
        with pytest.raises(exceptions.ClosedAccount):
            Transfer.custom_objects.create(
                source, destination, D('20.00'), user=user_superuser)
