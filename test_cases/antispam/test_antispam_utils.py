# -*- coding: utf-8 -*-
import pytest

from antispam.models import BlackWord, WhiteWord
from antispam.utils import normalize_word, detect_black_word, censorship_text


@pytest.fixture()
def f_word():
    words = ['солнце', "трава", "море"]
    for word in words:
        BlackWord.objects.create(word=word)

    words = ['солнцеликий', 'травамура', 'мореход']
    for word in words:
        WhiteWord.objects.create(word=word)


@pytest.mark.parametrize('word, resuld', [
    ('море ', 'море'),
    ('"море" ', 'море'),
    ('ser ', 'ser'),
    (' ser ', 'ser'),
    ('s er ', 'ser'),
    ('s     er ', 'ser'),
    ('SER ', 'ser'),
    ('sEr', 'ser'),
    ('_s!@#$%^&*()_+|\=-`~/*-+][}{><,.:;Er!', 'ser'),
])
def test_normalize_word(word, resuld):
    assert normalize_word(word) == resuld


@pytest.mark.parametrize('word', ['солнце', "трава", "море"])
def test_detect_black_word_black(f_word, word):
    assert detect_black_word(word) is True


@pytest.mark.parametrize('word', ['солнцеликий', 'травамура', 'мореход'])
def test_detect_black_word_white(f_word, word):
    assert detect_black_word(word) is False


def test_detect_black_word_not_word(f_word):
    assert detect_black_word('xthn') is False


@pytest.mark.parametrize('word, result', [
    ('солнцеликий', 'солнцеликий'),
    ('травамура', 'травамура'),
    ('мореход', 'мореход'),
    ('солнце', '******'),
    ('трава', '*****'),
    ('море', '****'),
    ('@#Солнце@#.', '@#******@#.'),
    # ('@#Сол@@нце@#.', '@#********@#.'),
])
def test_censorship_text_words(f_word, word, result):
    assert result == censorship_text(word)


@pytest.mark.xfail
def test_censorship_text(f_word):
    assert BlackWord.objects.filter(word="трава").exists() is True
    test1 = '"Солнце, я травa": - сказал я в море. Я мореход, а не с_о)л*н-ц!е. Солнцеликий, солнцеликий.'
    test2 = '"******, я *****": - сказал я в ****. Я мореход, а не ***********. Солнцеликий, солнцеликий.'
    assert test2 == censorship_text(test1)
