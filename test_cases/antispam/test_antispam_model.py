# -*- coding: utf-8 -*-
import pytest

from antispam.models import BlackWord


@pytest.mark.parametrize('word, resuld', [
    ('ser ', 'ser'),
    (' ser ', 'ser'),
    ('s er ', 'ser'),
    ('s     er ', 'ser'),
    ('SER ', 'ser'),
    ('sEr', 'ser'),
])
def test_black_word_add(word, resuld):
    new_word = BlackWord.objects.create(word=word)
    assert new_word.word == resuld


# комментарий состоит только из стоп слова, считаем спамом
# Комментарий содержит 1 стоп слово, замена не звездочки.
# комментарий содержит 2 и более стоп слова, пометка спам
