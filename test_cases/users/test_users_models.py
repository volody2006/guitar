# -*- coding: utf-8 -*-
from django.contrib.auth.models import Permission
from django.utils.translation import ugettext as _
import logging

from users.models import User, Contract

logger = logging.getLogger(__name__)


def test_bad_slug(user_user):
    slug = 'sh.in.am.in.sk2.0.1.51536310193'
    u = User.objects.get(pk=user_user.pk)
    u.slug = slug
    u.save()
    assert u.slug_or_pk == 'test_user'


def test_can_have_contract_cood(user_user, perm_fixture):
    assert user_user.can_have_contract is True


def test_can_have_contract_cood_superuser(user_superuser, perm_fixture):
    permission = Permission.objects.get(codename=Contract.CODE_PERMISSION)
    user_superuser.user_permissions.add(permission)
    user = User.objects.get(pk=user_superuser.pk)
    assert user.can_have_contract is False


def test_can_have_contract_system_user(system_user, perm_fixture):
    assert system_user.can_have_contract is False


def test_can_have_contract_not_contract_perm_user(user_user, perm_fixture):
    permission = Permission.objects.get(codename=Contract.CODE_PERMISSION)
    user_user.user_permissions.add(permission)
    user = User.objects.get(pk=user_user.pk)
    assert user.can_have_contract is False
