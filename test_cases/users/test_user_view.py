# -*- coding: utf-8 -*-
import logging

import os
import pytest
from django.conf import settings
from django.urls import reverse

from antispam.models import IpSpam
from users.models import User

logger = logging.getLogger(__name__)


def test_register_user(client, mixer):
    response = client.get(reverse('users_registration'))
    assert 200 == response.status_code
    data = {'password': 'test_pass',
            'email': 'Test+Test@mail.ru',
            'confirmPP': 'on',
            }
    recaptcha_response_field = 'recaptcha_response_field'
    if getattr(settings, 'NOCAPTCHA'):
        recaptcha_response_field = 'g-recaptcha-response'
    data.update({recaptcha_response_field: 'PASSED'})

    response = client.post(reverse('users_registration'), data=data)
    # print(response.content.decode("utf-8"))
    assert response.status_code == 302
    assert User.objects.all().count() == 1
    assert 'test+test@mail.ru' == User.objects.all()[0].email


@pytest.mark.parametrize("username", ["Test+Test@mail.ru", "test+test@mail.ru", "TEst+Test@mail.ru"])
@pytest.mark.parametrize("login", ["Test+Test@mail.ru", "test+test@mail.ru", "TEst+Test@mail.ru"])
def test_login_user(client, username, login):
    # os.environ['RECAPTCHA_TESTING'] = 'True'

    data = {'password': 'test_pass',
            'email': login,
            'confirmPP': 'on',
            }
    recaptcha_response_field = 'recaptcha_response_field'
    if getattr(settings, 'NOCAPTCHA'):
        recaptcha_response_field = 'g-recaptcha-response'
    data.update({recaptcha_response_field: 'PASSED'})

    response = client.post(reverse('users_registration'), data=data)
    assert response.status_code == 302
    user = User.objects.all()[0]
    assert 'test+test@mail.ru' == user.email

    user.is_active = True
    user.save()
    response = client.get(reverse('login'))
    assert 200 == response.status_code

    data = {'password': 'test_pass',
            'username': username,
            }

    response = client.post(reverse('login'), data=data)
    assert response.url == '/'
    assert response.status_code == 302


def test_register_user_email(client, mixer, mailoutbox):
    data = {'password': 'test_pass',
            'email': 'Test+Test@mail.ru',
            'confirmPP': 'on',
            }
    recaptcha_response_field = 'recaptcha_response_field'
    if getattr(settings, 'NOCAPTCHA'):
        recaptcha_response_field = 'g-recaptcha-response'
    data.update({recaptcha_response_field: 'PASSED'})

    response = client.post(reverse('users_registration'), data=data)
    # print(response.content.decode("utf-8"))
    # get_activate_url
    user = User.objects.all()[0]
    assert 'test+test@mail.ru' == user.email

    assert len(mailoutbox) == 1
    m = mailoutbox[0]
    assert settings.SITE_PROJECT_NAME in m.subject
    # assert user.get_activate_url() in m.body
    assert m.from_email == settings.SUPPORT_EMAIL
    assert list(m.to) == ['test+test@mail.ru']


def test_register_user_get_activate_url(client, mixer, mailoutbox):
    data = {'password': 'test_pass',
            'email': 'Test+Test@mail.ru',
            'confirmPP': 'on',
            }
    recaptcha_response_field = 'recaptcha_response_field'
    if getattr(settings, 'NOCAPTCHA'):
        recaptcha_response_field = 'g-recaptcha-response'
    data.update({recaptcha_response_field: 'PASSED'})

    response = client.post(reverse('users_registration'), data=data)
    user = User.objects.all()[0]
    assert 'test+test@mail.ru' == user.email
    assert user.is_active is False

    response = client.get(user.get_activate_url())
    assert 302 == response.status_code

    user = User.objects.get(pk=user.pk)
    assert user.is_active is True


def test_register_user_get_activate_url_client_login(client_login):
    user = User.objects.create_user(
        email='test_no_active@email.com',
        password='Test12345',
        is_active=False,
        slug='test_no_active'
    )

    response = client_login.get(user.get_activate_url())
    assert 302 == response.status_code

    user = User.objects.get(pk=user.pk)
    assert user.is_active is True


def test_spam_get_activate_url(client):
    user = User.objects.create_user(
        email='test_no_active@email.com',
        password='Test12345',
        is_active=False,
        slug='test_no_active'
    )
    IpSpam.objects.get_or_create(ip='46.50.132.231')

    response = client.get(user.get_activate_url())
    assert 302 == response.status_code

    user = User.objects.get(pk=user.pk)
    assert user.is_spam is True
