# -*- coding: utf-8 -*-
import logging

import pytest

from users.utils import generate_slug

logger = logging.getLogger(__name__)


@pytest.fixture()
def users(user_user, user_user_2):
    user_user.email = 'test.+test@test.ru'
    user_user.slug = 'testtest'
    user_user.save()

    user_user_2.email = 'test.test@test.ru'
    user_user_2.save()

    return [user_user, user_user_2]


@pytest.mark.parametrize('email, slug', [
    ('sem.sde@email.com', 'semsde'),
    ('sem.+sde@email.com', 'semsde'),
    ('sem.+sde1@email.com', 'semsde1'),
    ('sem.+sde1@email.com', 'semsde1'),
    ('sem_sde1@email.com', 'sem_sde1'),
    ('sem-sde1@email.com', 'sem-sde1'),
    ('bo.ris19.7.7g.orb.u.nov@gmail.com', 'boris1977gorbunov'),
])
def test_gen_slug_email(email, slug):
    assert generate_slug(email) == slug


def test_gen_slug_email_dubl(users):
    slug = generate_slug(users[1].email)
    assert slug != 'testtest'
    assert '.' not in slug


def test_referral_code(user_user):
    assert '' != user_user.referral_code
    assert 5 == len(user_user.referral_code)

# def test_referral_code_2(user_user):
#     user_user.referral_code = ''
#     assert '' != user_user.referral_code
#
#
# def test_referral_code_3_None(user_user):
#     user_user.referral_code = None
#     assert '' != user_user.referral_code
#     assert user_user.referral_code is not None
#
#
#
# def test_referral_code_3_False(user_user):
#     user_user.referral_code = False
#     assert '' != user_user.referral_code
#     assert user_user.referral_code is not None
#
#
#
# @pytest.mark.parametrize('code', [
#     (False),
#     (True),
#     (None),
#     (''),
#     ('     '),
# ])
# def test_gen_referral_code(code, user_user):
#     user_user.referral_code = code
#     user_user.save()
#     assert 5 == len(user_user.referral_code)
#     assert code != user_user.referral_code


# @pytest.mark.parametrize('email', [
#     ('sem.sde@email.com'),
#     ('sEm.sde@email.com'),
#     ('Sem.sde@email.com'),
# ])
# def test_form_register(email):
#     USER_PASSWORD = 'Test'
#     form_params = {'password': USER_PASSWORD,
#                    'email': email,
#                    'confirmPP': True,
#                    'g-recaptcha-response': 'PASSED'}
#     form = RegistrationForm(form_params)
#     assert form.is_valid() is True
#     form.save()
#     user = User.objects.get(email=email)
#     assert email.lower() == user.email
#     # self.assertEqual(user.email, USER_EMAIL
