# -*- coding: utf-8 -*-
import json

import pytest
from cart.models import CartItem
from django.urls import reverse
from django.utils.translation import ugettext as _
import logging

from gis.models import Country
from offer.models import Currency
from shop.models import ShopCategory, Composer, ShopOffers


@pytest.fixture
def shop_offer1(user_user, mixer):
    kwargs = {
        'name': 'Mane',
        'description': 'Distrip oil',
        'price': 100.00,
        'currency': mixer.blend(Currency),
        'country': mixer.blend(Country),
        'category': mixer.blend(ShopCategory, slug='cat1'),
        'composer': mixer.blend(Composer),
        'difficulty': 1,
        'user_created': user_user,
        'checked': True,
        'video_url': 'https://www.youtube.com/watch?v=EadZchN5_F8',
    }
    item = ShopOffers.objects.create(**kwargs)
    return item


@pytest.fixture
def shop_offer2(user_user, mixer, shop_offer1):
    kwargs = {
        'name': 'Mane2',
        'description': 'Distrip oil 1',
        'price': 110.00,
        'currency': shop_offer1.currency,
        'country': shop_offer1.country,
        'vendor': shop_offer1.vendor,
        'category': shop_offer1.category,
        'composer': shop_offer1.composer,
        'difficulty': 1,
        'user_created': user_user,
        'checked': True,
        'video_url': 'https://www.youtube.com/watch?v=E1adZchN5_F8',
    }
    item = ShopOffers.objects.create(**kwargs)
    return item


def test_anonymous_possibility(client, shop_offer1):
    # анонимный пользователь
    response = client.get(reverse('shop-guitar-sheets-tabs-detail', kwargs={'slug': shop_offer1.slug}))
    assert 200 == response.status_code
    assert 'cart_form' in response.context


def test_user_possibility(client_login, shop_offer1):
    # shop.views.ShopDetailView
    response = client_login.get(reverse('shop-guitar-sheets-tabs-detail', kwargs={'slug': shop_offer1.slug}))
    assert 200 == response.status_code
    assert 'cart_form' in response.context


# class CartAdditionTest(TestCase):
#     def setUp(self):
#         self.user = add_user_random(password='password')
#         self.shop_offer1 = add_shop_offer(price=Decimal(100))
#         self.shop_offer2 = add_shop_offer(price=Decimal(200))
#         self.shop_offer3 = add_shop_offer(price=Decimal(300))
#
#         self.client = Client()
#
def test_cart_add_1(client_login, shop_offer1):
    response = client_login.post(reverse('cart_add'), {'shop_offer': shop_offer1.pk},
                                 HTTP_X_REQUESTED_WITH='XMLHttpRequest'
                                 )
    print(response.content)
    assert 200 == response.status_code
    assert json.loads(response.content)['success'] == 1

def test_cart_add_2(client_login, shop_offer1):
    # дублирование заказа
    response = client_login.post(reverse('cart_add'), {'shop_offer': shop_offer1.pk},
                                 HTTP_X_REQUESTED_WITH='XMLHttpRequest')
    response = client_login.post(reverse('cart_add'), {'shop_offer': shop_offer1.pk},
                                 HTTP_X_REQUESTED_WITH='XMLHttpRequest')
    assert 200 == response.status_code
    assert json.loads(response.content)['success'] == 0


#
#
def test_cart_add_3(client_login, shop_offer1, shop_offer2):
    # новый заказ
    response = client_login.post(reverse('cart_add'), {'shop_offer': shop_offer1.pk},
                                 HTTP_X_REQUESTED_WITH='XMLHttpRequest')
    response = client_login.post(reverse('cart_add'), {'shop_offer': shop_offer2.pk},
                                 HTTP_X_REQUESTED_WITH='XMLHttpRequest')
    assert 200 == response.status_code
    assert json.loads(response.content)['success'] == 1
    assert CartItem.objects.all().count() == 2


def test_cart_unitor(client, shop_offer1):
    response = client.get('/shop/%s/' % shop_offer1.slug)
    assert 200 == response.status_code
    assert 'cart_form' in response.context


def test_user_possibility_cart_form(client_login, shop_offer1):
    # shop.views.ShopDetailView
    response = client_login.get('/shop/%s/' % shop_offer1.slug)
    assert 200 == response.status_code
    assert 'cart_form' in response.context
