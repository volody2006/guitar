# -*- coding: utf-8 -*-
import pytest

from antispam.models import BlackWord, WhiteWord
from antispam.utils import normalize_word, detect_black_word, censorship_text
from comments.models import Comment


@pytest.fixture()
def f_word():
    words = ['солнце', "трава", "море"]
    for word in words:
        BlackWord.objects.create(word=word)

    words = ['солнцеликий', 'травамура', 'мореход']
    for word in words:
        WhiteWord.objects.create(word=word)


def test_comment_censor_model_save(f_word):
    com = Comment.objects.create(
        user_name='солнце солнцеликий',
        text='солнце солнцеликий',
        advantage='солнце солнцеликий',
        disadvantages='солнце солнцеликий',
        experience='солнце солнцеликий',
        content_object=WhiteWord.objects.first(),
    )
    com.save()
    for qwe in ['солнце', 'user_name', 'text', 'advantage', 'disadvantages', 'experience']:
        assert qwe in com.source_text

    assert '****** солнцеликий' == com.user_name
    assert '****** солнцеликий' == com.text
    assert '****** солнцеликий' == com.advantage
    assert '****** солнцеликий' == com.disadvantages
    assert '****** солнцеликий' == com.experience
