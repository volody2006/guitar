# -*- coding: utf-8 -*-
import pytest

from comments.forms import CommentForm


def test_comment_bot():
    data = {
        'comment': 'spam boot',
        'text': 'text',
        'content_type': 1,
        'object_id': 1
    }
    com_form= CommentForm(data=data)
    assert com_form.is_valid()

