# -*- coding: utf-8 -*-
import pytest
from django.conf import settings
from mixer.backend.django import mixer

from antispam.models import IpSpam
from articles.forms import ArticleForm
from articles.models import TagArticle


@pytest.fixture
def article_form_data():
    data = {
        'title': 'title',
        'level': 1,
        'announce': 'announce',
        'content': 'content',
        'tags': ['test']
    }
    return data


def test_article_form_valid_not_spam(article_form_data, user_user):
    data = article_form_data

    ip = '127.0.0.1'
    assert IpSpam.objects.filter(ip=ip).exists() is False

    form = ArticleForm(data=data)
    print('error ', form.errors)
    assert form.is_valid() is True

    article = form.save(ip_address=ip, user=user_user, is_active=True)
    assert article.is_active is True
    # assert article.is_spam is True
    assert article.user.is_spam is False
    assert IpSpam.objects.filter(ip=ip).exists() is False


def test_article_form_valid_spam_honeypot(article_form_data, user_user):
    # data = {
    #     'title': 'title',
    #     'level': 1,
    #     'announce': 'announce',
    #     'content': 'content',
    #     'text': 'spam'
    # }
    article_form_data.update({
        'text': 'spam'
    })
    ip = '127.0.0.1'
    assert IpSpam.objects.filter(ip=ip).exists() is False

    form = ArticleForm(data=article_form_data)
    # print('error ', form.errors)
    assert form.is_valid() is True

    article = form.save(ip_address=ip, user=user_user, is_active=True)
    assert article.is_active is False
    # assert article.is_spam is True
    assert article.user.is_spam is True
    assert IpSpam.objects.filter(ip=ip).exists() is True
    assert IpSpam.objects.filter(ip=ip)[0].is_honeypot is True
