# -*- coding: utf-8 -*-
from datetime import datetime

from articles.models import Article, TagArticle
import pytest


@pytest.fixture
def article(user_user):
    article = Article.objects.create(
        user=user_user,
        title='tit',
        lang='ru',
        pub_date=datetime(1930, 1, 1),
        is_active=True,
    )
    return article


def test_no_tags_article(articles_tags):
    for tag in TagArticle.objects.all():
        assert tag.articles_count_ru == 0
        assert tag.articles_count_en == 0


def test_recalc_tag_article_add(articles_tags, article):
    article.tags.add(TagArticle.objects.get(pk=1))
    article.save()
    article = Article.objects.get(pk=article.pk)
    assert article.tags.all().count() == 1
    assert TagArticle.objects.get(pk=1).articles_count_ru == 1
    assert TagArticle.objects.get(pk=1).articles_count_en == 0


def test_recalc_tag_changing_language_article(articles_tags, article):
    article.tags.add(TagArticle.objects.get(pk=1))

    assert TagArticle.objects.get(pk=1).articles_count_ru == 1
    assert TagArticle.objects.get(pk=1).articles_count_en == 0

    # Меняем язык
    # article = Article.objects.get(pk=article.pk)
    article.lang = 'en'
    article.save()
    assert TagArticle.objects.get(pk=1).articles_count_ru == 0
    assert TagArticle.objects.get(pk=1).articles_count_en == 1


def test_recalc_tag_delete_tag_article(articles_tags, article):
    article.tags.add(TagArticle.objects.get(pk=1))

    article.tags.clear()
    assert TagArticle.objects.get(pk=1).articles_count_ru == 0
    assert TagArticle.objects.get(pk=1).articles_count_en == 0


def _test_recalc_tag_delete_article(articles_tags, article):
    assert TagArticle.objects.get(pk=1).articles_count_ru == 0
    assert TagArticle.objects.get(pk=1).articles_count_en == 0

    article.tags.add(TagArticle.objects.get(pk=1))
    article.delete()

    assert TagArticle.objects.get(pk=1).articles_count_ru == 0
    assert TagArticle.objects.get(pk=1).articles_count_en == 0
