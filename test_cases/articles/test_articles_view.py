# -*- coding: utf-8 -*-
from datetime import timedelta
from pprint import pprint

import articles
import pytest
from articles import views
from articles.models import Article, TagArticle
from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.urls import reverse
from django.utils import translation
from django.utils.timezone import now
from post_office.models import Email

from manage import rel

langs = list(code for code, _ in settings.LANGUAGES if code not in settings.PASS_LANGUAGES)


@pytest.fixture
def article(mixer, user_user, articles_tags):
    file = rel('src/test_cases/test.jpg')
    data = {
        'image': file,
        'title': 'title',
        'level': 1,
        'announce': 'announce',
        'content': 'content',
        'user': user_user,
        'is_active': True,
        'pub_date': now() - relativedelta(months=1),
        'slug': 'is_slug',
        'lang': settings.LANGUAGE_CODE,
    }
    art = mixer.blend('articles.Article', **data)
    return art


@pytest.fixture
def article_not_image(mixer, user_user, articles_tags):
    art = mixer.blend('articles.Article', user=user_user)
    return art


def test_article_create_anonim(client):
    response = client.get(reverse('article_add'))
    assert 302 == response.status_code


@pytest.mark.parametrize('lang', langs)
def test_article_create_user(client_login, mixer, user_user, articles_tags, mailoutbox, lang):
    with translation.override(lang):
        url = reverse('article_add')
        response = client_login.get(url)
        assert 200 == response.status_code
        file = rel('src/test_cases/test.jpg')
        tag = mixer.blend(TagArticle)
        with open(file=file, mode='rb') as file:
            data = {
                'image': file,
                'title': 'title',
                'level': 1,
                'announce': 'announce',
                'content': 'content',
                'tags': tag.pk
            }

            response = client_login.post(url, data=data)
            assert response.resolver_match.func.__name__ == views.ArticleCreateView.as_view().__name__

            print(response.content.decode("utf-8"))
            assert response.status_code == 302
            assert Article.objects.all().count() == 1
            assert response.url == Article.objects.all()[0].get_absolute_url()
            assert Article.objects.all()[0].user_id == user_user.id


def test_article_send_mail(client_login, mixer, user_user, articles_tags, mailoutbox):
    url = reverse('article_add')
    file = rel('src/test_cases/test.jpg')
    assert Email.objects.all().count() == 0
    with open(file=file, mode='rb') as file:
        data = {
            'image': file,
            'title': 'title',
            'level': 1,
            'announce': 'announce',
            'content': 'content',
            'tags': TagArticle.objects.last().pk
        }

        response = client_login.post(url, data=data)

        assert Article.objects.all().count() == 1
        assert Email.objects.all().count() == 1


def test_article_view_anonim(article, client):
    url = article.get_absolute_url()
    # Аноним
    assert 1 == Article.objects.published(lang=settings.LANGUAGE_CODE).count()
    response = client.get(url)
    assert 200 == response.status_code


def test_article_view_author(article, client_login):
    url = article.get_absolute_url()
    # Создатель
    assert 1 == Article.objects.published(lang=settings.LANGUAGE_CODE).count()
    response = client_login.get(url)
    # print(response.content.decode("utf-8"))
    assert 200 == response.status_code


def test_article_view_user(article, client_login_2):
    url = article.get_absolute_url()
    # Пользователь
    assert 1 == Article.objects.published(lang=settings.LANGUAGE_CODE).count()
    response = client_login_2.get(url)
    assert 200 == response.status_code


def test_article_edit_user(article, client_login, mixer, file_jpg, articles_tags):
    response = client_login.get(article.get_edit_url())
    assert 200 == response.status_code
    # file = rel('src/test_cases/test.jpg')
    with open(file=file_jpg, mode='rb') as file:
        data = {
            'image': file,
            'title': 'title',
            'level': 1,
            'announce': 'announce',
            'content': 'content',
            'tags': TagArticle.objects.last().pk
        }

        response = client_login.post(reverse('article_id_edit', kwargs={'pk': article.pk}), data=data)
        # print(response.content.decode("utf-8"))
        assert response.status_code == 302
        article = Article.objects.get(pk=article.pk)
        assert article.title == 'title'
        assert article.slug == 'is_slug'


def test_article_edit_not_by_owner(article, client_login_2):
    response = client_login_2.get(article.get_edit_url())
    assert 404 == response.status_code


def test_delete_article_anonim(article, client):
    response = client.get(article.get_delete_url())
    assert 302 == response.status_code


def test_delete_article_user_get_request(article, client_login):
    url = article.get_delete_url()
    response = client_login.get(url)
    assert response.status_code == 200


def test_delete_article_user_post_request(article_not_image, client_login):
    assert Article.objects.filter(pk=article_not_image.pk).exists() is True
    url = article_not_image.get_delete_url()
    response = client_login.post(url, follow=True)
    # print(response.content.decode('utf-8'))
    # print(response.url)
    # print(response.context['form'].errors)
    assert Article.objects.filter(pk=article_not_image.pk).exists() is False
    # assert response.status_code == 302

@pytest.mark.xfail
def test_delete_article_user_post_request_more(article_not_image, client_login, mixer, user_user):
    mixer.cycle(4).blend('articles.Article', user=user_user, is_active=True, pub_date=now() - relativedelta(months=1))
    assert Article.objects.filter(pk=article_not_image.pk).exists() is True
    url = article_not_image.get_delete_url()
    response = client_login.post(url, follow=True)
    print(response.request)
    print(response.context['form'].errors)
    assert Article.objects.filter(pk=article_not_image.pk).exists() is False
    # assert response.status_code == 302


def test_delete_article_is_not_the_owner_get(article, client_login_2):
    url = article.get_delete_url()
    response = client_login_2.get(url)
    assert response.status_code == 403


def test_delete_article_is_not_the_owner_post(article, client_login_2):
    url = article.get_delete_url()
    response = client_login_2.post(url, {})
    assert response.status_code == 403


@pytest.mark.parametrize('lang', langs)
def test_article_create_user_spam(client_login_spam, user_spam, articles_tags, lang):
    with translation.override(lang):
        url = reverse('article_add')
        response = client_login_spam.get(url)
        assert 200 == response.status_code
        file = rel('src/test_cases/test.jpg')
        with open(file=file, mode='rb') as file:
            data = {
                'image': file,
                'title': 'title',
                'level': 1,
                'announce': 'announce',
                'content': 'content',
                'tags': TagArticle.objects.last().pk
            }

            response = client_login_spam.post(url, data=data)
            assert response.resolver_match.func.__name__ == views.ArticleCreateView.as_view().__name__

            # print(response.content.decode("utf-8"))
            assert response.status_code == 302
            assert Article.objects.all().count() == 1
            article = Article.objects.all()[0]
            assert response.url == article.get_absolute_url()
            assert article.user_id == user_spam.id
            assert article.is_active is False

            assert Email.objects.all().count() == 1


def test_article_edit_user_no_active(article, client_login, mixer, file_jpg, articles_tags):
    article.is_active = False
    article.save()
    response = client_login.get(article.get_edit_url())
    assert 200 == response.status_code
    # file = rel('src/test_cases/test.jpg')
    with open(file=file_jpg, mode='rb') as file:
        data = {
            'image': file,
            'title': 'title',
            'level': 1,
            'announce': 'announce',
            'content': 'content',
            'tags': TagArticle.objects.last().pk
        }

        response = client_login.post(reverse('article_id_edit', kwargs={'pk': article.pk}), data=data)
        # print(response.content.decode("utf-8"))
        assert response.status_code == 302
        article = Article.objects.get(pk=article.pk)
        assert article.title == 'title'
        assert article.slug == 'is_slug'
        assert article.is_active is True


@pytest.mark.parametrize('lang', langs)
def test_article_get_absolute_url_client_login(article, client_login, lang):
    article.is_active = False
    article.save()
    with translation.override(lang):
        url = reverse('article_detail', kwargs={'slug': article.slug})
        response = client_login.get(url)
        assert 200 == response.status_code


@pytest.mark.parametrize('lang', langs)
def test_article_get_absolute_url_user_staff(article, client_login_moder, lang):
    article.is_active = False
    article.save()
    with translation.override(lang):
        url = reverse('article_detail', kwargs={'slug': article.slug})
        response = client_login_moder.get(url)
        assert 200 == response.status_code


@pytest.mark.parametrize('lang', langs)
def test_article_get_absolute_url_user_user_2(article, client_login_2, lang):
    article.is_active = False
    article.save()
    with translation.override(lang):
        url = reverse('article_detail', kwargs={'slug': article.slug})
        response = client_login_2.get(url)
        assert 404 == response.status_code


@pytest.mark.parametrize('lang', langs)
def test_article_get_absolute_url_anonim(article, client, lang):
    article.is_active = False
    article.save()
    with translation.override(lang):
        url = reverse('article_detail', kwargs={'slug': article.slug})
        response = client.get(url)
        assert 404 == response.status_code


@pytest.mark.parametrize('lang', langs)
def test_article_get_absolute_url_user_user_2_is_active(article, client_login_2, lang):
    with translation.override(lang):
        url = reverse('article_detail', kwargs={'slug': article.slug})
        response = client_login_2.get(url)
        if article.lang == lang:
            assert 200 == response.status_code
        else:
            assert 404 == response.status_code


@pytest.mark.parametrize('lang', langs)
def test_article_get_absolute_url_anonim_is_active(article, client, lang):
    with translation.override(lang):
        url = reverse('article_detail', kwargs={'slug': article.slug})
        response = client.get(url)
        if article.lang == lang:
            assert 200 == response.status_code
        else:
            assert 404 == response.status_code


@pytest.fixture()
def article_future_pub_date(article):
    art = Article.objects.get(pk=article.pk)
    art.pub_date = now() + timedelta(days=1)
    art.save()
    return art


@pytest.mark.parametrize('lang', langs)
def test_article_future_pub_date_client_login(article_future_pub_date, client_login, lang):
    article = article_future_pub_date
    with translation.override(lang):
        url = reverse('article_detail', kwargs={'slug': article.slug})
        response = client_login.get(url)
        assert 200 == response.status_code


@pytest.mark.parametrize('lang', langs)
def test_article_future_pub_date_user_staff(article_future_pub_date, client_login_moder, lang):
    article = article_future_pub_date
    with translation.override(lang):
        url = reverse('article_detail', kwargs={'slug': article.slug})
        response = client_login_moder.get(url)
        assert 200 == response.status_code


@pytest.mark.parametrize('lang', langs)
def test_article_future_pub_date_user_user_2(article_future_pub_date, client_login_2, lang):
    article = article_future_pub_date
    with translation.override(lang):
        url = reverse('article_detail', kwargs={'slug': article.slug})
        response = client_login_2.get(url)
        assert 404 == response.status_code


@pytest.mark.parametrize('lang', langs)
def test_article_future_pub_date_anonim(article_future_pub_date, client, lang):
    article = article_future_pub_date
    with translation.override(lang):
        url = reverse('article_detail', kwargs={'slug': article.slug})
        response = client.get(url)
        assert 404 == response.status_code
