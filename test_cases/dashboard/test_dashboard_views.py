# -*- coding: utf-8 -*-
import pytest
from django.urls import reverse


def test_user_supervisor_login_id_client_login(client_login, user_spam):
    response = client_login.get(reverse('dashboard_supervisor_id', args=[user_spam.pk]))
    assert 403 == response.status_code


def test_user_supervisor_login_id_client_login_spam(client_login_spam, user_spam):
    response = client_login_spam.get(reverse('dashboard_supervisor_id', args=[user_spam.pk]))
    assert 403 == response.status_code


def test_user_supervisor_login_id_client_login_moder(client_login_moder, user_spam):
    response = client_login_moder.get(reverse('dashboard_supervisor_id', args=[user_spam.pk]))
    assert 403 == response.status_code


def test_user_supervisor_login_id_client_login_superuser(client_login_superuser, user_spam):
    response = client_login_superuser.get(reverse('dashboard_supervisor_id', args=[user_spam.pk]))
    assert 302 == response.status_code


def test_purchaseuser_client_login_superuser(client_login_superuser):
    response = client_login_superuser.get(reverse('dashboard_purchaseuser'))
    assert 200 == response.status_code
