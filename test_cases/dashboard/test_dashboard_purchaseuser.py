# -*- coding: utf-8 -*-
from decimal import Decimal

import pytest
from django.urls import reverse

from base.utils import get_system_user
from cart.models import PurchaseUser
from manage import rel
from offer.models import Currency
from oscar_accounts.codes import get_no_credit_limit_account
from shop.models import Composer, ShopOffers, UserFile


@pytest.fixture()
def item_user_1(user_user, user_staff, currencies_rate):
    composer, cre = Composer.objects.update_or_create(
        slug='testcomposer',
        defaults={
            'name': 'Test Composer',
            'accepted': True,
        }
    )
    kwargs = {
        'name': 'Mane',
        'description': 'Distrip oil',
        'price': 100,
        'currency': Currency.objects.get(pk=1),
        'composer': composer,
        'difficulty': 1,
        'user_created': user_user,
        'checked': True,
        'video_url': 'https://www.youtube.com/watch?v=EadZchN5_F8',
    }
    item = ShopOffers.objects.create(**kwargs)
    filename = rel('src/test_cases/test.jpg')
    with open(filename, mode='rb') as f:
        uf = UserFile.objects.create(item=item, user_created=user_user)
        uf.file.save(filename, f)

    item.moderator = user_staff
    item.checked = True
    item.save()
    return item


@pytest.fixture()
def item_user_2(mixer, user_staff, currencies_rate):
    composer, cre = Composer.objects.update_or_create(
        slug='testcomposer2',
        defaults={
            'name': 'Test Composer 2',
            'accepted': True,
        }
    )
    kwargs = {
        'name': 'Mane2',
        'description': 'Distrip oil',
        'price': 200,
        'currency': Currency.objects.get(pk=1),
        'composer': composer,
        'difficulty': 1,
        'user_created': user_staff,
        'checked': True,
        'video_url': 'https://www.youtube.com/watch?v=EadZchN5_F8',
    }
    item = ShopOffers.objects.create(**kwargs)
    filename = rel('src/test_cases/test.jpg')
    with open(filename, mode='rb') as f:
        uf = UserFile.objects.create(item=item, user_created=user_staff)
        uf.file.save(filename, f)

    item.moderator = user_staff
    item.checked = True
    item.save()
    return item


@pytest.fixture()
def valid_purchaseuser_data(user_user_2, item_user_1, item_user_2):
    data = {
        'email': user_user_2.email,
        'currency': Currency.objects.get(pk=1).pk,
        'form-TOTAL_FORMS': 2,
        'form-INITIAL_FORMS': 0,
        'form-0-offer': item_user_1.pk,
        'form-0-amount': Decimal(100),
        'form-0-no_site_bonus': False,
        'form-1-offer': item_user_2.pk,
        'form-1-amount': Decimal(200),
        'form-1-no_site_bonus': True,
        'comment': 'comment',
    }
    return data


def test_add_purchaseuser_client_login_superuser(client_login_superuser, valid_purchaseuser_data, user_user, user_staff,
                                                 user_user_2, system_user):
    url = reverse('dashboard_purchaseuser_add')
    data = valid_purchaseuser_data
    client = client_login_superuser
    response = client.post(url, data=data)
    # print(response.content.decode('utf-8'))
    assert 302 == response.status_code
    assert 2 == PurchaseUser.objects.all().count()

    # Первый автор получил 70%, второй всю сумму.
    currency = Currency.objects.get(pk=valid_purchaseuser_data['currency'])
    assert get_no_credit_limit_account(currency=currency).balance == Decimal('-300.00')
    assert get_system_user().get_account(currency=currency).balance == Decimal('30.00')
    assert user_user.get_account(currency=currency).balance == Decimal('70.00')
    assert user_staff.get_account(currency=currency).balance == Decimal('200.00')

    assert 2 == PurchaseUser.objects.filter(user=user_user_2).count()
