# -*- coding: utf-8 -*-
from copy import deepcopy

import pytest
from django.conf import settings
from django.urls import reverse
from django.utils import translation

from guitar import views
from guitar.models import GuitarOffer, GuitarUrl, GuitarOfferImages
from manage import rel

langs = list(code for code, _ in settings.LANGUAGES if code not in settings.PASS_LANGUAGES)


@pytest.fixture
def image_file():
    import io

    image = rel('src/test_cases/test.jpg')

    with open(image, "rb", buffering=0) as fp:
        return io.BytesIO(fp.read())


@pytest.fixture
def valid_data(currencies, image_file, mixer):
    image_file.seek(0)
    image_file_1 = deepcopy(image_file)
    image_file_2 = deepcopy(image_file)

    data = {
        'category': mixer.blend('guitar.GuitarCategory').pk,
        'size': 1,
        'fret_markers': 1,
        'name': 'Name',
        'subtitle': 'Subtitle',
        'description': 'Description',
        'landing_content': '<p>landing content</p>',
        'tags': 'tag1,tag2',
        'price': 100,
        'currency': 1,

        'image-TOTAL_FORMS': 1,
        'image-INITIAL_FORMS': 0,
        'image-0-image': image_file_1,

        'url-TOTAL_FORMS': 1,
        'url-INITIAL_FORMS': 0,
        'url-0-url': 'https://test.ru',
        'url-0-description': 'url description',

    }
    return data


def test_guitar_main_page(client):
    url = reverse('guitaroffer_main')
    response = client.get(url)
    assert response.status_code == 200
    assert response.resolver_match.func.__name__ == views.GuitarMainPageView.as_view().__name__


def test_guitar_detail_view_moderator_checked(client, mixer, user_staff):
    go = mixer.blend(GuitarOffer, name='Test', moderator=user_staff, checked=True)
    url = reverse('guitaroffer_id_detail', kwargs={'pk': go.pk})
    response = client.get(url)
    assert response.status_code == 200
    assert response.resolver_match.func.__name__ == views.GuitarDetailView.as_view().__name__


def test_guitar_detail_view_not_moderator(client, mixer):
    go = mixer.blend(GuitarOffer, name='Test', checked=True)
    url = reverse('guitaroffer_id_detail', kwargs={'pk': go.pk})
    response = client.get(url)
    assert response.status_code == 404


def test_guitar_detail_view_not_checked(client, mixer, user_staff):
    go = mixer.blend(GuitarOffer, name='Test', checked=False, moderator=user_staff, )
    url = reverse('guitaroffer_id_detail', kwargs={'pk': go.pk})
    response = client.get(url)
    assert response.status_code == 404


def test_get_add_guitar_anonim(client):
    url = reverse('guitaroffer_add')
    response = client.get(url)
    assert response.status_code == 302
    assert response.resolver_match.func.__name__ == views.GuitarCreateView.as_view().__name__


def test_get_add_guitar_user(client_login):
    url = reverse('guitaroffer_add')
    response = client_login.get(url)
    assert response.status_code == 200
    assert response.resolver_match.func.__name__ == views.GuitarCreateView.as_view().__name__


@pytest.mark.parametrize('lang',
                         list(getattr(settings, 'MODELTRANSLATION_LANGUAGES', (l[0] for l in settings.LANGUAGES))))
def test_post_add_guitar_user_valid_data(client_login, valid_data, lang):
    with translation.override(lang):
        url = reverse('guitaroffer_add')
        response = client_login.post(url, valid_data)
        print(response.content.decode())
        # print(response.context['formset_image'].errors)
        # print(response.context['formset_url'].errors)
        assert response.status_code == 302
        assert 1 == GuitarOffer.objects.all().count()
        assert 1 == GuitarUrl.objects.all().count()
        assert 1 == GuitarOfferImages.objects.all().count()

        guitar = GuitarOffer.objects.all()[0]
        guitar_url = GuitarUrl.objects.all()[0]
        guitar_image = GuitarOfferImages.objects.all()[0]

        assert guitar == guitar_url.guitar
        assert guitar == guitar_image.guitar
        assert guitar.is_delete is False
        assert guitar.is_available is True
        assert guitar.moderator is None

        # Не отмодерированные гитары не показываются.
        assert GuitarOffer.objects.published().exists() is False


@pytest.mark.parametrize('lang', langs)
def test_update_guitar_user_valid_data(client_login, valid_data, mixer, lang):
    with translation.override(lang):
        url = reverse('guitaroffer_add')
        response = client_login.post(url, valid_data)

        guitar = GuitarOffer.objects.all()[0]
        url = reverse('guitaroffer_id_edit', kwargs={'pk': guitar.pk})
        response = client_login.get(url)
        assert response.status_code == 200

        image_file = GuitarOfferImages.objects.filter(guitar=guitar)[0]
        guitar_url = GuitarUrl.objects.all()[0]
        valid_data.update(
            {'name': 'New name',
             'image-TOTAL_FORMS': 1,
             'image-INITIAL_FORMS': 1,
             'image-0-image': '',
             'image-0-id': image_file.id,

             'url-TOTAL_FORMS': 1,
             'url-INITIAL_FORMS': 1,
             'url-0-id': guitar_url.pk,
             'url-0-url': 'http://new-url.ru/',
             'url-0-description': 'test url description',
             }
        )

        response = client_login.post(url, valid_data)
        assert response.status_code == 302
        assert response.resolver_match.func.__name__ == views.GuitarUpdateView.as_view().__name__

        assert 1 == GuitarOffer.objects.all().count()
        assert 1 == GuitarUrl.objects.all().count()
        assert 1 == GuitarOfferImages.objects.all().count()

        guitar = GuitarOffer.objects.all()[0]
        guitar_url = GuitarUrl.objects.all()[0]
        assert 'New name' == guitar.name
        assert 'http://new-url.ru/' == guitar_url.url

        assert GuitarOffer.objects.published().exists() is False


@pytest.mark.parametrize('lang',
                         list(getattr(settings, 'MODELTRANSLATION_LANGUAGES', (l[0] for l in settings.LANGUAGES))))
def test_post_add_guitar_user_spam_valid_data(client_login_spam, valid_data, lang):
    with translation.override(lang):
        url = reverse('guitaroffer_add')
        response = client_login_spam.post(url, valid_data)
        # print(response.content.decode())
        # print(response.context['formset_image'].errors)
        # print(response.context['formset_url'].errors)
        assert response.status_code == 302
        assert 1 == GuitarOffer.objects.all().count()
        assert 1 == GuitarUrl.objects.all().count()
        assert 1 == GuitarOfferImages.objects.all().count()

        guitar = GuitarOffer.objects.all()[0]
        guitar_url = GuitarUrl.objects.all()[0]
        guitar_image = GuitarOfferImages.objects.all()[0]

        assert guitar == guitar_url.guitar
        assert guitar == guitar_image.guitar

        assert guitar.is_available is False
        assert GuitarOffer.objects.published().exists() is False
