# -*- coding: utf-8 -*-
import pytest

from offer.models import Currency


@pytest.fixture()
def create_guitars():
    pass

@pytest.fixture
def guitar_item(mixer, country_rate):
    # image_file.seek(0)
    item = mixer.blend('guitar.GuitarOffer', price=4800, currency=Currency.objects.get(iso_code='RUB'))
    return item

def test_guitars_filter(create_guitars, client):
    url = '/ru/guitars-database/'
    response = client.get(url)
    assert response.status_code == 200