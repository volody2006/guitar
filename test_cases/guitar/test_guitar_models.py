# -*- coding: utf-8 -*-
import pytest
from decimal import Decimal
from django.utils.translation import ugettext as _
import logging

from guitar.models import GuitarOfferPrice
from offer.models import Currency

logger = logging.getLogger(__name__)


@pytest.fixture
def guitar_item(mixer, country_rate):
    item = mixer.blend('guitar.GuitarOffer', price=4800, currency=Currency.objects.get(iso_code='RUB'))
    return item


def test_price_save(guitar_item):
    GuitarOfferPrice.objects.filter(item=guitar_item).delete()
    assert GuitarOfferPrice.objects.filter(item=guitar_item).exists() is False
    guitar_item.price_save()

    assert GuitarOfferPrice.objects.filter(item=guitar_item).count() == 3


@pytest.mark.parametrize("iso_code, price", [
    ("EUR", Decimal('60.00')),
    ("USD", Decimal('80.00')),
    ("RUB", Decimal('4800.00')),
])
def test_price_currency(guitar_item, iso_code, price):
    guitar_item.price_save()

    assert guitar_item.get_price_currency(iso_code)[0] == price
    assert guitar_item.get_price_currency(iso_code)[1] == Currency.objects.get(iso_code=iso_code)


@pytest.mark.parametrize("iso_code, price", [
    ("EUR", Decimal('60.00')),
    ("USD", Decimal('80.00')),
    ("RUB", Decimal('4800.00')),
])
def test_price_currency_not_price_save(guitar_item, iso_code, price):
    assert guitar_item.get_price_currency(iso_code)[0] == price
    assert guitar_item.get_price_currency(iso_code)[1] == Currency.objects.get(iso_code=iso_code)


@pytest.mark.parametrize("iso_code, price, code_country", [
    ("EUR", Decimal('60.00'), 'DE'),
    ("USD", Decimal('80.00'), 'US'),
    ("USD", Decimal('80.00'), 'AZ'),
    ("RUB", Decimal('4800.00'), 'RU'),
])
def test_get_price_country(guitar_item, iso_code, price, code_country):
    assert guitar_item.get_price_country(code_country)[0] == price
    assert guitar_item.get_price_country(code_country)[1] == Currency.objects.get(iso_code=iso_code)
