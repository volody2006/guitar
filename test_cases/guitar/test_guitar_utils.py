# -*- coding: utf-8 -*-
import pytest
from django.utils.translation import ugettext as _
import logging

from guitar.models import GuitarOfferPrice
from guitar.utils import guitar_price_save
from offer.models import Currency

logger = logging.getLogger(__name__)


@pytest.fixture
def guitar_item(mixer, country_rate):
    # image_file.seek(0)
    item = mixer.blend('guitar.GuitarOffer', price=4800, currency=Currency.objects.get(iso_code='RUB'))
    return item


def test_guitar_price_save_obj(guitar_item):
    GuitarOfferPrice.objects.filter(item=guitar_item).delete()

    assert GuitarOfferPrice.objects.filter(item=guitar_item).exists() is False

    guitar_price_save(guitar_item)
    assert GuitarOfferPrice.objects.filter(item=guitar_item).count() == 3

    rub = Currency.objects.get(iso_code='RUB')
    assert float(GuitarOfferPrice.objects.get(item=guitar_item, currency=rub).price) == float(4800)

    euro = Currency.objects.get(iso_code='EUR')
    assert float(GuitarOfferPrice.objects.get(item=guitar_item, currency=euro).price) == float(60)

    usd = Currency.objects.get(iso_code='USD')
    assert float(GuitarOfferPrice.objects.get(item=guitar_item, currency=usd).price) == float(80)


def test_guitar_price_save_pk(guitar_item):
    GuitarOfferPrice.objects.filter(item=guitar_item).delete()
    guitar_item = guitar_item.pk
    assert GuitarOfferPrice.objects.filter(item=guitar_item).exists() is False

    guitar_price_save(guitar_item)
    assert GuitarOfferPrice.objects.filter(item=guitar_item).count() == 3

    rub = Currency.objects.get(iso_code='RUB')
    assert float(GuitarOfferPrice.objects.get(item=guitar_item, currency=rub).price) == float(4800)

    euro = Currency.objects.get(iso_code='EUR')
    assert float(GuitarOfferPrice.objects.get(item=guitar_item, currency=euro).price) == float(60)

    usd = Currency.objects.get(iso_code='USD')
    assert float(GuitarOfferPrice.objects.get(item=guitar_item, currency=usd).price) == float(80)


def test_guitar_price_save_euro(guitar_item):
    euro = Currency.objects.get(iso_code='EUR')

    guitar_item.price = 60
    guitar_item.currency = euro
    guitar_item.save()

    GuitarOfferPrice.objects.filter(item=guitar_item).delete()
    assert GuitarOfferPrice.objects.filter(item=guitar_item).exists() is False

    guitar_price_save(guitar_item)
    assert GuitarOfferPrice.objects.filter(item=guitar_item).count() == 3

    rub = Currency.objects.get(iso_code='RUB')
    assert float(GuitarOfferPrice.objects.get(item=guitar_item, currency=rub).price) == float(4800)

    assert float(GuitarOfferPrice.objects.get(item=guitar_item, currency=euro).price) == float(60)

    usd = Currency.objects.get(iso_code='USD')
    assert float(GuitarOfferPrice.objects.get(item=guitar_item, currency=usd).price) == float(80)
