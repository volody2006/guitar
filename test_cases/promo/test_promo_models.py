# -*- coding: utf-8 -*-
import logging
from datetime import timedelta

import pytest
from django.urls import reverse
from django.utils.timezone import now
from mixer.backend.django import mixer
from promo.models import Promo

logger = logging.getLogger(__name__)


@pytest.fixture()
def pub_promo(mixer):
    return mixer.blend(Promo, status=Promo.STATUS.run,
                       is_active=True,
                       date_pub=now() - timedelta(days=1),
                       date_start=now() + timedelta(days=1),
                       date_end=now() + timedelta(days=2),
                       )


@pytest.fixture()
def run_promo(mixer):
    return mixer.blend(Promo, status=Promo.STATUS.run,
                       is_active=True,
                       date_pub=now() - timedelta(days=1),
                       date_start=now() - timedelta(days=1),
                       date_end=now() + timedelta(days=1),
                       )


@pytest.fixture()
def waiting_promo(mixer):
    return mixer.blend(Promo, status=Promo.STATUS.waiting,
                       is_active=True,
                       date_start=now() + timedelta(days=1),
                       date_end=now() + timedelta(days=3),
                       )


@pytest.fixture()
def draft_promo(mixer):
    return mixer.blend(Promo, status=Promo.STATUS.draft,
                       date_start=now() + timedelta(days=10),
                       date_end=now() + timedelta(days=20),
                       )


@pytest.fixture()
def finished_promo(mixer):
    return mixer.blend(Promo, status=Promo.STATUS.finished,
                       date_start=now() - timedelta(days=10),
                       date_end=now() - timedelta(days=9),
                       )


class TestModelPromo:

    def get_promo(self, status):
        return mixer.blend(Promo, status=getattr(Promo.STATUS, status), is_active=True)

    def test_up_waiting_promo_date_start_future(self, waiting_promo):
        assert waiting_promo.up() is False

    def test_up_waiting_promo_date_start_now(self, waiting_promo):
        promo = waiting_promo
        promo.date_start = now()
        promo.save()
        assert promo.up() is True
        assert promo.status == promo.STATUS.run

    def test_other_promos_status_up(self):
        for status in 'draft run finished expired adorted'.split(' '):
            promo = self.get_promo(status)
            # print('test promo up. Status {}'.format(status))
            assert promo.up() is False

    def test_up_waiting_promo_date_start_now_not_date_end(self, waiting_promo):
        promo = waiting_promo
        promo.date_start = now()
        promo.date_end = None
        promo.save()
        assert promo.up() is True
        assert promo.status == promo.STATUS.run

    def test_up_waiting_promo_date_start_in_past_not_date_in_past(self, waiting_promo):
        promo = waiting_promo
        promo.date_start = now() - timedelta(days=10)
        promo.date_end = now() - timedelta(days=9)
        promo.save()
        assert promo.up() is False
        assert promo.status == promo.STATUS.expired

    def test_other_promo_down(self):
        for status in 'draft waiting finished expired adorted'.split(' '):
            promo = self.get_promo(status)
            print('test promo down. Status {}'.format(status))
            assert promo.down() is False

    def test_run_promo_down_not_end_date(self):
        promo = self.get_promo('run')
        assert promo.down() is True
        assert promo.actual_end_date is not None
        assert promo.status == promo.STATUS.finished

    def test_run_promo_down_past_end_date(self):
        promo = self.get_promo('run')
        promo.date_end = now() - timedelta(days=1)
        promo.save()
        assert promo.down() is True
        assert promo.status == promo.STATUS.finished

    def test_run_promo_down_future_end_date(self):
        promo = self.get_promo('run')
        promo.date_end = now() + timedelta(days=1)
        promo.save()
        assert promo.down() is True
        assert promo.status == promo.STATUS.adorted


class TestManagerPromo:

    # def get_promo(self, status):
    #     return mixer.blend(Promo, status=getattr(Promo.STATUS, status))

    def test_get_more_active_promo(self):
        mixer.cycle(5).blend(Promo, is_active=True)
        assert 5 == Promo.objects.active().count()

    def test_not_active_promo(self):
        mixer.cycle(5).blend(Promo, is_active=False)
        assert 0 == Promo.objects.active().count()

    @pytest.fixture()
    def fixture_promo(self):
        # Прошедшие распродажа
        promos = mixer.cycle(5).blend(Promo, is_active=False, status=Promo.STATUS.finished)
        for promo in promos:
            promo.date_start = now() - timedelta(days=(promo.pk * 2 * 365))
            promo.date_end = now() - timedelta(days=(promo.pk * 1 * 365))
            promo.save()

        # Распродажи в ожидание, даты все в будущем
        promos = mixer.cycle(4).blend(Promo, is_active=True, status=Promo.STATUS.waiting)
        for promo in promos:
            promo.date_start = now() + timedelta(days=(promo.pk * 2 * 2))
            promo.date_end = now() + timedelta(days=(promo.pk * 3 * 2))
            promo.save()

        # Распродажи опубликованы, между публикацией и стартом
        promos = mixer.cycle(3).blend(Promo, is_active=True, status=Promo.STATUS.waiting)
        for promo in promos:
            promo.date_start = now() + timedelta(days=(promo.pk * 2 * 2))
            promo.date_end = now() + timedelta(days=(promo.pk * 3 * 2))
            promo.save()

        # Распродажи активны, между стартом и концом
        promos = mixer.cycle(2).blend(Promo, is_active=True, status=Promo.STATUS.run)
        for promo in promos:
            promo.date_start = now() - timedelta(days=(promo.pk * 2 * 2))
            promo.date_end = now() + timedelta(days=(promo.pk * 1 * 2))
            promo.save()

        # Распродажи в черновиках
        mixer.blend(Promo, is_active=False, status=Promo.STATUS.draft)
        return True

    def test_more_running_promo(self, fixture_promo):
        promos = Promo.objects.running()
        for promo in promos:
            assert promo.is_active is True
            assert Promo.STATUS.run == promo.status
        assert 2 == promos.count()

    def test_is_promo_false(self):
        assert Promo.objects.is_promo() is False

    def test_is_promo_true(self, fixture_promo):
        assert Promo.objects.is_promo() is True
