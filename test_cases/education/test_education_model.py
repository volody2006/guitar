# -*- coding: utf-8 -*-
from _decimal import Decimal

import pytest

from education.models import EducationProduct, EducationUserFile, EducationPurchaseUser
from lang.models import Language
from manage import rel
from offer.models import Currency
from mixer.backend.django import mixer


@pytest.fixture()
def education_user(user_user, user_staff):
    item = mixer.blend(EducationProduct, user_created=user_user, price = Decimal('100.00'),
                       is_available=True, currency=Currency.objects.get(pk=1),
                       checked=True, moderator=user_staff, is_draft=False,
                       lang=Language.objects.get(code='ru'))
    filename = rel('src/test_cases/test.jpg')
    with open(filename, mode='rb') as f:
        uf = EducationUserFile.objects.create(offer=item)
        uf.file.save(filename, f)
    return item

@pytest.fixture()
def education_user_free(user_user, user_staff):
    item = mixer.blend(EducationProduct, user_created=user_user, price = Decimal('0.00'),
                       is_available=True, currency=Currency.objects.get(pk=1),
                       checked=True, moderator=user_staff, is_draft=False,
                       lang=Language.objects.get(code='ru'))
    filename = rel('src/test_cases/test.jpg')
    with open(filename, mode='rb') as f:
        uf = EducationUserFile.objects.create(offer=item)
        uf.file.save(filename, f)
    return item



def test_education_product_valid(education_user):
    assert education_user.is_valid() is True

# def test_education_product_purchase(education_user):
#     assert education_user.is_valid() is True

def test_education_product_purchase_free(education_user_free, user_user_2):
    purchase =  education_user_free.purchase(user=user_user_2)
    assert isinstance(purchase, EducationPurchaseUser)
