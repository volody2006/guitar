# -*- coding: utf-8 -*-
from decimal import Decimal

import pytest
from django.core.files.uploadedfile import SimpleUploadedFile
from mixer.backend.django import mixer

from education.forms import EducationProductLandingForm
from education.models import EducationProduct, TagEducationProduct
from lang.models import Language
from offer.models import Currency
from project import rel


# from io import BytesIO


@pytest.fixture
def image_file():
    import io

    image = rel('src/test_cases/test.jpg')

    with open(image, "rb", buffering=0) as fp:
        return io.BytesIO(fp.read())


@pytest.fixture()
def educationproductlandingform_data():
    data = {
        'type': EducationProduct.BOOK,
        'price': Decimal('100.00'),
        'name': 'name',
        'subtitle': 'subtitle',
        'description': 'description',
        'landing_content': 'landing_content',
        'tags': ['tag1', 'tag2'],
    }
    return data


@pytest.fixture()
def tag_education_product():
    return mixer.blend(TagEducationProduct)


def test_educationproductlandingform_form_valid_user_tags(currencies, user_user, educationproductlandingform_data,
                                                          image_file):
    data = educationproductlandingform_data

    form = EducationProductLandingForm(lang=Language.objects.get(code='ru'),
                                       user=user_user,
                                       currency=Currency.objects.get(iso_code=Currency.RUB),
                                       data=data,
                                       files={'image': SimpleUploadedFile('test.jpg', image_file.read())})
    # print(form.errors)
    assert form.is_valid() is True

    educationproduct = form.save()
    assert educationproduct.user_created == user_user
    assert TagEducationProduct.objects.all().exists() is True
    assert TagEducationProduct.objects.all().count() == 2
    assert TagEducationProduct.objects.get(name='tag1').approve is False


def test_education_form_tageducationproduct(currencies, user_user, tag_education_product,
                                            educationproductlandingform_data, image_file):
    data = educationproductlandingform_data

    educationproductlandingform_data.update({'tags': [tag_education_product.name, ]})
    form = EducationProductLandingForm(lang=Language.objects.get(code='ru'),
                                       user=user_user,
                                       currency=Currency.objects.get(iso_code=Currency.RUB),
                                       data=data,
                                       files={'image': SimpleUploadedFile('test.jpg', image_file.read())})
    assert form.is_valid() is True

    educationproduct = form.save()
    assert educationproduct.user_created == user_user
    assert educationproduct.tags.all().count() == 1
