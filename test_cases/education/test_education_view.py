# -*- coding: utf-8 -*-
from copy import deepcopy

import pytest


@pytest.fixture()
def valid_shop_item_data(currencies, image_file, mixer):
    image_file.seek(0)
    image_file_1 = deepcopy(image_file)
    image_file_2 = deepcopy(image_file)

    data = {
        'image': image_file_1,
        'name': 'Mane',
        'description': 'Distrip oil',
        'price': 4800.00,
        'composer_name': 'composer test',
        'difficulty': 1,
        'video_url': 'https://www.youtube.com/watch?v=EadZchN5_F8',
        'form-TOTAL_FORMS': 2,
        'form-INITIAL_FORMS': 0,
        'form-0-file': image_file_2,
        'tonality': 1,
    }
    return data

def test_add_education_valid(user_user):
    pass