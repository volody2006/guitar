# -*- coding: utf-8 -*-
import logging

from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from cart.user_payment_manager import UserPaymentsManager
from shop.models import ShopOffers

logger = logging.getLogger(__name__)


def create_or_update_payment(user, country, offers=list(), **kwargs):
    p = UserPaymentsManager(user, country, offers=offers, **kwargs)
    p.create_or_update()
    return p.payment


def create_buy_all_author_offers_payment(user, seller, country):
    offers = ShopOffers.objects.published().by_author(seller).filter(price__gt=0)
    # p = UserPaymentsManager(user, country, offers=offers, country=country)
    return create_or_update_payment(user, country, list(offers), buy_all=True)
