# -*- coding: utf-8 -*-

import json
import logging
from datetime import timedelta
from decimal import Decimal as D, ROUND_HALF_UP, Decimal

import pytest
from django.db.models import Sum
from django.utils.timezone import now
from facebook_business.adobjects import campaign

from base.utils import get_system_user
from cart.exceptions import UnknownPaymentSystem
from cart.models import PurchaseUser, UserPayments
from cart.utils import add_to_cart
from coupons.models import Coupon
from coupons.settings import TYPE_PERCENTAGE
from coupons.utils import coupon_name_generator_birthday
from education.models import EducationPurchaseUser
from offer.models import Currency
from oscar_accounts import facade
from oscar_accounts.codes import get_no_credit_limit_account, get_sale_account, get_unpaid_account
from oscar_accounts.models import Account, AccountType
from pinax.messages.models import Message
from project import constants
from shop.models import ShopOffers
from test_cases.utils import create_buy_all_author_offers_payment, create_or_update_payment
from yandex_money.models import STATUS_SUCCESS

logger = logging.getLogger(__name__)


# Покупка товара, у продавца на счету 70%, у магазина на счету 30%


class TestPurchaseRU:
    def user_account_setup(self, user_user, system_user, amount=None, bonys=None):
        currency = Currency.objects.get_rub()
        self.currency = currency
        # Начислили реальные деньги
        if amount:
            facade.transfer(source=get_no_credit_limit_account(currency=currency),
                            destination=user_user.get_account(currency=currency),
                            amount=D(amount),
                            user=system_user, )

        code = '{0}_{1}_{2}'.format(user_user.pk, 'UNPAID', currency.iso_code).upper()
        account_type = AccountType.objects.get(code='{0}_{1}'.format('UNPAID', currency.iso_code))
        account, _ = Account.objects.update_or_create(account_type=account_type, primary_user=user_user, code=code)

        # Начислили бонусы
        if bonys:
            facade.transfer(source=get_unpaid_account(currency=currency),
                            destination=account,
                            amount=D(bonys),
                            user=system_user, )
        return account

    def test_purchase_of_goods_1_item(self, user_user, seller_1, default_accounts, items_5_seller_rub,
                                      country_ru, system_user):
        payment = create_or_update_payment(user_user, country_ru, offers=[items_5_seller_rub[0], ])
        payment.create_payment_system_order(constants.YANDEX)
        assert payment.yam_payment is not None

        payment.yam_payment.status = constants.STATUS_COMPLETED
        payment.yam_payment.shop_amount = 90
        payment.yam_payment.save()
        payment.yam_payment.send_signals()

        assert payment.price == payment.order_price
        assert get_no_credit_limit_account(currency=payment.currency).balance == D('-180.00')
        assert get_system_user().get_account(currency=payment.currency).balance == D('27.00')
        assert user_user.get_account(currency=payment.currency).balance == D('0.00')
        assert seller_1.get_account(currency=payment.currency).balance == D('63.00')
        assert get_sale_account(payment.currency).balance == D('90')

        assert 1 == PurchaseUser.objects.filter(user=user_user).count()

    def test_purchase_of_goods_5_item(self, fixture_purchase_of_goods_5_item, user_user, seller_1, system_user,
                                      mailoutbox):
        payment_test_5_item = fixture_purchase_of_goods_5_item
        payment_test_5_item.yam_payment.send_signals()

        ya_shop_amount = payment_test_5_item.yam_payment.shop_amount

        assert get_no_credit_limit_account(currency=payment_test_5_item.currency).balance == ya_shop_amount * D("-2")
        assert get_system_user().get_account(currency=payment_test_5_item.currency).balance == D('132.55')
        assert user_user.get_account(currency=payment_test_5_item.currency).balance == D('0.00')
        assert D('309.2') == seller_1.get_account(currency=payment_test_5_item.currency).balance
        assert get_sale_account(payment_test_5_item.currency).balance == ya_shop_amount

        # seller_1 получил 1 письмо!
        # for m in mailoutbox:
        #     # print(m.body, m)
        #     print(1, m.to)
        assert 1 == Message.objects.all().count()
        # assert 2 == len(mailoutbox)

        # assert seller_1.email in list(mailoutbox[0].to)
        # items_json = json.loads(payment_test_5_item.contained)
        # for item_json in items_json:
        #     # Покупку записали
        #     shop_offer = ShopOffers.objects.get(pk=int(item_json[0]['pk']))
        #     assert shop_offer.name in mailoutbox[0].body
        #     # assert shop_offer.name in mailoutbox[1].body
        #
        # assert seller_1.email in list(mailoutbox[0].to)
        # assert user_user.email in list(mailoutbox[1].to)

        # Мои покупки появились?
        assert 5 == PurchaseUser.objects.filter(user=user_user).count()

    def test_purchase_of_goods_5_item_refer(self, fixture_purchase_of_goods_5_item, user_user, seller_1,
                                            invate_user, system_user):
        payment = fixture_purchase_of_goods_5_item
        payment.yam_payment.send_signals()
        invate_user = invate_user
        ya_shop_amount = payment.yam_payment.shop_amount
        assert D('13.95') == invate_user.get_account(currency=payment.currency).balance
        assert D('118.6') == get_system_user().get_account(currency=payment.currency).balance
        assert get_no_credit_limit_account(currency=payment.currency).balance == ya_shop_amount * D("-2")
        assert get_sale_account(payment.currency).balance == D('441.75')

    def test_purchase_of_goods_5_item_all_discount(self, fixture_purchase_of_goods_5_item_discount_5_item, user_user,
                                                   seller_1, system_user):
        payment = fixture_purchase_of_goods_5_item_discount_5_item
        payment.yam_payment.send_signals()

        assert D(250) == payment.price
        assert D(71.25) == get_system_user().get_account(currency=payment.currency).balance
        assert D(0) == user_user.get_account(currency=payment.currency).balance
        assert D(166.25) == seller_1.get_account(currency=payment.currency).balance
        assert D(-237.5 * 2) == get_no_credit_limit_account(currency=payment.currency).balance
        assert get_sale_account(payment.currency).balance == D('237.5')

    def test_not_currency_in_offer(self, default_accounts, user_user, items_5_seller_rub,
                                   customer, seller_1, country_ru):
        # У товаров нет валюты. Покупки, быть не должно. Оплата не должна проходить.
        cart_id_list = []
        for item in items_5_seller_rub:
            cart_item = add_to_cart(customer, item)
            cart_id_list.append(cart_item.pk)

        # Товар потерял валидность после того как попал в корзину.
        for offer in items_5_seller_rub:
            offer.currency = None
            offer.save()

        payment_test = create_or_update_payment(user_user, country_ru, items_5_seller_rub)

        assert 0 == payment_test.price_without_discount
        assert 0 == payment_test.price
        assert '[]' == payment_test.contained
        assert payment_test.currency == country_ru.get_currency()
        assert payment_test.status == constants.STATUS_PENDING

    def test_bad_payment(self, user_user, default_accounts, country_ru):
        payment_test = create_or_update_payment(user_user, country_ru, [])
        with pytest.raises(UnknownPaymentSystem):
            payment_test.create_payment_system_order('xeim')

        for ac in Account.objects.all():
            assert float(0) == float(ac.balance)

    def test_create_buy_all_author_offers_payment_25_items_good(self, user_user, items_25_seller_rub, seller_1,
                                                                mailoutbox, default_accounts, country_ru, system_user):
        payment = create_buy_all_author_offers_payment(user_user, seller_1, country_ru)
        payment.create_payment_system_order(constants.YANDEX)
        payment.yam_payment.status = constants.STATUS_COMPLETED

        # Коммисия яндекса 5% (взял наобум)
        ya_shop_amount = D(payment.price * D(str(0.95)))
        payment.yam_payment.shop_amount = ya_shop_amount
        payment.yam_payment.save()
        payment.yam_payment.send_signals()
        site_bonus = D(ya_shop_amount * seller_1.personal_site_bonus).quantize(D("1.00"), ROUND_HALF_UP)
        assert get_no_credit_limit_account(currency=payment.currency).balance == ya_shop_amount * D("-2")
        assert get_system_user().get_account(currency=payment.currency).balance == site_bonus
        assert user_user.get_account(currency=payment.currency).balance == D('0.00')
        assert seller_1.get_account(currency=payment.currency).balance == ya_shop_amount - site_bonus
        assert get_sale_account(payment.currency).balance == ya_shop_amount

        # Мои покупки появились?
        assert 25 == PurchaseUser.objects.filter(user=user_user).count()

        # Письма ушли?
        assert 2 == len(mailoutbox)
        assert seller_1.email in list(mailoutbox[0].to)
        assert user_user.email in list(mailoutbox[1].to)

    def test_purchase_of_goods_1_item_payment_from_account(self, user_user, seller_1, default_accounts,
                                                           items_5_seller_rub,
                                                           country_ru, system_user):
        self.user_account_setup(user_user, system_user, amount=100)
        payment = create_or_update_payment(user_user, country_ru, offers=[items_5_seller_rub[0], ])
        payment.get_purchase_url(payment_type=constants.YANDEX)
        assert payment.yam_payment is None

        assert get_system_user().get_account(currency=payment.currency).balance == D('30.00')
        assert seller_1.get_account(currency=payment.currency).balance == D('70.00')

        assert 1 == PurchaseUser.objects.filter(user=user_user).count()
        assert D('0.00') == payment.order_price

        assert D('0.00') == user_user.get_account(currency=self.currency)._balance()
        assert D('0.00') == get_unpaid_account(currency=self.currency)._balance()
        assert D('-200.00') == get_no_credit_limit_account(currency=self.currency)._balance()
        assert get_sale_account(payment.currency).balance == D('100')

    def test_add_funds_to_the_user(self, user_user, default_accounts, country_ru, system_user):
        payment = create_or_update_payment(user_user, country_ru, add_funds=D('100'))
        assert payment.order_price == D('100')
        payment.create_payment_system_order('yandex')
        assert payment.yam_payment is not None

        # Оплата прошла успешно
        payment.yam_payment.status = constants.STATUS_COMPLETED
        payment.yam_payment.shop_amount = 90
        payment.yam_payment.save()
        payment.yam_payment.send_signals()

        assert payment.order_price == D('100')
        assert get_no_credit_limit_account(currency=payment.currency).balance == D('-90.00')
        assert get_unpaid_account(payment.currency).balance == D('-10.00')
        assert user_user.get_account(payment.currency).balance == D('90.00')
        assert user_user.get_account(payment.currency, unpaid=True).balance == D('10.00')

        assert payment.price == payment.order_price

    def test_purchase_of_goods_1_item_edu(self, user_user, seller_1, default_accounts, items_5_seller_rub_edu,
                                          country_ru, system_user):
        payment = create_or_update_payment(user_user, country_ru, offers=[items_5_seller_rub_edu[0], ])
        payment.create_payment_system_order('yandex')
        assert payment.yam_payment is not None

        payment.yam_payment.status = constants.STATUS_COMPLETED
        payment.yam_payment.shop_amount = 900
        payment.yam_payment.save()
        payment.yam_payment.send_signals()

        assert 1 == EducationPurchaseUser.objects.filter(user=user_user).count()
        assert get_no_credit_limit_account(currency=payment.original_currency).balance == D('-1800.00')
        assert get_system_user().get_account(currency=payment.original_currency).balance == D('180.00')
        assert user_user.get_account(currency=payment.original_currency).balance == D('0.00')
        assert seller_1.get_account(currency=payment.original_currency).balance == D('720.00')
        assert get_sale_account(payment.original_currency).balance == D('900')

        assert payment.price == payment.order_price

    def test_purchase_of_goods_1_item_edu_100rub_balans(self, user_user, seller_1, default_accounts,
                                                        items_5_seller_rub_edu,
                                                        country_ru, system_user):
        currency = Currency.objects.get_rub()
        # Начислили реальные деньги
        facade.transfer(source=get_no_credit_limit_account(currency=currency),
                        destination=user_user.get_account(currency=currency),
                        amount=Decimal('90'),
                        user=system_user, )

        code = '{0}_{1}_{2}'.format(user_user.pk, 'UNPAID', currency.iso_code).upper()
        account_type = AccountType.objects.get(code='{0}_{1}'.format('UNPAID', currency.iso_code))
        account, _ = Account.objects.update_or_create(account_type=account_type, primary_user=user_user, code=code)

        # Начислили бонусы
        facade.transfer(source=get_unpaid_account(currency=currency),
                        destination=account,
                        amount=Decimal('10'),
                        user=system_user, )

        payment = create_or_update_payment(user_user, country_ru, offers=[items_5_seller_rub_edu[0], ])
        payment.create_payment_system_order('yandex')
        assert payment.yam_payment is not None

        payment.yam_payment.status = constants.STATUS_COMPLETED
        payment.yam_payment.shop_amount = 810
        payment.yam_payment.save()
        payment.yam_payment.send_signals()

        assert 1 == EducationPurchaseUser.objects.filter(user=user_user).count()
        assert get_no_credit_limit_account(currency=payment.original_currency).balance == D('-1800.00')
        assert get_system_user().get_account(currency=payment.original_currency).balance == D('180.00')
        assert user_user.get_account(currency=payment.original_currency).balance == D('0.00')
        assert seller_1.get_account(currency=payment.original_currency).balance == D('720.00')
        assert get_sale_account(payment.original_currency).balance == D('900')

        assert payment.price == payment.order_price

    def test_purchase_cuopon_on_1_item(self, user_user, items_25_seller_rub, seller_1,
                                       country_ru, system_user):
        # Купон на один товар. Применился на товар с макcимальной ценой
        currency = Currency.objects.get_rub()
        item1 = items_25_seller_rub[2]
        item1.price = 200
        item1.save()
        coupon50 = Coupon.objects.create_coupon(
            type=TYPE_PERCENTAGE,
            value=50,
            users=[user_user, ],
            valid_until=now() + timedelta(days=10),
            user_limit=1,
            code=coupon_name_generator_birthday(user_user),
            auto_created=True,
            item_count=1,
        )
        payment = create_or_update_payment(user_user, country_ru, offers=items_25_seller_rub, coupon=coupon50)
        assert payment.coupon == coupon50
        assert payment.discount == 20
        assert int(payment.order_price) == 2020  # 24 аранжировки со скидкой 20, одна со скидкой 50.


def test_purchase_of_goods_1_item_lang_en_mail(fixture_purchase_of_goods_5_item, user_user, seller_1,
                                               system_user, mailoutbox):
    payment_test_1_item = fixture_purchase_of_goods_5_item
    payment_test_1_item.lang = 'en'
    payment_test_1_item.save()
    payment_test_1_item.yam_payment.send_signals()

    # print(mailoutbox[1].subject)
    assert user_user.email in list(mailoutbox[1].to)
    assert 'Successful purchase on' in mailoutbox[1].subject


class TestPurchaseUSD:

    @pytest.fixture
    def items_5_seller_usd(self, seller_1, mixer):
        return mixer.cycle(5).blend('shop.ShopOffers', currency=Currency.objects.get(pk=2), price=D('25.00'),
                                    user_created=seller_1, checked=True, moderator=seller_1)

    @pytest.fixture
    def fixture_purchase_of_goods_1_item_usa(self, default_accounts, user_user, items_5_seller_usd, seller_1,
                                             country_usa):
        # Базовая фикстура,есть продавец, есть покупатель, есть товары за рубли
        payment = create_or_update_payment(user_user, country_usa, [items_5_seller_usd[0], ])
        payment.create_payment_system_order(constants.PAYPAL)
        payment.pp_payment.status = constants.STATUS_COMPLETED
        # payment.pp_payment.sale_status = constants.SALE_STATUS_COMPLETED
        payment.pp_payment.shop_amount = 90
        payment.pp_payment.save()

        return payment

    def test_purchase_of_goods_1_item_usa(self, fixture_purchase_of_goods_1_item_usa, user_user,
                                          system_user, seller_1):
        payment_test_1_item = fixture_purchase_of_goods_1_item_usa
        # payment_test_1_item.pp_payment.
        payment_test_1_item.pp_payment.send_signals()

        assert get_no_credit_limit_account(currency=payment_test_1_item.currency).balance == D('-180.00')
        assert get_system_user().get_account(currency=payment_test_1_item.currency).balance == D('27.00')
        assert user_user.get_account(currency=payment_test_1_item.currency).balance == D('0.00')
        assert seller_1.get_account(currency=payment_test_1_item.currency).balance == D('63.00')
        assert get_sale_account(payment_test_1_item.currency).balance == D('90')

        assert 1 == PurchaseUser.objects.filter(user=user_user).count()

    def test_add_funds_to_the_user_usd_ya(self, user_user, default_accounts, country_usa, system_user):
        user_user.payment_service = constants.YANDEX
        user_user.save()
        payment = create_or_update_payment(user_user, country_usa, add_funds=D('100'))

        assert payment.order_price == D('6000')  # В рублях по курсу 60
        assert payment.bonus_amount == D('10')  # На каждый полтиники 5 баксов бонусами
        payment.create_payment_system_order(constants.YANDEX)
        assert payment.yam_payment is not None

        # Оплата прошла успешно
        payment.yam_payment.status = constants.STATUS_COMPLETED
        payment.yam_payment.shop_amount = 6000 * 0.9
        payment.yam_payment.save()
        payment.yam_payment.send_signals()

        assert payment.order_price == D('6000')
        assert get_no_credit_limit_account(currency=payment.original_currency).balance == D('-90.00')
        assert get_unpaid_account(payment.original_currency).balance == D('-20.00')
        assert user_user.get_account(payment.original_currency).balance == D('90.00')
        assert user_user.get_account(payment.original_currency, unpaid=True).balance == D('10.00')

        assert payment.price == D('100')
        assert payment.order_price == D('6000')
        # assert Account.active.filter(primary_user=user_user).aggregate(Sum('balance')).get('balance_sum') == '110'

    def test_add_funds_to_the_user_usd_paypal(self, user_user, default_accounts, country_usa, system_user):
        user_user.payment_service = constants.PAYPAL
        user_user.save()
        payment = create_or_update_payment(user_user, country_usa, add_funds=D('100'))

        assert payment.order_price == D('100')
        assert payment.bonus_amount == D('10')  # На каждый полтиники 5 баксов бонусами
        payment.create_payment_system_order(constants.PAYPAL)
        assert payment.pp_payment is not None

        # Оплата прошла успешно
        payment.pp_payment.status = constants.STATUS_COMPLETED
        payment.pp_payment.shop_amount = 90
        payment.pp_payment.save()
        payment.pp_payment.send_signals()

        assert payment.order_price == D('100')
        assert get_no_credit_limit_account(currency=payment.original_currency).balance == D('-90.00')
        assert get_unpaid_account(payment.original_currency).balance == D('-20.00')
        assert user_user.get_account(payment.original_currency).balance == D('90.00')
        assert user_user.get_account(payment.original_currency, unpaid=True).balance == D('10.00')

        assert payment.price == D('100')
        assert payment.order_price == D('100')
        # assert Account.active.filter(primary_user=user_user).aggregate(Sum('balance')).get('balance_sum') == '110'
