# -*- coding: utf-8 -*-

from decimal import Decimal as D

import pytest

# pytest_plugins = ['base_fuxtures', ]
from cart.models import Customer
from project import constants
from test_cases.utils import create_or_update_payment
from offer.models import Currency
from yandex_money.models import STATUS_SUCCESS


@pytest.fixture
def source_rur(mixer, default_accounts):
    from offer.models import Currency
    return mixer.blend('oscar_accounts.Account', currency=Currency.objects.get(pk=1))


@pytest.fixture
def no_credit_limit_account_rur(mixer, default_accounts):
    from offer.models import Currency
    return mixer.blend('oscar_accounts.Account', currency=Currency.objects.get(pk=1), credit_limit=None)


@pytest.fixture
def destination_rur(mixer, default_accounts):
    from offer.models import Currency
    return mixer.blend('oscar_accounts.Account', currency=Currency.objects.get(pk=1))


@pytest.fixture
def seller_1():
    from users.models import User
    user = User.objects.create_user(
        email='seller_1@email.com',
        password='Test12345',
        is_active=True,
        slug='seller_1'
    )
    return user


@pytest.fixture
def customer(user_user):
    customer, created = Customer.objects.get_or_create(user=user_user,
                                                       defaults={'ip_address': '127.0.0.1'})
    return customer


@pytest.fixture
def items_5_seller_rub(seller_1, mixer):
    return mixer.cycle(5).blend('shop.ShopOffers', currency=Currency.objects.get(pk=1), price=D('100.00'),
                                user_created=seller_1, checked=True, moderator=seller_1)


@pytest.fixture
def items_25_seller_rub(seller_1, mixer):
    return mixer.cycle(25).blend('shop.ShopOffers', currency=Currency.objects.get(pk=1), price=D('100.00'),
                                 user_created=seller_1, checked=True, moderator=seller_1, is_available=True)


@pytest.fixture
def items_5_seller_rub_discount_50(seller_1, mixer):
    return mixer.cycle(5).blend('shop.ShopOffers', currency=Currency.objects.get(pk=1), price=D('100.00'),
                                user_created=seller_1, checked=True, moderator=seller_1, discount=50)


@pytest.fixture
def items_1_seller_usd(seller_1, mixer):
    return mixer.blend('shop.ShopOffers', currency=Currency.objects.get(iso_code=Currency.USD), price=D('100.00'),
                       user_created=seller_1, checked=True, moderator=seller_1, is_available=True)


@pytest.fixture
def fixture_purchase_of_goods_5_item(default_accounts, user_user, items_5_seller_rub, seller_1, country_ru):
    # Базовая фикстура,есть продавец, есть покупатель, есть товары за рубли
    payment_test = create_or_update_payment(user_user, country_ru, offers=items_5_seller_rub)

    # создаем платежку
    payment_test.create_payment_system_order(constants.YANDEX)
    # Оплата прошла успешно
    # payment_test.state = UserPayments.STATE_SUCCESS
    # payment_test.save()
    payment_test.yam_payment.status = constants.STATUS_COMPLETED
    # Коммисия яндекса 5% (взял на обум) shop_amount = 441.75
    ya_shop_amount = D(payment_test.price * D(str(0.95)))
    payment_test.yam_payment.shop_amount = ya_shop_amount
    payment_test.yam_payment.save()

    return payment_test


@pytest.fixture
def fixture_purchase_of_goods_5_item_discount_5_item(default_accounts, user_user, items_5_seller_rub_discount_50,
                                                     country_ru):
    # Базовая фикстура,есть продавец, есть покупатель, есть товары за рубли? на все пять товаров есть скидка 50%
    payment_test = create_or_update_payment(user_user, country_ru, items_5_seller_rub_discount_50)

    # создаем платежку
    payment_test.create_payment_system_order(constants.YANDEX)
    # Оплата прошла успешно
    payment_test.yam_payment.status = constants.STATUS_COMPLETED
    # Коммисия яндекса 5% (взял на обум) shop_amount = 441.75
    ya_shop_amount = D(payment_test.price * D(str(0.95)))
    payment_test.yam_payment.shop_amount = ya_shop_amount
    payment_test.yam_payment.save()

    return payment_test


@pytest.fixture
def invate_user(user_user):
    # Пользователь, который всех приглашает
    from users.models import User
    user = User.objects.create_user(
        email='invate_user@email.com',
        password='Test12345',
        is_active=True,
        slug='invate_user'
    )
    user_user.invate_user = user
    user_user.save()
    return user


@pytest.fixture
def items_25_seller_rub(seller_1, mixer):
    return mixer.cycle(25).blend('shop.ShopOffers', currency=Currency.objects.get(pk=1), price=D('100.00'),
                                 user_created=seller_1, checked=True, moderator=seller_1, is_available=True)


@pytest.fixture
def items_5_seller_rub_edu(seller_1, mixer):
    return mixer.cycle(5).blend('education.EducationProduct',
                                currency=Currency.objects.get(pk=1),
                                is_available = True,
                                is_delete = False,
                                is_draft = False,
                                parent = None,
                                price=D('1000.00'),
                                user_created=seller_1,
                                checked=True,
                                moderator=seller_1,)
