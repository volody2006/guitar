# -*- coding: utf-8 -*-
import json
import logging
from decimal import Decimal, Decimal as D

import pytest
from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.utils.timezone import now

from base.utils import get_system_user
from cart.exceptions import InvalidShopOffer, PaymentManagerError, AlreadyBought
from cart.models import CartItem, PurchaseUser, UserPayments
from cart.user_payment_manager import UserPaymentsManager
from cart.utils import add_to_cart, cancellation_of_purchase, \
    get_sales_range, redeem_user_accounts, payment_was_successful
from offer.utils import currency_exchange
from coupons.models import Coupon
from coupons.settings import TYPE_PERCENTAGE
from offer.models import Currency
from oscar_accounts import facade
from oscar_accounts.codes import get_no_credit_limit_account, get_unpaid_account
from oscar_accounts.exceptions import InsufficientFunds, InvalidAmount
from oscar_accounts.models import Account, AccountType, Transfer
from project import constants
from shop.models import ShopOffers
from test_cases.utils import create_buy_all_author_offers_payment, create_or_update_payment
from yandex_money.models import STATUS_SUCCESS

logger = logging.getLogger(__name__)



def test_create_or_update_payment_for_some_good_1_item(user_user, items_5_seller_rub, country_ru):
    payment = create_or_update_payment(user_user, country=country_ru, offers=[items_5_seller_rub[0], ])
    data = json.loads(payment.contained)
    assert 1 == len(data)
    assert 0 == payment.discount
    assert 100 == payment.price_without_discount
    assert items_5_seller_rub[0].currency == payment.currency
    assert payment.status == constants.STATUS_PENDING
    for item_data in json.loads(payment.contained):
        assert 100.0 == float(item_data[0]['fields']['cart_discount_price'])
        assert 100.0 == float(item_data[0]['fields']['price'])
        assert 'RUB' == item_data[0]['fields']['currency']
    assert payment.original_currency == country_ru.get_currency()
    assert payment.original_currency_rate == D('1.00')
    assert D('100.00') == payment.order_price


def test_create_or_update_payment_5_item(user_user, items_5_seller_rub, country_ru):
    country = country_ru
    payment = create_or_update_payment(user_user, country, offers=items_5_seller_rub)
    assert 5 == len(json.loads(payment.contained))
    assert 7 == payment.discount
    assert items_5_seller_rub[0].currency == payment.currency
    assert D(500) == payment.price_without_discount
    assert D(500 * 0.93) == payment.price
    assert payment.status == constants.STATUS_PENDING
    for item_data in json.loads(payment.contained):
        assert 93.0 == float(item_data[0]['fields']['cart_discount_price'])
        assert 100.0 == float(item_data[0]['fields']['price'])
        assert 'RUB' == item_data[0]['fields']['currency']
    assert payment.original_currency == country_ru.get_currency()
    assert payment.original_currency_rate == D('1.00')
    assert D(500 * 0.93) == payment.order_price


def test_create_or_update_payment_5_item_discount_50(customer, user_user, items_5_seller_rub_discount_50, country_ru):
    payment = create_or_update_payment(user_user, country_ru, items_5_seller_rub_discount_50)
    assert 5 == len(json.loads(payment.contained))
    assert 7 == payment.discount
    assert items_5_seller_rub_discount_50[0].currency == payment.currency
    assert D(500) == payment.price_without_discount
    assert D(250) == payment.price
    assert payment.status == constants.STATUS_PENDING
    for item_data in json.loads(payment.contained):
        assert 50.0 == float(item_data[0]['fields']['cart_discount_price'])
        assert 100.0 == float(item_data[0]['fields']['price'])
        assert 'RUB' == item_data[0]['fields']['currency']
    assert payment.original_currency == country_ru.get_currency()
    assert payment.original_currency_rate == D('1.00')
    assert D('250.00') == payment.order_price


@pytest.mark.parametrize('offers', [
    ([]),
    ([444444, ]),
])
def test_create_or_update_payment_bad(user_user, offers, country_ru):
    payment = create_or_update_payment(user_user, country_ru, offers=offers)
    assert 0 == payment.discount
    assert payment.currency == country_ru.get_currency()
    assert '[]' == payment.contained
    assert 0 == payment.price_without_discount
    assert D(0) == payment.price
    assert payment.status == constants.STATUS_PENDING
    assert payment.original_currency == country_ru.get_currency()
    assert payment.original_currency_rate == 1
    assert D('0.00') == payment.order_price


@pytest.mark.parametrize('offers', [
    (4444444),
    ('str'),
])
def test_create_or_update_payment_bad_raises(user_user, offers, country_ru):
    with pytest.raises(PaymentManagerError):
        create_or_update_payment(user_user, country_ru, offers=offers)


def test_add_to_cart(customer, items_5_seller_rub):
    shop_offer = items_5_seller_rub[0]
    cart_item = add_to_cart(customer, shop_offer)
    assert shop_offer == cart_item.shop_offer
    assert customer == cart_item.customer


def test_add_to_cart_not_valid_offer(customer, items_5_seller_rub):
    shop_offer = items_5_seller_rub[0]
    shop_offer.currency = None
    shop_offer.save()
    with pytest.raises(InvalidShopOffer):
        add_to_cart(customer, shop_offer)
        assert 0 == CartItem.objects.all().count()


def test_create_buy_all_author_offers_payment_25_items(user_user, items_25_seller_rub, seller_1, country_ru):
    payment = create_buy_all_author_offers_payment(user_user, seller_1, country_ru)
    assert 25 == ShopOffers.objects.published().by_author(seller_1).count()
    assert payment.user == user_user
    assert payment.status == constants.STATUS_PENDING
    assert payment.price == D('750.00')
    assert payment.price_without_discount == D('2500.00')
    assert payment.currency == items_25_seller_rub[0].currency
    assert payment.discount == 70
    assert payment.original_currency == country_ru.get_currency()
    assert payment.original_currency_rate == D('1.00')
    assert D('750.00') == payment.order_price


def test_create_buy_all_author_offers_payment_24_items(user_user, items_25_seller_rub, seller_1, country_ru):
    item = ShopOffers.objects.get(pk=items_25_seller_rub[0].pk)
    item.delete()
    payment = create_buy_all_author_offers_payment(user_user, seller_1, country_ru)
    assert 24 == ShopOffers.objects.published().by_author(seller_1).count()
    assert payment.discount == 20
    assert payment.price_without_discount == D('2400.00')
    assert payment.user == user_user
    assert payment.status == constants.STATUS_PENDING
    assert payment.price == D('1920.00')
    assert payment.discount == 20
    assert payment.original_currency == country_ru.get_currency()
    assert payment.original_currency_rate == D('1.00')
    assert payment.price == payment.order_price


def test_create_buy_all_author_offers_payment_24_items_one_free(user_user, items_25_seller_rub, seller_1, country_ru):
    item = ShopOffers.objects.get(pk=items_25_seller_rub[0].pk)
    item.price = 0
    item.save()
    payment = create_buy_all_author_offers_payment(user_user, seller_1, country_ru)
    assert 25 == ShopOffers.objects.published().by_author(seller_1).count()
    assert payment.discount == 20
    assert payment.price_without_discount == D('2400.00')
    assert payment.user == user_user
    assert payment.status == constants.STATUS_PENDING
    assert payment.price == D('1920.00')
    assert payment.discount == 20
    assert payment.original_currency == country_ru.get_currency()
    assert payment.original_currency_rate == D('1.00')
    assert payment.price == payment.order_price


def test_create_buy_all_author_offers_payment_24_items_one_free_2(user_user, items_25_seller_rub, seller_1, country_ru):
    item = ShopOffers.objects.get(pk=items_25_seller_rub[0].pk)
    item.price = None
    item.save()
    payment = create_buy_all_author_offers_payment(user_user, seller_1, country_ru)
    assert 25 == ShopOffers.objects.published().by_author(seller_1).count()
    assert payment.discount == 20
    assert payment.price_without_discount == D('2400.00')
    assert payment.user == user_user
    assert payment.status == constants.STATUS_PENDING
    assert payment.price == D('1920.00')
    assert payment.discount == 20
    assert payment.original_currency == country_ru.get_currency()
    assert payment.original_currency_rate == D('1.00')
    assert payment.price == payment.order_price


def test_get_sales_range(user_user, items_5_seller_rub, country_ru):
    payment = create_or_update_payment(user_user, country_ru, offers=[items_5_seller_rub[0], ])
    PurchaseUser.objects.create(user=user_user, shop_offer=items_5_seller_rub[0],
                                payment=payment,
                                date_created=now() - relativedelta(months=1))
    assert get_sales_range(items_5_seller_rub[0].user_created) == 0
    assert get_sales_range(items_5_seller_rub[0].user_created, date_end=now() - relativedelta(months=1)) == 1
    assert get_sales_range(items_5_seller_rub[0].user_created, date_end=now() - relativedelta(months=1)) == 1


@pytest.mark.skipif(not settings.PAYPAL_TURN_ON, reason="PAYPAL_TURN_ON Off")
def test_create_or_update_payment_for_some_good_1_item_usa(user_user, items_5_seller_rub, country_usa):
    payment = create_or_update_payment(user_user, country_usa, offers=[items_5_seller_rub[0], ])
    data = json.loads(payment.contained)
    assert 1 == len(data)
    assert 0 == payment.discount
    assert float(6.68) == float(payment.price_without_discount)
    assert country_usa.currency == payment.currency
    assert payment.status == constants.STATUS_PENDING
    for item_data in json.loads(payment.contained):
        assert 6.68 == float(item_data[0]['fields']['cart_discount_price'])
        assert 6.68 == float(item_data[0]['fields']['price'])
        assert 'USD' == item_data[0]['fields']['currency']
    assert payment.original_currency.iso_code == 'USD'
    assert payment.original_currency_rate == D('60.0000')
    assert payment.currency.iso_code == 'USD'
    assert payment.price == payment.order_price


@pytest.mark.skipif(not settings.PAYPAL_TURN_ON, reason="PAYPAL_TURN_ON Off")
def test_create_or_update_payment_5_item_usa(user_user, items_5_seller_rub, country_usa):
    country = country_usa
    payment = create_or_update_payment(user_user, country, items_5_seller_rub)
    assert 5 == len(json.loads(payment.contained))
    assert 7 == payment.discount
    assert country_usa.currency == payment.currency
    assert float(33.4) == float(payment.price_without_discount)
    assert float(31.05) == float(payment.price)
    assert payment.status == constants.STATUS_PENDING
    for item_data in json.loads(payment.contained):
        assert 6.21 == float(item_data[0]['fields']['cart_discount_price'])
        assert 6.68 == float(item_data[0]['fields']['price'])
        assert 'USD' == item_data[0]['fields']['currency']
    assert payment.original_currency.iso_code == 'USD'
    assert payment.original_currency_rate == D('60.0000')
    assert payment.currency.iso_code == 'USD'
    assert payment.price == payment.order_price


@pytest.mark.skipif(not settings.PAYPAL_TURN_ON, reason="PAYPAL_TURN_ON Off")
def test_create_or_update_payment_5_item_discount_50_usa(user_user, items_5_seller_rub_discount_50,
                                                         country_usa):
    payment = create_or_update_payment(user_user, country_usa, items_5_seller_rub_discount_50)
    assert 5 == len(json.loads(payment.contained))
    assert 7 == payment.discount
    assert country_usa.currency == payment.currency
    assert payment.status == constants.STATUS_PENDING
    for item_data in json.loads(payment.contained):
        assert 6.68 / 2 == float(item_data[0]['fields']['cart_discount_price'])
        assert 6.68 == float(item_data[0]['fields']['price'])
        assert 'USD' == item_data[0]['fields']['currency']
    assert payment.original_currency.iso_code == 'USD'
    assert payment.original_currency_rate == D('60.0000')
    assert payment.currency == country_usa.currency
    assert payment.price == payment.order_price


@pytest.mark.skipif(not settings.PAYPAL_TURN_ON, reason="PAYPAL_TURN_ON Off")
@pytest.mark.parametrize('offers', [
    ([]),
    ([444444, ]),
])
def test_create_or_update_payment_bad_usa(user_user, offers, country_usa):
    payment = create_or_update_payment(user_user, country_usa, offers)
    assert 0 == payment.discount
    assert payment.currency == country_usa.currency
    assert '[]' == payment.contained
    assert 0 == payment.price_without_discount
    assert D(0) == payment.price
    assert payment.status == constants.STATUS_PENDING
    assert payment.original_currency.iso_code == 'USD'
    assert payment.original_currency_rate == D('60.0000')
    assert payment.price == payment.order_price


@pytest.mark.skipif(not settings.PAYPAL_TURN_ON, reason="PAYPAL_TURN_ON Off")
@pytest.mark.parametrize('offers', [
    (4444444),
    ('str'),
])
def test_create_or_update_payment_bad_usa2(user_user, offers, country_usa):
    with pytest.raises(Exception):
        create_or_update_payment(user_user, country_usa, offers)


def test_create_or_update_payment_for_some_good_1_item_1_buy(user_user, items_5_seller_rub, country_ru):
    payment = UserPayments.objects.create(user=user_user,
                                          status=constants.STATUS_PENDING,
                                          price=100,
                                          contained={}, )
    PurchaseUser.objects.create(user=user_user,
                                shop_offer=items_5_seller_rub[0],
                                payment=payment)
    with pytest.raises(AlreadyBought) as e_info:
        create_or_update_payment(user_user, country_ru, [items_5_seller_rub[0], ])
    # data = json.loads(payment.contained)
    #
    # assert 0 == len(data)
    # assert 0 == payment.discount
    # assert 0 == payment.price_without_discount
    # assert payment.contained == '[]'
    # assert payment.price == payment.order_price


def test_create_or_update_payment_5_item_1_buy(user_user, items_5_seller_rub, country_ru):
    country = country_ru
    payment = UserPayments.objects.create(user=user_user,
                                          status=constants.STATUS_PENDING,
                                          price=100,
                                          contained={}, )
    PurchaseUser.objects.create(user=user_user,
                                shop_offer=items_5_seller_rub[0],
                                payment=payment)
    payment = create_or_update_payment(user_user, country, items_5_seller_rub)
    assert 4 == len(json.loads(payment.contained))
    assert 5 == payment.discount
    assert items_5_seller_rub[1].currency == payment.currency
    assert D(400) == payment.price_without_discount
    assert D(400 * 0.95) == payment.price
    assert payment.price == payment.order_price


def test_create_or_update_payment_original_currency_usd_currency_for_payment_evro(user_user,
                                                                                  items_1_seller_usd,
                                                                                  country_usa):
    payment = create_or_update_payment(user_user, country_usa, [items_1_seller_usd],
                                       currency=Currency.objects.get(iso_code='EUR'))
    assert 1 == len(json.loads(payment.contained))
    assert 0 == payment.discount
    assert payment.currency == Currency.objects.get(iso_code='EUR')
    assert D(100 * country_usa.ratio_price) == payment.price_without_discount
    assert D(100 * country_usa.ratio_price) == payment.price
    assert payment.order_price == Decimal(100 / 80 * 60 * country_usa.ratio_price)
    assert payment.original_currency == country_usa.get_currency()


@pytest.mark.parametrize('price, result, old_currend, new_currend', [
    (Decimal(100), Decimal(100), 'RUB', 'RUB'),
    (Decimal(1), Decimal(60), 'USD', 'RUB'),
    (Decimal(60), Decimal(1), 'RUB', 'USD'),
    (Decimal(100), Decimal(100), 'USD', 'USD'),
    (Decimal(100), Decimal(75), 'USD', 'EUR'),
    (Decimal(1), Decimal(80), 'EUR', 'RUB'),
])
def test_exchange_of_currency(price, result, old_currend, new_currend, currencies_rate):
    old = Currency.objects.get(iso_code=old_currend)
    new = Currency.objects.get(iso_code=new_currend)

    assert currency_exchange(price, old, new) == result


@pytest.fixture
def coupon50():
    return Coupon.custom_objects.create_coupon(type=TYPE_PERCENTAGE, value=50)


def test_create_or_update_payment_for_some_good_1_item_coupon_50(user_user, items_5_seller_rub,
                                                                 country_ru, coupon50):
    payment = create_or_update_payment(user_user, country=country_ru, offers=[items_5_seller_rub[0], ],
                                       coupon=coupon50)
    data = json.loads(payment.contained)
    assert 1 == len(data)
    assert 50 == payment.discount
    assert 100 == payment.price_without_discount
    assert items_5_seller_rub[0].currency == payment.currency
    assert payment.status == constants.STATUS_PENDING
    for item_data in json.loads(payment.contained):
        assert 50.0 == float(item_data[0]['fields']['cart_discount_price'])
        assert 100.0 == float(item_data[0]['fields']['price'])
        assert 'RUB' == item_data[0]['fields']['currency']
    assert payment.original_currency == country_ru.get_currency()
    assert payment.original_currency_rate == D('1.00')

    assert payment.coupon == coupon50
    assert coupon50.is_redeemed is False
    assert coupon50.users.count() == 1
    assert coupon50.users.first().user == user_user
    assert payment.price == payment.order_price


def test_create_or_update_payment_for_some_good_1_item_5discount_coupon_50(user_user, items_5_seller_rub,
                                                                           country_ru, coupon50):
    item = items_5_seller_rub[0]
    item.set_discount(5)

    payment = create_or_update_payment(user_user, country=country_ru, offers=[item, ],
                                       coupon=coupon50)
    data = json.loads(payment.contained)
    assert 1 == len(data)
    assert 5 == payment.discount
    assert 100 == payment.price_without_discount
    assert items_5_seller_rub[0].currency == payment.currency
    assert payment.status == constants.STATUS_PENDING
    for item_data in json.loads(payment.contained):
        assert 95.0 == float(item_data[0]['fields']['cart_discount_price'])
        assert 100.0 == float(item_data[0]['fields']['price'])
        assert 'RUB' == item_data[0]['fields']['currency']
    assert payment.original_currency == country_ru.get_currency()
    assert payment.original_currency_rate == D('1.00')

    assert payment.coupon is None
    assert coupon50.is_redeemed is False
    assert coupon50.users.count() == 1
    assert coupon50.users.first().user == user_user
    assert payment.price == payment.order_price


def test_create_or_update_payment_coupon_50_is_redeemed(user_user, items_5_seller_rub,
                                                        country_ru, coupon50):
    coupon50.redeem(user=user_user)
    assert coupon50.is_redeemed is True

    payment = create_or_update_payment(user_user, country=country_ru, offers=[items_5_seller_rub[0], ],
                                       coupon=coupon50)
    assert 0 == payment.discount
    assert 100 == payment.price_without_discount
    for item_data in json.loads(payment.contained):
        assert 100.0 == float(item_data[0]['fields']['cart_discount_price'])
        assert 100.0 == float(item_data[0]['fields']['price'])
        assert 'RUB' == item_data[0]['fields']['currency']
    assert payment.coupon is None
    assert payment.price == payment.order_price


def test_create_or_update_payment_coupon_not_valid_code(user_user, items_5_seller_rub,
                                                        country_ru):
    payment = create_or_update_payment(user_user, country=country_ru, offers=[items_5_seller_rub[0], ],
                                       coupon='Not_VAlid')
    assert 0 == payment.discount
    assert 100 == payment.price_without_discount
    for item_data in json.loads(payment.contained):
        assert 100.0 == float(item_data[0]['fields']['cart_discount_price'])
        assert 100.0 == float(item_data[0]['fields']['price'])
        assert 'RUB' == item_data[0]['fields']['currency']
    assert payment.coupon is None
    assert payment.price == payment.order_price


@pytest.fixture
def fixture_purchase_of_goods_5_item(default_accounts, user_user, items_5_seller_rub, seller_1, country_ru, coupon50):
    # Базовая фикстура,есть продавец, есть покупатель, есть товары за рубли
    payment_test = create_or_update_payment(user_user, country_ru, offers=items_5_seller_rub, coupon=coupon50)

    # создаем платежку
    payment_test.create_payment_system_order(constants.YANDEX)
    # Оплата прошла успешно
    # payment_test.status = UserPayments.status_SUCCESS
    # payment_test.save()
    payment_test.yam_payment.status = STATUS_SUCCESS
    ya_shop_amount = D(payment_test.price)
    payment_test.yam_payment.shop_amount = ya_shop_amount
    payment_test.yam_payment.save()

    return payment_test


def test_purchase_of_goods_1_item_coupon50(user_user, seller_1, default_accounts, items_5_seller_rub,
                                           country_ru, system_user, coupon50):
    payment = create_or_update_payment(user_user, country_ru, offers=[items_5_seller_rub[0], ], coupon=coupon50)

    payment.create_payment_system_order(constants.YANDEX)
    assert payment.yam_payment is not None

    payment.yam_payment.status = constants.STATUS_COMPLETED
    payment.yam_payment.shop_amount = payment.price
    payment.yam_payment.save()
    payment.yam_payment.send_signals()

    assert get_no_credit_limit_account(currency=payment.currency).balance == D('-100.00')
    assert get_system_user().get_account(currency=payment.currency).balance == D('15.00')
    assert user_user.get_account(currency=payment.currency).balance == D('0.00')
    assert seller_1.get_account(currency=payment.currency).balance == D('35.00')
    assert coupon50.is_redeemed is True
    assert coupon50.users.count() == 1
    assert coupon50.users.first().user == user_user
    assert payment.price == payment.order_price


def test_purchase_of_goods_5_item_coupon50(items_5_seller_rub, user_user, seller_1, system_user, coupon50, country_ru):
    # payment_test_5_item = fixture_purchase_of_goods_5_item
    # payment_test_5_item.yam_payment.send_signals()
    payment = create_or_update_payment(user_user, country_ru, offers=list(items_5_seller_rub), coupon=coupon50)

    payment.create_payment_system_order(constants.YANDEX)
    payment.yam_payment.status = constants.STATUS_COMPLETED
    payment.yam_payment.shop_amount = payment.price
    payment.yam_payment.save()
    payment.yam_payment.send_signals()

    ya_shop_amount = payment.yam_payment.shop_amount

    assert get_no_credit_limit_account(currency=payment.currency).balance == ya_shop_amount * D("-2")
    assert get_system_user().get_account(currency=payment.currency).balance == D('75.0')
    assert user_user.get_account(currency=payment.currency).balance == D('0.00')
    assert D('175') == seller_1.get_account(currency=payment.currency).balance

    assert payment.coupon.is_redeemed is True
    assert payment.coupon.users.count() == 1
    assert payment.coupon.users.first().user == user_user
    assert payment.price == payment.order_price


def test_cancellation_of_purchase_good(items_5_seller_rub, user_user, seller_1, system_user, country_ru, coupon50):
    payment = create_or_update_payment(user_user, country_ru, offers=list(items_5_seller_rub), coupon=coupon50)

    payment.create_payment_system_order(constants.YANDEX)
    payment.yam_payment.status = constants.STATUS_COMPLETED
    payment.yam_payment.shop_amount = payment.price
    payment.yam_payment.save()
    payment.yam_payment.send_signals()

    ya_shop_amount = payment.yam_payment.shop_amount

    assert get_no_credit_limit_account(currency=payment.currency).balance == ya_shop_amount * D("-2")
    assert get_system_user().get_account(currency=payment.currency).balance == D('75.0')
    assert user_user.get_account(currency=payment.currency).balance == D('0.00')
    assert D('175') == seller_1.get_account(currency=payment.currency).balance

    assert payment.coupon.is_redeemed is True
    assert payment.coupon.users.count() == 1
    assert payment.coupon.users.first().user == user_user
    payment = UserPayments.objects.get(pk=payment.pk)
    for offer in payment.contained_offers:
        purchase = PurchaseUser.objects.get(shop_offer=offer,
                                            payment=payment)
        cancellation_of_purchase(purchase, system_user)

    assert PurchaseUser.objects.filter(payment=payment).exists() is False

    assert get_no_credit_limit_account(currency=payment.currency)._balance() == D('-250')
    assert float(get_system_user().get_account(currency=payment.currency).balance) == float(0)
    assert float(user_user.get_account(currency=payment.currency).balance) == float(0)
    assert float(seller_1.get_account(currency=payment.currency).balance) == float(0)
    assert payment.price == payment.order_price


def test_redeem_user_accounts(default_accounts, user_user, system_user, country_ru):
    currency = Currency.objects.get_rub()
    # Начислили реальные деньги
    facade.transfer(source=get_no_credit_limit_account(currency=currency),
                    destination=user_user.get_account(currency=currency),
                    amount=Decimal(500),
                    user=system_user, )

    code = '{0}_{1}_{2}'.format(user_user.pk, 'UNPAID', currency.iso_code).upper()
    account_type = AccountType.objects.get(code='{0}_{1}'.format('UNPAID', currency.iso_code))
    account, _ = Account.objects.update_or_create(account_type=account_type, primary_user=user_user, code=code)

    # Начислили бонусы
    facade.transfer(source=get_unpaid_account(currency=currency),
                    destination=account,
                    amount=Decimal(500),
                    user=system_user, )

    payment = create_or_update_payment(user_user, country_ru)
    completed_transfers = redeem_user_accounts(payment, amount=Decimal('500'))
    assert Decimal('250.00') == account._balance()
    assert Decimal('250.00') == user_user.get_account(currency=currency)._balance()
    assert Decimal('-250.00') == get_unpaid_account(currency=currency)._balance()
    assert Decimal('-500.00') == get_no_credit_limit_account(currency=currency)._balance()
    assert 2 == len(completed_transfers)


def test_redeem_user_accounts_no_bonuses(default_accounts, user_user, system_user, country_ru):
    currency = Currency.objects.get_rub()
    # Начислили реальные деньги
    facade.transfer(source=get_no_credit_limit_account(currency=currency),
                    destination=user_user.get_account(currency=currency),
                    amount=Decimal(500),
                    user=system_user, )

    payment = create_or_update_payment(user_user, country_ru)
    completed_transfers = redeem_user_accounts(payment, amount=Decimal('400'))
    assert Decimal('100.00') == user_user.get_account(currency=currency)._balance()
    assert Decimal('0.00') == get_unpaid_account(currency=currency)._balance()
    assert Decimal('-500.00') == get_no_credit_limit_account(currency=currency)._balance()
    assert 1 == len(completed_transfers)


def test_redeem_user_accounts_bonuses_only(default_accounts, user_user, system_user, country_ru):
    currency = Currency.objects.get_rub()

    code = '{0}_{1}_{2}'.format(user_user.pk, 'UNPAID', currency.iso_code).upper()
    account_type = AccountType.objects.get(code='{0}_{1}'.format('UNPAID', currency.iso_code))
    account, _ = Account.objects.update_or_create(account_type=account_type, primary_user=user_user, code=code)

    # Начислили бонусы
    facade.transfer(source=get_unpaid_account(currency=currency),
                    destination=account,
                    amount=Decimal(500),
                    user=system_user, )

    payment = create_or_update_payment(user_user, country_ru)
    with pytest.raises(InsufficientFunds):
        redeem_user_accounts(payment, amount=Decimal('400'))

    assert Decimal('-500.00') == get_unpaid_account(currency=currency)._balance()
    assert Decimal('0.00') == get_no_credit_limit_account(currency=currency)._balance()
    # Транзакция одна, начисление. Блок атомарный.
    assert 1 == Transfer.objects.all().count()


def test_redeem_user_accounts_bonuses_more_than_real_money(default_accounts, user_user, system_user, country_ru):
    currency = Currency.objects.get_rub()

    # Начислили реальные деньги
    facade.transfer(source=get_no_credit_limit_account(currency=currency),
                    destination=user_user.get_account(currency=currency),
                    amount=Decimal(100),
                    user=system_user, )

    code = '{0}_{1}_{2}'.format(user_user.pk, 'UNPAID', currency.iso_code).upper()
    account_type = AccountType.objects.get(code='{0}_{1}'.format('UNPAID', currency.iso_code))
    account, _ = Account.objects.update_or_create(account_type=account_type, primary_user=user_user, code=code)

    # Начислили бонусы
    facade.transfer(source=get_unpaid_account(currency=currency),
                    destination=account,
                    amount=Decimal(1500),
                    user=system_user, )

    payment = create_or_update_payment(user_user, country_ru)
    with pytest.raises(InsufficientFunds):
        redeem_user_accounts(payment, amount=Decimal('400'))

    assert 2 == Transfer.objects.all().count()
    assert Decimal('1500.00') == account._balance()
    assert Decimal('100.00') == user_user.get_account(currency=currency)._balance()
    assert Decimal('-1500.00') == get_unpaid_account(currency=currency)._balance()
    assert Decimal('-100.00') == get_no_credit_limit_account(currency=currency)._balance()


def test_create_or_update_payment_order_price_zero(user_user, items_5_seller_rub, country_ru, system_user):
    currency = Currency.objects.get_rub()
    # Начислили реальные деньги
    facade.transfer(source=get_no_credit_limit_account(currency=currency),
                    destination=user_user.get_account(currency=currency),
                    amount=Decimal(500),
                    user=system_user, )

    code = '{0}_{1}_{2}'.format(user_user.pk, 'UNPAID', currency.iso_code).upper()
    account_type = AccountType.objects.get(code='{0}_{1}'.format('UNPAID', currency.iso_code))
    account, _ = Account.objects.update_or_create(account_type=account_type, primary_user=user_user, code=code)

    # Начислили бонусы
    facade.transfer(source=get_unpaid_account(currency=currency),
                    destination=account,
                    amount=Decimal(500),
                    user=system_user, )

    payment = create_or_update_payment(user_user, country=country_ru, offers=[items_5_seller_rub[0], ])

    assert 100 == payment.price_without_discount

    assert payment.original_currency == country_ru.get_currency()
    assert payment.original_currency_rate == D('1.00')
    assert D('0.00') == payment.order_price


def test_create_or_update_payment_order_price_there_are_bonuses_but_there_is_no_real_money(
        user_user, items_5_seller_rub, country_ru, system_user):
    currency = Currency.objects.get_rub()

    code = '{0}_{1}_{2}'.format(user_user.pk, 'UNPAID', currency.iso_code).upper()
    account_type = AccountType.objects.get(code='{0}_{1}'.format('UNPAID', currency.iso_code))
    account, _ = Account.objects.update_or_create(account_type=account_type, primary_user=user_user, code=code)

    # Начислили бонусы
    facade.transfer(source=get_unpaid_account(currency=currency),
                    destination=account,
                    amount=Decimal(500),
                    user=system_user, )

    payment = create_or_update_payment(user_user, country=country_ru, offers=[items_5_seller_rub[0], ])

    assert 100 == payment.price_without_discount

    assert payment.original_currency == country_ru.get_currency()
    assert payment.original_currency_rate == D('1.00')
    assert D('50.00') == payment.order_price


def test_create_or_update_payment_order_price_zero_2(user_user, items_5_seller_rub, country_ru, system_user):
    currency = Currency.objects.get_rub()
    # Начислили реальные деньги
    facade.transfer(source=get_no_credit_limit_account(currency=currency),
                    destination=user_user.get_account(currency=currency),
                    amount=Decimal(500),
                    user=system_user, )

    payment = create_or_update_payment(user_user, country=country_ru, offers=[items_5_seller_rub[0], ])

    assert 100 == payment.price_without_discount

    assert payment.original_currency == country_ru.get_currency()
    assert payment.original_currency_rate == D('1.00')
    assert D('0.00') == payment.order_price


def test_create_or_update_payment_order_price_partial_payment(user_user, items_5_seller_rub, country_ru, system_user):
    currency = Currency.objects.get_rub()
    # Начислили реальные деньги
    facade.transfer(source=get_no_credit_limit_account(currency=currency),
                    destination=user_user.get_account(currency=currency),
                    amount=Decimal(40),
                    user=system_user, )

    code = '{0}_{1}_{2}'.format(user_user.pk, 'UNPAID', currency.iso_code).upper()
    account_type = AccountType.objects.get(code='{0}_{1}'.format('UNPAID', currency.iso_code))
    account, _ = Account.objects.update_or_create(account_type=account_type, primary_user=user_user, code=code)

    # Начислили бонусы
    facade.transfer(source=get_unpaid_account(currency=currency),
                    destination=account,
                    amount=Decimal(40),
                    user=system_user, )

    payment = create_or_update_payment(user_user, country=country_ru, offers=[items_5_seller_rub[0], ])

    assert 100 == payment.price_without_discount

    assert payment.original_currency == country_ru.get_currency()
    assert payment.original_currency_rate == D('1.00')
    assert D('20.00') == payment.order_price


def test_create_or_update_payment_add_funds(user_user, country_ru):
    payment = create_or_update_payment(user_user, country=country_ru, add_funds=D('100'))

    assert D('0') == payment.price_without_discount
    assert D('100') == payment.price

    assert payment.original_currency == country_ru.get_currency()
    assert payment.original_currency_rate == D('1.00')
    assert D('100.00') == payment.order_price


def test_create_or_update_payment_add_funds_balance_is_there(user_user, country_ru, system_user):
    currency = Currency.objects.get_rub()
    # Начислили реальные деньги
    facade.transfer(source=get_no_credit_limit_account(currency=currency),
                    destination=user_user.get_account(currency=currency),
                    amount=Decimal(100),
                    user=system_user, )

    code = '{0}_{1}_{2}'.format(user_user.pk, 'UNPAID', currency.iso_code).upper()
    account_type = AccountType.objects.get(code='{0}_{1}'.format('UNPAID', currency.iso_code))
    account, _ = Account.objects.update_or_create(account_type=account_type, primary_user=user_user, code=code)

    # Начислили бонусы
    facade.transfer(source=get_unpaid_account(currency=currency),
                    destination=account,
                    amount=Decimal(100),
                    user=system_user, )

    payment = create_or_update_payment(user_user, country=country_ru, add_funds=D('100'))

    assert D('0') == payment.price_without_discount
    assert D('100') == payment.price

    assert payment.original_currency == country_ru.get_currency()
    assert payment.original_currency_rate == D('1.00')
    assert D('100.00') == payment.order_price


def test_create_or_update_payment_add_funds_balance_is_there_1(user_user, country_ru, system_user):
    currency = Currency.objects.get_rub()
    # Начислили реальные деньги
    facade.transfer(source=get_no_credit_limit_account(currency=currency),
                    destination=user_user.get_account(currency=currency),
                    amount=Decimal(10),
                    user=system_user, )

    code = '{0}_{1}_{2}'.format(user_user.pk, 'UNPAID', currency.iso_code).upper()
    account_type = AccountType.objects.get(code='{0}_{1}'.format('UNPAID', currency.iso_code))
    account, _ = Account.objects.update_or_create(account_type=account_type, primary_user=user_user, code=code)

    # Начислили бонусы
    facade.transfer(source=get_unpaid_account(currency=currency),
                    destination=account,
                    amount=Decimal(10),
                    user=system_user, )

    payment = create_or_update_payment(user_user, country=country_ru, add_funds=D('100'))

    assert D('0') == payment.price_without_discount
    assert D('100') == payment.price

    assert payment.original_currency == country_ru.get_currency()
    assert payment.original_currency_rate == D('1.00')
    assert D('100.00') == payment.order_price


def test_create_or_update_payment_add_funds_usd_yadnex(user_user, country_usa, system_user):
    '''
    Пользователь пополняет счет в долларах через яндекс
    :param user_user:
    :param country_ru:
    :param system_user:
    :return:
    '''
    user_user.payment_service = constants.YANDEX
    user_user.save()
    currency_usd = Currency.objects.get_usd()
    # Начислили реальные деньги
    facade.transfer(source=get_no_credit_limit_account(currency=currency_usd),
                    destination=user_user.get_account(currency=currency_usd),
                    amount=Decimal(10),
                    user=system_user, )

    code = '{0}_{1}_{2}'.format(user_user.pk, 'UNPAID', currency_usd.iso_code).upper()
    account_type = AccountType.objects.get(code='{0}_{1}'.format('UNPAID', currency_usd.iso_code))
    account, _ = Account.objects.update_or_create(account_type=account_type, primary_user=user_user, code=code)

    # Начислили бонусы
    facade.transfer(source=get_unpaid_account(currency=currency_usd),
                    destination=account,
                    amount=Decimal(10),
                    user=system_user, )

    payment = create_or_update_payment(user_user, country=country_usa, add_funds=D('100'))

    assert D('0') == payment.price_without_discount
    assert D('100') == payment.price

    assert payment.original_currency == country_usa.get_currency()
    assert payment.original_currency_rate == D('60.00')
    assert D('6000.00') == payment.order_price


def test_payment_was_successful_raises_not_currency(user_user, country_usa, system_user):
    payment = create_or_update_payment(user_user, country=country_usa)
    payment.currency = None
    payment.save()
    with pytest.raises(InvalidAmount) as e_info:
        payment_was_successful(payment, send_mail=False)


def test_payment_was_successful_raises_not_original_currency(user_user, country_usa, system_user):
    payment = create_or_update_payment(user_user, country=country_usa)
    payment.original_currency = None
    payment.save()
    with pytest.raises(InvalidAmount) as e_info:
        payment_was_successful(payment, send_mail=False)


def test_payment_was_successful_raises_not_price(user_user, country_usa, system_user):
    payment = create_or_update_payment(user_user, country=country_usa)
    assert payment.get_income() == 0
    with pytest.raises(InvalidAmount) as e_info:
        payment_was_successful(payment, send_mail=False)
