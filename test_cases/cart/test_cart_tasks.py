# -*- coding: utf-8 -*-
from datetime import timedelta
from decimal import Decimal

import pytest
from django.utils.timezone import now
from post_office.models import Email

from project import constants
from cart.models import Customer, UserPayments
from cart.tasks import EMAIL_SEND_ALERT, task_send_alert
from cart.utils import add_to_cart
from test_cases.utils import create_or_update_payment
from offer.models import Currency


@pytest.fixture()
def payment(user_user, mixer, country_ru):
    item = mixer.blend('shop.ShopOffers', currency=Currency.objects.get(pk=1), price=Decimal('100.00'),
                       user_created=user_user, checked=True, moderator=user_user)
    return create_or_update_payment(user=user_user, country=country_ru, offers=[item,])


@pytest.fixture()
def payments(user_user, mixer, country_ru):

    item = mixer.blend('shop.ShopOffers', currency=Currency.objects.get(pk=1), price=Decimal('100.00'),
                       user_created=user_user, checked=True, moderator=user_user)
    payments = []
    for i in range(0, 4):
        payments.append(create_or_update_payment(user=user_user, country=country_ru, offers=[item,]))
    return payments


def test_task_send_alert_1(payment, user_user):
    # Счет не оплачен более 30 минут, других счетов нет. Ушло письмо клиенту и нам
    date_end_mod = now() - timedelta(minutes=32)
    date_end = now() - timedelta(minutes=30)
    date_start = date_end - timedelta(hours=2)
    payment_f = UserPayments.objects.filter(pk=payment.pk)
    payment_f.update(modified_date=date_end_mod,
                     status=constants.STATUS_PENDING)
    payment = UserPayments.objects.get(pk=payment.pk)
    assert payment.modified_date > date_start
    assert payment.modified_date < date_end
    assert payment.status == constants.STATUS_PENDING

    task_send_alert()
    assert Email.objects.all().count() == 2
    assert Email.objects.filter(to=[user_user.email, ]).count() == 1
    assert Email.objects.filter(to=EMAIL_SEND_ALERT).count() == 1
    em = Email.objects.filter(to=EMAIL_SEND_ALERT)[0]
    assert em.subject == 'Неоплачено 1 счетов, исключено 0'


def test_task_send_alert_2(payments, user_user):
    # Два счета, одно не оплачено, следом оплаченное. Писем нет.
    payment1 = payments[0]
    payment2 = payments[1]

    date_end_mod = now() - timedelta(minutes=32)
    date_end = now() - timedelta(minutes=30)
    payment_f1 = UserPayments.objects.filter(pk=payment1.pk)
    payment_f1.update(modified_date=date_end_mod,
                      status=constants.STATUS_PENDING)
    payment_f2 = UserPayments.objects.filter(pk=payment2.pk)
    payment_f2.update(modified_date=date_end,
                      status=constants.STATUS_COMPLETED)

    task_send_alert()
    assert Email.objects.all().count() == 1
    assert Email.objects.filter(to=[user_user.email, ]).count() == 0
    assert Email.objects.filter(to=EMAIL_SEND_ALERT).count() == 1
    em = Email.objects.filter(to=EMAIL_SEND_ALERT)[0]
    assert em.subject == 'Неоплачено 1 счетов, исключено 1'


def test_task_send_alert_3(payments, user_user):
    # Два счета, одно оплачено, следом не оплаченное. Письмо по второму счету нам и клиенту.
    payment1 = payments[0]
    payment2 = payments[1]

    date_end_mod = now() - timedelta(minutes=35)
    date_end = now() - timedelta(minutes=32)
    payment_f1 = UserPayments.objects.filter(pk=payment1.pk)
    payment_f1.update(modified_date=date_end_mod,
                      status=constants.STATUS_COMPLETED)
    payment_f2 = UserPayments.objects.filter(pk=payment2.pk)
    payment_f2.update(modified_date=date_end,
                      status=constants.STATUS_PENDING)

    task_send_alert()
    assert Email.objects.all().count() == 2
    assert Email.objects.filter(to=[user_user.email, ]).count() == 1
    assert Email.objects.filter(to=EMAIL_SEND_ALERT).count() == 1
    em = Email.objects.filter(to=EMAIL_SEND_ALERT)[0]
    assert em.subject == 'Неоплачено 1 счетов, исключено 0'


def test_task_send_alert_4(payment, user_user):
    # Счет не оплачен, менее 30 минут. писем нет.
    payment_f1 = UserPayments.objects.filter(pk=payment.pk)
    payment_f1.update(
        modified_date=now() - timedelta(minutes=12),
        status=constants.STATUS_PENDING)

    task_send_alert()
    assert Email.objects.all().count() == 0
