# -*- coding: utf-8 -*-
import pytest
from decimal import Decimal

from project import constants
from base.templatetags.editlink_tags import get_url_admin
from cart.models import UserPayments
from test_cases.utils import create_or_update_payment
from offer.models import Currency


@pytest.fixture()
def payment(user_user, mixer, country_ru):
    item = mixer.blend('shop.ShopOffers', currency=Currency.objects.get(pk=1), price=Decimal('100.00'),
                       user_created=user_user, checked=True, moderator=user_user)
    return create_or_update_payment(user=user_user, country=country_ru, offers=[item, ])


def test_admin_open_userpayments_payment_no_action(payment, client_login_superuser):
    url = get_url_admin(payment)

    assert payment.status == constants.STATUS_PENDING

    print(url)
    response = client_login_superuser.get(url)
    # print(response.context['adminform'])
    # print(response)
    assert response.status_code == 200

    data = {
        "action": '-',
        "service_note": 'test',
        "actual_amount": Decimal('0'),
        "transfer_set-TOTAL_FORMS": "0",
        "transfer_set-INITIAL_FORMS": "0",
        "transfer_set-MIN_NUM_FORMS": "0",
        "transfer_set-MAX_NUM_FORMS": "0"

    }
    response = client_login_superuser.post(url, data=data)

    assert response.status_code == 302

    payment = UserPayments.objects.get(pk=payment.pk)
    assert payment.status == constants.STATUS_PENDING
    assert payment.service_note == 'test'
    assert payment.actual_amount == Decimal('0')


def test_admin_open_userpayments_payment_action_wire(payment, client_login_superuser, system_user):
    url = get_url_admin(payment)

    data = {
        'action': 'wire',
        'service_note': 'test',
        'actual_amount': Decimal('0'),
        "transfer_set-TOTAL_FORMS": "0",
        "transfer_set-INITIAL_FORMS": "0",
        "transfer_set-MIN_NUM_FORMS": "0",
        "transfer_set-MAX_NUM_FORMS": "0"
    }
    response = client_login_superuser.post(url, data=data)

    assert response.status_code == 302

    payment = UserPayments.objects.get(pk=payment.pk)
    assert payment.status == constants.STATUS_COMPLETED
    assert payment.service_note == 'test'
    assert payment.actual_amount == Decimal('0')


def test_admin_open_userpayments_payment_action_close(payment, client_login_superuser):
    url = get_url_admin(payment)

    data = {
        'action': 'close',
        'service_note': 'test',
        'actual_amount': Decimal('0'),
        "transfer_set-TOTAL_FORMS": "0",
        "transfer_set-INITIAL_FORMS": "0",
        "transfer_set-MIN_NUM_FORMS": "0",
        "transfer_set-MAX_NUM_FORMS": "0"
    }
    response = client_login_superuser.post(url, data=data)

    assert response.status_code == 302

    payment = UserPayments.objects.get(pk=payment.pk)
    assert payment.status == constants.STATUS_CANCELLED
    assert payment.service_note == 'test'
    assert payment.actual_amount == Decimal('0')