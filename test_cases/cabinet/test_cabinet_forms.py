# -*- coding: utf-8 -*-
from datetime import date

import pytest
from django.utils.timezone import now

from cabinet.forms import ContractForm
from users.models import Contract


@pytest.fixture()
def contract(user_user):
    return Contract.objects.create(user=user_user)


def test_contract_form_fz_valid(user_user, contract, mixer):
    allowed_payment_method = [1, 4, 5, 6, 7]
    form_data = {'contract_type': 1,
                 'fio': "Ivanov Ivan Ivanovoch",
                 'birthdate': now().date(),
                 'citi': 'Sititower',
                 'address': 'address 1 kv.2',
                 'passport_number': '0000 000000',
                 'passport_date': now().date(),
                 'passport_issued_by': 'ROVD Sititower 457',
                 'bank_details': mixer.blend('users.UserPaymentData', user=user_user, payment_method=1).pk,
                 'inn': '1234567896544',
                 'same_address': True,
                 'post_postcode': '123456'
                 }
    form = ContractForm(user_user, 'ru', allowed_payment_method, data=form_data)
    print(form.errors)
    assert form.is_valid() is True


def test_contract_form_IP_valid(user_user, contract, mixer):
    allowed_payment_method = [1, 4, 5, 6, 7]
    form_data = {'contract_type': 2,
                 'fio': "Ivanov Ivan Ivanovoch",
                 'birthdate': now().date(),
                 'citi': 'Sititower',
                 'address': 'address 1 kv.2',

                 'bank_details': mixer.blend('users.UserPaymentData', user=user_user, payment_method=1).pk,
                 'inn': '1234567896544',
                 'ogrn': '123456789',
                 'same_address': True,
                 'post_postcode': '123456'
                 }
    form = ContractForm(user_user, 'ru', allowed_payment_method, data=form_data)
    print(form.errors)
    assert form.is_valid() is True


def test_contract_form_IP_valid_not_bank_details(user_user, contract, mixer):
    allowed_payment_method = [1, 4, 5, 6, 7]
    form_data = {'contract_type': 2,
                 'fio': "Ivanov Ivan Ivanovoch",
                 'birthdate': now().date(),
                 'citi': 'Sititower',
                 'address': 'address 1 kv.2',

                 'bank_details': '',
                 'payment_method': 1,
                 'payment_information': '1231 1212 1212 1212 1212',
                 'bic': '123456789',
                 'inn': '1234567896544',
                 'ogrn': '123456789',
                 'same_address': True,
                 'post_postcode': '123456'
                 }
    form = ContractForm(user_user, 'ru', allowed_payment_method, data=form_data)
    print(form.errors)
    assert form.is_valid() is True


def test_contract_form_fizic_valid_not_bank_details(user_user, contract, mixer):
    allowed_payment_method = [1, 4, 5, 6, 7]
    form_data = {'contract_type': 2,
                 'fio': "Ivanov Ivan Ivanovoch",
                 'birthdate': now().date(),
                 'citi': 'Sititower',
                 'address': 'address 1 kv.2',

                 'bank_details': '',
                 'payment_method': 1,
                 'payment_information': '1231 1212 1212 1212 1212',
                 'bic': '123456789',
                 'inn': '1234567896544',
                 'ogrn': '123456789',
                 'same_address': True,
                 'post_postcode': '123456'
                 }
    form = ContractForm(user_user, 'ru', allowed_payment_method, data=form_data)
    print(form.errors)
    assert form.is_valid() is True


@pytest.mark.parametrize('key', ['fio', 'birthdate', 'citi', 'address',
                                 'passport_number', 'passport_date', 'passport_issued_by',
                                 'payment_method', 'payment_information', 'bic', 'inn',
                                 'post_postcode'])
def test_contract_form_fizic_unvalid(user_user, contract, mixer, key):
    # Удаляем из формы по одному ключу, форма должна валится.
    allowed_payment_method = [1, 4, 5, 6, 7]
    form_data = {'contract_type': 1,
                 'fio': "Ivanov Ivan Ivanovoch",
                 'birthdate': now().date(),
                 'citi': 'Sititower',
                 'address': 'address 1 kv.2',

                 'passport_number': '0000 000000',
                 'passport_date': now().date(),
                 'passport_issued_by': 'ROVD Sititower 457',

                 'bank_details': '',
                 'payment_method': 1,
                 'payment_information': '1231 1212 1212 1212 1212',
                 'bic': '123456789',
                 'inn': '1234567896544',
                 'same_address': True,
                 'post_postcode': '123456'
                 }
    form_data.pop(key)
    form = ContractForm(user_user, 'ru', allowed_payment_method, data=form_data)
    print(form.errors)
    assert form.is_valid() is False


@pytest.mark.parametrize('key', ['fio', 'birthdate', 'citi', 'address',
                                 # 'passport_number', 'passport_date', 'passport_issued_by',
                                 'payment_method', 'payment_information', 'bic', 'inn', 'ogrn',
                                 'post_postcode'])
def test_contract_form_ip_unvalid(user_user, contract, mixer, key):
    # Удаляем из формы по одному ключу, форма должна валится.
    allowed_payment_method = [1, 4, 5, 6, 7]
    form_data = {'contract_type': 2,
                 'fio': "Ivanov Ivan Ivanovoch",
                 'birthdate': now().date(),
                 'citi': 'Sititower',
                 'address': 'address 1 kv.2',

                 'bank_details': '',
                 'payment_method': 1,
                 'payment_information': '1231 1212 1212 1212 1212',
                 'bic': '123456789',
                 'inn': '1234567896544',
                 'ogrn': '1234567896544',
                 'same_address': True,
                 'post_postcode': '123456'
                 }
    form_data.pop(key)
    form = ContractForm(user_user, 'ru', allowed_payment_method, data=form_data)
    print(form.errors)
    assert form.is_valid() is False
