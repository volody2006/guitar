# -*- coding: utf-8 -*-
import pytest
from django.conf import settings
from django.urls import reverse
from django.utils import translation
import logging

from django.utils.timezone import now
from reversion.models import Version

from base.utils import get_translation_fieldnames

from users.models import User, Contract

logger = logging.getLogger(__name__)

langs = list(code for code, _ in settings.LANGUAGES if code not in settings.PASS_LANGUAGES)


@pytest.mark.parametrize('lang', langs)
def test_my_profile(client_login, lang):
    with translation.override(lang):
        url = reverse('my_profile')
        response = client_login.get(url)
        assert 200 == response.status_code


@pytest.mark.parametrize('lang', langs)
def test_save_my_profile(client_login, lang):
    with translation.override(lang):
        url = reverse('my_profile')
        data = {
            'slug': 'testsluguser',
            'first_name': 'Имя',
            'last_name': 'Фамилия',
            'about': 'Очень крутое описание',

        }
        response = client_login.post(url, data)
        assert 302 == response.status_code
        assert reverse('cabinet_index') == response.url


@pytest.mark.parametrize('lang', langs)
def test_my_profile_versions(client_login, user_user, lang):
    with translation.override(lang):
        url = reverse('my_profile')
        data = {
            'slug': 'testsluguser'
        }
        versions = Version.objects.get_for_object(user_user)
        assert 0 == len(versions)
        response = client_login.post(url, data)
        assert 302 == response.status_code
        versions = Version.objects.get_for_object(user_user)
        assert 1 == len(versions)
        # assert versions[1].field_dict["slug"] == "testuser"
        assert versions[0].field_dict["slug"] == "testsluguser"


def test_edit_my_profile_ru(client_login, user_user):
    with translation.override('ru'):
        url = reverse('my_profile')
        data = {
            'slug': 'testsluguser',
            'first_name': 'Имя',
            'last_name': 'Фамилия',
            'about': 'Очень крутое описание',

        }
        response = client_login.post(url, data)
        assert 302 == response.status_code

        user = User.objects.get(pk=user_user.pk)

        for f_lang in settings.MODELTRANSLATION_LANGUAGES:
            if f_lang == 'ru':
                field_lang = '{fld}_{lang_code}'.format(fld='first_name', lang_code=f_lang.replace('-', '_'))
                assert 'Имя' == getattr(user, field_lang)
                field_lang = '{fld}_{lang_code}'.format(fld='last_name', lang_code=f_lang.replace('-', '_'))
                assert 'Фамилия' == getattr(user, field_lang)
                continue

            field_lang = '{fld}_{lang_code}'.format(fld='first_name', lang_code=f_lang.replace('-', '_'))
            assert 'Imia' == getattr(user, field_lang)

            field_lang = '{fld}_{lang_code}'.format(fld='last_name', lang_code=f_lang.replace('-', '_'))
            assert 'Familiia' == getattr(user, field_lang)


@pytest.mark.parametrize('field', ['first_name', 'last_name'])
def test_edit_my_profile_ru_is_en(client_login, user_user, field):
    user = User.objects.get(pk=user_user.pk)
    user.first_name_en = 'first_name_en'
    user.last_name_en = 'last_name_en'
    user.save()
    with translation.override('ru'):
        url = reverse('my_profile')
        data = {
            'slug': 'testsluguser',
            'first_name': 'first_name_ru',
            'last_name': 'last_name_ru',
        }
        response = client_login.post(url, data)
        assert 302 == response.status_code

        user = User.objects.get(pk=user_user.pk)
        for field_lang in get_translation_fieldnames(field):
            if field_lang.endswith('_ru'):
                assert '{}_ru'.format(field) == getattr(user, field_lang)
            if field_lang.endswith('_en'):
                assert '{}_en'.format(field) == getattr(user, field_lang)


@pytest.fixture()
def contract(user_user):
    return Contract.objects.create(user=user_user)


@pytest.mark.xfail
@pytest.mark.parametrize('url', ['user_contract', 'user_contract_update', 'user_contract_pdf'])
def test_open_contract_url_user_not_seller_404(client_login_2, user_user, url):
    with translation.override('ru'):
        url = reverse(url)
        response = client_login_2.get(url)
        assert response.status_code == 404


def test_open_contract_url_not_form_save_ru(client_login, user_user, contract):
    with translation.override('ru'):
        url = reverse('user_contract')
        response = client_login.get(url)
        assert response.url == reverse('user_contract_update')
        assert response.status_code == 302


def test_open_contract_url_form_save(client_login, user_user, contract):
    url = reverse('user_contract')
    contract.is_form_save = True
    contract.save()
    response = client_login.get(url)
    assert response.status_code == 200


def test_post_contract_valid(client_login, user_user, contract, mixer):
    url = reverse('user_contract_update')
    form_data = {'contract_type': 1,
                 'fio': "Ivanov Ivan Ivanovoch",
                 'birthdate': now().date(),
                 'citi': 'Sititower',
                 'address': 'address 1 kv.2',
                 'passport_number': '0000 000000',
                 'passport_date': now().date(),
                 'passport_issued_by': 'ROVD Sititower 457',
                 'bank_details': mixer.blend('users.UserPaymentData', user=user_user, payment_method=1).pk,
                 'inn': '1234567896544',
                 'same_address': True,
                 'post_postcode': '123456'
                 }
    response = client_login.post(url, data=form_data)
    assert response.status_code == 302
    assert response.url == reverse('user_contract')
