# -*- coding: utf-8 -*-
from django.urls import reverse
from django.utils.translation import ugettext as _
import logging

logger = logging.getLogger(__name__)


def test_main_accounts(client_login, user_user):
    url = reverse('my_accounts')
    response = client_login.get(url)
    assert 200 == response.status_code
