# -*- coding: utf-8 -*-
import pytest
from django.conf import settings
from django.urls import reverse
from django.utils import translation
from django.utils.translation import ugettext as _
import logging

from forum import views
from forum.models import ForumCategory

logger = logging.getLogger(__name__)

langs = list(code for code, _ in settings.LANGUAGES if code not in settings.PASS_LANGUAGES)


@pytest.mark.parametrize('lang', langs)
def test_get_forum_index_anonim(lang, client):
    with translation.override(lang):
        url = reverse('forum_index')
        response = client.get(url)

        assert response.status_code == 200


@pytest.mark.parametrize('lang', langs)
def test_get_forum_index_user(lang, client_login):
    with translation.override(lang):
        url = reverse('forum_index')
        response = client_login.get(url)

        assert response.status_code == 200

        assert response.resolver_match.func.__name__ == views.IndexView.as_view().__name__


@pytest.fixture
def forum_category(mixer):
    for lang, _temp in settings.LANGUAGES:
        mixer.blend('forum.ForumCategory', lang=lang, is_active=True)


def test_get_forum_category_detail_anonim(forum_category, client):
    for cat in ForumCategory.objects.all():
        response = client.get(cat.get_absolute_url())
        assert response.status_code == 200


def test_get_forum_category_detail_client(forum_category, client_login):
    for cat in ForumCategory.objects.all():
        response = client_login.get(cat.get_absolute_url())
        assert response.status_code == 200


@pytest.fixture
def valid_date_topic(mixer, forum_category):
    data = {
        'title': 'title',
        'category': ForumCategory.objects.all()[0],
        'content': 'content',
        'lang': settings.LANGUAGE_CODE,
    }
    return data


def test_get_forum_add_topic_anonim(client):
    url = reverse('forum_add_topic')
    response = client.get(url)
    assert response.status_code == 302


def test_get_forum_add_topic_client(client_login):
    url = reverse('forum_add_topic')
    response = client_login.get(url)
    assert response.status_code == 200


def test_get_forum_add_topic_client_spam(client_login_spam):
    url = reverse('forum_add_topic')
    response = client_login_spam.get(url)
    assert response.status_code == 200


@pytest.mark.parametrize('lang', langs)
def test_post_forum_add_topic_client(client_login, forum_category, lang):
    with translation.override(lang):
        assert ForumCategory.objects.filter(lang=lang).exists() is True

        url = reverse('forum_add_topic')
        data = {
            'title': 'title',
            'category': ForumCategory.objects.filter(lang=lang)[0].pk,
            'content': 'content',
        }
        response = client_login.post(url, data=data)
        # print(response.content.decode('utf-8'))
        assert response.status_code == 302


def test_post_forum_add_topic_spam(client_login, mixer):
    cat = mixer.blend('forum.ForumCategory', lang='ru', is_active=True)
    url = reverse('forum_add_topic')
    data = {
        'title': 'title',
        'category': cat.pk,
        'content': 'content',
        'text': 'spam'
    }
    response = client_login.post(url, data=data)
    assert response.status_code == 302
