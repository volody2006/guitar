# -*- coding: utf-8 -*-
import pytest
from mixer.backend.django import mixer

from antispam.models import IpSpam
from forum.forms import TopicForm


def test_topic_form_valid_not_spam(user_user):
    cat = mixer.blend('forum.ForumCategory', lang='ru', is_active=True)
    data = {
        'title': 'title',
        'category': cat.pk,
        'content': 'content',
    }
    form = TopicForm(lang='ru', data=data)
    assert form.is_valid() is True

    topic = form.save(ip_address='127.0.0.1', user=user_user)
    assert topic.is_active is True
    assert topic.is_spam is False
    assert topic.user.is_spam is False


def test_topic_form_valid_spam_honeypot(user_user):
    cat = mixer.blend('forum.ForumCategory', lang='ru', is_active=True)
    data = {
        'title': 'title',
        'category': cat.pk,
        'content': 'content',
        'text': 'spam'
    }
    ip = '127.0.0.1'
    assert IpSpam.objects.filter(ip=ip).exists() is False

    form = TopicForm(lang='ru', data=data)
    assert form.is_valid() is True

    topic = form.save(ip_address=ip, user=user_user)
    assert topic.is_active is False
    assert topic.is_spam is True
    assert topic.user.is_spam is True
    assert IpSpam.objects.filter(ip=ip).exists() is True
    assert IpSpam.objects.filter(ip=ip)[0].is_honeypot is True


def test_topic_form_valid_spam_ip(user_user):
    cat = mixer.blend('forum.ForumCategory', lang='ru', is_active=True)
    data = {
        'title': 'title',
        'category': cat.pk,
        'content': 'content',
    }
    ip = '127.0.1.1'

    IpSpam.objects.create(ip=ip)

    form = TopicForm(lang='ru', data=data)
    assert form.is_valid() is True

    topic = form.save(ip_address=ip, user=user_user)
    assert topic.is_active is False
    assert topic.is_spam is True
    assert topic.user.is_spam is True
    assert IpSpam.objects.filter(ip=ip).exists() is True
    assert IpSpam.objects.filter(ip=ip)[0].is_honeypot is False
