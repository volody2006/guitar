# -*- coding: utf-8 -*-
import pytest
from django.conf import settings

from antispam.models import IpSpam
from comments.models import Comment
from forum.models import Topic


@pytest.fixture()
def topic(user_user, mixer):
    user_user.reg_ip = '10.1.21.120'
    user_user.save()

    topic = Topic.objects.create(
        title='title',
        content='spam',
        user=user_user,
        category=mixer.blend('forum.ForumCategory'),
        lang=settings.LANGUAGE_CODE,
        ip_address='10.1.21.121'
    )
    for i in range(0, 4):
        com = Comment(content_object=topic,
                      user=user_user,
                      text='test',
                      is_spam=False,
                      is_active=True,
                      remote_addr='10.1.21.12{}'.format(i),
                      )
        com.save()
    for i in range(0, 4):
        com = Comment(content_object=topic,
                      text='test',
                      is_spam=False,
                      is_active=True,
                      remote_addr='10.1.2{}.125'.format(i),
                      )
        com.save()
    return topic


def test_topic_mark_spam(user_user, topic):
    assert user_user.is_spam is False
    assert topic.is_spam is False
    assert IpSpam.objects.filter(ip=topic.ip_address).exists() is False
    assert IpSpam.objects.filter(ip=user_user.reg_ip).exists() is False
    assert Comment.objects.get_for_object(topic).count() == 8

    topic.is_spam = True
    topic.save()

    assert user_user.is_spam is True
    assert topic.is_spam is True
    assert IpSpam.objects.filter(ip=topic.ip_address).exists() is True
    assert IpSpam.objects.filter(ip=user_user.reg_ip).exists() is True
    for com in Comment.objects.get_for_object(topic):
        assert com.is_spam is False
        assert com.is_active is False


def test_topic_not_active(user_user, topic):
    assert user_user.is_spam is False
    assert topic.is_spam is False
    assert IpSpam.objects.filter(ip=topic.ip_address).exists() is False
    assert IpSpam.objects.filter(ip=user_user.reg_ip).exists() is False
    assert Comment.objects.get_for_object(topic).count() == 8

    topic.is_active = False
    topic.save()

    assert user_user.is_spam is False
    assert topic.is_spam is False
    assert IpSpam.objects.filter(ip=topic.ip_address).exists() is False
    assert IpSpam.objects.filter(ip=user_user.reg_ip).exists() is False
    for com in Comment.objects.get_for_object(topic):
        assert com.is_spam is False
        assert com.is_active is False
