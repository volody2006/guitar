# -*- coding: utf-8 -*-
# http://django-haystack.readthedocs.io/en/v2.4.1/tutorial.html

from decimal import Decimal

from django.utils.html import strip_tags
from haystack import indexes
from haystack.fields import FacetMultiValueField, FacetBooleanField

from base.mixins_search import SynonymsIndexMixin
from base.utils import get_translation_fieldnames
from lang.models import Language
from shop.models import Composer, Film, Performer, ShopOffers, ShopOffersProxy


class ShopOffersIndex(SynonymsIndexMixin,
                      indexes.SearchIndex,
                      indexes.Indexable):
    text = indexes.CharField(document=True)
    name = indexes.MultiValueField()
    description = indexes.CharField(model_attr='description', default='')
    composer = FacetMultiValueField()
    performer = FacetMultiValueField()
    film = FacetMultiValueField()
    author = indexes.CharField(model_attr='user_created', default='', faceted=True)
    price = indexes.DecimalField()
    discount = indexes.IntegerField(model_attr='discount')

    is_discount = indexes.BooleanField(model_attr='is_discount')

    author_id = indexes.FacetIntegerField()
    published = indexes.BooleanField(default=False)
    date_created = indexes.DateTimeField(model_attr='date_created')
    date_ordering = indexes.DateTimeField()
    criteria_votes_score = indexes.FloatField()
    votes_count = indexes.IntegerField(model_attr='votes_count', default=0)
    pay_count = indexes.IntegerField(model_attr='pay_count', default=0)
    views_count = indexes.IntegerField(model_attr='views_count', default=0)
    comments_count = indexes.IntegerField(model_attr='comments_count', default=0)
    votes_score = indexes.FloatField(model_attr='votes_score', default=0)
    difficulty = indexes.FacetIntegerField(model_attr='difficulty')
    tonality = indexes.FacetIntegerField(model_attr='tonality')
    agree_promotion = FacetBooleanField(model_attr='agree_promotion', default=True)

    tags_id = FacetMultiValueField()
    mlt = indexes.CharField()
    composer_id = indexes.IntegerField()
    performer_id = indexes.IntegerField()
    film_id = indexes.IntegerField()

    free = indexes.BooleanField(default=False)
    sale_day = indexes.BooleanField(model_attr='sale_day', default=False)

    shopoffers_type_id = indexes.IntegerField()
    langs = FacetMultiValueField()

    def get_model(self):
        return ShopOffers

    def index_queryset(self, using='default'):
        if using and using != 'default':
            return self.get_model().objects.none()
        return self.get_model().objects.filter(is_delete=False)

    def add_text(self, add, text):
        if add == 'None':
            return text
        from antispam.utils import normalize_word
        for i in add.split(' '):
            text = '{} {}'.format(text, i)
            i = normalize_word(i)
            text = '{} {}'.format(text, i)
        return text

    def prepare(self, object):
        self.prepared_data = super().prepare(object)

        # Add in tags (assuming there's a M2M relationship to Tag on the model).
        # Note that this would NOT get picked up by the automatic
        # schema tools provided by Haystack.
        self.prepared_data['tags'] = [tag.name for tag in object.tags.all()]

        return self.prepared_data

    def prepare_price(self, obj):
        if obj.price:
            return obj.price
        return Decimal('0.00')

    def prepare_free(self, obj):
        return obj.is_free

    def prepare_mlt(self, obj):
        return self.prepare_text(obj)

    def prepare_text(self, obj):
        fieldnames = ['name', ]
        text = ''
        for fieldname in fieldnames:
            for localized_fieldname in get_translation_fieldnames(fieldname):
                if getattr(obj, localized_fieldname):
                    add_text = str(getattr(obj, localized_fieldname))
                    text = self.add_text(add_text, text)

        f_fieldnames = {'user_created': ['first_name', 'last_name'],
                        'composer': ['name', ],
                        'performer': ['name', ],
                        'film': ['name', ]}
        for key in f_fieldnames.keys():
            if getattr(obj, key):
                f_obj = getattr(obj, key)
                for fieldname in f_fieldnames.get(key):
                    for localized_fieldname in get_translation_fieldnames(fieldname):
                        if getattr(f_obj, localized_fieldname):
                            add_text = str(getattr(f_obj, localized_fieldname))
                            text = self.add_text(add_text, text)

        tags = obj.tags.all()
        if tags.exists():
            for tag in tags:
                for localized_fieldname in get_translation_fieldnames('name'):
                    if getattr(tag, localized_fieldname):
                        add_text = str(getattr(tag, localized_fieldname))
                        text = self.add_text(add_text, text)

        text = strip_tags(text)
        text = '{} {}'.format(text, self._prepare_synonyms_text(obj))
        fieldnames = ['composer', 'performer', 'film']
        for fieldname in fieldnames:
            text = '{} {}'.format(text, self._prepare_cpf_text(obj, fieldname))
        return text.strip()

    def prepare_name(self, obj):
        p_list = []
        for l_field in get_translation_fieldnames('name'):
            if getattr(obj, l_field):
                p_list.append(getattr(obj, l_field))
        return p_list

    def _prepare_cpf(self, obj, fieldname):
        cpf_obj = getattr(obj, fieldname, None)
        p_list = []
        if cpf_obj:
            for localized_fieldname in get_translation_fieldnames('name'):
                if getattr(cpf_obj, localized_fieldname):
                    p_list.append(getattr(cpf_obj, localized_fieldname))
            p_list = p_list + self.prepare_synonyms(cpf_obj)
        return p_list

    def _prepare_cpf_text(self, obj, fieldname):
        cpf_obj = getattr(obj, fieldname, None)
        if cpf_obj:
            return self._prepare_synonyms_text(cpf_obj)
        return ''

    def prepare_composer(self, obj):
        return self._prepare_cpf(obj, 'composer')

    def prepare_performer(self, obj):
        return self._prepare_cpf(obj, 'performer')

    def prepare_film(self, obj):
        return self._prepare_cpf(obj, 'film')

    def prepare_tags_id(self, obj):
        tags_id = list(obj.tags.all().values_list('id', flat=True))
        return tags_id

    def prepare_langs(self, obj):
        if obj.lang.all().exists():
            langs = list(obj.lang.all().values_list('code', flat=True))
        else:
            langs = list(Language.objects.all().values_list('code', flat=True))
        return langs

    def prepare_date_ordering(self, obj):
        if obj.date_approval_moderator:
            return obj.date_approval_moderator
        return obj.date_created

    def prepare_published(self, obj):
        return obj.is_valid()

    def prepare_author_id(self, obj):
        try:
            return obj.user_created.pk
        except AttributeError:
            return 0

    def prepare_film_en(self, obj):
        try:
            return obj.film.name_en
        except AttributeError:
            return ''

    def prepare_film_ru(self, obj):
        try:
            return obj.film.name_ru
        except AttributeError:
            return ''

    def prepare_film_id(self, obj):
        try:
            return obj.film.pk
        except AttributeError:
            return 0

    def prepare_composer_id(self, obj):
        try:
            return obj.composer.pk
        except AttributeError:
            return 0

    def prepare_performer_id(self, obj):
        try:
            return obj.performer.pk
        except AttributeError:
            return 0

    def prepare_shopoffers_type_id(self, obj):
        try:
            return obj.shopoffers_type.pk
        except AttributeError:
            return 0

    def prepare_criteria_votes_score(self, obj):
        return float(obj.criteria_votes_score)


class CPFMix(SynonymsIndexMixin, indexes.SearchIndex):
    text = indexes.CharField(document=True)
    name = indexes.MultiValueField()
    content_auto = indexes.NgramField()
    published = indexes.BooleanField(default=False)

    def index_queryset(self, using=None):
        if using != 'CPF':
            return self.get_model().objects.none()
        return self.get_model().objects.all()

    def prepare_name(self, obj):
        p_list = []
        for l_field in get_translation_fieldnames('name'):
            if getattr(obj, l_field):
                p_list.append(getattr(obj, l_field))
        return p_list

    def prepare_text(self, obj):
        text = ''
        for l_field in get_translation_fieldnames('name'):
            if getattr(obj, l_field):
                text = '{} {}'.format(text, getattr(obj, l_field))
        text = '{} {}'.format(text, self._prepare_synonyms_text(obj))
        return text.strip()

    def prepare_content_auto(self, obj):
        return self.prepare_text(obj)

    def prepare_published(self, obj):
        return obj.is_valid()


class FilmIndex(CPFMix, indexes.Indexable):

    def get_model(self):
        return Film


class ComposerIndex(CPFMix, indexes.Indexable):

    def get_model(self):
        return Composer


class PerformerIndex(CPFMix, indexes.Indexable):

    def get_model(self):
        return Performer


class NameOfferIndex(CPFMix, indexes.Indexable):

    def get_model(self):
        return ShopOffersProxy
