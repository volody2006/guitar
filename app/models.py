# -*- coding: utf-8 -*-
import logging
import os
import random
import uuid
from collections import namedtuple
from decimal import Decimal, ROUND_CEILING, ROUND_HALF_UP
from os.path import basename

import reversion
from bs4 import BeautifulSoup
from dirtyfields import DirtyFieldsMixin
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.db import models
from django.template.defaultfilters import truncatewords_html
from django.templatetags.static import static
from django.urls import NoReverseMatch, reverse
from django.utils import translation
from django.utils.safestring import mark_safe
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _
from django_resized import ResizedImageField
from embed_video.fields import EmbedVideoField
from lxml.html.clean import Cleaner

from autoslug.fields import AutoSlugField
from base.models import AbstractTagsMixin, DateModelsMixin, FullUrlMixin, IsActiveMixin, OrderFieldModelMixin, \
    SeoFieldsModelMixin, UploadToMixin, SynonymAbstractMixin
from base.utils import convert_size, slugify
from base.validators import FileValidator
from offer.models import AbstractOfferPrice, AbstractOffers, Currency, OfferRatingHandler
from offer.utils import get_meta_from_description
from ratings.handlers import ratings
from shop.managers import CPFManager, SaleQuerySet, ShopQuerySet, TagManager

logger = logging.getLogger(__name__)
cleaner = Cleaner()


def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/fi/le/filename.ext
    extension = os.path.splitext(filename)[1].lower().strip('.')
    fname = '{}.{}'.format(uuid.uuid4(), extension)
    bits = []
    bits.extend([fname[i * 2:(i + 1) * 2] for i in range(2)])
    bits.append(fname)
    bits.insert(0, 'user_{0}'.format(instance.user_created.id))
    return os.path.join(*bits)


class CPFAbstract(SynonymAbstractMixin, FullUrlMixin, SeoFieldsModelMixin):
    name = models.CharField(_('name'), max_length=255, unique=True)
    description = models.TextField(verbose_name=_('Описание'), blank=True)
    slug = AutoSlugField(populate_from='name',
                         max_length=260,
                         unique=True,
                         slugify=slugify,
                         db_index=True,
                         always_update=False,
                         editable=True,
                         null=True,
                         blank=True
                         )

    user = models.ForeignKey('users.User', null=True, on_delete=models.SET_NULL)
    accepted = models.BooleanField(default=False)

    objects = CPFManager()

    def __str__(self):
        return '{name}{accepted}'.format(
            name=self.name,
            accepted=' *' if not self.accepted else ''
        )

    def class_name(self):
        return self.__class__.__name__.lower()

    def get_absolute_url(self):
        return reverse('{prefix}_main'.format(prefix=self._meta.model_name),
                       kwargs={'slug': self.slug})

    def gau(self):
        return self.get_absolute_url()

    def count_item(self):
        return self.shopoffers_set.published().count()

    def is_valid(self):
        return self.count_item() > 0

    class Meta:
        abstract = True


class Composer(CPFAbstract):
    class Meta:
        verbose_name = _("композитор")
        verbose_name_plural = _("композиторы")


class Performer(CPFAbstract):
    class Meta:
        verbose_name = _("исполнитель")
        verbose_name_plural = _("исполнители")


class Film(CPFAbstract):
    class Meta:
        verbose_name = _("фильм")
        verbose_name_plural = _("фильмы")


def upload_main_image(s, f):
    return s.upload_to(f, prefix='shop')


def upload_sale_image(s, f):
    return s.upload_to(f, prefix='sale')


def upload_youtube_preview(s, f):
    return s.upload_to(f, prefix='youtube_preview')


def upload_song_file(s, f):
    return s.upload_to(f, prefix='song')


def upload_technique_image(s, f):
    return s.upload_to(f, prefix='technique')


class ShopOffersType(SeoFieldsModelMixin):
    """
    Категории товаров
    """
    name = models.CharField(max_length=255, verbose_name=_('Название категории'),
                            help_text=_('Название категории будет выводится на сайте'))
    slug = models.SlugField(_('Слаг'), max_length=255, unique=True, db_index=True,
                            help_text=_('Часть УРЛ на каталог товаров'))
    icon = models.CharField(max_length=255, verbose_name=_('Класс иконки'), default='icon-music',
                            help_text=_('Класс иконки, которая будет отображаться в списках товаров'))

    def get_absolute_url(self):
        return reverse('shop-type-main', kwargs={'shopoffers_type_slug': self.slug})

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Категория товара")
        verbose_name_plural = _("Категории товаров")


class Techniques(UploadToMixin):
    """
    Техники игры
    """
    image = ResizedImageField(_('Изображение'), size=[50, 50], crop=['middle', 'center'], quality=75,
                              help_text=_('Изображение для более наглядного отображения техники, '
                                          'будет кадрировано до квадрата и уменьшено до 50px.'),
                              upload_to=upload_technique_image, blank=True, null=True)
    name = models.CharField(max_length=255, verbose_name=_('Название техники'))
    description = models.TextField(verbose_name=_('Описание'), blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Техника")
        verbose_name_plural = _("Техники")


class ShopOfferTechniques(OrderFieldModelMixin):
    """
    Промежуточная модель для ManyToManyField('Techniques') в ShopOffers моделе
    """
    offer = models.ForeignKey('shop.ShopOffers', on_delete=models.CASCADE)
    technique = models.ForeignKey(Techniques, on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Техника")
        verbose_name_plural = _("Техники. В порядке важности.")
        unique_together = ['offer', 'technique']
        ordering = ['order']


class TagOffers(AbstractTagsMixin):
    """
    Теги для товаров, разделены по категориям
    """
    shopoffers_type = models.ForeignKey(ShopOffersType, verbose_name=_('Категория товаров'), related_name='tags',
                                        on_delete=models.CASCADE, blank=True, null=True)

    objects = TagManager()

    def get_absolute_url(self):
        if self.shopoffers_type:
            return reverse('shop-tag-main',
                           kwargs={'tag': self.slug,
                                   'shopoffers_type_slug': self.shopoffers_type.slug
                                   })
        return reverse('tag-main', kwargs={'tag': self.slug, })


class TagOffersCategory(AbstractTagsMixin):
    # TODO: Модель нигде не используется, на удаление?
    approve = None
    hidden = None
    tags = models.ManyToManyField(TagOffers, blank=True)
    objects = TagManager()

    class Meta:
        verbose_name = _('Категория тегов')
        verbose_name_plural = _('Категории тегов')


@reversion.register()
class ShopOffers(AbstractOfferPrice, AbstractOffers):
    """
    Файлы (pdf, zip, guitarpro)
    """
    statsy_group = 'tab'
    DIFFICULTY_CHOICES = [
        (1, _('ур.1 Начинающий')),
        (2, _('ур.2 Продолжающий')),
        (3, _('ур.3 Продвинутый')),
        (4, _('ур.4 Эксперт')),
        (5, _('ур.5 Мастер')),
    ]
    VOTE_CRITERIAS = [
        ('accuracy', _('Точность переложения')),
        ('convenience', _('Удобство апликатур')),
        ('ease', _('Качество записи нот/табов')),  # quality не ставить, уже задействованно
    ]
    VOTE_LESSONS_CRITERIAS = [
        ('shop_relevance', _('Понятность изложения')),
        ('shop_quality', _('Качество материала')),
        ('shop_utility', _('Полезность материала')),
    ]

    TONALITY_CHOICES = [
        (1, 'C/Am'),
        (2, 'G/Em'),
        (3, 'F/Dm'),
        (4, 'D/Bm'),
        (5, 'Bb/Gm'),
        (6, 'A/F#m'),
        (7, 'Eb/Cm'),
        (8, 'E/C#m'),
        (9, _('Другая')),
    ]

    shopoffers_type = models.ForeignKey(ShopOffersType, verbose_name=_('Тип товаров'), related_name='offers',
                                        on_delete=models.SET_NULL, blank=True, null=True)
    lang = models.ManyToManyField('lang.Language', blank=True, related_name='offers_lang')

    image = models.ImageField(_('Изображение'), null=True, blank=True, upload_to=upload_main_image)
    difficulty = models.SmallIntegerField(choices=DIFFICULTY_CHOICES, default=1, verbose_name=_('Сложность'))

    video_url = EmbedVideoField(max_length=2000, verbose_name=_('Ссылка на видео-демонстрацию (Youtube)'))
    video_thumbnail = ResizedImageField(_('Видео превью'),
                                        size=[170, 170],
                                        crop=['middle', 'center'],
                                        quality=75,
                                        upload_to=upload_youtube_preview,
                                        blank=True,
                                        null=True)
    freeze = models.BooleanField(default=False, verbose_name=_('Заморозка'), help_text=_('Запрет на редактирования'))
    date_start_freeze = models.DateTimeField(null=True, blank=True, help_text=_('Время начало заморозки'))
    date_end_freeze = models.DateTimeField(null=True, blank=True, help_text=_('Время окончания заморозки'))
    cause_freezing = models.CharField(default='', max_length=254, help_text=_('Причина заморозки'), null=True,
                                      blank=True, )
    agree_promotion = models.BooleanField(
        default=True, verbose_name=_('Участвовать в акциях'),
        help_text=_('Выключение данной опции, запретит выбирать аранжировку для продвижения.'))

    related_offers = models.ManyToManyField('self', verbose_name=_('связанные предложения'), symmetrical=False,
                                            blank=True, null=True)

    # Скидка дня.
    sale_day = models.BooleanField(default=False)

    moderator = models.ForeignKey('users.User', on_delete=models.SET_NULL,
                                  blank=True, null=True, verbose_name=_('Модератор'),
                                  related_name='moderator_shop')

    file = models.FileField(verbose_name=_('Файл'), help_text=_('Zip архив'),
                            storage=FileSystemStorage(location=settings.FILES_ROOT, base_url=settings.FILES_URL),
                            blank=True, null=True, upload_to=user_directory_path)

    # только для аранжировок
    composer = models.ForeignKey(Composer, on_delete=models.SET_NULL, null=True, blank=True,
                                 verbose_name=_('Композитор'))
    performer = models.ForeignKey(Performer, on_delete=models.SET_NULL, null=True, blank=True,
                                  verbose_name=_('Эстрадный исполнитель'))
    film = models.ForeignKey(Film, on_delete=models.SET_NULL, null=True, blank=True,
                             verbose_name=_('Кинофильм'))
    tonality = models.SmallIntegerField(choices=TONALITY_CHOICES, verbose_name=_('Тональность'), null=True, blank=True)

    song_langs = models.ManyToManyField('lang.Language', blank=True)
    song_text = models.TextField(verbose_name=_('Текст песни'), null=True, blank=True)
    song_file = models.FileField(verbose_name=_('PDF Файл'), help_text=_('Pdf файл, с текстом песни'),
                                 validators=[FileValidator(allowed_extensions=('pdf',),
                                                           allowed_mimetypes=('application/pdf',)), ],
                                 blank=True, null=True, upload_to=upload_song_file)

    author_music = models.BooleanField(default=False, verbose_name=_('Авторская музыка'))

    tags = models.ManyToManyField(TagOffers, blank=True)
    techniques = models.ManyToManyField(Techniques, through=ShopOfferTechniques, blank=True,
                                        verbose_name=_('Используемые техники'),
                                        help_text=_('Добавляйте техники в порядке их важности'))

    objects = ShopQuerySet.as_manager()

    @property
    def offer_type(self):
        return '{}-{}'.format(settings.SITE_PROJECT, self.shopoffers_type.slug)

    @property
    def is_arrangement(self):
        return bool(self.shopoffers_type.slug == settings.SHOP_ARRANGEMENTS_TYPE_SLUG)

    def get_vote_criterias(self):
        if self.is_arrangement:
            return self.__class__.VOTE_CRITERIAS
        return self.__class__.VOTE_LESSONS_CRITERIAS

    def get_absolute_url(self):
        try:
            return reverse('shop-detail',
                           kwargs={'slug': self.slug, 'shopoffers_type_slug': self.shopoffers_type.slug})
        except NoReverseMatch:
            return reverse('shop-detail', kwargs={'pk': self.pk})
        except AttributeError as e:
            logger.error(e)
        return '#'

    def get_edit_url(self, user=None):
        if user and user.is_authenticated and user.is_moderator:
            url = 'my_moderator_arrangements_update' if self.is_arrangement else 'my_moderator_lessons_update'
            return reverse(url, kwargs={'pk': self.pk})
        url = 'my_arrangements_update' if self.is_arrangement else 'my_lessons_update'
        return reverse(url, kwargs={'pk': self.pk})

    def gau(self):
        return self.get_absolute_url()

    def get_buy_url(self):
        return reverse('shop-buy',
                       kwargs={'slug': self.slug,
                               'shopoffers_type_slug': self.shopoffers_type.slug})

    @property
    def get_full_video_thumbnail_url(self):
        if self.video_thumbnail:
            return settings.SITE_ROOT + self.video_thumbnail.url
        return ''

    @property
    def get_composer(self):
        if self.composer:
            return self.composer
        return ''

    @property
    def get_performer(self):
        if self.performer:
            return self.performer
        return ''

    @property
    def get_film(self):
        if self.film:
            return self.film
        return ''

    @property
    def is_freeze(self):
        if self.discount:
            return True
        return self.freeze

    def set_discount(self, discount, freeze=True, date_start=now(), date_end=None, cause='', sale_day=True):
        self.discount = discount
        self.freeze = freeze
        self.date_start_freeze = date_start
        self.date_end_freeze = date_end
        self.cause_freezing = cause
        self.sale_day = sale_day
        self.save(update_fields=['discount', 'freeze', 'date_start_freeze', 'date_end_freeze',
                                 'cause_freezing', 'sale_day'])

    def price_save(self, force=False):
        from shop.utils import item_price_save
        item_price_save(self, save_force=force)

    def get_price_currency(self, currency):
        '''
        Возвращаем цену в зависимости от валюты.
        :param currency: Валюта, объект или ISO Code
        :return: price, currency
        '''
        if isinstance(currency, Currency):
            currency = currency
        else:
            currency = Currency.objects.get(iso_code=currency)

        if not self.price:
            return Decimal(0), currency

        price_attr = 'price_{}'.format(currency.iso_code.lower())
        if hasattr(self, price_attr) and getattr(self, price_attr):
            return getattr(self, price_attr), currency
        elif ShopOfferPrice.objects.filter(item=self, currency=currency).exists():
            price = ShopOfferPrice.objects.get(item=self, currency=currency).price
        else:
            logger.debug('{} {} Не имеет цены в {}. Пробуем сделать.'.format(self.id, self.name, currency))
            self.price_save()
            if ShopOfferPrice.objects.filter(item=self, currency=currency).exists():
                price = ShopOfferPrice.objects.get(item=self, currency=currency).price
            else:
                currency = self.currency
                price = self.price
        return Decimal(price), currency

    def get_price_country(self, country):
        '''
        Возвращаем цену и валюту, с учетом особенностями страны
        :param country:
        :return: real_price, currency
        '''
        from gis.models import Country
        if isinstance(country, Country):
            country = country
        else:
            country = Country.objects.get(code=country)
        # logger.debug('get_price_country country {}'.format(country))
        if country.currency:
            price, currency = self.get_price_currency(country.currency)
        else:
            currency_def = Currency.objects.get(iso_code=settings.DEFAULT_CURRENCY_ISO_CODE)
            price, currency = self.get_price_currency(currency_def)
        if self.user_created.reg_country and self.user_created.is_country_coefficient:
            real_price = price / Decimal(self.user_created.reg_country.ratio_price) * Decimal(country.ratio_price)
        elif self.user_created.is_country_coefficient:
            real_price = price * Decimal(country.ratio_price)
        else:
            real_price = price
        if currency.min_price > 0 and currency.min_price > real_price:
            real_price = currency.min_price
        real_price = Decimal(real_price)
        if currency.iso_code == currency.RUB:
            return real_price.quantize(Decimal('1'), ROUND_CEILING), currency
        return real_price.quantize(Decimal('1.00'), ROUND_HALF_UP), currency

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        super().save(force_insert=force_insert, force_update=force_update, using=using,
                     update_fields=update_fields)
        if not self.is_valid():
            cart_items = self.cartitem_set.all()
            cart_items.delete()

    def seo_save(self, force=False):
        """
            СЕО Параметры заполняем автоматически, если есть из чего.
        """
        if self.is_arrangement:
            self.seo_save_arrangement(force)
        else:
            self.seo_save_lesson(force)

    def seo_save_arrangement(self, force=False):
        """
            СЕО Параметры заполняем автоматически, если есть из чего.
            титл: Название аранжировки на гитаре. Ноты и табы для гитары.
            описание: Авторская аранжировка для гитары с нотами и табами - название аранжировки, КомпозиторИлиИсполнитель.
                      Это переложение для гитары от Автор.
            ключевые слова: Аранжировка, ноты, табы, Название аранжировки, КомпозиторИлиИсполнитель, гитара, переложение, автор, Автор
        """
        words_limit = 100
        if force or self.seo_title == '':
            for lang_code in settings.MODELTRANSLATION_LANGUAGES:
                with translation.override(lang_code):
                    seo_title = '{name} {text_name}. {text}.'.format(name=self.name,
                                                                     text_name=_('на гитаре'),
                                                                     # author=self.user_created,
                                                                     text=_('Ноты и табы для гитары'))
                    field_seo_title = '{fld}_{lang_code}'.format(fld='seo_title', lang_code=lang_code.replace('-', '_'))
                    setattr(self, field_seo_title, seo_title)
            # новая схема только для русского языка:
            # Ноты и табы [Название песни мелодии] для гитары скачать
            setattr(self, 'seo_title_ru', 'Ноты и табы %s для гитары скачать.' % self.name)
        if force or self.seo_metadesc == '':
            for lang_code in settings.MODELTRANSLATION_LANGUAGES:
                with translation.override(lang_code):
                    field_description = '{fld}_{lang_code}'.format(fld='description',
                                                                   lang_code=lang_code.replace('-', '_'))
                    if getattr(self, field_description):
                        description = get_meta_from_description(getattr(self, field_description, ''), words_limit)
                    else:
                        description = '{text} - {name}, {composer_performer}. {text2} {author}.'.format(
                            text=_('Авторская аранжировка для гитары с нотами и табами'),
                            name=self.name,
                            composer_performer=self.get_cpf(),
                            text2=_('Это переложение для гитары от'),
                            author=self.user_created
                        )
                    field_seo_metadesc = '{fld}_{lang_code}'.format(fld='seo_metadesc',
                                                                    lang_code=lang_code.replace('-', '_'))
                    setattr(self, field_seo_metadesc, description)
            # новая схема только для русского языка:
            # description + guitarsolo.info – самая большая база нот и табов для гитары.
            description = '{text} - {name}, {composer_performer}. {text2} {author}.'.format(
                text=_('Авторская аранжировка для гитары с нотами и табами'),
                name=self.name,
                composer_performer=self.get_cpf(),
                text2=_('Это переложение для гитары от'),
                author=self.user_created
            )
            description = description + ' ' + 'guitarsolo.info – самая большая база нот и табов для гитары.'
            setattr(self, 'seo_metadesc_ru', description)

        if force or self.seo_metakeywords == '':
            for lang_code in settings.MODELTRANSLATION_LANGUAGES:
                with translation.override(lang_code):
                    seo_metakeywords = '{text}, {name}, {composer_performer}, {text2}, {author}'.format(
                        text=_('Аранжировка, ноты, табы'),
                        name=self.name,
                        composer_performer=self.get_cpf(),
                        text2=_('гитара, переложение, автор'),
                        author=self.user_created
                    )
                    field_seo_metakeywords = '{fld}_{lang_code}'.format(fld='seo_metakeywords',
                                                                        lang_code=lang_code.replace('-', '_'))
                    setattr(self, field_seo_metakeywords, seo_metakeywords)
        self.save()

    def seo_save_lesson(self, force=False):
        """
            СЕО Параметры заполняем автоматически, если есть из чего.
            титл: Название урока - урок для гитары.
            описание: Подробный урок для гитаристов на тему: Название урока. Видео и конспект PDF.
            ключевые слова: Название урока разбитое запятыми, урок, обучение, видео, гитара, гитарист
        """
        words_limit = 100
        if force or self.seo_title == '':
            for lang_code in settings.MODELTRANSLATION_LANGUAGES:
                with translation.override(lang_code):
                    seo_title = '{name} - {text}.'.format(name=self.name, text=_('урок для гитары'))
                    field_seo_title = '{fld}_{lang_code}'.format(fld='seo_title', lang_code=lang_code.replace('-', '_'))
                    setattr(self, field_seo_title, seo_title)
        if force or self.seo_metadesc == '':
            for lang_code in settings.MODELTRANSLATION_LANGUAGES:
                with translation.override(lang_code):
                    field_description = '{fld}_{lang_code}'.format(fld='description',
                                                                   lang_code=lang_code.replace('-', '_'))
                    if getattr(self, field_description):
                        description = get_meta_from_description(getattr(self, field_description, ''), words_limit)
                    else:
                        description = '{text}: {name}. {text2}.'.format(
                            text=_('Подробный урок для гитаристов на тему'),
                            name=self.name,
                            text2=_('Видео и конспект PDF.'),
                        )
                    field_seo_metadesc = '{fld}_{lang_code}'.format(fld='seo_metadesc',
                                                                    lang_code=lang_code.replace('-', '_'))
                    setattr(self, field_seo_metadesc, description)
        if force or self.seo_metakeywords == '':
            for lang_code in settings.MODELTRANSLATION_LANGUAGES:
                with translation.override(lang_code):
                    # Название урока разбитое запятыми, урок, обучение, видео, гитара, гитарист
                    splited_name = []
                    for word in self.name.split(' '):
                        if len(word) > 3:  # исключаем предлоги, тире и тд
                            splited_name.append(word)
                    seo_metakeywords = '{splited_name}, {text2}'.format(
                        splited_name=','.join(splited_name),
                        text2=_('урок, обучение, видео, гитара, гитарист'),
                    )
                    field_seo_metakeywords = '{fld}_{lang_code}'.format(fld='seo_metakeywords',
                                                                        lang_code=lang_code.replace('-', '_'))
                    setattr(self, field_seo_metakeywords, seo_metakeywords)
        self.save()

    def is_valid(self):
        if self.is_delete:
            m = 'Not valid offer, is_delete'
            # logger.debug('OFFER {id} Message: {m}'.format(id=self.pk, m=m))
            return False

        if not self.user_created:
            m = 'Not valid offer, not user_created'
            # logger.debug('OFFER {id} Message: {m}'.format(id=self.pk, m=m))
            return False

        if not self.is_available:
            m = 'Not valid offer, not is_available'
            # logger.debug('OFFER {id} Message: {m}'.format(id=self.pk, m=m))
            return False

        if not self.currency:
            m = 'Not valid offer, not currency'
            # logger.debug('OFFER {id} Message: {m}'.format(id=self.pk, m=m))
            return False

        if not self.moderator and not self.user_created.is_premoderation:
            m = 'Not valid offer, not moderator'
            # logger.debug('OFFER {id} Message: {m}'.format(id=self.pk, m=m))
            return False

        if not self.checked and not self.user_created.is_premoderation:
            m = 'Not valid offer, not checked'
            # logger.debug('OFFER {id} Message: {m}'.format(id=self.pk, m=m))
            return False

        return True

    def get_cpf(self):
        cpf_name = ''
        if self.composer:
            cpf_name = self.composer.name
        elif self.performer:
            cpf_name = self.performer.name
        elif self.film:
            cpf_name = self.film.name
        return cpf_name

    @property
    def cpf(self):
        return self.get_cpf()

    @property
    def cpf_object(self):
        if self.composer:
            return self.composer
        elif self.performer:
            return self.performer
        elif self.film:
            return self.film
        return None

    @property
    def title(self):
        cpf = self.get_cpf()
        if cpf:
            return '{name} - {cpf}'.format(name=self.name, cpf=cpf)
        # logger.debug('Аранжировка {} {} не имеет композитора, исполнителя, фильм.'.format(self.pk, self.name))
        return '{name}'.format(name=self.name)

    @property
    def preview(self):
        if self.video_thumbnail:
            return mark_safe('<img src="{url}" title="{title}" alt="{title}"  />'.format(url=self.video_thumbnail.url,
                                                                                         title=self.title))
        return None

    @property
    def thumbnail(self):
        if self.video_thumbnail:
            return self.video_thumbnail.url
        return static('base/images/no-image-available.png')

    @property
    def difficulty_range(self):
        return range(self.difficulty)

    def is_moder(self):
        if self.moderator:
            return True
        return False

    def purchase(self, payment=None, **kwargs):
        user = payment.user if payment else kwargs.get('user')
        # создаем покупку
        if not self.is_free and not payment:
            raise ValueError('Платный товар должен иметь счет. Offer {} {}'.format(self.pk, self))
        if not user:
            raise ValueError('Нет покупателя. Offer {} {}'.format(self.pk, self))

        if payment:
            logger.info('Оформляю покупку в разделе Магазин. Offer {} {}, user {}, payment {}'.format(
                self.pk,
                self,
                user.log_name,
                payment.pk))
        else:
            logger.info('Оформляю бесплатную покупку в разделе Магазин. Offer {} {}, user {}'.format(
                self.pk,
                self,
                user.log_name, ))

        from cart.models import PurchaseUser, OTHER, YANDEX, PAYPAL, PAYSSION
        # if PurchaseUser.objects.filter(user=user, shop_offer=self).exists():
        #     p = PurchaseUser.objects.filter(user=user, shop_offer=self)[0]
        #     logger.info('Покупка есть, возвращаем её. purchase {}, payment {}'.format(p.pk, p.payment))
        #     return p
        if payment and payment.yam_payment is not None:
            payment_type = YANDEX
        elif payment and payment.pp_payment is not None:
            payment_type = PAYPAL
        elif payment and payment.payssion_payment is not None:
            payment_type = PAYSSION
        else:
            payment_type = OTHER
        purchase = PurchaseUser.objects.create(user=user,
                                               shop_offer=self,
                                               payment=payment,
                                               type=payment_type
                                               )
        count = self.purchaseuser_set.count()
        setattr(self, 'pay_count', count)
        self.save(update_fields=['pay_count'])
        return purchase

    def user_buy(self, user, need_secret=True):
        if user.is_anonymous:
            return False
        from cart.models import PurchaseUser
        try:
            if need_secret:
                return PurchaseUser.objects.get(user=user, shop_offer=self).secret
            return PurchaseUser.objects.filter(user=user, shop_offer=self).exists()
        except PurchaseUser.DoesNotExist:
            return False

    def get_purchase(self, user):
        if user.is_anonymous:
            return None
        from cart.models import PurchaseUser
        try:
            return self.purchaseuser_set.get(user=user)
        except PurchaseUser.DoesNotExist:
            return None

    @property
    def is_buy(self):
        # Покупки были?
        from cart.models import PurchaseUser
        return PurchaseUser.objects.filter(shop_offer=self).exists()

    def get_json(self):
        from django.core import serializers
        return serializers.serialize('json', [self, ], fields=('name', 'user_created', 'price', 'currency', 'discount'))

    @property
    def short_song_text(self):
        rows = self.song_text.split('\n')
        return '\n'.join(rows[:4])

    @property
    def collapse_song_text(self):
        rows = self.song_text.split('\n')
        return '\n'.join(rows[4:])

    def get_tags(self):
        if self.tags.all().exists():
            return self.tags.all()
        return []

    @property
    def user_model_file(self):
        return UserFile

    @property
    def get_tag_model(self):
        return TagOffers

    @property
    def get_model_purchase(self):
        from cart.models import PurchaseUser
        return PurchaseUser

    @property
    def video_duration_in_sec(self):
        if self.video_duration:
            import isodate
            return int(isodate.parse_duration(self.video_duration).total_seconds())

    @property
    def vote_criterias_title(self):
        return _('Оцените покупку')

    @property
    def vote_criterias_description(self):
        return _('Оцените качественные характеристики купленной аранжировки')

    class Meta:
        ordering = ['-pk', ]


class ShopOfferPrice(models.Model):
    currency = models.ForeignKey(Currency, on_delete=models.CASCADE, verbose_name=_('Валюта'))
    item = models.ForeignKey(ShopOffers, on_delete=models.CASCADE, verbose_name=_('Аранжировка'))
    price = models.DecimalField(default=Decimal('0.00'), verbose_name=_('Цена'), max_digits=15, decimal_places=2,
                                help_text=_('только цифры'))

    class Meta:
        unique_together = (('currency', 'item'),)

    def __str__(self):
        return "{} {} {}".format(self.item.name, self.price, self.currency.short_name)


class Lesson(models.Model):
    """
    Таблица содержащая поля только для уроков
    """
    offer = models.OneToOneField(ShopOffers, on_delete=models.CASCADE, primary_key=True, verbose_name=_('Товар'))
    content = models.TextField(_('Подробное описание урока'), default='')
    video_content_url = EmbedVideoField(max_length=2000, verbose_name=_('Ссылка на полное видео урока (Youtube)'),
                                        help_text=_('Продолжительность видео должна состовлять 5-10 минут.'))

    class Meta:
        ordering = ['-pk', ]
        verbose_name = _('Информация по уроку')
        verbose_name_plural = _('Данные по урокам')


class MimeTypeFile(models.Model):
    name = models.CharField(default='', max_length=255, verbose_name=_('Описание типа файла.'))
    mime_type = models.CharField(default='', max_length=5, verbose_name=_('Тип файла. Расширение.'))
    autodetect = models.BooleanField(default=False, verbose_name=_('Автоопределение'))

    class Meta:
        verbose_name = _("Тип файла")
        verbose_name_plural = _("Типы файлов")

    def __str__(self):
        return '{} {}'.format(self.name, self.mime_type.upper())


class UserFile(DateModelsMixin, models.Model):
    """
    При загрузке файл не ассоциируетя с товаром: item = None
    Ассоциация происходит при сохранении изменений в товаре.
    Возможна ситуация с брошенными файлами. @todo вычищать брошенные файлы старше суток
    """
    user_created = models.ForeignKey('users.User', verbose_name=_('Пользователь'), on_delete=models.CASCADE)
    item = models.ForeignKey(ShopOffers, verbose_name=_('Товар'), on_delete=models.SET_NULL, blank=True, null=True)
    file = models.FileField(verbose_name=_('Файл'),
                            storage=FileSystemStorage(location=settings.FILES_ROOT, base_url=settings.FILES_URL),
                            upload_to=user_directory_path)
    user_filename = models.CharField(default='', max_length=255, verbose_name=_('Название файла.'),
                                     help_text=_('Название файла у пользователя'))
    mime_type = models.CharField(default='', max_length=5, verbose_name=_('Тип файла. Расширение.'))
    page_count = models.PositiveSmallIntegerField(default=0, verbose_name=_('Количество страниц'),
                                                  help_text=_('Количество страниц. Актуально для pdf файлов'),
                                                  editable=False)
    page = models.PositiveSmallIntegerField(null=True, blank=True, verbose_name=_('Номер страницы'),
                                            help_text=_('Страница, номер отображения'),
                                            editable=False)

    format_file = models.ForeignKey(MimeTypeFile, verbose_name=_('Тип файла'), on_delete=models.SET_NULL, blank=True,
                                    null=True)

    note = models.CharField(verbose_name=_('Заметка'), max_length=500,
                            help_text=_('Краткое описание содержания файла.'), blank=True, null=True)

    def __str__(self):
        if self.item:
            return '%s (%s)' % (self.item.name, self.modified_date.strftime('%d.%m.%Y %H:%M'))
        return 'File №%d' % self.pk

    # @property
    # def user(self):
    #     if self.item:
    #         return self.item.user_created

    @property
    def st_size(self):
        try:
            return os.path.getsize(self.file.path)
        except FileNotFoundError:
            return 0

    @property
    def size(self):
        return convert_size(self.st_size)

    def get_file_path(self):
        return self.file.path

    def get_file_url(self):
        return self.file.url

    def get_absolute_url(self):
        return reverse('user-get-file', kwargs={'pk': self.pk})

    def get_buyer_url(self):
        return reverse('buyer-get-file', kwargs={'pk': self.pk})

    def detect(self):
        if self.mime_type:
            try:
                obj, created = MimeTypeFile.objects.update_or_create(mime_type=self.mime_type)
                if created:
                    from django.core.mail import send_mail
                    send_mail(subject='Новый тип файла {}'.format(self.mime_type),
                              message='Новый тип файла {}'.format(self.mime_type),
                              from_email=settings.SUPPORT_EMAIL,
                              recipient_list=[settings.SUPPORT_EMAIL, ],
                              )
            except MimeTypeFile.MultipleObjectsReturned:
                pass

            if MimeTypeFile.objects.filter(mime_type=self.mime_type, autodetect=True).exists():
                try:
                    self.format_file = MimeTypeFile.objects.get(mime_type=self.mime_type, autodetect=True)
                except MimeTypeFile.MultipleObjectsReturned:
                    logger.error(
                        'Автоопределение файла не сработало, '
                        'файлов с типом {} autodetect, больше двух.'.format(self.mime_type))

    def save(self, **kwargs):
        self.detect()
        return super().save(**kwargs)

    def delete(self, *args, **kwargs):
        self.file.delete()
        return super().delete(**kwargs)


class ShopOffersProxy(ShopOffers):

    def __str__(self):
        return '{}-{}'.format(self.name, self.user_created)

    class Meta:
        proxy = True


class Sale(UploadToMixin, DirtyFieldsMixin, SeoFieldsModelMixin, IsActiveMixin, FullUrlMixin):
    statsy_group = 'sale'
    STATUS = namedtuple('STATUS', 'draft waiting run finished expired adorted cancelled archive')._make(range(8))
    STATUS_CHOICES = [
        (STATUS.draft, 'Черновик'),
        (STATUS.waiting, 'Ожидает старта'),
        (STATUS.run, 'Работает'),
        (STATUS.finished, 'Закончена'),
        (STATUS.expired, 'Просрочена'),
        (STATUS.adorted, 'Прерванная'),
        (STATUS.cancelled, 'Отменённая'),
        (STATUS.archive, 'Архивная'),
    ]
    name = models.CharField(verbose_name=_('Название'), max_length=255, blank=True)
    title = models.CharField(_('Заголовок'), max_length=255, blank=True)
    announce = models.TextField(_('Анонс'),
                                blank=True,
                                default='',
                                help_text=_('Краткий анонс вашей статьи. '
                                            'Будет выводиться вместе с миниатюрой в списке статей раздела. '
                                            'Если оставить пустым, в качестве анонса будут использоваться первые '
                                            'предложения основного текста.'))
    description = models.TextField(verbose_name=_('Описание'), blank=True)

    date_pub = models.DateTimeField(_('Дата публикации'), blank=True, null=True)

    date_start = models.DateTimeField(null=True, blank=True, verbose_name=_('Время начала'))
    date_end = models.DateTimeField(null=True, blank=True, verbose_name=_('Время окончания'))
    slug = AutoSlugField(populate_from='name',
                         max_length=260,
                         unique=True,
                         slugify=slugify,
                         db_index=True,
                         always_update=False,
                         editable=True, blank=True)
    min_discount = models.PositiveSmallIntegerField(_('Минимальная скидка'), default=35, help_text=_('Скидка, в %'))
    max_discount = models.PositiveSmallIntegerField(_('Максимальная скидка'), default=60, help_text=_('Скидка, в %'))
    step_discount = models.PositiveSmallIntegerField(_('Шаг скидки'), default=5, help_text=_('Скидка, в %'))

    status = models.PositiveIntegerField(choices=STATUS_CHOICES, default=STATUS.draft, db_index=True,
                                         verbose_name='Статус')
    actual_start_date = models.DateTimeField(null=True, blank=True, verbose_name=_('Дата фактического начала'),
                                             editable=False)
    actual_end_date = models.DateTimeField(null=True, blank=True, verbose_name=_('Дата фактического окончания'),
                                           editable=False)

    image = models.ImageField(_('Изображение'),
                              help_text=_('Плашка вверху страницы'),
                              upload_to=upload_sale_image, blank=True, )

    composer = models.ManyToManyField(Composer, blank=True, verbose_name=_('Композитор'))
    performer = models.ManyToManyField(Performer, blank=True, verbose_name=_('Эстрадный исполнитель'))
    film = models.ManyToManyField(Film, blank=True, verbose_name=_('Кинофильм'))
    seller = models.ManyToManyField('users.User', blank=True, verbose_name=_('Автор'), )
    tag = models.ManyToManyField(TagOffers, blank=True, verbose_name=_('Тег'), )

    lang = models.ManyToManyField('lang.Language', blank=True)
    country = models.ManyToManyField('gis.Country', blank=True)
    offer = models.ManyToManyField(ShopOffers, blank=True, related_name='offer')
    shopoffers_type = models.ManyToManyField(ShopOffersType, verbose_name=_('Тип товаров'),
                                             related_name='shopoffers_type',
                                             blank=True)
    show_offer = models.BooleanField(default=True, help_text=_('Показывать товары на странице распродажи'))
    sale_day_on = models.BooleanField(default=False, help_text=_('Скидки дня включены'))

    preview_sale_offers = models.ManyToManyField(ShopOffers, blank=True, editable=False,
                                                 related_name='preview_sale_offers',
                                                 help_text=_("Товар, который попадет в распродажу."))
    sale_offers = models.ManyToManyField(ShopOffers, blank=True, editable=False, related_name='sale_offers',
                                         help_text=_("Товар, который попал в распродажу."))

    objects = SaleQuerySet.as_manager()

    class Meta:
        verbose_name = _('распродажа')
        verbose_name_plural = _('распродажи')

    def __str__(self):
        return self.name

    def get_announce(self, limit=50):
        if self.announce:
            return self.announce
        cleaner.style = True
        cleaner.kill_tags = ['img']
        from lxml.etree import ParserError
        try:
            return mark_safe(truncatewords_html(cleaner.clean_html(self.description), limit))
        except ParserError:
            return ''

    @property
    def short_announce(self):
        return self.get_announce(limit=10)

    @property
    def is_global(self):
        if self.composer.exists() or \
                self.performer.exists() or \
                self.film.exists() or \
                self.seller.exists() or \
                self.tag.exists() or \
                self.offer.exists():
            return False
        return True

    def valid_offer(self, item):
        if item.sale_offers.filter(status=Sale.STATUS.run).exists():
            logger.info('Аранжировка уже участвует в распродаже {}'.format(item))
            # Аранжировка уже участвует в распродаже.
            return False

        if self.is_global:
            return True

        if self.shopoffers_type.all().exists():
            # Выбран тип распродажи
            if not self.shopoffers_type.filter(id=item.shopoffers_type_id).exists():
                return False

        if self.composer.filter(id=item.composer_id).exists():
            return True

        if self.performer.filter(id=item.performer_id).exists():
            return True

        if self.film.filter(id=item.film_id).exists():
            return True

        if self.seller.filter(id=item.user_created_id).exists():
            return True

        if self.offer.filter(id=item.id).exists():
            return True

        if self.tag.filter(id__in=list(item.tags.values_list('id', flat=True))).exists():
            return True
        return False

    @property
    def run(self):
        return self.status == Sale.STATUS.run

    @property
    def cause_freeze(self):
        return _('Распродажа {}'.format(self.name))

    def preview_add_offers(self):
        offers = ShopOffers.objects.published(
            user_created__not_agree_promotion=False,
            user_created__is_sale=True,
            price__gt=Decimal('0.00'),
            sale_day=False,
        )
        self.preview_sale_offers.clear()
        for offer in offers:
            if self.valid_offer(offer):
                self.preview_sale_offers.add(offer)

    def add_offer(self, item, force=False):
        if self.valid_offer(item) and not force:
            if self.step_discount == 0:
                discount = self.max_discount
            else:
                discount = random.randrange(self.min_discount, self.max_discount + self.step_discount,
                                            self.step_discount)
            item.set_discount(discount,
                              freeze=True,
                              date_start=self.date_start,
                              date_end=self.date_end,
                              cause=self.cause_freeze,
                              sale_day=False)
            self.sale_offers.add(item)
            from lang.models import Language
            for lang in Language.objects.all():
                cache_key = 'sale-{lang}-{id}'.format(
                    lang=lang.code, id=self.pk)
                from django.core.cache import cache
                cache.delete(key=cache_key, version=settings.CACHES_VERSION)

    def set_offers_discount(self):
        offers = ShopOffers.objects.published(
            user_created__not_agree_promotion=False,
            user_created__is_sale=True,
            price__gt=Decimal('0.00'),
            sale_day=False,
            discount=0
        )
        for offer in offers:
            self.add_offer(offer)

    def up(self, force=False):
        # Нормальное включение
        if self.is_active and self.status == self.STATUS.waiting and self.date_start <= now() or force:
            if self.date_end and now() >= self.date_end:
                self.status = self.STATUS.expired
                self.save()
                return False
            self.status = self.STATUS.run
            self.actual_start_date = now()
            self.save()
            self.set_offers_discount()
            return True
        return False

    def down(self):
        # Выключение по плану
        time = now()
        if self.is_active and self.status == self.STATUS.run:
            self.actual_end_date = time
            self.is_active = False
            if self.date_end and self.date_end > time:  # Прерываем раньше времени?
                self.status = self.STATUS.adorted
            else:
                self.status = self.STATUS.finished
            self.sale_offers.all().update(
                discount=0,
                freeze=False,
                date_start_freeze=None,
                date_end_freeze=None,
                cause_freezing=''
            )
            self.save()
            return True
        return False

    def deactivate(self):
        # Мягкая деактивация.
        # Останавливаем распродажу,если она запущена.
        stat = self.down()
        if not stat:
            self.is_active = False
            self.save()

    def get_date_pub(self):
        if self.date_pub:
            return self.date_pub
        return self.date_start

    def get_title(self):
        if self.title:
            return self.title
        return self.name

    def get_absolute_url(self):
        return reverse('sale', kwargs={'slug': self.slug})

    def gau(self):
        return self.get_absolute_url()

    def get_image(self):
        if self.description:
            soup = BeautifulSoup(self.description, 'html.parser')
            images = soup.find_all('img')
            try:
                return images[0]['src']
            except KeyError:
                pass
        return None

    def create_new(self):
        from news.models import News
        for lang in self.lang.all():
            with translation.override(lang.code):
                kwargs = dict(
                    title=self.get_title(),
                    announce=self.short_announce,
                    content=self.description,
                    pub_date=self.get_date_pub(),
                    end_date=self.date_end,
                    lang=lang.code,
                    is_active=True,
                    can_comment=True,
                )
                new, created = News.objects.update_or_create(url=self.get_absolute_url(),
                                                             defaults=kwargs)

                if self.get_image():
                    new_path = settings.PUBLIC_ROOT + self.get_image()
                    try:
                        with open(new_path, 'rb') as f:
                            new.image.save(basename(new_path), f)
                    except FileNotFoundError:
                        pass

    @property
    def get_full_image_url(self):
        if self.image:
            return settings.SITE_ROOT + self.image.url
        return ''


class ShopOffersHandler(OfferRatingHandler):
    pass


class ShopOffersDelete(ShopOffers):
    class Meta:
        proxy = True


ratings.register(ShopOffers, ShopOffersHandler)
