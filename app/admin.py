from django import forms
from django.contrib import admin, messages
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.contenttypes.admin import GenericTabularInline
from django.db import models
from django.db.models import Q
from django.urls import reverse_lazy
from django.utils.safestring import mark_safe
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _
from import_export import resources
from import_export.admin import ImportExportMixin
from modeltranslation.admin import TranslationTabularInline
from reversion.admin import VersionAdmin

from base.admin import column
from base.mixins import MetaTranslationAdminMixin
from base.utils import get_translation_fieldnames
from offer.admin import TagsTranslationAdmin
from ratings.models import Score, Vote
from redactor.widgets import AdminRedactorEditor
from shop.models import Composer, Film, Lesson, MimeTypeFile, Performer, Sale, ShopOfferPrice, ShopOffers, \
    ShopOffersDelete, ShopOffersType, TagOffers, TagOffersCategory, Techniques, UserFile
from shop.tasks import set_daily_sale, task_sale_up, task_set_preview_sale_offers
from shop.utils import notifying_all_users_about_discounts
from users.models import User


class TagOffersResource(resources.ModelResource):
    class Meta:
        model = TagOffers
        import_id_fields = ('slug',)
        exclude = ('id', 'countrys', 'langs')
        export_order = ('slug', 'name',)


@admin.register(ShopOffersType)
class AdminShopOffersCategory(MetaTranslationAdminMixin):
    pass


@admin.register(Techniques)
class AdminTechniques(MetaTranslationAdminMixin):
    pass


class NotEnTranslateNameDesc(admin.SimpleListFilter):
    title = 'Нет перевода'
    parameter_name = 'noten'

    def lookups(self, request, model_admin):
        return (
            ('No', 'Нет Перевода'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'No':
            q = Q(name_en='') | Q(description_en='')
            return queryset.filter(q)


class NotEnTranslateName(admin.SimpleListFilter):
    title = 'Нет перевода'
    parameter_name = 'noten'

    def lookups(self, request, model_admin):
        return (
            ('No', 'Нет Перевода'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'No':
            return queryset.filter(Q(name_en='') | Q(name_en__isnull=True))


class CPFAdminMix(MetaTranslationAdminMixin):
    list_display = ('id', 'name', 'name_ru', 'name_en', 'count_item', 'accepted')
    search_fields = get_translation_fieldnames('name')
    list_filter = [NotEnTranslateName, ]
    raw_id_fields = ['user']
    fieldsets = (
        (None, {
            'fields': ('name', 'description', 'user', 'accepted', 'synonyms')
        }),
        ('SEO', {
            'fields': ('slug', 'seo_title', 'seo_metadesc', 'seo_metakeywords')
        }),

    )


@admin.register(Composer)
class AdminShopComposer(CPFAdminMix):
    pass


@admin.register(Performer)
class AdminShopPerformer(CPFAdminMix):
    pass


@admin.register(Film)
class AdminShopFilm(CPFAdminMix):
    pass


class UserFileInline(TranslationTabularInline):
    model = UserFile
    extra = 0
    raw_id_fields = ['user_created', ]


class ShopOfferPriceLine(admin.TabularInline):
    model = ShopOfferPrice
    extra = 0


class ModerListFilter(admin.SimpleListFilter):
    title = _('Mодератор')
    parameter_name = 'moder'

    def lookups(self, request, model_admin):
        return (
            ('Yes', _('Модератор есть')),
            ('No', _('Модератора нет')),
        )

    def queryset(self, request, queryset):
        if self.value() == 'Yes':
            return queryset.filter(moderator__isnull=False)
        if self.value() == 'No':
            return queryset.filter(moderator__isnull=True)


class AuthorListFilter(admin.SimpleListFilter):
    title = _('Автор')
    parameter_name = 'author'

    def lookups(self, request, model_admin):
        try:
            return ((offer.user_created.pk, offer.user_created)
                    for offer in ShopOffers.objects.published().order_by('user_created_id').distinct('user_created'))
        except NotImplementedError as e:
            return ((offer.user_created.pk, offer.user_created)
                    for offer in ShopOffers.objects.published().order_by('user_created_id'))

    def queryset(self, request, queryset):
        if self.value() and self.value().isdigit():
            return queryset.filter(user_created__pk=int(self.value()))


class ShopOffersAdminForm(forms.ModelForm):
    seo_save = forms.BooleanField(help_text=_('Перегенерировать SEO при сохранение'), required=False)
    price_save = forms.BooleanField(help_text=_('Обновить цены при сохранение'), required=False)

    def __init__(self, *args, **kwargs):
        super(ShopOffersAdminForm, self).__init__(*args, **kwargs)

    class Meta:
        model = ShopOffers
        exclude = []
        widgets = {
            'tags': FilteredSelectMultiple(verbose_name=_('Тэги'), is_stacked=False),
            'song_langs': FilteredSelectMultiple(verbose_name=_('Языки на которых будет выведен текст песни'),
                                                 is_stacked=False),
        }


class ScoreInline(GenericTabularInline):
    model = Score


class VoteInline(GenericTabularInline):
    model = Vote
    readonly_fields = ['user', 'ip_address']


class LessonInline(TranslationTabularInline):
    model = Lesson


class TechniquesInlineAdmin(admin.TabularInline):
    model = ShopOffers.techniques.through


@admin.register(ShopOffers)
class AdminShopOffers(MetaTranslationAdminMixin):
    form = ShopOffersAdminForm
    readonly_fields = ['modified_date', 'is_delete', 'user_delete', ]
    list_display = ('id', 'name_ru', 'name_en', 'user_created', 'shopoffers_type', 'moderator', 'checked')
    raw_id_fields = ['user_created', 'moderator', 'composer', 'performer', 'film']
    inlines = [LessonInline, UserFileInline, ShopOfferPriceLine, TechniquesInlineAdmin]
    list_filter = ['shopoffers_type', 'discount', 'freeze', 'sale_day', 'is_delete',
                   NotEnTranslateNameDesc, 'is_available', ModerListFilter, 'checked',
                   'agree_promotion',
                   'is_bestseller',

                   # Всегда в конце
                   'tags', AuthorListFilter,
                   ]
    search_fields = ('user_created__first_name', 'user_created__last_name', 'slug') + get_translation_fieldnames('name')
    date_hierarchy = 'date_created'
    actions = ['send_list_of_arrangements_to_e_mail']

    def send_list_of_arrangements_to_e_mail(self, request, queryset):
        from dashboard.utils import item_import
        item_import(request.user)

    send_list_of_arrangements_to_e_mail.short_description = 'Отправить список аранжировок на e-mail'

    fieldsets = (
        (None, {
            'fields': ('shopoffers_type', 'name', 'tags', 'description', 'is_available', 'image',
                       'composer', 'performer', 'film', 'difficulty', 'tonality',
                       'file', 'agree_promotion', 'author_music', 'is_bestseller',
                       'lang', 'synonyms')
        }),
        ('Song Text', {
            'fields': ('song_text', 'song_file', 'song_langs')
        }),
        ('Price', {
            'fields': ('price', 'currency', 'discount', 'price_rub', 'price_usd', 'price_eur', 'price_save')
        }),
        ('video', {
            'fields': ('video_url', 'video_thumbnail',
                       'video_thumbnail_url',
                       'video_start', 'video_upload_date', 'video_duration',)}),
        ('SEO', {
            'fields': ('slug', 'seo_title', 'seo_metadesc', 'seo_metakeywords', 'seo_save')
        }),
        ('manifesto', {
            'fields': ('user_created',)
        }),
        ('Moder', {
            'fields': ('moderator', 'checked', 'date_approval_moderator')
        }),
        ('freeze', {
            'fields': ('freeze', 'date_start_freeze', 'date_end_freeze', 'cause_freezing')
        }),
        ('note', {
            'fields': ('service_note',)
        }),
        ('system info', {
            'fields': ('modified_date', 'user_delete', 'is_delete')
        }),
    )

    def get_moder(self, obj):
        pass

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.filter(is_delete=False)

    def delete_queryset(self, request, queryset):
        """Given a queryset, delete it from the database."""
        for q in queryset:
            q.delete(user_obj=request.user)

    def save_model(self, request, obj, form, change):
        if obj.is_delete:
            messages.warning(request, 'Удаленные товары нельзя редактировать.')
            return
        dirty_fileds = obj.get_dirty_fields(check_relationship=True)
        if 'moderator' in dirty_fileds:
            if obj.moderator is None:
                obj.checked = False
            else:
                obj.checked = True
                if not obj.date_approval_moderator:
                    obj.date_approval_moderator = now()

        if 'checked' in dirty_fileds:
            if obj.checked is True:
                if obj.moderator is None:
                    obj.moderator = request.user
                if not obj.date_approval_moderator:
                    obj.date_approval_moderator = now()
        super().save_model(request, obj, form, change)
        if form.cleaned_data['seo_save']:
            obj.seo_save(force=True)
        if form.cleaned_data['price_save']:
            obj.price_save(force=True)
        obj.refresh_from_db()
        if Sale.objects.is_running() and not obj.user_created.not_agree_promotion and obj.user_created.is_sale and obj.price:
            sales = Sale.objects.running()
            for sale in sales:
                sale.add_offer(obj)
            notifying_all_users_about_discounts([obj.id, ])
        if not form.cleaned_data['file']:
            from shop.utils import compression_file
            compression_file(obj)
            messages.info(request, 'Архив файлов был (пере)создан.')


@admin.register(MimeTypeFile)
class AdminMimeTypeFile(admin.ModelAdmin):
    list_display = ('mime_type', 'name', 'autodetect')
    list_filter = ('autodetect', 'mime_type')


@admin.register(UserFile)
class AdminUserFile(admin.ModelAdmin):
    list_display = ('user_filename', 'item', 'date_created', 'modified_date', 'note')
    list_filter = ('mime_type',)

    def delete_queryset(self, request, queryset):
        for obj in queryset:
            self.delete_model(request, obj)


@admin.register(TagOffers)
class AdminTagOffers(ImportExportMixin, TagsTranslationAdmin):
    resource_class = TagOffersResource
    list_display = TagsTranslationAdmin.list_display + ['shopoffers_type', ]
    list_filter = TagsTranslationAdmin.list_filter + ('shopoffers_type',)
    fieldsets = (
        (None, {'fields': ('name', 'shopoffers_type', 'description', 'langs', 'approve', 'hidden')}),
        ('SEO', {'fields': ('slug', 'seo_title', 'seo_metadesc', 'seo_metakeywords')})
    )


#@admin.register(TagOffersCategory)
class AdminTagOffersCategory(MetaTranslationAdminMixin, VersionAdmin):
    list_display = ['name', 'langs_list', ]

    search_fields = get_translation_fieldnames('name')
    list_filter = (
        ('countrys', admin.RelatedOnlyFieldListFilter),
        ('langs', admin.RelatedOnlyFieldListFilter),
    )

    @column(legend='Языки')
    def langs_list(self, p):
        return mark_safe('<br>'.join([x.name for x in p.langs.all()]))

    fieldsets = (
        (None, {'fields': ('name', 'description', 'langs', 'tags')}),
        ('SEO', {'fields': ('slug', 'seo_title', 'seo_metadesc', 'seo_metakeywords')})
    )


class SaleAdminForm(forms.ModelForm):
    publish_news = forms.BooleanField(label=_('Опубликовать новости'), required=False)

    def __init__(self, *args, **kwargs):
        super(SaleAdminForm, self).__init__(*args, **kwargs)
        redactor_widget = AdminRedactorEditor(redactor_settings={
            'toolbarFixed': False,
            'autoformat': True,
            'lang': 'ru',
            'overlay': True,
            'maxHeight': '600px',
            'minHeight': '600px',
            'tidyHtml': True,
            'replaceDivs': True,
            'plugins': ['fontcolor', 'fontsize', 'video', 'table', 'imagemanager', 'fullscreen'],
            'imageUpload': reverse_lazy('upload_images'),
            'imageManagerJson': reverse_lazy('recent_images')
        }, include_jquery=True)

        for field_name in get_translation_fieldnames('description'):
            self.fields[field_name].widget = redactor_widget

    def clean(self):
        cleaned_data = super(SaleAdminForm, self).clean()

        if cleaned_data.get('is_active') is True:
            date_start = cleaned_data.get('date_start')
            if not date_start:
                error = forms.ValidationError(_('Укажите дату начала'), code='invalid date_start')
                self.add_error('date_start', error)
                raise error
            date_end = cleaned_data.get('date_end')
            if date_end and date_start >= date_end:
                error = forms.ValidationError(_('Дата окончания не может быть позже даты начала.'),
                                              code='invalid date_end')
                self.add_error('date_start', error)
                self.add_error('date_end', error)
                raise error

    class Meta:
        model = Sale
        exclude = []

    class Media:
        js = ('django-redactor/redactor/plugins/fullscreen.min.js',
              'django-redactor/redactor/plugins/fontcolor.min.js',
              'django-redactor/redactor/plugins/fontsize.min.js',
              'django-redactor/redactor/plugins/table.min.js',
              'django-redactor/redactor/plugins/video.min.js',
              'django-redactor/redactor/plugins/imagemanager.min.js')


@admin.register(Sale)
class AdminSale(MetaTranslationAdminMixin):
    save_as = True
    save_on_top = True
    formfield_overrides = {
        models.ManyToManyField: {'widget': FilteredSelectMultiple(verbose_name='', is_stacked=False)},
    }

    form = SaleAdminForm

    actions = ['sale_up', 'sale_stop', 'run_set_daily_sale']
    search_fields = get_translation_fieldnames('name')
    list_display = ['name', 'date_start', 'date_end', 'status']
    list_filter = ['status', ]
    prepopulated_fields = {"slug": ("name_en",)}
    readonly_fields = [
        'status', 'sale_offers', 'preview_sale_offers',
        'actual_start_date', 'actual_end_date']

    fieldsets = (
        (None, {
            'fields': ('image', 'name', 'title', 'announce', 'description', 'date_pub', 'date_start', 'date_end')
        }),
        (_('Параметры распродажи'), {
            'fields': ('min_discount', 'max_discount', 'step_discount', 'shopoffers_type',
                       'composer', 'performer', 'film', 'seller', 'tag',
                       'lang', 'country', 'offer', 'show_offer', 'sale_day_on', 'publish_news')
        }),
        (_("СЕО"), {
            'fields': ('slug', 'seo_title', 'seo_metadesc', 'seo_metakeywords',)
        }),

        (_('Статус'), {
            'fields': ('status', 'actual_start_date', 'actual_end_date',
                       'is_active', 'sale_offers', 'preview_sale_offers')
        }),
    )

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == "seller":
            kwargs["queryset"] = User.objects.filter(shopoffers__isnull=False, is_sale=True,
                                                     not_agree_promotion=False).distinct()
        return super().formfield_for_manytomany(db_field, request, **kwargs)

    def save_model(self, request, obj, form, change):
        if obj.status >= obj.STATUS.finished:
            raise Exception(_('Вы не можете редактировать законченные распродажи'))
        if obj.is_active is True:
            if obj.status == obj.STATUS.draft:
                obj.status = obj.STATUS.waiting
        else:
            if obj.status == obj.STATUS.waiting:
                obj.status = obj.STATUS.draft
        super().save_model(request, obj, form, change)
        if form.cleaned_data.get('publish_news'):
            obj.create_new()
        if not obj.is_global:
            task_set_preview_sale_offers.delay(obj.id)
            messages.info(request, 'Запущена задача по просчету возможных аранжировок.')

    def sale_up(self, request, queryset):
        if queryset.count() > 1:
            messages.success(request, 'Вы не можете запустить больше одной распродажи.')
        else:
            task_sale_up.delay(queryset.get().pk, force=True)

    sale_up.short_description = "Запустить распродажу"

    def sale_stop(self, request, queryset):
        for sale in queryset:
            sale.deactivate()
            messages.success(request, 'Распродажа {} остановлена и деактивирована'.format(sale))

    sale_stop.short_description = 'Остановить и деактивировать распродажу.'

    def run_set_daily_sale(self, request, queryset):
        set_daily_sale()
        messages.success(request, 'Перезапустили скидки дня.')

    run_set_daily_sale.short_description = 'Перезапустить скидки дня.'


@admin.register(ShopOffersDelete)
class AdminShopOffersDelete(MetaTranslationAdminMixin):
    list_display = ('id', 'name_ru', 'name_en', 'user_created', 'shopoffers_type')
    raw_id_fields = ['user_created', 'moderator', 'composer', 'performer', 'film']
    inlines = [LessonInline, UserFileInline]
    list_filter = ['shopoffers_type',
                   'tags', AuthorListFilter,
                   ]
    search_fields = ('user_created__first_name', 'user_created__last_name', 'slug') + get_translation_fieldnames('name')
    actions = ['restore_offer', ]

    def restore_offer(self, request, queryset):
        count = queryset.update(is_delete=False)
        messages.info(request, 'Восстановлено {} товар(ов)'.format(count))

    restore_offer.short_description = 'Восстановить товар.'

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields] + ['tags', 'lang', 'song_langs']

    fieldsets = (
        (None, {
            'fields': ('shopoffers_type', 'name', 'tags', 'description', 'is_available', 'image',
                       'composer', 'performer', 'film', 'difficulty', 'tonality',
                       'file', 'agree_promotion', 'author_music', 'is_bestseller',
                       'lang')
        }),
        ('Song Text', {
            'fields': ('song_text', 'song_file', 'song_langs')
        }),
        ('Price', {
            'fields': ('price', 'currency', 'discount', 'price_rub', 'price_usd', 'price_eur',)
        }),
        ('video', {
            'fields': ('video_url', 'video_thumbnail',
                       'video_thumbnail_url',
                       'video_start', 'video_upload_date', 'video_duration',)}),
        ('SEO', {
            'fields': ('slug', 'seo_title', 'seo_metadesc', 'seo_metakeywords',)
        }),
        ('manifesto', {
            'fields': ('user_created',)
        }),
        ('Moder', {
            'fields': ('moderator', 'checked', 'date_approval_moderator')
        }),

        ('note', {
            'fields': ('service_note',)
        }),
        ('system info', {
            'fields': ('modified_date', 'user_delete', 'is_delete')
        }),
    )

    #
    def has_add_permission(self, request):
        pass

    def has_delete_permission(self, request, obj=None):
        pass

    def delete_queryset(self, request, queryset):
        pass

    def save_model(self, request, obj, form, change):
        messages.warning(request, 'Удаленные товары нельзя редактировать.')
        return

    def get_queryset(self, request):
        qs = self.model._default_manager.get_queryset()
        ordering = self.get_ordering(request)
        if ordering:
            qs = qs.order_by(*ordering)
        return qs.filter(is_delete=True)
