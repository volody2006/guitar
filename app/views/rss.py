# -*- coding: utf-8 -*-
import logging
import os
from datetime import timedelta

from django.conf import settings
from django.contrib.syndication.views import Feed
from django.db.models import Q
from django.utils import translation
from django.utils.feedgenerator import Rss201rev2Feed
from django.utils.text import format_lazy
from django.utils.timezone import now
from django.utils.translation import gettext, gettext_lazy

from shop.models import Sale, ShopOffers

logger = logging.getLogger(__name__)


class GetMonthMix:
    def get_month(self, val):
        month = now().strftime("%B")
        language = translation.get_language()
        if language == 'ru':
            months = {1: gettext('Января'),
                      2: gettext('Февраля'),
                      3: gettext('Марта'),
                      4: gettext('Апреля'),
                      5: gettext('Мая'),
                      6: gettext('Июня'),
                      7: gettext('Июля'),
                      8: gettext('Августа'),
                      9: gettext('Сентября'),
                      10: gettext('Октября'),
                      11: gettext('Ноября'),
                      12: gettext('Декабря')}
            return months[val].lower()
        return month


class RssItemLastDayFeed(GetMonthMix, Feed):
    description = ""
    link = "/"
    item_image = None

    def get_lang(self):
        return translation.get_language()

    def title(self):
        text = gettext_lazy('Интересные новинки')
        return format_lazy('{day} {month} {year} - {text}', day=now().day,
                           month=self.get_month(now().month),
                           year=now().year,
                           text=text)

    def items(self):
        result = []
        delta = timedelta(days=1)
        last_day_start = now() - delta
        last_day_start = last_day_start.replace(hour=0, minute=0, second=0)
        last_day_end = last_day_start.replace(hour=23, minute=59, second=59)
        items = ShopOffers.objects.published().filter(date_approval_moderator__range=(last_day_start, last_day_end))
        items = items.filter(Q(lang__code=self.get_lang()) | Q(lang__isnull=True))
        sellers_id = set(items.values_list('user_created_id', flat=True))
        exlude_item = []

        if not items.exists():
            return []

        if items.count() <= 4:
            self.item_image = items[0]
            return [items, ]

        for seller_id in sellers_id:
            item = items.filter(user_created=seller_id).order_by('?').first()
            result.append(item)
            exlude_item.append(item.pk)
        i = 0
        while len(result) < 4:
            new_items = items.exclude(pk__in=exlude_item)
            item = new_items.order_by("?").first()
            result.append(item)
            exlude_item.append(item.pk)
            i += 1
            if i > 10:
                break
        self.item_image = result[0]
        return [result[:4], ]

    def item_title(self, item):
        text = gettext('Интересные новинки на')
        return format_lazy('{day} {month} {year} {text} {short_project_name}.', day=now().day,
                           month=self.get_month(now().month), year=now().year,
                           text=text, short_project_name=settings.SITE_INFO['short_project_name'])

    def item_link(self, item):
        return self.link

    def item_get_full_url(self, item):
        link = '{url}?utm_campaign={data}&utm_content=dayarrangement'.format(
            url=item.get_full_url,
            data=now().strftime('%d%m%Y'),
        )
        return link

    def _item_description(self, item):
        if item.shopoffers_type.slug == settings.SHOP_LESSONS_TYPE_SLUG:
            text_in_feed = settings.SITE_INFO['text_in_feed_lessons']
        else:
            text_in_feed = settings.SITE_INFO['text_in_feed']
        kwargs = dict(discount=item.discount,
                      name=item.name,
                      composer=self.composer(item),
                      text_in_feed=text_in_feed,
                      user=item.user,
                      url=self.item_get_full_url(item)
                      )

        avtor = gettext('Автор')
        text1 = gettext('ПОСМОТРЕТЬ')
        if item.shopoffers_type.slug == settings.SHOP_LESSONS_TYPE_SLUG:
            return format_lazy('<p>"{name}" - {text_in_feed}. {avtor} {user}. <br>{text1}: {url}</p><br>',
                               avtor=avtor, text1=text1, **kwargs)
        return format_lazy('<p>"{name}" - {composer}. {text_in_feed}. {avtor} {user}. <br>{text1}: {url}</p><br>',
                           avtor=avtor, text1=text1, **kwargs)

    def item_description(self, item):
        text = gettext('Интересные новинки на {short_project_name}.'.format(
            short_project_name=settings.SITE_INFO['short_project_name']))
        description = '<p>{text}</p>'.format(text=text)
        for i in item:
            description = '{} {}'.format(description, self._item_description(i))
        return description

    def item_enclosure_url(self):
        """
        Returns the enclosure URL for every item in the feed.
        """
        if self.item_image:
            return '/media/youtube_preview/source/{}_source.jpg'.format(self.item_image.pk)
        return ''

    def item_enclosure_length(self):
        """
        Returns the enclosure length for every item in the feed.
        """
        if self.item_image:
            path = '{}/youtube_preview/source/{}_source.jpg'.format(settings.MEDIA_ROOT, self.item_image.pk)
            try:
                return os.path.getsize(path)
            except FileNotFoundError:
                from youtube_video.utils import download_video_thumbnail_source
                download_video_thumbnail_source(shop_offer=self.item_image)
                try:
                    return os.path.getsize(path)
                except FileNotFoundError:
                    return 0
        return 0

    item_enclosure_mime_type = "image/jpeg"

    def composer(self, item):
        if item.composer:
            return item.composer
        if item.performer:
            return item.performer
        return ''


class ItemFeed(GetMonthMix, Feed):
    """
    Выдаем 4 поста, на каждую скидку дня.
    """
    title = ""
    description = ""
    link = "/"

    def item_link(self, item):
        link = '{url}?utm_campaign={data}&utm_content={discount}discount'.format(
            url=item.get_absolute_url(),
            data=now().strftime('%d%m%Y'),
            discount=item.discount
        )
        return link

    def items(self):
        step = 0
        if Sale.objects.is_running(sale_day_on=False):
            # step = now().day % 3
            return []
        items = ShopOffers.objects.published().filter(discount__gt=0, sale_day=True).order_by(
            '-discount')  # [step * 4:step * 4 + 4]
        return items

    def item_title(self, item):
        return '{date} {it}'.format(date='{0} {1} {2}'.format(now().day, self.get_month(now().month), now().year),
                                    it=item.name)

    def item_description(self, item):

        '''
        04 июня скидка дня - 90% - "Я шагаю по Москве" - Петров Андрей.
        Аранжировка для шестиструнной гитары (ноты и табы). Автор Игорь Горохов.
        :param item:
        :return:
        '''
        month = now().strftime("%B")
        language = translation.get_language()
        if language == 'ru':
            month = self.get_month(now().month)
        kwargs = dict(day=now().day,
                      month=month,
                      discount=item.discount,
                      name=item.name,
                      composer=self.composer(item),
                      text_in_feed=settings.SITE_INFO['text_in_feed'],
                      user=item.user)
        # February 12. Daily Discount: 90 % Off - Sailing Over the City - Maxim Leonidov.Six - string guitar arrangement
        # by Andrey Golvikh.
        if language == 'ru':
            return '{day} {month} скидка дня - {discount}% - "{name}" - {composer}. ' \
                   '{text_in_feed}. Автор {user}. '.format(**kwargs)
        return '{month} {day}, Daily Discount: {discount}% Off - "{name}" - {composer}. ' \
               '{text_in_feed} by {user}.'.format(**kwargs)

    def composer(self, item):
        if item.composer:
            return item.composer
        if item.performer:
            return item.performer
        return ''


class ImageRssFeedGenerator(Rss201rev2Feed):
    def add_root_elements(self, handler):
        super(ImageRssFeedGenerator, self).add_root_elements(handler)
        handler.startElement(u'image', {})

        # url= self.feed['link'] + 'media/youtube_preview/source/' + 'feed_image.jpg'
        handler.addQuickElement(u"url", self.feed['link'] + 'media/youtube_preview/source/' + 'feed_image.jpg')
        # handler.addQuickElement(u"title", self.feed['title'])
        # handler.addQuickElement(u"link", self.feed['link'])
        handler.endElement(u'image')


class ItemFeed2(GetMonthMix, Feed):
    """
    Выдаем 1 пост.
    """

    description = ""
    link = "/"

    def title(self):
        language = translation.get_language()
        text = gettext_lazy('СКИДКИ ДНЯ на аранжировки.')
        title = format_lazy(
            '{month} {day}, {year} - {text}',
            day=now().day,
            month=self.get_month(now().month),
            year=now().year,
            text=text
        )
        if language == 'ru':
            title = format_lazy(
                '{day} {month} {year} - {text}',
                day=now().day,
                month=self.get_month(now().month),
                year=now().year,
                text=text
            )
        return title

    def items(self):
        step = 0
        if Sale.objects.is_running(sale_day_on=False):
            return []
            # step = now().day % 3
        items = ShopOffers.objects.published().filter(discount__gt=0, sale_day=True).order_by('discount')
        # [step * 4:step * 4 + 4]
        return [items, ]

    def item_title(self, item):
        return self.title()

    def item_link(self, item):
        return self.link

    def item_get_full_url(self, item):
        link = '{url}?utm_campaign={data}&utm_content={discount}discount'.format(
            url=item.get_full_url,
            data=now().strftime('%d%m%Y'),
            discount=item.discount
        )
        return link

    def _item_description(self, item):

        kwargs = dict(discount=item.discount,
                      name=item.name,
                      composer=self.composer(item),
                      text_in_feed=settings.SITE_INFO['text_in_feed'],
                      user=item.user,
                      url=self.item_get_full_url(item)
                      )
        result = format_lazy(
            '<p>Discount {discount}% - "{name}" - {composer}. {text_in_feed} by {user}.<br>LOOK: {url}</p><br>',
            **kwargs)
        language = translation.get_language()
        if language == 'ru':
            result = format_lazy(
                '<p>Cкидка {discount}% - "{name}" - {composer}. {text_in_feed}. Автор {user}.<br>ПОСМОТРЕТЬ: {url}</p><br>',
                **kwargs)
        return result

    def item_description(self, item):
        description = ''
        for i in item:
            print(self._item_description(i))
            description = description + str(self._item_description(i))
        return description

    def item_enclosure_url(self):
        """
        Returns the enclosure URL for every item in the feed.
        """
        return '/media/youtube_preview/source/' + 'feed_image.jpg'

    def item_enclosure_length(self):
        """
        Returns the enclosure length for every item in the feed.
        """
        path = '{}/youtube_preview/source/feed_image.jpg'.format(settings.MEDIA_ROOT)
        try:
            return os.path.getsize(path)
        except FileNotFoundError:
            return 0

    item_enclosure_mime_type = "image/jpeg"

    def composer(self, item):
        if item.composer:
            return item.composer
        if item.performer:
            return item.performer
        return ''
