# -*- coding: utf-8 -*-
import logging
from django.views.generic import DetailView
import statsy
from base.mixins import JsonResponseMixin
from shop.models import ShopOffers

logger = logging.getLogger(__name__)


class ShopLoadVideoPlayerView(JsonResponseMixin, DetailView):
    http_method_names = ['get']
    model = ShopOffers

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        size = int(request.GET.get('size', 170))
        if self.object.checked:
            user = self.request.user if self.request.user.is_authenticated else None
            event = 'item_load_video'
            if self.object.is_discount:
                event = 'item_load_video_discount'
            statsy.send(group=ShopOffers.statsy_group, event=event, user=user,
                        content_object=self.object)
        return self.create_response({'html': self.render_to_string('shop/include/shop_item_player.html',
                                                                   context={
                                                                       'item': self.object,
                                                                       'size': '{size}x{size}'.format(size=size),
                                                                   }), })