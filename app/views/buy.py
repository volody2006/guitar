# -*- coding: utf-8 -*-
import logging

from offer.views import OfferBuyView
from shop.models import ShopOffers
from shop.views.mixins import ShopOffersTypeMixin

logger = logging.getLogger(__name__)


class ShopBuyView(ShopOffersTypeMixin, OfferBuyView):
    template_name = 'shop/buy.html'
    overwrite_template_name = False
    model = ShopOffers
