import logging
import os

from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.cache import caches
from django.forms import modelformset_factory
from django.http import Http404, HttpResponseRedirect, HttpResponseForbidden
from django.urls import reverse_lazy
from django.utils import translation
from django.utils.datastructures import MultiValueDictKeyError
from django.utils.decorators import method_decorator
from django.utils.translation import gettext_lazy as _
from django.views.decorators.cache import never_cache
from django.views.generic import UpdateView, DetailView
from reversion.views import RevisionMixin
from django.db import transaction

from base.mixins import (ChangesBetweenModelsMix, FormPageContextMixin, MessagesMixin, ModerSendMailMixin,
                         PriceGeoIpMix, JsonResponseMixin)
from lang.models import Language
from offer.views import GetPermissionMix, OfferCreateView
from shop.forms import AdminShopOffersForm, ShopOffersForm, ShopOffersWithoutAgreePromotionForm, SongTextForm, \
    UserFilEditForm
from shop.models import ShopOffers, UserFile
from shop.utils import compression_file
from shop.views.mixins import ShopOffersTypeMixin
from youtube_video.tasks import task_get_video_info

logger = logging.getLogger(__name__)


class ShopCreateArrangementView(PriceGeoIpMix, ShopOffersTypeMixin, OfferCreateView):
    form_class = ShopOffersForm
    additional_form_class = ShopOffersWithoutAgreePromotionForm
    admin_form_class = AdminShopOffersForm
    model = ShopOffers
    title = _('Добавить аранжировку')
    template_name = 'shop/forms/form.html'
    overwrite_template_name = False

    inline_model = UserFile
    inline_fields = ['file', ]
    inline_error = _('Пожалуйста, прикрепите хотя бы один файл.')

    def get_form_class(self):
        # если пользователь в профиле отказался от участия в любых акциях
        # то убираем поле про участие в акциях
        if not self.request.user.agree_for_promotion:
            return self.additional_form_class
        return super().get_form_class()

    def get_initial(self):
        initial = super().get_initial()
        initial.update({
            'currency': self.get_currency()
        })
        return initial

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({
            'lang': Language.objects.try_get(code=translation.get_language()),
            'user': self.request.user,
        })
        return kwargs

    def get_formset(self):
        FormSet = modelformset_factory(self.inline_model, fields=self.inline_fields, validate_min=True, min_num=1)
        if self.request.method in ('POST', 'PUT'):
            formset = FormSet(self.request.POST, self.request.FILES)
        else:
            formset = FormSet(queryset=self.inline_model.objects.none())
        return formset

    def form_valid(self, form, formset):
        from shop.translation import ShopOffersTranslationOptions
        self.tr_fields = ShopOffersTranslationOptions(self.model).fields

        lang = self.request.LANGUAGE_CODE

        for fld in self.tr_fields:
            for lang_code in settings.MODELTRANSLATION_LANGUAGES:
                # python 3.5 not supported f-prefix    f'{fld}_{lang_code}'
                field = '{fld}_{lang_code}'.format(fld=fld, lang_code=lang_code.replace('-', '_'))
                if not getattr(form.instance, field):
                    setattr(form.instance, field, '')

        for tr_field in self.tr_fields:
            field_lang = '{0}_{1}'.format(tr_field, lang.replace('-', '_'))
            attr = getattr(form.instance, tr_field, '')
            setattr(form.instance, field_lang, attr)

        form.instance.user_created = self.request.user

        # Если у пользователя уже есть права, он уже проверил свою работу )))
        if self.get_permission():
            form.instance.checked = True
            form.instance.moderator = self.request.user

        self.object = form.save()
        self.object.currency = self.get_currency()
        self.object.save()

        self.object.seo_save()

        self.formset_save(formset)

        self.object.price_save()
        compression_file(self.object)
        self.send_moder_mail(subject='Новый ShopOffers')
        logger.info('Дальше пойдет запуск задач в celery')
        transaction.on_commit(lambda: task_get_video_info.delay(self.object.pk, self.object.content_type.pk))
        #   run_task_compression_file.apply_async(args=[self.object.pk], countdown=30)

        self.get_success_message()

        return HttpResponseRedirect(self.get_success_url())

    def formset_save(self, formset):
        instances = formset.save(commit=False)
        for obj in instances:
            obj.item = self.object
            obj.user_filename = obj.file.name
            obj.mime_type = os.path.splitext(obj.file.name)[1][1:].strip().lower() or ''
            obj.save()

    def get_success_message(self):
        m1 = _('Ваша аранжировка успешно добавлена.')
        m2 = _('Она появится на сайте после проверки модератором.')
        title = _("Перейти на страницу добавления аранжировки")
        m3 = _('Добавить еще одну аранжировку.')
        self.msg_success(
            '{m1}<br>{m2}<br><a href="{url}" title="{title}">{m3}</a>'.format(
                url=reverse_lazy('shop-create-guitar-sheets-tabs'), m1=m1, m2=m2, m3=m3, title=title))


class DoesNotUpdateDeletedMixin:
    """
    Миксин не дающий редактировать удаленные объекты
    """
    def get_object(self, queryset=None):
        self.object = super().get_object()
        if self.object.is_delete:
            # Удаленные товары доступны только для администраторов, какой ссмысл их редактировать.
            raise Http404
        return self.object


@method_decorator(never_cache, name='dispatch')
class ShopUpdateView(ChangesBetweenModelsMix, LoginRequiredMixin, FormPageContextMixin,
                     MessagesMixin, GetPermissionMix, RevisionMixin, ModerSendMailMixin,
                     DoesNotUpdateDeletedMixin, UpdateView):
    code_action = 'change'
    form_class = ShopOffersForm
    additional_form_class = ShopOffersWithoutAgreePromotionForm
    admin_form_class = AdminShopOffersForm
    model = ShopOffers
    title = _('Редактировать аранжировку')
    template_name = 'shop/forms/form.html'

    inline_model = UserFile
    inline_fields = ['file', ]
    inline_error = str(_('Пожалуйста, прикрепите хотя бы один файл.'))
    can_delete = True
    validate_min = True

    def get_initial(self):
        initial = super().get_initial()
        initial.update({
            'currency': self.object.currency,
            'free': True if self.object.is_free else False,
            'price': 0 if self.object.is_free else self.object.price,
            'composer_name': self.object.get_composer.name if self.object.get_composer else '',
            'performer_name': self.object.get_performer.name if self.object.get_performer else '',
            'film_name': self.object.get_film.name if self.object.get_film else '',
        })
        return initial

    def get_form_kwargs(self):
        self.object = self.get_object()
        kwargs = super().get_form_kwargs()
        kwargs.update({
            'lang': Language.objects.try_get(code=translation.get_language()),
            'user': self.request.user,
        })
        return kwargs

    def post(self, request, *args, **kwargs):
        self.object_init = self.get_object()
        self.object = self.get_object()
        form = self.get_form()
        formset = self.get_formset()
        if form.is_valid() and formset is not None and formset.is_valid():
            return self.form_valid(form, formset)
        elif form.is_valid() and formset is None:
            return self.form_valid(form, formset)
        else:
            error = []
            if formset is not None and formset.non_form_errors():
                error.append(self.inline_error)
            if 'image' in form.errors:
                error.append(form.errors['image'][0])
            return self.form_invalid(form, '<br>'.join(error))

    def form_invalid(self, form, formset_error):
        return self.render_to_response(self.get_context_data(form=form, formset_error=formset_error))

    def get_form_class(self):
        if self.request.user.is_superuser or self.request.user.is_moderator:
            return self.admin_form_class
        # если пользователь в профиле отказался от участия в любых акциях
        # то убираем поле про участие в акциях
        if not self.request.user.agree_for_promotion:
            return self.additional_form_class
        # если аранжировка обозначена как авторская
        # то убираем поле про участие в акциях
        self.object = super().get_object()
        if self.object.author_music:
            return self.additional_form_class
        return super().get_form_class()

    def get_success_url(self):
        return self.object.get_absolute_url()

    def get_object(self, queryset=None):
        """
        Удалить объект может только создатель и пользователь с необходимыми правами.
        У СуперПользователя такие права по умолчанию
        """
        self.object = super().get_object()
        if self.object.is_freeze:
            raise HttpResponseForbidden(_('Вы не можете редактировать данный товар сейчас.'))
        if self.object.user_created != self.request.user:
            if self.get_permission():
                return self.object
            if self.karma_permission():
                return self.object
            raise HttpResponseForbidden(_('Пользователь %s не может редактировать этот объект.'))
        return self.object

    def get_formset(self):
        if self.object.is_buy:
            self.validate_min = False
        FileFormSet = modelformset_factory(UserFile, can_delete=self.can_delete, fields=['file', ],
                                           validate_min=self.validate_min, min_num=1 if self.validate_min else 0)
        if self.request.method in ('POST', 'PUT'):
            formset = FileFormSet(self.request.POST, self.request.FILES)
        else:
            if not self.object.is_buy:
                formset = FileFormSet(queryset=UserFile.objects.filter(item=self.object))
            else:
                formset = FileFormSet(queryset=UserFile.objects.none())
        return formset

    def formset_save(self, formset):
        formset_log = {}
        if formset is not None:
            instances = formset.save(commit=False)
            files_new = []
            for obj in instances:
                obj.item = self.object
                obj.user_filename = obj.file.name
                obj.mime_type = os.path.splitext(obj.file.name)[1][1:].strip().lower() or ''
                files_new.append(obj.file.name)
                obj.save()
            files_del = []
            for obj in formset.deleted_objects:
                files_del.append(obj.file.name)
                obj.delete()
            if files_new:
                formset_log.update({'files_new': files_new})
            if files_del:
                formset_log.update({'files_del': files_del})
        return formset_log

    def form_valid(self, form, formset):
        dirty_fields = self.object.get_dirty_fields()
        if self.get_permission():
            if form.cleaned_data['checked'] if 'checked' in form.cleaned_data else False:
                self.object.checked = True
                self.object.moderator = self.request.user

        self.object.save()
        self.object.seo_save()
        formset_log = self.formset_save(formset)
        self.msg_success(_('Внесенные изменения успешно сохранены.'))

        self.object = form.save()
        fields = ['image', 'name', 'description', 'video_url',
                  'song_text', 'song_file', 'difficulty', 'tonality',
                  'currency', 'tags', 'composer_name', 'performer_name', 'film_name']
        changes = self.get_changes_between_models(self.object_init, self.object, fields=fields)
        self.object.price_save()

        if 'video_url' in dirty_fields:
            task_get_video_info.delay(self.object.pk, self.object.content_type.pk)

        text = ''
        if formset_log:
            compression_file(self.object)
            for i in formset_log:
                text += '<b>{}</b>:<i>{}</i>\n'.format(i, formset_log[i], )
        file_delete = False
        for file in self.get_files():
            key = 'file_text_{}'.format(file.pk)
            try:
                if self.request.POST[key]:
                    file_delete = True
                    text += '<b>{id} {name}</b>:<i>{text}</i>\n'.format(id=file.pk, name=file.user_filename,
                                                                        text=self.request.POST[key])
            except MultiValueDictKeyError:
                pass
        if changes or file_delete:
            self.object.checked = False
            self.object.save()
            self.send_moder_mail(content={'text': text,
                                          'new': False,
                                          'changes': changes},
                                 subject='Обновленный ShopOffers')
        try:
            cached = caches['default']
            cached.clear()
        except KeyError:
            pass
        return HttpResponseRedirect(self.get_success_url())

    def karma_permission(self):
        # На товарах карма не важна
        return False

    def get_files(self):
        if not self.object.is_buy:
            return []
        else:
            return UserFile.objects.filter(item=self.object)

    def get_context_data(self, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs.update({
            'formset': self.get_formset(),
            'can_delete': self.can_delete,
            'files': self.get_files(),
            'object_tags': self.object.tags.all() if self.object.tags else None,
        })
        return kwargs


class CheckPermissionMixin:
    def get_object(self, queryset=None):
        """
        Редактировать объект может только создатель и пользователь с необходимыми правами.
        """
        self.object = super().get_object()
        if self.object.user_created != self.request.user:
            if self.get_permission():
                return self.object
            raise HttpResponseForbidden(_('У вас нет прав для редактирования этого объекта.'))
        return self.object


@method_decorator(never_cache, name='dispatch')
class ShopUpdateSongTextView(LoginRequiredMixin, GetPermissionMix, RevisionMixin, DoesNotUpdateDeletedMixin,
                             CheckPermissionMixin, UpdateView):
    template_name = 'shop/forms/part-edit-form.html'
    code_action = 'change'
    form_class = SongTextForm
    model = ShopOffers


@method_decorator(never_cache, name='dispatch')
class ShopUpdateReactView(LoginRequiredMixin, GetPermissionMix, RevisionMixin, DoesNotUpdateDeletedMixin,
                          CheckPermissionMixin, DetailView):
    code_action = 'change'
    model = ShopOffers
    title = ''

    def get_context_data(self, **kwargs):
        kwargs.update(title=self.title)
        return kwargs
