# -*- coding: utf-8 -*-
import logging
import mimetypes
import os

from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, HttpResponseNotFound
from django.views.generic import DetailView

from cart.models import PurchaseUser
from shop.models import UserFile

logger = logging.getLogger(__name__)


class GetUserFileView(LoginRequiredMixin, DetailView):
    """
    Отдает отдельный файл на скачивание, только создателю
    """
    http_method_names = ['get']

    def get_queryset(self):
        return UserFile.objects.filter(item__user_created=self.request.user)

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        mimetypes.init()
        try:
            return self.get_response()
        except IOError:
            response = HttpResponseNotFound()
        return response

    def get_response(self):
        file_name = self.get_file_name()
        mime_type_guess = mimetypes.guess_type(file_name)
        if mime_type_guess:
            response = HttpResponse(content_type=mime_type_guess[0])
            response['Content-Disposition'] = 'attachment; filename=%s' % file_name
        else:
            response = HttpResponse()
        response['x-accel-redirect'] = self.get_nurl()
        return response

    def get_nurl(self):
        file_url = self.object.get_file_url()
        return settings.FILES_URL_NGINX + file_url

    def get_file_name(self):
        file_name = os.path.basename(self.object.get_file_path())
        extension = os.path.splitext(file_name)[1].lower().strip('.')
        name = self.object.user_filename
        return '%s.%s' % (name, extension)


class GetBuyerFileView(GetUserFileView):
    """
    Отдает отдельный файл на скачивание, только покупателю
    """
    def get_queryset(self):
        ids = list(PurchaseUser.objects.filter(user=self.request.user).values_list('shop_offer_id', flat=True))
        return UserFile.objects.filter(item_id__in=ids)
