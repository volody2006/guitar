# -*- coding: utf-8 -*-
import logging
import elasticsearch

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import View

from base.mixins import JsonResponseMixin
from shop.utils import autocomplete_cpf, composer_autocomplete, film_autocomplete, performer_autocomplete

from users.models import User

logger = logging.getLogger(__name__)


class CPFView(JsonResponseMixin, View):
    template_name = None
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):
        q_search = request.GET.get('q')
        result = autocomplete_cpf(q_search)
        return self.create_response(result)


class ComposerAutocompleteView(LoginRequiredMixin, JsonResponseMixin, View):
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):
        q_search = request.GET.get('q')
        result = []
        if q_search and len(q_search) >= 3:
            result = composer_autocomplete(q_search)
        return self.create_response(result)


class PerformerAutocompleteView(LoginRequiredMixin, JsonResponseMixin, View):
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):
        q_search = request.GET.get('q')
        result = []
        if q_search and len(q_search) >= 3:
            result = performer_autocomplete(q_search)
        return self.create_response(result)


class FilmAutocompleteView(LoginRequiredMixin, JsonResponseMixin, View):
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):
        q_search = request.GET.get('q')
        result = []
        if q_search and len(q_search) >= 3:
            result = film_autocomplete(q_search)
        return self.create_response(result)

