import logging
from collections import Counter

import statsy
from dateutil.relativedelta import relativedelta
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from django.http import Http404
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _
from django.views.generic import ListView, TemplateView
from statsy.models import StatsyObject
from base.mixins import PriceGeoIpMix
from offer.utils import get_bestsellers_offers
from offer.views import OfferDetailView, OfferMainPageView
from project.site_settings import SITE_INFO, SHOP_ARRANGEMENTS_TYPE_SLUG
from shop.models import ShopOffers, TagOffers, ShopOffersType
from shop.views.mixins import SelectShopSortFormMixin, ShopOffersTypeMixin

logger = logging.getLogger(__name__)


class TempTestView(TemplateView):
    template_name = 'temp_test.html'

    def get(self, request, *args, **kwargs):
        print(args, kwargs)
        from django.contrib import messages
        kwargs.update({'changes': {'Название': ('[eq', 'новое')},
                       # 'form': TestForm,
                       })
        context = self.get_context_data(**kwargs)
        # messages.success(self.request, _('Изменения сохранены'))
        return self.render_to_response(context)


class TempTestMailView(TemplateView, PriceGeoIpMix):
    template_name = 'base/mail/buy_addfunds.html'

    extra_context = {
        'extends_template': 'base/mail/base_email.html',
        'recipient': 'Name User',
        'coupon': 'coupon',
        'recomendations': 'recomendations',
        'addfunds_amount': 1000,
        'addfunds_bonus': 100,
        'currency': '$',
        'projectname': 'guitarsolo.info'
    }

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)


class ShopMainPageView(SelectShopSortFormMixin, PriceGeoIpMix, ShopOffersTypeMixin, ListView):
    template_name = 'list.html'
    model = ShopOffers
    paginate_by = 20
    tag_model = TagOffers

    def get_template_names(self):
        filtered_params = self.get_param_filters()
        if 'q' in filtered_params:
            return 'shop/search.html'
        return super().get_template_names()

    def get_queryset_kwargs(self, **kwargs):
        if self.shopoffers_type:
            kwargs.update({'shopoffers_type_id': self.shopoffers_type.pk})
        return super().get_queryset_kwargs(**kwargs)


class ShopDetailView(ShopOffersTypeMixin, OfferDetailView):
    model = ShopOffers
    template_name = 'detail.html'

    def get_context_data(self, **kwargs):
        user = self.request.user if self.request.user.is_authenticated else None
        event = 'item_view'
        if self.object.is_discount:
            event = 'item_discount_view'
        statsy.send(group=self.model.statsy_group, event=event, user=user, content_object=self.object)
        context = super(ShopDetailView, self).get_context_data(**kwargs)
        context.update({
            'shopoffers_type': self.object.shopoffers_type,
            'vote_criterias': self.object.get_vote_criterias(),
            'user_bought': self.object.user_buy(self.request.user, need_secret=False),
        })
        return context


class ShopBestSellersPageView(PriceGeoIpMix, ListView):
    template_name = 'shop/base-list.html'
    model = ShopOffers
    paginate_by = 20
    extra_context = {'bestsellers': True,
                     'is_elasticsearch': False,
                     'has_filter': True,
                     'title': SITE_INFO.get('bestsellers_title')
                     }

    def get_queryset(self, *args, **kwargs):
        lang = self.request.LANGUAGE_CODE
        return get_bestsellers_offers(self.model, lang)


class ShopDiscountsPageView(PriceGeoIpMix, ListView):
    template_name = 'shop/base-list.html'
    model = ShopOffers
    paginate_by = 20
    extra_context = {'discounts': True,
                     'is_elasticsearch': False,
                     'has_filter': True,
                     'title': _('Сегодняшние Скидки')
                     }

    def get_queryset(self, **kwargs):
        offers = self.model.objects.published(discount__gt=0, ).order_by('-sale_day')
        lang = self.request.LANGUAGE_CODE
        if lang:
            offers = offers.filter(tags__langs__code=lang, ).distinct()
        return offers


class ShopPopularsPageView(PriceGeoIpMix, ListView):
    template_name = 'shop/base-list.html'
    model = ShopOffers
    paginate_by = 20
    extra_context = {'populars': True,
                     'is_elasticsearch': False,
                     'has_filter': True,
                     'title': _('Популярное за сутки')
                     }

    def get_queryset(self, **kwargs):
        lang = self.request.LANGUAGE_CODE
        end = now()
        start = end - relativedelta(days=1)
        content_type = ContentType.objects.get_for_model(self.model)
        stats = StatsyObject.objects.by_time(start=start, end=end).filter(
            object_id__isnull=False, content_type=content_type)
        exlude_offers = []

        if hasattr(self.model, 'tags') and lang:
            exlude_tags_lang_offers = self.model.objects.exclude(tags__langs__code=lang)
            exlude_lang_offers_values_list = list(exlude_tags_lang_offers.values_list('id', flat=True))
            exlude_offers.extend(exlude_lang_offers_values_list)

        exlude_offers = set(exlude_offers)
        stats = stats.exclude(object_id__in=exlude_offers).only('object_id').values_list('object_id', flat=True)

        c = Counter(stats)
        c = c.most_common(60)
        ids = [i for i, b in c]
        offers = []
        for id in ids:
            if self.model.objects.published().filter(pk=id, shopoffers_type__slug=SHOP_ARRANGEMENTS_TYPE_SLUG).exists():
                offers.append(self.model.objects.published().get(pk=id))
        return offers
