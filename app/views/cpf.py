# -*- coding: utf-8 -*-
import logging

from django.conf import settings
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.generic import ListView
from django.utils.translation import gettext_lazy as _

from base.utils import get_object_or_None
from shop.views.views import ShopMainPageView

logger = logging.getLogger(__name__)


class CPFListView(ListView):
    paginate_by = 500
    template_name = 'shop/cpf_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(model=self.model,
                            name_plural=self.model._meta.verbose_name_plural,
                            breadcrumb_url='{}_list'.format(self.model._meta.model_name)
                            )
                       )
        return context


class CPFPageView(ShopMainPageView):
    cpf_model = None
    cpf = None
    shopoffers_type_slug = settings.SHOP_ARRANGEMENTS_TYPE_SLUG
    types = {
        'composer': _('Композитор'),
        'performer': _('Исполнитель'),
        'film': _('Кинофильм'),
    }
    types2 = {
        'composer': _('композитора'),
        'performer': _('исполнителя'),
        'film': _('из кинофильма'),
    }

    def dispatch(self, request, *args, **kwargs):
        self.cpf = get_object_or_404(self.cpf_model, slug=self.kwargs.get('slug'))
        return super().dispatch(request, *args, **kwargs)

    # def get_queryset(self, cached=True, published=True):
    #     sqs = super().get_queryset()
    #     obj = get_object_or_404(self.cpf_model, slug=self.kwargs.get('slug'))
    #     model_name = self.cpf_model._meta.model_name
    #     key = '{}_id'.format(model_name)
    #     sqs = sqs.filter(**{key: obj.pk})
    #     return sqs
    # TODO: в шаблоне подставлять "Ноты и Табы Композитора Баха"

    def get_cpf_type(self):
        return '{type} {name}'.format(type=self.types[self.cpf_model._meta.model_name], name=self.cpf)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({'has_filter': True,
                        'filter_url': reverse('shop-type-main',
                                              kwargs={'shopoffers_type_slug': settings.SHOP_ARRANGEMENTS_TYPE_SLUG}),
                        'cpf_type': self.get_cpf_type(),
                        'cpf_type2': self.types2[self.cpf_model._meta.model_name],
                        'cpf': self.cpf,
                        'search_count': self._queryset.count()
                        })
        return context

    def get_queryset_kwargs(self, **kwargs):
        kwargs = super().get_queryset_kwargs(**kwargs)
        model_name = self.cpf_model._meta.model_name
        key = '{}_id'.format(model_name)
        kwargs.update({key: self.cpf.pk})
        logger.debug('Kwargs %s' % kwargs)
        return kwargs
