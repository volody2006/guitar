# -*- coding: utf-8 -*-
import logging

from django.contrib.auth.mixins import PermissionRequiredMixin
from django.views.generic import ListView

from shop.models import ShopOffers

logger = logging.getLogger(__name__)


class ShopListForModerator(PermissionRequiredMixin, ListView):
    template_name = 'shop/moder_list.html'
    permission_required = ('shop.change_shopoffers',)
    model = ShopOffers
    paginate_by = 20

    def get_queryset(self, *args, **kwargs):
        queryset = super().get_queryset()
        return queryset.active(checked=False, is_available=True)
