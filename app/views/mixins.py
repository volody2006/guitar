# -*- coding: utf-8 -*-
import logging

from django.http import Http404
from django.utils import translation
from django.utils.translation import gettext_lazy as _, ugettext_lazy as _

from lang.models import Language
from offer.views import BaseQuerySetFacetMixin
from shop.forms import SelectShopSortForm
from shop.models import Sale, ShopOffers, ShopOffersType, TagOffers
from shop.utils import get_difficulty_variants, get_tonality_variants
from project.site_settings import TEMPLATE_SHOP
from users.models import User

logger = logging.getLogger(__name__)


class ShopOffersTypeMixin:
    """
    Миксин получающий тип товара исходя из урл
    """
    shopoffers_type = None
    overwrite_template_name = True
    shopoffers_type_slug = None

    def dispatch(self, request, *args, **kwargs):
        shopoffers_type_slug = kwargs.get('shopoffers_type_slug', self.shopoffers_type_slug)
        try:
            self.shopoffers_type = ShopOffersType.objects.get(slug=shopoffers_type_slug)
        except ShopOffersType.DoesNotExist:
            if kwargs.get('tag'):
                self.overwrite_template_name = False
            else:
                raise Http404
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(shopoffers_type=self.shopoffers_type)
        return context

    def get_template_names(self):
        if self.overwrite_template_name:
            return 'shop/{shopoffers_type}/{tpl}'.format(shopoffers_type=TEMPLATE_SHOP.get(self.shopoffers_type.slug),
                                                         tpl=self.template_name)
        self.template_name = '{}'.format(self.template_name)
        return super().get_template_names()


class QuerySetFacetMixin(BaseQuerySetFacetMixin):
    model = ShopOffers
    kwargs = {}
    allowed_params = ['price_from', 'price_to', 'order', 'q', 'page', 'author_id', 'lvl', 'tnl', 'tag']

    def get_sale(self):
        if not hasattr(self, 'is_sale'):
            lang = Language.objects.get(code=translation.get_language())
            self.is_sale = Sale.objects.is_running(lang=lang)
        return self.is_sale

    def get_sort_order(self):
        if self.get_sale():
            self.default_sort_order = '-z'
        return self.request.GET.get('o', self.default_sort_order)

    def _get_queryset(self, cached=True, published=True, **kwargs):
        if not self.is_elasticsearch():
            print('Нет Эластика')
            return super().get_queryset().published()

        if not hasattr(self, '_queryset'):
            sqs = super()._get_queryset(cached, published, **kwargs)
            if self.get_sale() and self.get_sort_order() == self.default_sort_order:
                sqs = sqs.order_by('-is_discount', '-date_ordering')
            facet_list = ('author_id', 'tags_id', 'category_id')

            for f in facet_list:
                sqs = sqs.facet(f)

            self._queryset = sqs
        return self._queryset

    def get_queryset(self, *args, **kwargs):
        return self._get_queryset(*args, **kwargs).order_by(self.get_ordering())

    def get_queryset_kwargs(self, **kwargs):
        kwargs = super().get_queryset_kwargs(**kwargs)
        for param in self.allowed_params:
            kwargs.pop(param, '')
        if self.get_author():
            kwargs.update({'author_id': self.get_author().pk})
        if self.get_difficulty():
            kwargs.update({'difficulty': self.get_difficulty()})
        if self.get_tonality():
            kwargs.update({'tonality': self.get_tonality()})
        logger.debug('kwargs {}'.format(kwargs))
        return kwargs

    def get_difficulty(self):
        if not hasattr(self, 'difficulty'):
            self.difficulty = None
            if 'lvl' in self.get_param_filters():
                difficulty = self.request.GET['lvl']
                if difficulty.isdigit():
                    difficulty = int(difficulty)
                    if difficulty in get_difficulty_variants():
                        self.difficulty = difficulty
        return self.difficulty

    def get_tonality(self):
        if not hasattr(self, 'tonality'):
            self.tonality = None
            if 'tnl' in self.get_param_filters():
                tonality = self.request.GET['tnl']
                if tonality.isdigit():
                    tonality = int(tonality)
                    if tonality in get_tonality_variants():
                        self.tonality = tonality
        return self.tonality

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            difficulty=self.get_difficulty(),
            tonality=self.get_tonality(),
            author=self.get_author(),
            tags=self.load_shop_tags(),
        )
        return context

    def load_shop_tags(self, cols=3):
        if not self.is_elasticsearch():
            return []
        if not hasattr(self, 'tag_list'):
            tag_list = []
            tags = TagOffers.objects.filter(langs__code=self.get_lang()).order_by('name')
            facet = self.get_facet_counts('tags_id')
            tag_dict = {}
            try:
                facets = facet['fields']['tags_id']
            except KeyError:
                return {}
            for facet_tag in facets:
                tag_id = facet_tag[0]
                tag_dict[tag_id] = facet_tag[1]

            for tag in tags:
                active = False
                if str(tag.pk) in tag_dict:
                    if tag.pk in self.get_tags():
                        active = True
                    tag_list.append({
                        'id': tag.pk,
                        'name': tag.name,
                        'slug': tag.slug,
                        'count': tag_dict[str(tag.pk)],
                        'active': active
                    })
            self.tag_list = tag_list
        from shop.templatetags.shop_tags import get_column_list
        output = get_column_list(self.tag_list, cols, column_name='tags')
        output.update({'main_count': self._get_queryset().count()})
        return output


class SelectShopSortFormMixin(QuerySetFacetMixin):
    def get_form_initial(self):
        initial = {
            'o': self.get_sort_order(),
            'lvl': self.get_difficulty(),
            'tnl': self.get_tonality(),
            'author_id': self.get_author().pk if self.get_author() else '0',
            'tag': self.get_slug_tags(),
            'tonality_choices': self.get_choices('tonality', ShopOffers.TONALITY_CHOICES),
            'difficulty_choices': self.get_choices('difficulty', ShopOffers.DIFFICULTY_CHOICES),
        }
        if 'q' in self.filtered_params:
            initial.update({'q': self.filtered_params['q']})
        return initial

    def get_slug_tags(self):
        tags_id = self.get_tags()
        return list(TagOffers.objects.filter(pk__in=tags_id).values_list('slug', flat=True))

    def get_full_name_for_filter(self, user_dict):
        if user_dict['first_name'] and user_dict['last_name']:
            return '%s %s' % (user_dict['last_name'], user_dict['first_name'])
        if user_dict['last_name']:
            return user_dict['last_name']
        if user_dict['first_name']:
            return user_dict['first_name']
        if user_dict['slug']:
            return user_dict['slug']
        return str(_('User %s' % user_dict['pk']))

    def get_user_choices(self):
        if not self.is_elasticsearch():
            return []
        author_choices = (('0', _('Все авторы')),)
        facet = self.get_facet_counts('author_id')
        auth_dict = {}
        auth_list = []
        try:
            facets = facet['fields']['author_id']
        except KeyError:
            return {}
        for facet_cat in facets:
            author_id = facet_cat[0]
            auth_dict[author_id] = facet_cat[1]
            auth_list.append(author_id)
        users = User.objects.filter(is_active=True, pk__in=auth_list).values('pk', 'first_name', 'last_name',
                                                                             'slug').order_by('last_name')
        form_author = self.get_author() if self.get_author() else None
        if form_author is not None:
            if form_author.pk not in auth_list:
                author_choices += ((form_author.pk, '{name} (0)'.format(name=form_author.get_full_name_for_filter())),)

        author_choices += tuple(
            (
                (user['pk'], '{name} ({count})'.format(name=self.get_full_name_for_filter(user),
                                                       count=auth_dict[user['pk']])) for user in users
            )
        )
        return author_choices

    def get_form_kwargs(self):
        return {
            'author_choices': self.get_user_choices(),
            'current_author': self.get_author(),
            'initial': self.get_form_initial(),
        }

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(sort_form=SelectShopSortForm(**self.get_form_kwargs()))
        if 'q' in self.filtered_params:
            context.update(search_count=self._queryset.count())
        return context
