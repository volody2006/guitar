# -*- coding: utf-8 -*-
import logging

import pickle
import statsy
from django.conf import settings
from django.core.cache import cache

from django.http import Http404, HttpResponsePermanentRedirect
from django.urls import reverse
from django.utils import translation
from django.utils.timezone import now
from django.views.generic import ListView

from shop.models import Sale

logger = logging.getLogger(__name__)


class SaleDetailView(ListView):
    model = Sale
    template_name = 'shop/sale_detail.html'
    paginate_by = 20

    def get_object(self):
        slug = self.kwargs.get('slug')
        try:
            sale = Sale.objects.get(slug=slug)
            self.sale = sale
            return sale
        except Sale.DoesNotExist:
            raise Http404

    def get_queryset(self):
        self.get_object()

        if self.sale.status < self.sale.STATUS.run:
            return self.sale.preview_sale_offers.all()

        if not self.sale.sale_offers.all().exists():
            return []

        lang = translation.get_language()
        cache_key = 'sale-{lang}-{id}'.format(
            lang=lang, id=self.get_object().pk)
        offers_cache = cache.get(cache_key)

        if offers_cache:
            offers = pickle.loads(offers_cache)
            logger.debug('{} достали из кеша'.format(cache_key))
            return offers

        offers = []
        raw_offers = self.sale.sale_offers.filter(tags__langs__code=lang, ).distinct()
        for offer in raw_offers:
            if offer.tags.filter(langs__code=lang).count() == offer.tags.all().count():
                offers.append(offer)
        value = pickle.dumps(offers)
        if self.sale.date_end:
            timeout = (self.sale.date_end - self.sale.get_date_pub()).seconds
        else:
            timeout = settings.CACHES_TIMEOUT
        if timeout > 0:
            cache.set(key=cache_key, value=value, timeout=timeout, version=settings.CACHES_VERSION)
        return offers
        # return self.get_object().sale_offers.filter(tags__langs__code=lang, ).distinct()
        # return self.get_object().sale_offers.all()

    def get_context_data(self, **kwargs):
        self.object_list = self.get_queryset()
        context = super().get_context_data(**kwargs)
        context.update(sale=self.object,
                       sale_status=self.object.status)
        return context

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(**kwargs)
        if self.request.user.is_superuser or (self.object.is_active and self.object.get_date_pub() <= now()):
            event = 'sale_view'
            statsy.send(group=self.model.statsy_group,
                        event=event,
                        user=self.request.user if self.request.user.is_authenticated else None,
                        content_object=self.object)
            return self.render_to_response(context)
        elif self.object.status > Sale.STATUS.run:
            return HttpResponsePermanentRedirect(
                reverse('shop-type-main', kwargs={'shopoffers_type_slug': settings.SHOP_ARRANGEMENTS_TYPE_SLUG}))
        raise Http404
