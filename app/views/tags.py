# -*- coding: utf-8 -*-
import logging

from django.views.generic import ListView

from shop.models import TagOffers
from shop.views.mixins import ShopOffersTypeMixin
from shop.views.views import ShopMainPageView

logger = logging.getLogger(__name__)


class TagOffersListView(ShopOffersTypeMixin, ListView):
    template_name = 'shop/tag_list.html'
    overwrite_template_name = False

    def get_queryset(self):
        return self.shopoffers_type.tags.all()


class TagOffersPageView(ShopMainPageView):
    """
    tag`s object list
    """
    paginate_by = 20

    def get_tag(self):
        tag = None
        tag_slug = self.kwargs.get('tag')
        if tag_slug:
            try:
                tag = TagOffers.objects.get(slug=tag_slug)
            except TagOffers.DoesNotExist:
                return tag
        return tag

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({'has_filter': True,
                        'tag': self.get_tag()})

        return context
