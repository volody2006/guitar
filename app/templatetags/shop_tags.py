import logging
import math
from decimal import Decimal, ROUND_HALF_UP
from urllib.parse import urlencode

from django import template
from django.conf import settings
from django.db.models import Sum
from django.shortcuts import resolve_url
from django.utils import translation
from django.utils.translation import gettext_lazy as _, pgettext_lazy

from base.utils import format_price
from cart.models import PurchaseUser
from gis.managers import get_country_context
from shop.forms import ShopBuyAllForm
from shop.models import ShopOffers

register = template.Library()

logger = logging.getLogger(__name__)


@register.inclusion_tag('offer/include/offers_horizontal.html', takes_context=True)
def load_discount_shop_offers(context, shopoffers_type, count=4):
    # load_discount_shop_offers --- 0.002332925796508789 seconds ---
    offers = ShopOffers.objects.published().filter(discount__gt=0, shopoffers_type=shopoffers_type).order_by(
        '-discount')
    difficulty = context.get('difficulty')
    if difficulty:
        offers = offers.filter(difficulty=difficulty)
    tag = context.get('tag', '')
    if tag:
        offers = offers.filter(tags=tag)
    author = context.get('author')
    if author:
        offers = offers.filter(user_created=author)
    cpf = []
    if 'cpf' in context['request'].GET:
        cpf = context['request'].GET.get('cpf', '').strip()
    elif 'q' in context['request'].GET:
        cpf = context['request'].GET.get('q', '').strip()
    if cpf:
        offers = []
    context.update({
        'offers': offers[:count],
        'discount': True,
        'include_tpl': 'shop/include/item_horizontal.html',
    })
    return context


@register.simple_tag(takes_context=True)
def get_can_buy_all_and_one_price(context, author):
    if not author.can_buy_all():
        return None
    count = author.shop_offers_count
    country = get_country_context(context)
    price_sum_cache = author.get_price_for_all_offers(country)
    price_sum = Decimal(price_sum_cache).quantize(Decimal('1.00'), ROUND_HALF_UP)
    price_sum_one = (price_sum / count).quantize(Decimal('1.00'), ROUND_HALF_UP)
    return [count, price_sum, price_sum_one]


@register.simple_tag(takes_context=True)
def get_can_buy_all_description(context, author):
    # (7344 руб. за 149 шт., 49,2 руб. за 1 шт.)
    buy_description_text = None
    if not author.can_buy_all():
        return None
    count = author.shop_offers_count
    if count:
        country = get_country_context(context)
        discount = settings.BUY_ALL_DISCOUNT
        currency = country.get_currency()

        price_attr = 'price_{}'.format(currency.iso_code.lower())
        money_sum = ShopOffers.objects.filter(user_created=author).aggregate(price_sum=Sum(price_attr))['price_sum']
        discount_money_sum = (money_sum - money_sum * discount / 100).quantize(Decimal('1.00'), ROUND_HALF_UP)
        price_for_one = (discount_money_sum / count).quantize(Decimal('1.00'), ROUND_HALF_UP)

        signs = {'USD': '$', 'EUR': '€'}

        buy_description_text = '({currency_sign}{sum}{currency} - {count}, ' \
                               '{currency_sign}{for_one}{currency} - 1)'.format(
            currency_sign=signs[currency.iso_code] if currency.iso_code in signs else '',
            sum=format_price(discount_money_sum),
            currency='' if currency.iso_code in signs else ' %s' % currency,
            count=count,
            for_one=format_price(price_for_one),
        )
    return buy_description_text


@register.inclusion_tag('shop/include/author_offers.html', takes_context=True)
def load_author_shop_offers(context, author, count=4, order_by='-pk', **kwargs):
    shopoffers_type_slug = kwargs.get('shopoffers_type', settings.SHOP_ARRANGEMENTS_TYPE_SLUG)
    offers = ShopOffers.objects.published().filter(shopoffers_type__slug=shopoffers_type_slug).order_by(order_by)
    offers = offers.by_author(author)
    url_param = '?%s' % urlencode({'author_id': author.pk})
    context.update({
        'title': '{text} - {author}'.format(text=settings.SITE_PRODUCT_NAME.capitalize(), author=author),
        'include_tpl': 'shop/include/item_horizontal.html',
        'offers': offers[:count],
        'count': offers.count(),
        'author': author,
        'user': context['request'].user,
        'url': '{path}{param}'.format(
            path=resolve_url('shop-type-main', shopoffers_type_slug=settings.SHOP_ARRANGEMENTS_TYPE_SLUG),
            param=url_param),
        'link': settings.SITE_INFO['last_author_offers_link'],
    })
    return context


@register.inclusion_tag('shop/include/last_user_offers.html', takes_context=True)
def load_last_user_offers(context, count=4):
    user = context.get('user')
    offers = ShopOffers.objects.by_author(user).order_by('-pk')
    context.update({
        'title': _('Последние добавленные ноты и табы'),
        'include_tpl': 'shop/include/item_horizontal.html',
        'offers': offers[:count],
        'edit_link': True,
    })
    return context


@register.inclusion_tag('shop/include/last_user_purchases.html', takes_context=True)
def load_last_user_purchases(context, count=4):
    user = context['request'].user
    purchases = PurchaseUser.objects.filter(user=user).order_by('-date_created')[:count]
    context.update({
        'title': _('Последние купленные ноты и табы'),
        'include_tpl': 'cabinet/shop/purchase_item.html',
        'purchases': purchases,
    })
    return context


@register.simple_tag
def get_shop_offers_from_user(author, count=4, **kwargs):
    offers = ShopOffers.objects.published().by_author(author).order_by('-pk')
    exclude_pk = kwargs.get('exclude_pk')
    if exclude_pk:
        offers = offers.exclude(pk=exclude_pk)
    return offers[0:count]


def get_column_list(column_list, cols=3, column_name=None):
    rows = 0
    if len(column_list):
        rows = math.ceil(len(column_list) / cols)
    if not column_name:
        column_name = 'list'
    output_list = {
        column_name: column_list,
        'in_cols': [i * rows + rows for i in range(cols)]
    }
    return output_list


@register.simple_tag(takes_context=True)
def get_shop_list_description(context):
    types = {
        'composer': _('композитора'),
        'performer': pgettext_lazy('band', 'исполнителя'),
        'film': _('из кинофильма'),
    }
    if 'cpf' in context:
        # print('has')
        cpf = context['cpf']
        return '{begin} {type} {cpf}.'.format(
            begin=settings.SITE_INFO['shop_cpf_info_begin'],
            type=types[cpf.class_name()],
            cpf=cpf,
            # end=settings.SITE_INFO['shop_cpf_info_end'],
        )
    return settings.SITE_INFO['shop_info']


@register.inclusion_tag('shop/include/buy-all-form.html', takes_context=True)
def get_buy_all_form(context, author, view_as='button'):
    """
    Выводит форму "Купить все сразу" если автору позволенно продавать все аранжировки за один раз
    """
    if author.can_buy_all():
        country = get_country_context(context)
        currency = country.get_currency()

        context.update({
            'buy_all_form': ShopBuyAllForm(user=context['request'].user, currency=currency),
            'author': author,
            'show_modal': True if context['request'].user.is_anonymous else False,
            'view_as': view_as,
        })
    return context


@register.inclusion_tag('shop/include/song_text.html', takes_context=True)
def get_song_text(context):
    show_song_text = False
    obj = context.get('object')
    if obj:
        if obj.song_text and obj.song_file:
            # если указаны языки для отображения, то проверяем входит ли текущий в этот список
            if obj.song_langs.all().count():
                lang = translation.get_language()
                if obj.song_langs.filter(code=lang).exists():
                    show_song_text = True
            else:
                show_song_text = True
    context.update({
        'show_song_text': show_song_text
    })
    return context


@register.inclusion_tag('shop/include/favorite_button.html')
def favorite_button(user, offer):
    from users.models import User
    in_list = None
    context = {
        'in_list': in_list,
    }
    if isinstance(user, User) and isinstance(offer, ShopOffers):
        if user.is_authenticated and not offer.is_free:
            from users.models import UserFavoritesShopOffers
            in_list = UserFavoritesShopOffers.objects.filter(user_id=user.id, offer_id=offer.pk).exists()
        context.update({'user': user, 'offer_id': offer.id, 'in_list': in_list})
    return context
