import logging
import random
import urllib
from decimal import Decimal

from django import template
from django.conf import settings
from django.db.models import Q
from django.shortcuts import resolve_url
from django.urls import reverse
from haystack.query import SearchQuerySet

from gis.managers import get_country_context
from offer.templatetags.offer_tags import get_similar_items
from django.utils.translation import gettext_lazy as _
from django.utils import translation

from offer.utils import get_bestsellers_offers_count, get_popular_day_offers
from shop.models import ShopOffersType, ShopOfferTechniques, ShopOffers
from project.site_settings import TEMPLATE_SHOP

register = template.Library()

logger = logging.getLogger(__name__)


@register.inclusion_tag('shop/include/offers_last.html', takes_context=True)
def free_offers(context, count=4, **kwargs):
    """
    Выводит рандомом бесплатные товары у которых более 25 покупок, по категориям
    """
    pay_count = 25
    country = get_country_context(context)
    language = translation.get_language().replace('-', '_')
    shopoffers_type_slug = kwargs.get('shopoffers_type_slug', settings.SHOP_ARRANGEMENTS_TYPE_SLUG)
    offers = ShopOffers.objects.published().filter(shopoffers_type__slug=shopoffers_type_slug,
                                                   price__isnull=True,
                                                   pay_count__gte=pay_count) \
        .filter(Q(lang__code=language) | Q(lang__isnull=True)) \
        .order_by('-date_approval_moderator')

    offers_count = offers.count()
    if offers_count > count:
        slice = random.random() * (offers_count - count)
        offers = offers[slice: slice + count]
    else:
        offers = offers[:count]

    return {
        'title': settings.SITE_INFO['free_offers_title'],
        'offers': offers,
        'country': country,
        'user': context.get('user', None),
        'include_tpl': 'shop/{template_shopoffers_type}/include/item_horizontal.html'.format(
            template_shopoffers_type=TEMPLATE_SHOP.get(shopoffers_type_slug)),
        'link': _('Бесплатные ноты'),
        'url': resolve_url('shop-type-main', shopoffers_type_slug=shopoffers_type_slug)
               + '?' + urllib.parse.urlencode({'attr_free': 1}),
    }


@register.inclusion_tag('shop/include/offers_last.html', takes_context=True)
def last_offers(context, count=4, **kwargs):
    """
    выводит последние товары, по категориям
    """
    country = get_country_context(context)
    language = translation.get_language().replace('-', '_')
    shopoffers_type_slug = kwargs.get('shopoffers_type', settings.SHOP_ARRANGEMENTS_TYPE_SLUG)
    offers = ShopOffers.objects.published().filter(shopoffers_type__slug=shopoffers_type_slug) \
        .filter(Q(lang__code=language) | Q(lang__isnull=True)) \
        .order_by('-date_approval_moderator')
    return {
        'title': settings.SITE_INFO['last_offers_title'],
        'offers': offers[:count],
        'country': country,
        'user': context.get('user', None),
        'include_tpl': 'shop/{template_shopoffers_type}/include/item_horizontal.html'.format(
            template_shopoffers_type=TEMPLATE_SHOP.get(shopoffers_type_slug)),
        'link': _('Все аранжировки'),
        'url': resolve_url('shop-type-main', shopoffers_type_slug=shopoffers_type_slug),
    }


@register.inclusion_tag('shop/include/offers_last.html', takes_context=True)
def bestsellers_offers(context, count=4, **kwargs):
    """
    выводит лучшие товары, по категориям
    """
    country = get_country_context(context)
    language = translation.get_language().replace('-', '_')
    shopoffers_type_slug = kwargs.get('shopoffers_type', settings.SHOP_ARRANGEMENTS_TYPE_SLUG)

    # todo учет категории
    offers = get_bestsellers_offers_count(model=ShopOffers, lang=language, count=count)

    return {
        'title': _('Бестселлеры'),
        'offers': offers,
        'country': country,
        'user': context.get('user', None),
        'include_tpl': 'shop/{template_shopoffers_type}/include/item_horizontal.html'.format(
            template_shopoffers_type=TEMPLATE_SHOP.get(shopoffers_type_slug)),
        'link': _('Все бестселлеры'),
        'url': resolve_url('shopoffers_bestsellers'),
    }


@register.inclusion_tag('base/include/extend_template.html', takes_context=True)
def random_offers(context, count=4, **kwargs):
    layout = kwargs.get('layout', 'vertical')
    country = get_country_context(context)
    sale = context.get('SALE')
    shopoffers_type_slug = kwargs.get('shopoffers_type', settings.SHOP_ARRANGEMENTS_TYPE_SLUG)
    if not shopoffers_type_slug or shopoffers_type_slug == '':
        shopoffers_type_slug = settings.SHOP_ARRANGEMENTS_TYPE_SLUG
    if sale and False:
        title = sale.get_title()
        offers = sale.sale_offers.all().order_by('?')[:4]
        url = sale.get_absolute_url()
    else:
        try:
            title = settings.SITE_INFO['%s-title' % shopoffers_type_slug]
        except KeyError:
            title = shopoffers_type_slug
        language = translation.get_language().replace('-', '_')
        offers = ShopOffers.objects.published().filter(
            shopoffers_type__slug=shopoffers_type_slug,
            price__gt=Decimal('0.00')).filter(
            Q(lang__code=language) | Q(lang__isnull=True))
        offers_count = offers.count()
        # count = 4
        if offers_count > count:
            slice = random.random() * (offers_count - count)
            offers = offers[slice: slice + count]
        offers = offers[:count]
        url = reverse('shop-type-main', kwargs={'shopoffers_type_slug': shopoffers_type_slug})

    extend_template = 'offer/include/offers_vertical.html' if layout == 'vertical' else 'shop/include/offers_last.html'
    return {
        'template': extend_template,
        'title': title,
        'link': _('Все') + ' ' + str(title),
        'url': url,
        'offers': offers,
        'country': country,
        'user': context.get('user', None),
        'shopoffers_type': shopoffers_type_slug,
        'include_tpl': 'shop/include/item_{layout}.html'.format(layout=layout),
    }


@register.inclusion_tag('base/include/extend_template.html', takes_context=True)
def popular_offers(context, group, count=4, start=None, end=None, exlude_count=0, layout='vertical', **kwargs):
    shopoffers_type_slug = kwargs.get('shopoffers_type', settings.SHOP_ARRANGEMENTS_TYPE_SLUG)
    offers = get_popular_day_offers(group=group, lang_code=context.get('LANGUAGE_CODE'), count=count, start=start,
                                    end=end, exlude_count=exlude_count, shopoffers_type=shopoffers_type_slug)
    # print(offers)
    country = get_country_context(context)
    if not offers:
        # если нет популярных и это не запрещено - заменяем список рандомом
        if kwargs.get('show_replacement', False):
            return random_offers(context, count=count, **kwargs)

    extend_template = 'offer/include/offers_vertical.html' if layout == 'vertical' else 'shop/include/offers_last.html'

    return {
        'template': extend_template,
        'title': _('Популярное за сутки'),
        'link': _('Все популярные'),
        'url': resolve_url('shopoffers_populars'),
        'offers': offers,
        'country': country,
        'user': context.get('user', None),
        'include_tpl': 'shop/{template_shopoffers_type}/include/item_{layout}.html'.format(
            template_shopoffers_type=TEMPLATE_SHOP.get(shopoffers_type_slug), layout=layout),
    }


@register.inclusion_tag('offer/include/offers_vertical.html', takes_context=True)
def load_related_offers_or_similar_by_other_authors(context, object, count=4):
    """
    Пока используется только на стр урока
    """
    country = get_country_context(context)
    language = translation.get_language().replace('-', '_')
    if object.related_offers.exists():
        offers = object.related_offers.published().filter(Q(lang__code=language) | Q(lang__isnull=True))[:count]
        title = _('С этим уроком покупают еще')
    else:
        offers = get_similar_items([object, ], count=count, exlude_authors=[object.user_created, ],
                                   lang_code=language, shopoffers_type_id=object.shopoffers_type_id)
        title = _('Похожие уроки')

    return {
        'title': title,
        'offers': offers,
        'country': country,
        'user': context.get('user', None),
        'include_tpl': 'shop/lessons/include/item_vertical.html',
    }


@register.inclusion_tag('offer/include/offers_vertical.html', takes_context=True)
def mini_search_results(context, exclude_category, q, count=4):
    """
    Поиск из другой категории. Подключается на странице с результатами поиска
    """
    country = get_country_context(context)
    category = ShopOffersType.objects.exclude(pk=exclude_category.pk).first()
    model = category.offers.model
    sqs = SearchQuerySet()
    sqs = sqs.models(model).filter(published=True, lang__code=context.get('LANGUAGE_CODE'),
                                   category_id=category.pk)
    sqs = sqs.auto_query(q)[:count]

    return {
        'title': category.name,
        'offers': sqs,
        'country': country,
        'user': context.get('user', None),
        'include_tpl': 'shop/lessons/include/item_vertical.html',
    }


@register.inclusion_tag('shop/lessons/include/offers_by_techniques.html', takes_context=True)
def lessons_for_arrangement_by_technique(context, object):
    """
    Страница аранжировки. Вывод уроков соответствующих технике аранжировки, одна техника - один урок.
    - Купленные не показываются
    - соблюдать порядок выбранных техник!
    - если аранжировка 1 и 2 уровня сложности получаем 2 урока из списка со сложностью=1 (2 нижнее)
    - По ссылке "Подробнее" - подробная информация открывается в модальном окне.
    - если всего получилось больше 6 - вывести ссылку “Показать все уроки к этой аранжировке”
    """
    visible_amount = 6
    offers_list = []
    exclude_offers = []
    country = get_country_context(context)
    shopoffers_type = ShopOffersType.objects.get(slug=settings.SHOP_LESSONS_TYPE_SLUG)
    language = translation.get_language().replace('-', '_')
    # список техник прикрепленных к аранжировке
    techniques_ids = list(ShopOfferTechniques.objects.filter(offer=object).order_by('order')
                          .values_list('technique_id', flat=True))
    if len(techniques_ids):
        # купленные уроки
        user = context.get('user', None)
        if user and user.is_authenticated:
            exclude_offers = list(user.purchaseuser_set.filter(shop_offer__shopoffers_type_id=shopoffers_type.pk)
                                  .values_list('shop_offer_id', flat=True))
        # ищем уроки с нужными техниками, исключая купленные
        offers = shopoffers_type.offers.published(techniques__in=techniques_ids).by_lang(language) \
            .exclude(id__in=exclude_offers)
        # сортируем список по порядку установлнному техникам аранжировки
        offers_list = sorted(offers, key=lambda x: techniques_ids.index(x.techniques.all().first().pk))
        # если аранжировка 1-го или 2-го уровня, рандомом получаем 2 урока 1й категории сложности,
        # за исключением уже купленных и уже выбранных
        if object.difficulty <= 2:
            exclude_offers = exclude_offers + [x.pk for x in offers]
            beginners_offers = shopoffers_type.offers.published(difficulty=1).exclude(id__in=exclude_offers)
            offers_count = beginners_offers.count()
            if offers_count > 2:
                slice = random.random() * (offers_count - 2)
                beginners_offers = beginners_offers[slice: slice + 2]
            # определяем позицию вставки элементов для новичков:
            # или после 4-го или в конец
            insert_index = 4 if len(offers_list) > 4 else len(offers_list)
            for b_offer in beginners_offers:
                offers_list.insert(insert_index, b_offer)

    return {
        'title': shopoffers_type.name,
        'offers': offers_list,
        'invisible': bool(len(offers_list) > visible_amount),
        'visible_amount': visible_amount,
        'country': country,
        'user': context.get('user', None),
        'request': context.get('request', None),
        'include_tpl': 'shop/lessons/include/item_vertical.html',
    }
