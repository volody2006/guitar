# coding: utf-8
import logging

from django.db import models
from django.db.models import Q
from django.utils.timezone import now

from offer.managers import OfferQuerySet

logger = logging.getLogger(__name__)


class ShopQuerySet(OfferQuerySet):

    def published_author(self, user, **kwargs):
        return self.active(is_available=True,
                           moderator__isnull=False,
                           user_created=user, **kwargs)



class CPFManager(models.Manager):
    def accepted(self, *args, **kwargs):
        return self.filter(accepted=True, *args, **kwargs)

    def create_item(self, name, lang, user, accepted=False):
        name_tr_field = '{0}_{1}'.format('name', lang)
        create_kwargs = {
            'name': name,
            name_tr_field: name,
            'user': user,
            'accepted': accepted
        }
        item = self.model(**create_kwargs)
        item.save(using=self._db)

        return item


class TagManager(models.Manager):

    def langs(self, langs):
        return self.filter(langs__in=langs)

    def langs_code(self, codes):
        return self.filter(langs__code__in=codes)

    def lang(self, lang):
        logger.debug('lang {}'.format(lang))
        from lang.models import Language
        if isinstance(lang, Language):
            code = lang.code
        else:
            code = lang
        return self.filter(langs__code=code)


class SaleQuerySet(models.QuerySet):

    def active(self, *args, **kwargs):
        """
        Фильтр, пропускающий только активные записи
        """
        return self.filter(is_active=True, *args, **kwargs)

    def published(self, *args, **kwargs):
        # возражает опубликованние распродажу. На текущие время
        return self.active(date_pub__lt=now(), date_end__gte=now(), *args, **kwargs).order_by('-date_pub')

    def time_to_run(self):
        return self.active(status=self.model.STATUS.waiting,
                           date_start__lt=now()).order_by('-date_start')

    def running(self, *args, **kwargs):
        return self.active(status=self.model.STATUS.run, *args, **kwargs)

    def time_to_stop(self):
        return self.running(date_end__lt=now()).order_by('-date_end')

    def is_sale(self, *args, **kwargs):
        # Флаг показывающий, что сейчас есть по крайней мере одна активная распродажа.
        return self.published(*args, **kwargs).exists()

    def is_running(self, *args, **kwargs):
        return self.running(*args, **kwargs).exists()

    def get_published(self, *args, **kwargs):
        return self.published(*args, **kwargs).order_by("?").first()
