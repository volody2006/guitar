// добавление нескольких Listener к одному объекту
function addMultipleEventListener(element, events, handler) {
  events.forEach(e => element.addEventListener(e, handler))
}

// смена порядкового номера преимуществ
function correctLabelOrder(num) {
    let labelSortItemCounter = num;
    document.querySelectorAll('.sort-item').forEach((item) => {
        let label = item.querySelector('.col-form-label');
        label.innerText = gettext('Техника') + ' ' +labelSortItemCounter;
        labelSortItemCounter++;
    });
};

//Writes the correct current order in the order form fields
let correctOrder = function () {
    let localSortItemCounter = 1;
    document.querySelectorAll('[id$=ORDER]').forEach((input) => {
        input.setAttribute('value', localSortItemCounter);
        localSortItemCounter++;
    });
};

// меняем порядковые номера преимуществам и валидируем формсеты, при добавлении новых форм
$(document).on('mouseup', '.add-form-form', function () {
    setTimeout(correctLabelOrder, 100, 1);
});

// проставляет порядковый номер преимуществам
correctLabelOrder(0);

document.querySelectorAll('button.form-submit').forEach((button) => {
  button.addEventListener('click', (event) => {
      correctOrder();
      correctLabelOrder(1);
      return true;
  });
});

