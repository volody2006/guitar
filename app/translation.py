# -*- coding: utf-8 -*-
from modeltranslation.translator import TranslationOptions, register

from shop.models import Composer, Film, Performer, Sale, ShopOffersType, ShopOffers, ShopOffersProxy, TagOffers, \
    MimeTypeFile, UserFile, Techniques, TagOffersCategory, Lesson, ShopOffersDelete


@register(ShopOffersType)
class ShopOffersCategoryTranslationOptions(TranslationOptions):
    fields = ('name', 'seo_title', 'seo_metadesc', 'seo_metakeywords')


@register(Techniques)
class ShopOffersTechniqueTranslationOptions(TranslationOptions):
    fields = ('name', 'description')


class NameDescriptionSeo(TranslationOptions):
    fields = ('name', 'description', 'seo_title', 'seo_metadesc', 'seo_metakeywords')


@register(ShopOffers)
class ShopOffersTranslationOptions(NameDescriptionSeo):
    pass


@register(ShopOffersProxy)
class ShopOffersProxyTranslationOptions(NameDescriptionSeo):
    pass


@register(ShopOffersDelete)
class ShopOffersDeleteTranslationOptions(NameDescriptionSeo):
    pass


@register(Film)
class FilmTranslationOptions(NameDescriptionSeo):
    pass


@register(Composer)
class ComposerTranslationOptions(NameDescriptionSeo):
    pass


@register(Performer)
class PerformerTranslationOptions(NameDescriptionSeo):
    pass


@register(TagOffers)
class TagOffersTranslationOptions(NameDescriptionSeo):
    pass


@register(TagOffersCategory)
class TagOffersCategoryTranslationOptions(NameDescriptionSeo):
    pass


@register(Sale)
class SaleTranslationOptions(NameDescriptionSeo):
    fields = ('name', 'title', 'announce', 'description',
              'seo_title', 'seo_metadesc', 'seo_metakeywords',
              'image')


@register(MimeTypeFile)
class MimeTypeFileTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(UserFile)
class UserFileTranslationOptions(TranslationOptions):
    fields = ('note',)


@register(Lesson)
class LessonsTranslationOptions(TranslationOptions):
    fields = ('content',)
