# -*- coding: utf-8 -*-
import logging
import mimetypes
import os
from copy import copy

from django.conf import settings
from django.db.models import Q
from django.http import Http404, HttpResponseNotFound, HttpResponse
from django.utils.translation import ugettext_lazy as _
from django.utils import translation
from rest_framework import exceptions, permissions, viewsets, generics, status, filters, mixins, views
from rest_framework.authentication import BasicAuthentication
from rest_framework.decorators import action
from rest_framework.parsers import FileUploadParser
from rest_framework.response import Response
from rest_framework.settings import api_settings
from sorl.thumbnail import delete
from django_filters.rest_framework import DjangoFilterBackend

from base.authentication import CsrfExemptSessionAuthentication
from base.mixins import PriceGeoIpMix, UserIPMixin, ChangesBetweenModelsMix
from base.permissions import IsEditorOrReadOnly, IsModeratorOnly, IsModeratorOrOwner
from base.utils import get_object_or_None
from cart.mixins import CreateUpdatePayment, PaymentDisabled
from lang.models import Language
from .filters import IsOwnerFilterBackend, IsUncheckedFilterBackend, IsArrangementsFilterBackend, \
    IsLessonsFilterBackend, IsOwnerOrModeratorFilterBackend
from .serializers import TagsSerializer, TechniquesSerializer, OfferTechniquesSerializer, \
    UserFileSerializer, ShopLessonSerializer, ComposerSerializer, PerformerSerializer, FilmSerializer, \
    ShopArrangementSerializer, ShopModeratorArrangementSerializer, ShopImageSerializer, \
    OfferRelatedSerializer, TinyShopSerializer, UserFileNoteSerializer

from shop.models import ShopOffers, TagOffers, Techniques, ShopOfferTechniques, UserFile, Composer, Performer, Film, \
    ShopOffersType

logger = logging.getLogger(__name__)


class ShopViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows manage shop arrangements.
    """
    permission_classes = [permissions.AllowAny]
    queryset = ShopOffers.objects.published()
    serializer_class = TinyShopSerializer
    filter_backends = [DjangoFilterBackend, filters.SearchFilter]
    filterset_fields = ['shopoffers_type']
    search_fields = ['name']


class TagsListView(generics.ListAPIView):
    """
    API endpoint that allows offers tags to be viewed.
    """
    permission_classes = [permissions.AllowAny]
    queryset = TagOffers.objects.all().order_by('name')
    serializer_class = TagsSerializer
    pagination_class = None

    def get_queryset(self):
        queryset = self.queryset.filter(approve=True, langs__code=translation.get_language(), hidden=False)
        shopoffers_type_slug = self.kwargs.get('shopoffers_type_slug')
        if shopoffers_type_slug:
            try:
                shopoffers_type = ShopOffersType.objects.get(slug=shopoffers_type_slug)
                queryset = queryset.filter(Q(shopoffers_type=shopoffers_type) | Q(shopoffers_type__isnull=True))
            except ShopOffersType.DoesNotExist:
                pass
        return queryset


class ShopCPFViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = [permissions.AllowAny]
    pagination_class = None

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        response['Cache-Control'] = 'no-cache, no-store, must-revalidate'
        response['Pragma'] = 'no-cache'
        return response


class ShopComposersViewSet(ShopCPFViewSet):
    queryset = Composer.objects.all().order_by('name')
    serializer_class = ComposerSerializer


class ShopPerformersViewSet(ShopCPFViewSet):
    queryset = Performer.objects.all().order_by('name')
    serializer_class = PerformerSerializer


class ShopFilmsViewSet(ShopCPFViewSet):
    queryset = Film.objects.all().order_by('name')
    serializer_class = FilmSerializer


class TechniquesListView(generics.ListAPIView):
    """
    API endpoint that allows offers techniques to be viewed.
    """
    permission_classes = [permissions.AllowAny]
    queryset = Techniques.objects.all().order_by('name')
    serializer_class = TechniquesSerializer
    pagination_class = None

    def get_queryset(self):
        exclude_ids = self.request.GET.getlist('exclude[]', [])
        return self.queryset.exclude(pk__in=exclude_ids)


class DifficultiesListView(views.APIView):
    permission_classes = [permissions.AllowAny]

    def get(self, request, *args, **kwargs):
        return Response([{'value': int(key), 'label': value} for (key, value) in ShopOffers.DIFFICULTY_CHOICES])


class TonalitiesListView(views.APIView):
    permission_classes = [permissions.AllowAny]

    def get(self, request, *args, **kwargs):
        return Response([{'value': int(key), 'label': value} for (key, value) in ShopOffers.TONALITY_CHOICES])


class SystemsListView(views.APIView):
    permission_classes = [permissions.AllowAny]

    def get(self, request, *args, **kwargs):
        return Response([{'value': key, 'label': value} for (key, value) in settings.AVAILABLE_PAYMENT_SYSTEM])


class ShopBuyView(PriceGeoIpMix, CreateUpdatePayment, generics.CreateAPIView):
    """
    API endpoint for buy one offer. Only for authenticated user yet.
    @todo free, payssion
    """
    permission_classes = [permissions.IsAuthenticated]
    queryset = ShopOffers.objects.published()

    def post(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
        except Http404:
            raise exceptions.ValidationError(_('Ошибка. Аранжировка не найдена.'))

        try:
            payment_url = self.make_payment(self.request.user,
                                            [instance],
                                            payment_type=request.data.get('payment_type', None),
                                            coupon=request.data.get('coupon', None))
            if payment_url is not None:
                return Response({'redirect_url': payment_url}, status=status.HTTP_200_OK)
        except PaymentDisabled as e:
            raise exceptions.ValidationError(e)
        return Response({}, status=status.HTTP_400_BAD_REQUEST)


class OfferTechniquesListView(mixins.CreateModelMixin, generics.RetrieveUpdateDestroyAPIView):
    """
    API endpoint that allows add and destroy techniques in offer.
    """
    permission_classes = [permissions.IsAuthenticated, IsEditorOrReadOnly]
    serializer_class = OfferTechniquesSerializer
    queryset = ShopOffers.objects.published()
    pagination_class = None

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        technique_id = int(request.data.get('id', 0))
        order = int(request.data.get('order', 0))
        technique = get_object_or_None(Techniques, pk=technique_id)
        if technique is None:
            raise exceptions.ValidationError(_('Техника не найдена, обновите страницу и попробуйте еще раз.'))

        instance = self.get_object()
        ShopOfferTechniques.objects.create(offer=instance, technique=technique, order=order)
        return Response(TechniquesSerializer(technique).data, status=status.HTTP_201_CREATED)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        technique_id = int(request.GET.get('technique_id', 0))
        technique = get_object_or_None(Techniques, pk=technique_id)
        if technique is None:
            raise exceptions.ValidationError(_('Техника не найдена, обновите страницу и попробуйте еще раз.'))

        instance = self.get_object()
        instance.techniques.remove(technique)
        return Response(status=status.HTTP_204_NO_CONTENT)


class OfferRelatedListView(mixins.CreateModelMixin, generics.RetrieveUpdateDestroyAPIView):
    """
    API endpoint that allows add and destroy relatedOffers in offer.
    """
    permission_classes = [permissions.IsAuthenticated, IsEditorOrReadOnly]
    serializer_class = OfferRelatedSerializer
    queryset = ShopOffers.objects.published()
    pagination_class = None

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        related_id = int(request.data.get('id', 0))
        related_offer = get_object_or_None(ShopOffers, pk=related_id)
        if related_offer is None:
            raise exceptions.ValidationError(_('Товар не найден, обновите страницу и попробуйте еще раз.'))

        instance = self.get_object()
        instance.related_offers.add(related_offer)
        return Response(TinyShopSerializer(related_offer).data, status=status.HTTP_201_CREATED)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        related_id = int(request.GET.get('related_id', 0))
        related_offer = get_object_or_None(ShopOffers, pk=related_id)
        if related_offer is None:
            raise exceptions.ValidationError(_('Товар не найден, обновите страницу и попробуйте еще раз.'))

        instance = self.get_object()
        instance.related_offers.remove(related_offer)
        return Response(status=status.HTTP_204_NO_CONTENT)


class ShopArrangementsViewSet(UserIPMixin, viewsets.ModelViewSet):
    """
    API endpoint that allows manage shop arrangements.
    """
    permission_classes = [permissions.IsAuthenticated, IsModeratorOrOwner]
    filter_backends = [IsOwnerFilterBackend, IsArrangementsFilterBackend]
    queryset = ShopOffers.objects.active()
    serializer_class = ShopArrangementSerializer
    pagination_class = None

    @action(detail=False,
            permission_classes=[permissions.IsAuthenticated, IsModeratorOnly],
            filter_backends=[IsUncheckedFilterBackend, IsArrangementsFilterBackend])
    def mlist(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    @action(detail=True,
            permission_classes=[permissions.IsAuthenticated, IsModeratorOnly],
            filter_backends=[IsArrangementsFilterBackend],
            serializer_class=ShopModeratorArrangementSerializer)
    def mget(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    @action(methods=['patch'], detail=True,
            permission_classes=[permissions.IsAuthenticated, IsModeratorOnly],
            filter_backends=[IsArrangementsFilterBackend],
            serializer_class=ShopModeratorArrangementSerializer)
    def mudpate(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)
        response['Cache-Control'] = 'no-cache, no-store, must-revalidate'
        response['Pragma'] = 'no-cache'
        return response

    def get_language(self):
        if not hasattr(self, 'lang'):
            self.lang = Language.objects.filter(code=translation.get_language())
        return self.lang

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context.update({
            'user': self.request.user,
            'currency': self.get_currency(),
            'lang': self.get_language(),
        })
        return context

    def get_object(self, queryset=None):
        self.object = super().get_object()
        if self.object.is_freeze:
            raise exceptions.PermissionDenied(_('Вы не можете редактировать данный товар сейчас.'))
        return self.object

    def set_cpf_data(self, data):
        lang = self.get_language()[0].code.replace('-', '_')
        name_tr_field__iexact = '{0}_{1}__iexact'.format('name', lang)

        cpf_fields = {'composer': Composer, 'performer': Performer, 'film': Film}

        for key, model in cpf_fields.items():
            if key in data and data[key]:
                value = data[key]['value']
                # новое значение
                if '__isNew__' in data[key].keys():
                    value = value.strip()
                    if len(value):
                        try:
                            cpf = model.objects.get(**{name_tr_field__iexact: value})
                        except model.DoesNotExist:
                            cpf = model.objects.create_item(name=value, lang=lang, user=self.request.user,
                                                            accepted=self.request.user.is_moderator)
                        data[key] = cpf.pk
                    else:
                        data[key] = None
                else:
                    data[key] = value
        return data

    def create(self, request, *args, **kwargs):
        data = copy(request.data)
        data = self.set_cpf_data(data)
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()

        data = copy(request.data)
        data = self.set_cpf_data(data)

        if not request.user.is_moderator:
            from dirtyfields.compare import compare_states, raw_compare
            from django.forms import model_to_dict
            fields = ['name', 'description', 'video_url',
                      'composer', 'performer', 'film']
            instance_dict = model_to_dict(instance, fields=fields)
            instance_dict.update({
                'tags': [x.id for x in instance.tags.all()]
            })
            modified_fields = compare_states(instance_dict,
                                             data,
                                             (raw_compare, {}))
            if len(modified_fields):
                data.update({
                    'modified_fields': modified_fields,
                    'checked': False
                })

        serializer = self.get_serializer(instance, data=data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)

    def perform_destroy(self, instance):
        if instance.is_buy:
            # Удаление товаров, по которым были покупки, запрещено
            raise exceptions.PermissionDenied(detail=_('Вы %s не можете удалить этот объект.'))
        instance.delete(user_obj=self.request.user)


class ShopLessonsViewSet(ChangesBetweenModelsMix, ShopArrangementsViewSet):
    """
    API endpoint that allows user retrieve, create and update lesson offer.
    """
    filter_backends = [IsOwnerFilterBackend, IsLessonsFilterBackend]
    serializer_class = ShopLessonSerializer

    @action(detail=False,
            permission_classes=[permissions.IsAuthenticated, IsModeratorOnly],
            filter_backends=[IsUncheckedFilterBackend, IsLessonsFilterBackend])
    def mlist(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    @action(detail=True,
            permission_classes=[permissions.IsAuthenticated, IsModeratorOnly],
            filter_backends=[IsLessonsFilterBackend])
    def mget(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    @action(methods=['patch'], detail=True,
            permission_classes=[permissions.IsAuthenticated, IsModeratorOnly],
            filter_backends=[IsLessonsFilterBackend])
    def mudpate(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def set_lesson_data(self, data):
        data.update({
            'lesson': {
                'content': data.get('lesson_content', ''),
                'video_content_url': data.get('lesson_video_content_url', ''),
            }
        })
        return data

    def create(self, request, *args, **kwargs):
        data = copy(request.data)
        data = self.set_lesson_data(data)
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        data = copy(request.data)
        data = self.set_lesson_data(data)
        serializer = self.get_serializer(instance, data=data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)


class ShopUserFilesViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows upload/download files of offers.
    """
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
    permission_classes = [permissions.IsAuthenticated]
    queryset = UserFile.objects.all()
    serializer_class = UserFileSerializer
    filter_backends = [IsOwnerOrModeratorFilterBackend]
    parser_class = (FileUploadParser,)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        mimetypes.init()
        try:
            response = self.get_response(instance)
        except IOError:
            response = HttpResponseNotFound()
        return response

    def get_response(self, instance):
        mime_type_guess = mimetypes.guess_type(instance.user_filename)
        if mime_type_guess:
            response = HttpResponse(content_type=mime_type_guess[0])
            file_name = os.path.basename(instance.get_file_path())
            response['Content-Disposition'] = 'attachment; filename=%s' % file_name
        else:
            response = HttpResponse()
        response['x-accel-redirect'] = settings.FILES_URL_NGINX + instance.get_file_url()
        return response

    def delete(self, request, *args, **kwargs):
        pk = request.query_params.get('fileId')
        if pk:
            self.kwargs.update(pk=pk)
        return self.destroy(request, *args, **kwargs)


class UpdateOfferImage(generics.UpdateAPIView):
    permission_classes = [permissions.IsAuthenticated]
    queryset = ShopOffers.objects.active()
    serializer_class = ShopImageSerializer
    filter_backends = [IsOwnerOrModeratorFilterBackend]
    parser_class = (FileUploadParser,)

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context.update({
            'user': self.request.user,
        })
        return context

    def post(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    def perform_update(self, serializer):
        instance = self.get_object()
        if instance.image:
            delete(instance.image)
            instance.image.delete(save=True)
        serializer.save()
        return Response(serializer.data)


class UpdateOfferFile(generics.UpdateAPIView):
    permission_classes = [permissions.IsAuthenticated]
    queryset = UserFile.objects.all()
    serializer_class = UserFileNoteSerializer
    filter_backends = [IsOwnerOrModeratorFilterBackend]
