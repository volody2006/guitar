from django.conf import settings
from rest_framework import filters


class IsOwnerFilterBackend(filters.BaseFilterBackend):
    """
    Filter that only allows users to see their own objects.
    """
    def filter_queryset(self, request, queryset, view):
        return queryset.filter(user_created=request.user)


class IsOwnerOrModeratorFilterBackend(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        if request.user.is_moderator:
            return queryset
        return queryset.filter(user_created=request.user)


class IsUncheckedFilterBackend(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        return queryset.filter(checked=False, is_available=True)


class IsArrangementsFilterBackend(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        return queryset.filter(shopoffers_type__slug=settings.SHOP_ARRANGEMENTS_TYPE_SLUG)


class IsLessonsFilterBackend(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        return queryset.filter(shopoffers_type__slug=settings.SHOP_LESSONS_TYPE_SLUG)
