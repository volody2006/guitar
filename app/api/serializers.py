# -*- coding: utf-8 -*-
import logging
import os

from django.conf import settings
from django.db.utils import ProgrammingError
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _
from embed_video.backends import UnknownBackendException, detect_backend
from rest_framework import serializers

from base.mail_utils import send_moder_mail
from base.utils import format_price, get_object_or_None
from gis.managers import get_country_context
from shop.models import Composer, Film, Lesson, Performer, ShopOfferTechniques, ShopOffers, ShopOffersType, TagOffers, \
    Techniques, UserFile
from shop.tasks import task_approve_cpf_after_offer_checked, task_price_seo_save
from shop.translation import LessonsTranslationOptions, ShopOffersTranslationOptions
from shop.utils import compression_file, convert_user_pdf_to_shop_offer_image

logger = logging.getLogger(__name__)

try:
    arrangements_type_pk = ShopOffersType.objects.get(slug=settings.SHOP_ARRANGEMENTS_TYPE_SLUG).pk
    lessons_type_pk = ShopOffersType.objects.get(slug=settings.SHOP_LESSONS_TYPE_SLUG).pk
except (ProgrammingError, ShopOffersType.DoesNotExist):
    arrangements_type_pk = 0
    lessons_type_pk = 0


class TagsSerializer(serializers.ModelSerializer):
    class Meta:
        model = TagOffers
        fields = ['id', 'name']


class TechniquesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Techniques
        fields = ['id', 'name']


class TinyShopSerializer(serializers.ModelSerializer):
    author = serializers.CharField(source='user_created.full_name', read_only=True)
    date_created = serializers.SerializerMethodField(source='get_date_created', read_only=True)

    def get_date_created(self, obj):
        from django.utils.formats import date_format
        return date_format(obj.date_created)

    class Meta:
        model = ShopOffers
        fields = ['id', 'name', 'shopoffers_type', 'author', 'date_created']


class OfferTechniquesSerializer(serializers.ModelSerializer):
    techniques = serializers.SerializerMethodField('get_techniques_list')

    def get_techniques_list(self, instance):
        ordered_queryset = [x.technique
                            for x in ShopOfferTechniques.objects.filter(offer_id=instance.id).order_by('order')]
        return TechniquesSerializer(ordered_queryset, many=True, context=self.context).data

    class Meta:
        model = ShopOffers
        fields = ['id', 'techniques']


class OfferRelatedSerializer(serializers.ModelSerializer):
    related_offers = TinyShopSerializer(many=True)

    class Meta:
        model = ShopOffers
        fields = ['id', 'shopoffers_type', 'related_offers']


class ComposerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Composer
        fields = ['id', 'name']


class PerformerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Performer
        fields = ['id', 'name']


class FilmSerializer(serializers.ModelSerializer):
    class Meta:
        model = Film
        fields = ['id', 'name']


class ShopImageSerializer(serializers.ModelSerializer):
    def update(self, instance, validated_data):
        user = self.context.get('user')
        if not user.is_moderator:
            validated_data.update({'checked': False})
        offer = super(ShopImageSerializer, self).update(instance, validated_data)
        if not user.is_moderator:
            send_moder_mail(offer, subject='Обновленный Товар', message='Автор заменил скриншот нот/лекций.')
        return offer

    class Meta:
        model = ShopOffers
        fields = ['id', 'image']


class ShopCreateUpdateSerializer(serializers.ModelSerializer):
    shopoffers_type_id = serializers.HiddenField(
        default=serializers.CreateOnlyDefault(arrangements_type_pk)
    )

    price = serializers.DecimalField(max_digits=15, decimal_places=2)
    tags = serializers.PrimaryKeyRelatedField(many=True, allow_empty=False, queryset=TagOffers.objects.all())
    agree_promotion = serializers.BooleanField(default=False)
    files = serializers.DictField(default={}, allow_empty=True)
    modified_fields = serializers.DictField(default={}, allow_empty=True)

    # поля только для отображения
    image = serializers.CharField(source='get_full_image_url', read_only=True)
    url = serializers.CharField(source='get_absolute_url', read_only=True)
    difficulty_display = serializers.CharField(source='get_difficulty_display', read_only=True)
    price_format = serializers.SerializerMethodField(read_only=True)
    existing_files = serializers.SerializerMethodField(read_only=True)

    def get_price_format(self, obj):
        price = format_price(obj.price if obj.price else 0)
        if format:
            return '{}{}'.format(obj.currency, price)
        return price

    def get_votes_score(self, obj):
        return int(obj.votes_score)

    def get_date_created(self, obj):
        from django.utils.formats import date_format
        return date_format(obj.date_created)

    def get_existing_files(self, obj):
        files = []
        for file in UserFile.objects.filter(item=obj):
            files.append({
                'id': file.pk,
                'name': file.user_filename,
                'size': file.st_size,
                'lastModifiedDate': file.date_created
            })
        return files

    def set_translation_fields(self, fields, validated_data):
        # заполняем переведенные поля
        lang = self.context.get('lang')[0]
        for tr_field in fields:
            field_lang = '{0}_{1}'.format(tr_field, lang.code.replace('-', '_'))
            attr = validated_data.get(tr_field, '')
            validated_data.update({field_lang: attr})
        return validated_data

    def set_moderator_data(self, validated_data, is_new):
        # заполняем поля про модератора
        user = self.context.get('user')
        # если модератор создает новые контент, то он его уже проверил
        if user.is_moderator and is_new:
            checked = True
        # если не модератор создает новые контент то он должен быть проверен
        elif not user.is_moderator and is_new:
            checked = False
        # обновление контента - модеатор сам ставит галочку
        else:
            checked = validated_data.get('checked', False)

        validated_data.update({'checked': checked})

        if user.is_moderator and checked:
            validated_data.update({
                'moderator': user,
                'date_approval_moderator': now()
            })
        return validated_data

    def set_additional_data(self, validated_data, is_new):
        # заполняем поля про модератора
        validated_data = self.set_moderator_data(validated_data, is_new)

        # заполняем переведенные поля
        tr_fields = ShopOffersTranslationOptions(ShopOffers).fields
        validated_data = self.set_translation_fields(tr_fields, validated_data)
        return validated_data

    def after_save(self, offer, files, is_new, **kwargs):
        # асинхронно выполняем остальные действия с товаром, чтоб пользователь не ждал
        checked = kwargs.get('checked', False)
        modified_fields = kwargs.get('modified_fields', {})

        files_changed = False
        # список id UserFile, которые были добавлены но не ассоциированы с товаром
        new_files = files.get('newUploadedFiles')
        if new_files and len(new_files):
            modified_fields.update({'files_new': new_files})
            files_changed = True
            UserFile.objects.filter(pk__in=new_files).update(item=offer)
        if not offer.is_buy:
            # список id UserFile, которые были ассоциированы с товаром ранее, и должны быть удалены
            del_files = files.get('alreadyAssociatedRemovedFiles')
            if del_files and len(del_files):
                files_del = []
                files_changed = True
                for file in UserFile.objects.filter(pk__in=del_files):
                    files_del.append(file.file.name)
                    file.delete()
                modified_fields.update({'files_del': files_del})

        if files_changed:
            convert_user_pdf_to_shop_offer_image(offer)

        # пересохранение цен и сео параметров
        task_price_seo_save.apply_async(args=[offer.id, is_new], countdown=10)

        user = self.context.get('user')
        if user.is_moderator and checked:
            # если модератор принял товар, значит принял и прикрепленные к нему CPF параметры
            task_approve_cpf_after_offer_checked.apply_async(args=[offer.id, ], countdown=10)
            # формируем архив
            compression_file(offer)

        # if not user.is_moderator and not checked:
        if not checked:
            changes = None
            if len(modified_fields):
                changes = self.format_modified_fields(modified_fields)
            if is_new:
                subject_start = 'Обновленный Товар'
            else:
                subject_start = 'Новый Товар'
            send_moder_mail(offer,
                            subject='[Модер] {} #{} {}'.format(subject_start, offer.pk, offer),
                            changes=changes)

    def format_modified_fields(self, modified_fields):
        changes = []
        for key, value in modified_fields.items():
            if key in ['name', 'description', 'video_url']:
                changes.append(tuple((key, value['current'], value['saved'])))
            if key in ['composer', 'performer', 'film']:
                changes.append(self.format_cpf_modified_fields(key, value))
            if key == 'tags':
                old_value = value['current']
                if old_value:
                    old_value = ', '.join(list(TagOffers.objects.filter(pk__in=value['current'])
                                               .values_list('name', flat=True)))
                new_value = value['saved']
                if new_value:
                    new_value = ', '.join(list(TagOffers.objects.filter(pk__in=value['saved'])
                                               .values_list('name', flat=True)))
                changes.append(tuple((key, old_value, new_value)))
            if key == 'files_new':
                old_value = '-'
                files = UserFile.objects.filter(pk__in=value)
                new_value = ', '.join([x.file.name for x in files])
                changes.append(tuple((key, old_value, new_value)))
            if key == 'files_del':
                old_value = ', '.join(value)
                new_value = '-'
                changes.append(tuple((key, old_value, new_value)))
        return changes

    def format_cpf_modified_fields(self, key, value):
        old_value = value['current']
        new_value = value['saved']

        if key == 'composer':
            model = Composer
        if key == 'performer':
            model = Performer
        if key == 'film':
            model = Film
        if old_value:
            item = get_object_or_None(model, pk=int(old_value))
            old_value = item.name if item else None
        if new_value:
            item = get_object_or_None(model, pk=int(new_value))
            new_value = item.name if item else None
        return key, old_value, new_value

    def create(self, validated_data):
        """
        Create and return a `ShopOffer` instance, given the validated data.
        """
        files = validated_data.pop('files')
        # print(validated_data)
        validated_data.update({
            'user_created': self.context.get('user'),
            'currency': self.context.get('currency'),
            # 'lang': self.context.get('lang')
        })
        # дополняем необходимыми данными: про модератора, переводы
        validated_data = self.set_additional_data(validated_data, is_new=True)
        validated_data.pop('modified_fields')
        offer = super(ShopCreateUpdateSerializer, self).create(validated_data)
        self.after_save(offer, files, is_new=True)
        return offer

    def update(self, instance, validated_data):
        # дополняем необходимыми данными: про модератора, переводы
        files = validated_data.pop('files')
        modified_fields = validated_data.pop('modified_fields')
        checked = validated_data.get('checked', False)
        validated_data = self.set_additional_data(validated_data, is_new=False)

        offer = super(ShopCreateUpdateSerializer, self).update(instance, validated_data)
        self.after_save(offer, files, is_new=False, checked=checked, modified_fields=modified_fields)
        return offer

    class Meta:
        model = ShopOffers
        fields = ['id', 'user_created', 'shopoffers_type_id', 'name', 'description', 'tags', 'video_url', 'price',
                  'difficulty', 'agree_promotion', 'is_free', 'is_buy', 'checked', 'files', 'modified_fields',
                  'image', 'url', 'difficulty_display', 'author', 'price_format', 'date_format',
                  # 'thumbnail',
                  'existing_files', 'title',
                  'comments_count', 'views_count', 'int_votes_score', 'pay_count']


class LessonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lesson
        fields = ['content', 'video_content_url']


class ShopLessonSerializer(ShopCreateUpdateSerializer):
    shopoffers_type_id = serializers.HiddenField(
        default=serializers.CreateOnlyDefault(lessons_type_pk)
    )
    lesson = LessonSerializer()

    def create(self, validated_data):
        """
        Create and return a `ShopOffer` instance, given the validated data.
        """
        lesson_data = validated_data.pop('lesson')
        offer = super(ShopLessonSerializer, self).create(validated_data)

        # заполняем переведенные поля для урока
        tr_fields = LessonsTranslationOptions(Lesson).fields
        lesson_data = self.set_translation_fields(tr_fields, lesson_data)

        Lesson.objects.create(offer=offer, **lesson_data)
        return offer

    def update(self, instance, validated_data):
        lesson_data = validated_data.pop('lesson')
        offer = super(ShopLessonSerializer, self).update(instance, validated_data)

        # заполняем переведенные поля для урока
        tr_fields = LessonsTranslationOptions(Lesson).fields
        lesson_data = self.set_translation_fields(tr_fields, lesson_data)

        for attr, value in lesson_data.items():
            setattr(offer.lesson, attr, value)
        offer.lesson.save()

        return offer

    class Meta(ShopCreateUpdateSerializer.Meta):
        fields = ShopCreateUpdateSerializer.Meta.fields + ['lesson']


class ShopArrangementSerializer(ShopCreateUpdateSerializer):
    free = serializers.BooleanField(source='is_free', read_only=True)

    tonality_display = serializers.CharField(source='get_tonality_display', read_only=True)

    def validate(self, attrs):
        if not attrs.get('composer') and not attrs.get('performer'):
            raise serializers.ValidationError({'composer': _('Одно из полей "Композитор" или '
                                                             '"Эстрадный исполнитель" '
                                                             'должно быть обязательно заполнено')})
        return attrs

    class Meta(ShopCreateUpdateSerializer.Meta):
        fields = ShopCreateUpdateSerializer.Meta.fields + ['composer', 'performer', 'film', 'tonality', 'author_music',
                                                           'free', 'tonality_display']


class ShopModeratorArrangementSerializer(ShopArrangementSerializer):
    class Meta(ShopArrangementSerializer.Meta):
        fields = ShopArrangementSerializer.Meta.fields + ['seo_title', 'seo_metadesc', 'seo_metakeywords']


class UserFileSerializer(serializers.ModelSerializer):
    user_created = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )

    # @todo в текущем дропзоне нет возможности изменить вывод ошибки
    # def validate_file(self, value):
    #     print('validate_file', value.content_type)
    #     if value.content_type not in ['application/pdf', ]:
    #         raise serializers.ValidationError(_('Неверный формат файла'))
    #     return value

    def create(self, validated_data):
        # example {'file': <_io.BytesIO object at 0x7fab20126f68>, '_name': '2020-05-12 at 19.10.00.jpeg',
        # 'size': 170342, 'content_type': 'image/jpeg', 'charset': None, 'content_type_extra': {}, 'field_name': 'file'}
        file_obj = validated_data['file']
        validated_data.update({
            'user_filename': file_obj.name,
            'mime_type': os.path.splitext(file_obj.name)[1][1:].strip().lower() or '',
        })
        file = super(UserFileSerializer, self).create(validated_data)
        return file

    class Meta:
        model = UserFile
        fields = ['id', 'user_created', 'file', 'user_filename', 'mime_type']


class UserFileNoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserFile
        fields = ['note']


class ShopInfoSerializer(serializers.ModelSerializer):
    checked = serializers.BooleanField(read_only=True)

    url = serializers.CharField(source='get_absolute_url', read_only=True)
    difficulty_display = serializers.CharField(source='get_difficulty_display', read_only=True)
    tonality_display = serializers.CharField(source='get_tonality_display', read_only=True)

    price_format = serializers.SerializerMethodField()
    # цена без скидки, только число, необходимо для расчетов в корзине
    price_number = serializers.SerializerMethodField()
    discount_price = serializers.SerializerMethodField()
    # цена со скидкой, только число, необходимо для сортировки по цене
    discount_price_number = serializers.SerializerMethodField()
    # цена со скидкой если есть скидка, иначе просто цена, только число, необходимо для отправки аналитики
    real_price_number = serializers.SerializerMethodField()

    video_code = serializers.SerializerMethodField()
    type_icon = serializers.CharField(source='shopoffers_type.icon', read_only=True)
    type_name = serializers.CharField(source='shopoffers_type.name', read_only=True)

    def _price(self, obj, price_type='price', format=True):
        country = get_country_context(self.context)
        try:
            if price_type == 'price':
                price, currency = obj.get_price_country(country)
            else:
                price, currency = obj.get_discount_price_country(country)
        except AttributeError:
            return ''
        price = format_price(price)
        if format:
            return '{}{}'.format(currency, price)
        return price

    def get_price_format(self, obj):
        return self._price(obj, price_type='price')

    def get_price_number(self, obj):
        return self._price(obj, price_type='price', format=False)

    def get_discount_price(self, obj):
        return self._price(obj, price_type='discount_price')

    def get_discount_price_number(self, obj):
        return self._price(obj, price_type='discount_price', format=False)

    def get_real_price_number(self, obj):
        if obj.discount > 0:
            return self._price(obj, price_type='discount_price', format=False)
        return self._price(obj, price_type='price', format=False)

    def get_video_code(self, obj):
        try:
            video_backend = detect_backend(obj.video_url)
            return video_backend.code
        except UnknownBackendException:
            return None

    class Meta:
        model = ShopOffers
        fields = ['id', 'shopoffers_type', 'url', 'name', 'title', 'offer_type', 'video_code', 'thumbnail',
                  'user_created', 'author', 'author_url',
                  'difficulty', 'difficulty_display', 'tonality', 'tonality_display', 'type_icon', 'type_name',
                  'price_format', 'price_number', 'discount', 'discount_price', 'discount_price_number',
                  'real_price_number',
                  'comments_count', 'views_count', 'int_votes_score', 'pay_count',
                  'checked', 'date_created', 'date_format', 'is_buy', 'is_freeze', 'is_valid',
                  ]
