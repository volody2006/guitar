# -*- coding: utf-8 -*-
from django.conf import settings
from django.urls import include, path
from rest_framework import routers
from shop.api import views

router = routers.DefaultRouter()

router.register('shop', views.ShopViewSet)
router.register('shop-composers', views.ShopComposersViewSet, basename='shop_composers')
router.register('shop-performers', views.ShopPerformersViewSet, basename='shop_performers')
router.register('shop-films', views.ShopFilmsViewSet, basename='shop_films')
router.register('shop-files', views.ShopUserFilesViewSet, basename='shop_files')

router.register('shop-lessons', views.ShopLessonsViewSet, basename='shop_lessons')
router.register('shop-arrangements', views.ShopArrangementsViewSet, basename='shop_arrangements')

urlpatterns = [
    path('shop/buy/<int:pk>/', views.ShopBuyView.as_view()),
    path('shop/tags/{}-<slug:shopoffers_type_slug>/'.format(settings.SITE_PROJECT), views.TagsListView.as_view()),
    path('shop/techniques/', views.TechniquesListView.as_view(), name='shop_techniques'),
    path('shop/difficulties/', views.DifficultiesListView.as_view()),
    path('shop/tonalities/', views.TonalitiesListView.as_view()),
    path('shop/systems/', views.SystemsListView.as_view()),
    path('shop/<int:pk>/', include([
        path('offer-techniques/', views.OfferTechniquesListView.as_view()),
        path('offer-related/', views.OfferRelatedListView.as_view()),
    ])),
    path('shop/image/<int:pk>/', views.UpdateOfferImage.as_view()),
    path('shop/file/<int:pk>/', views.UpdateOfferFile.as_view(), name='shop_update_file'),
    path('', include(router.urls)),
]
