# -*- coding: utf-8 -*-
import os
import random
from datetime import timedelta
from decimal import Decimal

from celery.schedules import crontab
from celery.task import periodic_task, task
from celery.utils.log import get_task_logger
from django.conf import settings
from django.core.mail import mail_managers
from django.template.loader import render_to_string
from django.utils import translation
from django.utils.timezone import now
from django.utils.translation import ugettext as _
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from premailer import Premailer

from base import mail_utils
from shop.models import Sale, ShopOffers, UserFile
from shop.utils import compression_file, disable_all_discounts, notifying_all_users_about_discounts
from yandex_turbo.tasks import upload_turbo_rss

logger = get_task_logger(__name__)


@periodic_task(run_every=timedelta(hours=1))
def task_download_video_preview():
    logger.info('start download_video_preview')
    from django.core.management import call_command
    call_command('download_video_preview')


@task
def task_price_seo_save(id, force=False):
    logger.info('Start task_price_seo_save')
    try:
        offer = ShopOffers.objects.get(id=id)
        offer.seo_save(force=force)
        offer.price_save(force=force)
    except ShopOffers.DoesNotExist:
        pass


@task
def task_approve_cpf_after_offer_checked(id):
    logger.info('Start task_approve_cpf_after_offer_checked')
    try:
        item = ShopOffers.objects.get(id=id)
        if item.checked:
            if item.composer and not item.composer.accepted:
                item.composer.accepted = True
                item.composer.save()
            if item.performer and not item.performer.accepted:
                item.performer.accepted = True
                item.performer.save()
            if item.film and not item.film.accepted:
                item.film.accepted = True
                item.film.save()
    except ShopOffers.DoesNotExist:
        pass


@periodic_task(run_every=crontab(minute=0, hour=0))
def set_daily_sale(test=False):
    """
    Назначаем аранжировки для дневной распродажи
    каждый день в 00:05
    """
    logger.info('set_daily_sale start task')
    if Sale.objects.running(sale_day_on=False).exists():
        return
    disables = disable_all_discounts(sale_day=True)
    if Sale.objects.time_to_run().filter(sale_day_on=False).exists():
        logger.info('set_daily_sale: На сегодня есть распродажа, пропускаем ежедневные скидки')
        return
    old_offer_ids = disables['offers_ids']
    old_offer_sellers_ids = disables['sellers_ids']
    shop_published = ShopOffers.objects.published().filter(user_created__not_agree_promotion=False,
                                                           agree_promotion=True,
                                                           price__gt=Decimal('0.00'),
                                                           discount=0,
                                                           shopoffers_type__slug=settings.SHOP_ARRANGEMENTS_TYPE_SLUG)
    seller_id = shop_published.values_list('user_created_id', flat=True)
    seti = set(seller_id)
    delta = len(set(seller_id)) - len(old_offer_sellers_ids)
    logger.info('set_daily_sale: delta {} {}'.format(delta, seti))
    if delta < 4:
        old_offer_sellers_ids = []
    offer_list = []
    for discount in [90, 60, 45, 30]:
        offers = shop_published.exclude(pk__in=old_offer_ids, )
        user_dic = shop_published.filter(**{'user_created__agree_sale_{}'.format(discount): True}).values_list(
            'user_created_id', flat=True)
        result = list(set(user_dic) - set(old_offer_sellers_ids))
        if result:
            logger.info('set_daily_sale: Есть пользователи для исключения. result: {}'.format(result))
            offers = offers.exclude(user_created__in=old_offer_sellers_ids)
        logger.info('set_daily_sale: пользователи для скидки {}%  {}, Исключены {}'.format(discount, set(user_dic),
                                                                                           old_offer_sellers_ids))

        tmp_offer_ids = offers.filter(**{'user_created__agree_sale_{}'.format(discount): True}
                                      ).values_list('id', flat=True)
        logger.info('len(tmp_offer_ids) {}'.format(len(tmp_offer_ids)))
        if tmp_offer_ids:
            id = random.choice(tmp_offer_ids)
            old_offer_ids.append(id)
            offer = ShopOffers.objects.get(pk=id)
            cause_freezing = _('Участвует в акции.')
            offer.set_discount(discount=discount, date_end=now() + timedelta(days=1),
                               cause=cause_freezing, sale_day=True)
            offer_list.append(offer.id)
            if discount == 90 and not test is True:
                from youtube_video.utils import download_video_thumbnail_source
                download_video_thumbnail_source(shop_offer=offer)

                file_path_water = '{}/youtube_preview/source/{}_source.jpg'.format(settings.MEDIA_ROOT,
                                                                                   offer.pk)
                file_path_feed = '{}/youtube_preview/source/feed_image.jpg'.format(settings.MEDIA_ROOT)
                # print(file_path_water)

                if os.path.isfile(file_path_water):
                    # print(111)
                    os.rename(file_path_water, file_path_feed)
            old_offer_sellers_ids.append(offer.user_created_id)
        else:
            logger.info('set_daily_sale: Выбирать не из чего.')
    notifying_all_users_about_discounts(offer_list)
    return ShopOffers.objects.filter(discount__gt=0).count()


@task
def run_task_compression_file(item_id):
    logger.info('Start run_task_compression_file item_id {}'.format(item_id))
    compression_file(item_id)


@periodic_task(run_every=timedelta(seconds=59 * 59))
def task_all_compression_files(force=False, all=False):
    if all:
        items = ShopOffers.objects.all()
    else:
        items = ShopOffers.objects.published()
    for item in items:
        if item.file.name == '' or force:
            compression_file(item)


@periodic_task(run_every=timedelta(seconds=1 * 60 * 5))
def check_start_or_end_sale():
    logger.debug('check_start_or_end_sale start task')
    # Проверяем пора ли запустить(остановить) распродажу.
    for sale in Sale.objects.time_to_run():
        task_sale_up(sale.pk)
    for sale in Sale.objects.time_to_stop():
        task_sale_down(sale.pk)


@task()
def task_sale_up(sale, force=False):
    if isinstance(sale, Sale):
        sale = sale
    else:
        sale = Sale.objects.get(pk=sale)
    logger.info('task_sale_up run')
    if sale.up(force=force):
        logger.info('Распродажа {} успешно поднялась'.format(sale))
        offer_list = sale.sale_offers.all().values_list('id', flat=True)
        notifying_all_users_about_discounts(offer_list)
    else:
        logger.info('Распродажа {} не смогла запуститься'.format(sale))


@task()
def task_sale_down(sale):
    if isinstance(sale, Sale):
        sale = sale
    else:
        sale = Sale.objects.get(pk=sale)
    logger.info('task_sale_up run')
    if sale.down():
        logger.info('Распродажа {} успешно остановлена'.format(sale))
    else:
        logger.info('Распродажа {} не может быть остановлена'.format(sale))


@periodic_task(run_every=crontab(minute=15, hour=7))
def upload_shop_turbo_rss():
    """
    Список аранжировок для турбо рсс
    """
    logger.info('Start Upload shop turbo rss')
    shop_offers = ShopOffers.objects.filter(is_delete=False, moderator__isnull=False, is_available=True, checked=True)
    upload_turbo_rss(object_list=shop_offers, tpl='shop/turbo_rss.xml')


@periodic_task(run_every=timedelta(minutes=10))
def task_get_video_info_offers():
    from youtube_video.tasks import task_get_video_info
    logger.info('Start task_get_video_info')
    shop_offers = ShopOffers.objects.filter(video_url__isnull=False,
                                            video_thumbnail_url__isnull=True)
    for offer in shop_offers:
        task_get_video_info.delay(offer.pk, offer.content_type.pk)


@periodic_task(run_every=timedelta(minutes=10))
def check_video_available():
    """
    проверка доступности видео в опубликованных аранжировках
    """
    if settings.DEBUG:
        logger.info('check_video_available при DEBUG не работает!')
        return

    videos = []

    shop_offers = {}

    one_day_ago = now() - timedelta(days=1)
    offers = ShopOffers.objects.filter(is_delete=False,
                                       moderator__isnull=False,
                                       is_available=True,
                                       checked=True,
                                       date_video_check__lt=one_day_ago,
                                       video_url__isnull=False,
                                       ).order_by('date_video_check')[:10]
    logger.info('check_video_available: start offers count check {}'.format(offers.count()))

    for offer in offers:
        videos.append(offer.video_id)
        shop_offers.update({offer.video_id: offer})
    try:
        service = build('youtube',
                        'v3',
                        developerKey=settings.YOUTUBE_DEVELOPER_API_KEY,
                        cache_discovery=False)
        # max 10 video id
        # shop_video_ids = videos[slice: slice + count]
        # video_ids = ','.join(shop_video_ids)
        results = service.videos().list(id=videos, part='status').execute()
        try:
            videos_from_youtube = [video_info['id'] for video_info in results['items']]
            for shop_video_id in videos:
                offer = shop_offers[shop_video_id]
                logger.debug('check_video_available: Update offer {} {}'.format(offer.pk, offer))
                if shop_video_id not in videos_from_youtube:
                    logger.debug('Video info from youtube for %s not found. send moder email.' % offer)
                    offer.checked = False
                    offer.service_note = 'Добавленный к контенту видео ролик недоступен'

                    # send moder email
                    tpl = 'manager_update_item.html'
                    subject = _('Добавленный к контенту видео ролик недоступен')
                    subject = '{} {}'.format(subject, offer)
                    context = dict(item=offer)
                    context.update({
                        'rootsiteurl': settings.SITE_ROOT,
                        'rootsitename': settings.SITE_NAME_FOR_EMAIL,
                        'supportemail': settings.SUPPORT_EMAIL,
                        'extends_template': 'base/mail/base_moder.html'
                    })
                    body_tpl = 'base/mail/%s' % tpl
                    html_email_message = render_to_string(body_tpl, context)
                    p = Premailer(html_email_message)
                    html_email_message_transform = p.transform()
                    message = subject
                    mail_managers(subject, message, html_message=html_email_message_transform)

                    # send author mail
                    lang = settings.LANGUAGE_CODE
                    if offer.user_created.lang:
                        lang = offer.user_created.lang.code
                    with translation.override(lang):
                        # отправляем автору письмо на предпочитаемом языке
                        mail_utils.send_email(offer.user_created,
                                              _('Аранжировка снята с продажи'),
                                              'video_not_available.html',
                                              {'offer': offer})
                offer.date_video_check = now()
                offer.save()
        except IndexError:
            logger.debug('We have no results')

    except HttpError as e:
        logger.error('Get video info from youtube. An HTTP error %d occurred:\n%s' % (e.resp.status, e.content))


@task()
def task_set_preview_sale_offers(sale_id):
    logger.info('Start task_set_preview_sale_offers #{}'.format(sale_id))
    sale = Sale.objects.get(pk=sale_id)
    if sale.is_global:
        logger.info('Распродажа глобальная. Пропускаем set_preview_sale_offers')
        return
    sale.preview_add_offers()
    logger.info('В распродаже будет товаров {}'.format(sale.preview_sale_offers.all().count()))


@periodic_task(run_every=crontab(minute=0, hour=1))
def delete_old_user_files():
    """
    Удаляем брошенные пользовательские файлы, размещенные более суток назад
    каждый день в 01:00
    """
    one_day_ago = now() - timedelta(days=1)
    for file in UserFile.objects.filter(date_created__lt=one_day_ago, item__isnull=True):
        file.delete()
