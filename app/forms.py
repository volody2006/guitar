# -*- coding: utf-8 -*-
from decimal import Decimal

from captcha.fields import ReCaptchaField
from django import forms
from django.conf import settings
from django.utils import translation
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _
from django_select2.forms import ModelSelect2MultipleWidget, Select2Widget
from sorl.thumbnail.fields import ImageFormField

from base.fields import EmptyChoiceField
from base.forms import OverrideMixin, StripVideoUrlMixin
from base.widgets import FormImageWidget
from offer.forms import DescriptionRedactorForm
from payssion.models import PAYMENT_TYPE_CHOICES
from project import constants
from shop.models import Composer, Film, Performer, ShopOffers, TagOffers, UserFile


class ShopOffersWithoutAgreePromotionForm(OverrideMixin, DescriptionRedactorForm, StripVideoUrlMixin, forms.ModelForm):
    override_label = {
        'name': _('Название кавера, аранжировки, сочинения'),
        'description': _('Подробное описание'),
    }

    override_help_text = {
        'name': _('Не пишите в названии "ноты", "табы", "имя композитора"'),
        'video_url': _('Разрешается добавлять ссылки только на youtube-ролик'),
        'agree_promotion': _('Выключение данной опции, запретит выбирать эту аранжировку для участия в акциях.'),
    }
    override_required = {
        'name': 'required',
        'description': 'required',
        'tonality': 'required',
    }
    required_css_class = 'required'

    image = ImageFormField(label=_('Изображение'), widget=FormImageWidget(), required=True,
                           help_text=_('Демо-изображение половины первой страницы в формате ноты+табы (jpg или png)'))

    composer_name = forms.CharField(label=_('Композитор'), max_length=255, required=False,
                                    help_text=_('Если вы не знаете композитора данного произведения, '
                                                'то укажите эстрадного исполнителя'))
    performer_name = forms.CharField(label=_('Эстрадный исполнитель'), max_length=255, required=False,
                                     help_text=_('Поле является обязательным, '
                                                 'если вы не указали композитора данного произведения'))
    film_name = forms.CharField(label=_('Кинофильм'), max_length=255, required=False,
                                help_text=_('Укажите кинофильм, в котором звучало данное произведение'))

    tags = forms.ModelMultipleChoiceField(label=_('Теги'),
                                          queryset=TagOffers.objects.filter(hidden=False),
                                          required=True,
                                          help_text=_('Укажите теги, подходящие к вашей аранжировке'),
                                          widget=ModelSelect2MultipleWidget(queryset=None,
                                                                            model=TagOffers,
                                                                            search_fields=['name__icontains'],
                                                                            attrs={'class': 'form-control',
                                                                                   'data-placeholder': _('Теги')}
                                                                            ))

    free = forms.BooleanField(
        label=_('Я автор произведения и хочу опубликовать его бесплатно'),
        required=False,
    )

    def __init__(self, *args, **kwargs):
        initial = kwargs.get('initial')
        instance = kwargs.get('instance')
        self.user = kwargs.pop('user')
        self.lang = kwargs.pop('lang')
        self.user_free_count = self.user.shopoffers_set.filter(price__isnull=True).count()
        currency = initial.get('currency')
        if currency:
            help_text = str(_('Укажите стоимость, только цифры. Валюта %s.')) % currency.full_name
        else:
            raise Exception('Передайте валюту в форму.')
        self.override_help_text.update({'price': help_text})

        super().__init__(*args, **kwargs)
        self.fields['free'].help_text = '%s %d' % (_('Вам можно еще'),
                                                   (settings.SHOP_FREE_COUNT - self.user_free_count))

        if self.user_free_count == settings.SHOP_FREE_COUNT and not instance:
            self.fields['free'].widget.attrs = {'disabled': 'disabled'}
            self.fields['free'].help_text = _(
                'Вы уже опубликовали %s бесплатных аранжировок') % settings.SHOP_FREE_COUNT

        if instance:
            if self.instance.is_free:
                self.fields['price'].widget.attrs = {'disabled': 'disabled'}
                self.fields['free'].help_text = _('Снимите галочку для перевода аранжировки в платные.')
            else:
                # если были покупки, то перевод в бесплатные запрещен
                if self.instance.pay_count:
                    self.fields['free'].widget.attrs = {'disabled': 'disabled'}
                    self.fields['free'].help_text = _('Аранжировка уже была куплена. Перевод в бесплатные невозможен.')
                else:
                    if self.user_free_count == settings.SHOP_FREE_COUNT:
                        self.fields['free'].widget.attrs = {'disabled': 'disabled'}
                        self.fields['free'].help_text = _(
                            'Вы уже опубликовали %s бесплатных аранжировок') % settings.SHOP_FREE_COUNT
            # если аранжировка прошла модерацию и НЕ стоит галочка Авторское то в бесплатное перевести нельзя
            if instance.moderator is not None and not instance.author_music:
                try:
                    del (self.fields['price'].widget.attrs['disabled'])
                    del (self.fields['free'])
                except KeyError:
                    pass

        # for js validator
        self.fields['image'].widget.attrs = {'data-validate-field': 'image'}
        self.fields['name'].widget.attrs = {'data-validate-field': 'title'}
        self.fields['description'].widget.attrs = {'data-validate-field': 'content'}
        self.fields['tags'].widget.attrs = {'data-validate-field': 'tags'}
        self.fields['video_url'].widget.attrs = {'data-validate-field': 'video'}
        self.fields['composer_name'].widget.attrs = {'data-validate-field': 'composer'}
        self.fields['performer_name'].widget.attrs = {'data-validate-field': 'performer'}
        self.fields['price'].widget.attrs = {'data-validate-field': 'price'}

    def clean(self):
        price = self.cleaned_data.get('price') if self.cleaned_data.get('price') else Decimal(0)
        is_free = int(self.cleaned_data.get('free', 0))
        if not is_free:
            if price <= Decimal(0):
                self.add_error('price', _('Вы не указали цену.'))
        else:
            if not self.instance.is_free:
                if self.user_free_count + 1 > settings.SHOP_FREE_COUNT:
                    self.add_error('free', _('Вы превысили максимально возможно количество бесплатных аранжировок.'))

        composer_name = self.cleaned_data.get('composer_name')
        performer_name = self.cleaned_data.get('performer_name')
        if not composer_name and not performer_name:
            self.add_error('composer_name', '')
            self.add_error('performer_name', '')
            raise forms.ValidationError(_('Одно из полей "Композитор" или "Эстрадный исполнитель" '
                                          'должно быть обязательно заполнено'))

    def save(self, **kwargs):
        # Переделать в приемлемый вид, если у композитора (исполнителя, фильма) нет перевода и редактируется на
        #  другом языке, находить старого,  а не создавать нового.
        shop_offer = super().save(commit=False)

        composer_name = self.cleaned_data.get('composer_name').strip()
        if composer_name and composer_name != '':
            composer = None
            for lang_code in settings.MODELTRANSLATION_LANGUAGES:
                with translation.override(lang_code):
                    field_name = '{fld}_{lang_code}__iexact'.format(fld='name', lang_code=lang_code.replace('-', '_'))
                    if Composer.objects.filter({field_name: composer_name}).exists():
                        composer = Composer.objects.filter({field_name: composer_name})[0]
            shop_offer.composer = composer

        performer_name = self.cleaned_data.get('performer_name').strip()
        if performer_name and performer_name != '':
            performer = None
            for lang_code in settings.MODELTRANSLATION_LANGUAGES:
                with translation.override(lang_code):
                    field_name = '{fld}_{lang_code}__iexact'.format(fld='name', lang_code=lang_code.replace('-', '_'))
                    if Performer.objects.filter({field_name: performer_name}).exists():
                        performer = Performer.objects.filter({field_name: performer_name})[0]
            shop_offer.performer = performer

        film_name = self.cleaned_data.get('film_name').strip()
        if film_name and film_name != '':
            film = None
            for lang_code in settings.MODELTRANSLATION_LANGUAGES:
                with translation.override(lang_code):
                    field_name = '{fld}_{lang_code}__iexact'.format(fld='name', lang_code=lang_code.replace('-', '_'))
                    if Film.objects.filter({field_name: film_name}).exists():
                        film = Film.objects.filter({field_name: film_name})[0]
            shop_offer.film = film

        if shop_offer.checked is True and shop_offer.date_approval_moderator is None:
            if shop_offer.date_approval_moderator is None:
                shop_offer.date_approval_moderator = now()

        is_free = int(self.cleaned_data.get('free', 0))
        if is_free == 1:
            # бесплатно
            shop_offer.price = None
        shop_offer.save()
        return shop_offer

    class Meta:
        model = ShopOffers
        fields = ['image', 'name', 'tags', 'description', 'video_url',  # 'file',
                  'price', 'difficulty', 'tonality',
                  'agree_promotion']
        fieldsets = (
            (_('Описание'),
             '',
             {'fields': ('image', 'name', 'description', 'tags', 'video_url')},  # , 'file'
             ),
            (_('Характеристики'),
             '',
             {'fields': ('composer_name', 'performer_name', 'film_name', 'price', 'difficulty', 'tonality')},
             ),
        )


class ShopOffersForm(ShopOffersWithoutAgreePromotionForm):
    class Meta(ShopOffersWithoutAgreePromotionForm.Meta):
        fields = ShopOffersWithoutAgreePromotionForm.Meta.fields + ['agree_promotion', ]
        fieldsets = (
            (_('Описание'),
             '',
             {'fields': ('image', 'name', 'description', 'tags', 'video_url')},  # , 'file'
             ),
            (_('Характеристики'),
             '',
             {'fields': ('composer_name', 'performer_name', 'film_name', 'price', 'difficulty', 'tonality')},
             ),
            (_('Продвижение'),
             '',
             {'fields': ('agree_promotion',)},
             ),
        )


class AdminShopOffersForm(ShopOffersForm):
    class Meta(ShopOffersForm.Meta):
        fields = ShopOffersForm.Meta.fields + ['seo_title', 'seo_metadesc', 'seo_metakeywords', 'checked',
                                               'author_music']

        fieldsets = ShopOffersForm.Meta.fieldsets + (
            (_('SEO поля (не обязательно)'),
             '',
             {'fields': ('seo_title', 'seo_metadesc', 'seo_metakeywords')},
             ),
            (_('Модератор'),
             '',
             {'fields': ('checked', 'author_music')},
             ),
            # ('Только для разработчиков',  # Закоментировать, если нечего добавлять.
            #  '',
            #  {'fields': ('tags',)},
            #  ),
        )
        widgets = {
            'seo_title': forms.TextInput(),
            'seo_metadesc': forms.TextInput(),
            'seo_metakeywords': forms.TextInput(),
        }


class SelectShopSortForm(forms.Form):
    EMPTY_LABEL = _('Все')
    o = forms.CharField(required=False, widget=forms.HiddenInput())
    q = forms.CharField(required=False, widget=forms.HiddenInput())
    tag = forms.MultipleChoiceField(required=False, widget=forms.MultipleHiddenInput())
    lvl = EmptyChoiceField(label=_('Сложность'),
                           required=False,
                           choices=ShopOffers.DIFFICULTY_CHOICES,
                           empty_label=EMPTY_LABEL)
    tnl = EmptyChoiceField(label=_('Тональность'),
                           required=False,
                           choices=ShopOffers.TONALITY_CHOICES,
                           empty_label=EMPTY_LABEL)
    author_id = forms.ChoiceField(label=_('Автор'), choices=(), required=False,
                                  widget=Select2Widget(attrs={'class': 'form-control-sm',
                                                              'width': '100%',
                                                              'data-placeholder': EMPTY_LABEL}))

    def __init__(self, author_choices, current_author, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['author_id'].choices = author_choices
        if current_author:
            self.fields['author_id'].choices = ((current_author.pk, current_author.get_full_name_for_filter()),)


class SongTextForm(OverrideMixin, forms.ModelForm):
    override_required = {
        'song_text': 'required',
        'song_file': 'required',
    }
    override_label = {
        'song_text': _('Текст песни и аккорды'),
    }
    required_css_class = 'required'

    class Meta:
        model = ShopOffers
        fields = ['song_text', 'song_file']


class UserFileForm(forms.ModelForm):
    is_delete = forms.BooleanField()
    note = forms.CharField(max_length=255)

    class Meta:
        model = UserFile
        fields = ['file', ]


class UserFilEditForm(forms.ModelForm):
    note = forms.CharField(max_length=255)

    class Meta:
        model = UserFile
        fields = ['note', ]


class ShopBuyAllForm(forms.Form):
    """
    комплексная форма покупки всех аранжировок автора, формирующаяся в зависимости от входящих данных
    """
    required_css_class = 'required'

    def __init__(self, user, currency, *args, **kwargs):
        super(ShopBuyAllForm, self).__init__(*args, **kwargs)
        self.user = user
        use_payssion = False

        # накручиваем доп поля
        # если payssion включен и прикреплен к текущей валюте
        if settings.PAYSSION_TURN_ON and currency.payment_service == constants.PAYSSION:
            use_payssion = True
        # если пользователь авторизован и выбрал предпочитаемую систему оплаты
        if user.is_authenticated and user.payment_service:
            # если предпочитаемая система оплаты - Payssion
            if user.payment_service == constants.PAYSSION:
                use_payssion = True

        # если используем Пейссион
        if use_payssion:
            self.fields['payment_type'] = forms.ChoiceField(choices=PAYMENT_TYPE_CHOICES,
                                                            widget=forms.Select,
                                                            label=_('Вариант оплаты'))

        # если пользователь НЕ авторизован
        if user.is_anonymous:
            self.fields['email'] = forms.EmailField(label=_('Адрес электронной почты'),
                                                    help_text=_('Введите реальный адрес электронной почты, на него '
                                                                'будут отправлены выбранные товары и данные для входа '
                                                                'на сайт.')
                                                    )
            self.fields['captcha'] = ReCaptchaField(label='', attrs={'theme': 'clean', })
