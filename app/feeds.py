from django.urls import reverse

from base.mixins import SiteMapMix
from shop.models import ShopOffers
from site_settings import SHOP_ARRANGEMENTS_TYPE_SLUG, SHOP_LESSONS_TYPE_SLUG


class ShopArrangementsSitemap(SiteMapMix):
    changefreq = 'daily'
    priority = 0.5
    i18n = True

    def items(self):
        return ShopOffers.objects.filter(is_delete=False, moderator__isnull=False,
                                         shopoffers_type__slug=SHOP_ARRANGEMENTS_TYPE_SLUG).order_by('-pk')

    def lastmod(self, obj):
        return obj.date_created


class ShopLessonsSitemap(SiteMapMix):
    changefreq = 'daily'
    priority = 0.5
    i18n = True

    def items(self):
        return ShopOffers.objects.filter(is_delete=False, moderator__isnull=False,
                                         shopoffers_type__slug=SHOP_LESSONS_TYPE_SLUG).order_by('-pk')

    def lastmod(self, obj):
        return obj.date_created


class CPFTSitemap(SiteMapMix):
    changefreq = 'monthly'

    def items(self):
        item = []
        urls = ['composer_list', 'performer_list', 'film_list',
                # 'shop-tag-list',
                ]
        for url_r in urls:
            item.append(reverse(url_r))
        return item

    def location(self, obj):
        return obj
