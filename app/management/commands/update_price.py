# -*- coding: utf-8 -*-
from django.core.management import BaseCommand
import logging

from offer.tasks import update_rate_currency
from shop.models import ShopOffers
from shop.utils import item_price_save

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Обновляем цены для аранжировок'

    def handle(self, **options):
        print('Обновили курс')
        update_rate_currency()
        offers = ShopOffers.objects.all().order_by('pk')
        for offer in offers:
            item_price_save(offer)
