# -*- coding: utf-8 -*-
import logging

from django.conf import settings
from django.core.management.base import BaseCommand

from articles.models import Article
from guitar.models import GuitarOffer

from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
import time
import math

from shop.models import ShopOffers

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Get and Set video information from youtube for content with video.'

    def handle(self, *args, **options):
        videos = []

        shop_videos = {}
        for offer in ShopOffers.objects.filter(video_thumbnail_url='').exclude(video_url__isnull=True):
            self.stdout.write('ShopOffer has video - %s (%d, %s)' % (offer, offer.pk, offer.video_id))
            videos.append(offer.video_id)
            shop_videos.update({offer.video_id: offer})

        try:
            service = build('youtube',
                            'v3',
                            developerKey=settings.YOUTUBE_DEVELOPER_API_KEY,
                            cache_discovery=False)
            # max 10 video id
            count = 10
            times = math.ceil(len(videos) / count)
            i = 0
            while i < times:
                self.stdout.write('Youtube request for %d time' % (i + 1))
                slice = i * count
                video_ids = ','.join(videos[slice: slice + count])
                results = service.videos().list(id=video_ids, part='snippet, contentDetails').execute()
                try:
                    for video_info in results["items"]:
                        video_id = video_info['id']
                        self.stdout.write('Try find object for video ID %s' % video_id)
                        try:
                            object = shop_videos[video_id]
                            self.stdout.write('Find Shop object %s' % object)
                        except KeyError:
                            self.stdout.write('Not find video for %s' % video_id)
                            continue
                        object.video_thumbnail_url = video_info['snippet']['thumbnails']['default']['url']
                        object.video_upload_date = video_info['snippet']['publishedAt']
                        object.video_duration = video_info['contentDetails']['duration']
                        object.save()
                except IndexError:
                    self.stdout.write('We have no results')
                i += 1
                time.sleep(10)

        except HttpError as e:
            logger.error('Get video info from youtube. An HTTP error %d occurred:\n%s' % (e.resp.status, e.content))
