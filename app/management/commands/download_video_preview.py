# encoding: utf-8

from __future__ import unicode_literals

from django.core.management.base import BaseCommand

from shop.models import ShopOffers
from youtube_video.utils import download_video_thumbnail


class Command(BaseCommand):
    help = 'Загрузка превью для уже существующих аранжировок'

    def handle(self, **options):
        print('Загрузка превью для уже существующих аранжировок')
        offers = ShopOffers.objects.all().order_by('pk')
        for offer in offers:
            if not offer.video_thumbnail:
                print("Получаем картинку для %s (%d)" % (offer, offer.id))
                download_video_thumbnail(offer)
