# -*- coding: utf-8 -*-
import logging

from django.conf import settings
from django.core.management import BaseCommand

from shop.models import ShopOffersType, TagOffers

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Задаем изначальным тегам тип - аранжировки'

    def handle(self, **options):
        print('Start')
        arrangements_type = ShopOffersType.objects.get(slug=settings.SHOP_ARRANGEMENTS_TYPE_SLUG)
        tags = TagOffers.objects.filter(shopoffers_type__isnull=True)
        for tag in tags:
            setattr(tag, 'shopoffers_type', arrangements_type)
            tag.save()
        print('End.')
