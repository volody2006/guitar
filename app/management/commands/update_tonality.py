# -*- coding: utf-8 -*-
import logging

from django.core.management import BaseCommand
from django.utils.translation import ugettext_lazy as _

from shop.models import ShopOffers

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Обновляем тональности'

    def handle(self, **options):
        print('Start')
        offers = ShopOffers.objects.filter(tonality=8)
        for offer in offers:
            print(offer.pk, offer)
            offer.tonality=9
            offer.save()
        print('End.')
