# encoding: utf-8

from __future__ import unicode_literals
from django.core.management.base import BaseCommand

from shop.models import ShopOffers


class Command(BaseCommand):
    help = 'Recalculate votes score for shop offers '

    def handle(self, *args, **options):
        for s in ShopOffers.objects.all():
            try:
                s.set_votes_score()
                s.set_votes_score_by_criteria()
            except:
                pass
        self.stdout.write('Аранжировки прошли')

        self.stdout.write('The end')
