# -*- coding: utf-8 -*-
from django.core.management import BaseCommand
import logging

from shop.models import ShopOffers

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Устанавливаем дату создания файлов равными дате создания товара'

    def handle(self, **options):
        print('Start')
        offers = ShopOffers.objects.all()
        for offer in offers:
            print(offer.pk, offer)
            for file in offer.userfile_set.all():
                print(file)
                file.date_created = offer.date_created
                file.save()
            print('-' * 10)
        print('End.')
