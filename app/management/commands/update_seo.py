# -*- coding: utf-8 -*-
from django.core.management import BaseCommand
import logging

from shop.models import ShopOffers

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Обновляем SEO аранжировок'

    def handle(self, **options):
        print('Start')
        offers = ShopOffers.objects.published()
        for offer in offers:
            print(offer.pk, offer)
            offer.seo_save(force=True)
        print('End.')
