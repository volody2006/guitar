# -*- coding: utf-8 -*-
import logging

from django.core.management import BaseCommand

from shop.models import Composer, Performer, Film

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Принимаем существующие CPF данные'

    def handle(self, **options):
        print('Start')
        for c in Composer.objects.all():
            setattr(c, 'accepted', True)
            c.save()
        for p in Performer.objects.all():
            setattr(p, 'accepted', True)
            p.save()
        for f in Film.objects.all():
            setattr(f, 'accepted', True)
            f.save()
        print('End.')
