# -*- coding: utf-8 -*-
import logging

from django.core.management import BaseCommand
from django.utils.translation import ugettext_lazy as _

from shop.models import UserFile

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Определить по возможности тип файлов.'

    def handle(self, **options):
        print('Start detect')
        files = UserFile.objects.filter(format_file__isnull=True).order_by('pk')
        print('files count {}'.format(files.count()))
        for file in files:
            file.save()
        print('End')

