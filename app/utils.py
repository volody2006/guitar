# -*- coding: utf-8 -*-
import logging
import os
import zipfile
from decimal import Decimal, ROUND_HALF_UP

from django.conf import settings
from django.db.models import Q
from django.template import loader
from django.utils.translation import ugettext_lazy as _
from haystack.query import SearchQuerySet

from base.utils import get_translation_fieldnames
from cart.models import PurchaseUser
from offer.models import Currency
from project import rel
from shop.models import Composer, Film, Performer, ShopOfferPrice, ShopOffers, UserFile
from users.models import User
from django.utils.translation import ugettext as _

logger = logging.getLogger(__name__)


class CustomQ(Q):
    default = 'OR'


def autocomplete_cpf(query):
    # logger.debug('autocomplete_cpf {}'.format(query))

    def sort_key(name):
        return name.find(query)

    result = []
    temp_list = []
    temp_list_lower = []
    if query and len(query) >= 3:
        sqs_all = SearchQuerySet().using('CPF').filter(published=True).autocomplete(content_auto=query)
        sqs = sqs_all[:100]
        for item in sqs:
            for name in item.name:
                if query.lower() in name.lower() and name.lower() not in temp_list_lower:
                    temp_list.append(name.strip())
                    temp_list_lower.append(name.lower())

        temp_set = set([result for result in temp_list])
        temp_list = list(temp_set)
        temp_list.sort()
        temp_list.sort(key=sort_key)
        for item in temp_list:
            result.append(item)
        logger.debug('sqs.count: {0}, query: {1}, result: {2}'.format(sqs_all.count(), query, result))
    return result


def autocomplete_model(query, model, fieldname='name'):
    result = []
    if query:
        fieldnames = get_translation_fieldnames(fieldname)
        queryset_kwargs = {}
        for f in fieldnames:
            f = '{}__icontains'.format(f)
            queryset_kwargs.update({f: query})
        q_object = CustomQ(**queryset_kwargs)
        data = model.objects.filter(q_object).values_list(fieldname, flat=True)
        for item in data:
            result.append(item)
    return result


def composer_autocomplete(query):
    return autocomplete_model(query=query, model=Composer)


def performer_autocomplete(query):
    return autocomplete_model(query=query, model=Performer)


def film_autocomplete(query):
    return autocomplete_model(query=query, model=Film)


def handle_uploaded_file(f):
    with open('some/file/name.txt', 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)


def compression_file(item, **kwargs):
    log = logger.info

    if not isinstance(item, ShopOffers):
        logger.info('compression_file start {} {}'.format(item, kwargs))
        item = ShopOffers.objects.get(pk=item)

    from shop.models import UserFile
    files = UserFile.objects.filter(item=item)
    if files.exists():
        log('compression_file {item_id} {message}'.format(item_id=item.pk, message='Удалил файлы'))
        item.file.delete()

    import time
    file_name_zip = '{}-{}-{}.zip'.format(item.slug, item.id, int(time.time()))
    zip_dir = '{}/{}'.format(settings.FILES_ROOT, 'user_{0}'.format(item.user.id))
    try:
        os.makedirs(zip_dir)
        log('compression_file {item_id} {message}'.format(item_id=item.pk, message='makedirs'))
    except os.error:
        pass
    with zipfile.ZipFile('{}/{}'.format(zip_dir, file_name_zip), 'w') as myzip:
        for file in files:
            arcname = '{}-{}'.format(file.pk, getattr(file, 'user_filename', file.pk))
            try:
                myzip.write(file.file.path, arcname)
                log('compression_file {item_id} {message}'.format(item_id=item.pk, message='myzip {}'.format(arcname)))
            except FileNotFoundError:
                logger.info('FileNotFoundError item_id: %s, file_name: %s', item.id, arcname)
                file.delete()

        filename = myzip.filename

    with open(filename, mode='rb') as f:
        item.file.save(file_name_zip, f)
        item.save()
        log('compression_file {item_id} {message}'.format(item_id=item.pk,
                                                          message='item save {}'.format(file_name_zip)))

    item = ShopOffers.objects.get(pk=item.id)
    log('compression_file {item_id} {message}'.format(item_id=item.pk, message='item.file {}'.format(item.file)))


def _compression_file_all_item():
    from shop.models import ShopOffers
    items = ShopOffers.objects.all()
    for item in items:
        compression_file(item.id)


def count(request):
    from shop.models import ShopOffers
    return ShopOffers.objects.all().count()


def new_shop_offers_count(type_slug=None):
    from shop.models import ShopOffers
    queryset = ShopOffers.objects.active(checked=False, is_available=True)
    if type_slug:
        queryset = queryset.filter(shopoffers_type__slug=type_slug)
    return queryset.count()


def get_difficulty_variants():
    from shop.models import ShopOffers
    return [int(key) for (key, value) in ShopOffers.DIFFICULTY_CHOICES]


def get_tonality_variants():
    from shop.models import ShopOffers
    return [int(key) for (key, value) in ShopOffers.TONALITY_CHOICES]


def get_select_author_choices(category, difficulty, tonality):
    from shop.models import ShopOffers
    queryset = ShopOffers.objects.published()

    user_ids = []
    try:
        user_ids = list(
            queryset.order_by('user_created_id').distinct('user_created').values_list('user_created_id', flat=True)
        )
    except NotImplementedError as e:
        logger.exception('Error %s', e)
    from users.models import User
    users = User.objects.filter(is_active=True, pk__in=user_ids).order_by('last_name')
    author_choices = (('0', _('Все авторы')),)
    author_choices = author_choices + tuple(
        ((user.pk, '{name} ({count})'.format(name=user.get_full_name_for_filter(),
                                             count=user.get_shop_offers_count(category, difficulty)
                                             )
          ) for user in users))
    return author_choices


def item_price_save(item, save_force=False):
    if isinstance(item, ShopOffers):
        item = item
    else:
        item = ShopOffers.objects.get(pk=item)
    for cur in Currency.objects.all():
        price = None
        try:
            if item.price and item.currency.iso_code == 'RUB' and cur.iso_code != 'RUB':
                price = item.price / cur.rate_rub
            elif item.price and item.currency.iso_code == 'RUB' and cur.iso_code == 'RUB':
                price = item.price
            elif item.price and cur.iso_code == 'RUB':
                price = item.price * item.currency.rate_rub
            else:
                if cur == item.currency:
                    price = item.price
                else:
                    if cur.rate_rub:
                        price_rub = item.price * item.currency.rate_rub
                        price = price_rub / cur.rate_rub
        except TypeError:
            logger.debug('Error rate {} {}'.format(cur, cur.rate_rub))
        if price:
            price = Decimal(price).quantize(Decimal('1.00'), ROUND_HALF_UP)
            price_attr = 'price_{}'.format(cur.iso_code.lower())
            if hasattr(item, price_attr):
                setattr(item, price_attr, price)  # x.y = v
                save_force = True
            ShopOfferPrice.objects.update_or_create(
                item=item,
                currency=cur,
                defaults={
                    'price': price
                }
            )
    if save_force:
        item.save()


def latest_offer(request):
    return ShopOffers.objects.published().ordered('-pk')[0].modified_date


def disable_all_discounts(sale_day=False):
    logger.info('run disable_all_discounts')
    if sale_day is True:
        old_offer = ShopOffers.objects.filter(sale_day=True)
    else:
        old_offer = ShopOffers.objects.filter(discount__gt=0)
    old_offer_ids = list(old_offer.values_list('id', flat=True))
    old_offer_sellers_ids = list(old_offer.values_list('user_created_id', flat=True))
    old_offer.update(discount=0, freeze=False,
                     date_start_freeze=None,
                     date_end_freeze=None,
                     cause_freezing='',
                     )
    if sale_day:
        old_offer.update(sale_day=False)

    return {'offers_ids': old_offer_ids, 'sellers_ids': old_offer_sellers_ids}


def compression_all_file_user(user, shopoffers_type, **kwargs):
    if not isinstance(user, User):
        try:
            user = User.objects.get(pk=user)
        except User.DoesNotExist:
            logger.debug('Пользователь {} не найден'.format(user))
            return

    if user.is_buyer is False:
        logger.debug('Пользователь {} не имеет покупок. Нет ручек, нет конфеток. )'.format(user))
        return

    logger.info('compression_file start user_id:{}'.format(user.pk))

    def makedirs(path):
        try:
            os.makedirs(path)
        except os.error:
            pass

    purchases = PurchaseUser.objects.filter(user=user, shop_offer__shopoffers_type_id=shopoffers_type.id)
    file_name_zip = 'purchases-{}-{}.zip'.format(shopoffers_type.slug, user.pk)
    zip_dir = '{}/{}'.format(settings.FILES_ROOT, 'user_{0}'.format(user.id))
    makedirs(zip_dir)

    full_file_name_zip = '{}/{}'.format(zip_dir, file_name_zip)
    temp_file_name = 'temp-{}'.format(file_name_zip)

    temp_dir = rel(settings.FILES_ROOT, 'tmp')
    makedirs(temp_dir)
    temp_full_file_name_zip = '{}/{}'.format(temp_dir, temp_file_name)

    with zipfile.ZipFile(temp_full_file_name_zip, 'w') as myzip:
        for purchase in purchases:
            try:
                myzip.write(purchase.get_file_path(), purchase.get_file_name())
            except FileNotFoundError:
                logger.debug('FileNotFoundError purchase_id: %s, file_name: %s', purchase.id, purchase.get_file_name())
    try:
        os.replace(temp_full_file_name_zip, full_file_name_zip)
    except FileNotFoundError:
        pass


def notifying_all_users_about_discounts(offers_id=[]):
    '''
    По списку оферов вычисляем пользователей достойных получить ЛС.
    :param offers_id:
    :return:
    '''
    from users.models import UserFavoritesShopOffers
    from pinax.messages.utils import send_system_pm
    user_list = []
    for id in offers_id:
        if UserFavoritesShopOffers.objects.filter(offer_id=id).exists():
            user_list.extend(UserFavoritesShopOffers.objects.filter(offer_id=id).values_list('user_id', flat=True))
    user_id_set = set(user_list)

    for user_id in user_id_set:
        user = User.objects.get(id=user_id)
        country = None
        currency = None
        different = 0
        offers_favorits = UserFavoritesShopOffers.objects.filter(offer_id__in=offers_id, user_id=user_id)
        if user.reg_country:
            country = user.reg_country

            for offer_favorit in offers_favorits:
                offer = offer_favorit.offer
                price, currency = offer.get_price_country(country)
                discount_price, discount_currency = offer.get_discount_price_country(country)
                different = different + (price - discount_price)
            # покупателю ЛС
        if country:
            currency = country.get_currency()
        send_system_pm(recipient=user,
                       subject='%s %s' % (_('Скидки на Избранные аранжировки на'), settings.SITE_PROJECT_NAME),
                       content=loader.render_to_string(
                           'base/pm/favorites_sale.html',
                           {
                               'recipient': user,
                               'offers_favorits': offers_favorits,
                               'currency': currency,
                               'different': different,
                               'country': country
                           }
                       ),
                       signal=False
                       )


def convert_user_pdf_to_shop_offer_image(offer):
    """
    Конвертирует первый пдф файл в изображение, сохраняет его в товаре
    :return:
    """
    logger.info('convert_user_pdf_to_shop_offer_image {} {}'.format(offer.pk, offer))
    from pdf2image import convert_from_path
    from pdf2image.exceptions import (PDFInfoNotInstalledError, PDFPageCountError, PDFSyntaxError)
    import tempfile
    from django.core.files.base import ContentFile
    import uuid
    from io import BytesIO
    try:
        file = UserFile.objects.filter(item=offer, mime_type='pdf')[0]
        with tempfile.TemporaryDirectory() as path:
            try:
                image_from_path = convert_from_path(file.get_file_path(), output_folder=path,
                                                    first_page=1, last_page=1, fmt='png')[0]
                width, height = image_from_path.size
                if offer.is_arrangement:
                    image_height = height // 3
                else:
                    image_height = height
                image_from_path = image_from_path.crop((0, 0, width, image_height))
                ratio = width / 800
                ratio_height = image_height / ratio
                size = 800, ratio_height
                image_from_path.thumbnail(size)

                new_image_io = BytesIO()
                image_from_path.save(new_image_io, format='PNG')

                temp_name = uuid.uuid4().hex
                offer.image.delete(save=False)

                offer.image.save(
                    temp_name,
                    content=ContentFile(new_image_io.getvalue()),
                    save=False
                )
                offer.save()
                logger.info('convert_user_pdf_to_shop_offer_image {} {}. Закончил.'.format(offer.pk, offer))
            except PDFPageCountError:
                pass
    except IndexError:
        pass
