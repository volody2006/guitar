# Generated by Django 2.2.5 on 2020-05-28 12:56

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django_resized.forms
import shop.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('shop', '0023_merge_20200528_1031'),
    ]

    operations = [
        migrations.AddField(
            model_name='composer',
            name='accepted',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='composer',
            name='user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='film',
            name='accepted',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='film',
            name='user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='performer',
            name='accepted',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='performer',
            name='user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='shopoffers',
            name='video_thumbnail',
            field=django_resized.forms.ResizedImageField(blank=True, crop=['middle', 'center'], force_format='PNG', keep_meta=True, null=True, quality=75, size=[170, 170], upload_to=shop.models.upload_youtube_preview, verbose_name='Видео превью'),
        ),
        migrations.AlterField(
            model_name='userfile',
            name='page_count',
            field=models.PositiveSmallIntegerField(default=0, editable=False, help_text='Количество страниц. Актуально для pdf файлов', verbose_name='Количество страниц'),
        ),
    ]
