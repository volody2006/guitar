# Generated by Django 2.2.5 on 2019-12-05 08:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0010_auto_20191125_0738'),
    ]

    operations = [
        migrations.AddField(
            model_name='sale',
            name='announce',
            field=models.TextField(blank=True, default='', help_text='Краткий анонс вашей статьи. Будет выводиться вместе с миниатюрой в списке статей раздела. Если оставить пустым, в качестве анонса будут использоваться первые предложения основного текста.', verbose_name='Анонс'),
        ),
        migrations.AddField(
            model_name='sale',
            name='announce_de',
            field=models.TextField(blank=True, default='', help_text='Краткий анонс вашей статьи. Будет выводиться вместе с миниатюрой в списке статей раздела. Если оставить пустым, в качестве анонса будут использоваться первые предложения основного текста.', null=True, verbose_name='Анонс'),
        ),
        migrations.AddField(
            model_name='sale',
            name='announce_en',
            field=models.TextField(blank=True, default='', help_text='Краткий анонс вашей статьи. Будет выводиться вместе с миниатюрой в списке статей раздела. Если оставить пустым, в качестве анонса будут использоваться первые предложения основного текста.', null=True, verbose_name='Анонс'),
        ),
        migrations.AddField(
            model_name='sale',
            name='announce_es',
            field=models.TextField(blank=True, default='', help_text='Краткий анонс вашей статьи. Будет выводиться вместе с миниатюрой в списке статей раздела. Если оставить пустым, в качестве анонса будут использоваться первые предложения основного текста.', null=True, verbose_name='Анонс'),
        ),
        migrations.AddField(
            model_name='sale',
            name='announce_fr',
            field=models.TextField(blank=True, default='', help_text='Краткий анонс вашей статьи. Будет выводиться вместе с миниатюрой в списке статей раздела. Если оставить пустым, в качестве анонса будут использоваться первые предложения основного текста.', null=True, verbose_name='Анонс'),
        ),
        migrations.AddField(
            model_name='sale',
            name='announce_it',
            field=models.TextField(blank=True, default='', help_text='Краткий анонс вашей статьи. Будет выводиться вместе с миниатюрой в списке статей раздела. Если оставить пустым, в качестве анонса будут использоваться первые предложения основного текста.', null=True, verbose_name='Анонс'),
        ),
        migrations.AddField(
            model_name='sale',
            name='announce_pt',
            field=models.TextField(blank=True, default='', help_text='Краткий анонс вашей статьи. Будет выводиться вместе с миниатюрой в списке статей раздела. Если оставить пустым, в качестве анонса будут использоваться первые предложения основного текста.', null=True, verbose_name='Анонс'),
        ),
        migrations.AddField(
            model_name='sale',
            name='announce_ru',
            field=models.TextField(blank=True, default='', help_text='Краткий анонс вашей статьи. Будет выводиться вместе с миниатюрой в списке статей раздела. Если оставить пустым, в качестве анонса будут использоваться первые предложения основного текста.', null=True, verbose_name='Анонс'),
        ),
        migrations.AddField(
            model_name='sale',
            name='announce_zh_hans',
            field=models.TextField(blank=True, default='', help_text='Краткий анонс вашей статьи. Будет выводиться вместе с миниатюрой в списке статей раздела. Если оставить пустым, в качестве анонса будут использоваться первые предложения основного текста.', null=True, verbose_name='Анонс'),
        ),
    ]
