# Generated by Django 2.2.5 on 2020-05-25 09:30

from django.db import migrations
from shop.models import UserFile


def set_files_user_created(apps, schema_editor):
    if UserFile.objects.all().count():
        for file in UserFile.objects.all():
            setattr(file, 'user_created', file.item.user_created)
            file.save()


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0020_auto_20200525_1628'),
    ]

    operations = [
        migrations.RunPython(set_files_user_created),
    ]
