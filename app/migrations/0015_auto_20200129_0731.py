# Generated by Django 2.2.5 on 2020-01-29 04:31

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0014_shopoffers_refund'),
    ]

    operations = [
        migrations.CreateModel(
            name='MimeTypeFile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='', max_length=255, verbose_name='Описание типа файла.')),
                ('name_ru', models.CharField(default='', max_length=255, null=True, verbose_name='Описание типа файла.')),
                ('name_en', models.CharField(default='', max_length=255, null=True, verbose_name='Описание типа файла.')),
                ('name_de', models.CharField(default='', max_length=255, null=True, verbose_name='Описание типа файла.')),
                ('name_es', models.CharField(default='', max_length=255, null=True, verbose_name='Описание типа файла.')),
                ('name_fr', models.CharField(default='', max_length=255, null=True, verbose_name='Описание типа файла.')),
                ('name_it', models.CharField(default='', max_length=255, null=True, verbose_name='Описание типа файла.')),
                ('name_pt', models.CharField(default='', max_length=255, null=True, verbose_name='Описание типа файла.')),
                ('name_zh_hans', models.CharField(default='', max_length=255, null=True, verbose_name='Описание типа файла.')),
                ('mime_type', models.CharField(default='', max_length=5, verbose_name='Тип файла. Расширение.')),
                ('autodetect', models.BooleanField(default=False, verbose_name='Автоопределение')),
            ],
        ),
        migrations.AddField(
            model_name='userfile',
            name='format_file',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='shop.MimeTypeFile', verbose_name='Тип файла'),
        ),
    ]
