# -*- coding: utf-8 -*-
import logging

from django.db.models.signals import post_save
from django.forms.models import _get_foreign_key
from django.utils import translation

from shop.models import ShopOffers
from youtube_video.tasks import task_get_video_info

logger = logging.getLogger(__name__)


# def shop_cpf_save(sender, instance, **kwargs):
#     cpf = instance
#     if cpf.accepted:
#         with translation.override(cpf.lang.code):
#             if cpf.get_cpf_model():
#                 model = cpf.get_cpf_model()
#                 cpf_obj, _ = model.objects.update_or_create(name=cpf.name)
#                 fk = _get_foreign_key(model, ShopOffers)
#
#                 for offer in cpf.offer.all():
#                     setattr(offer, fk.name, cpf_obj)
#                     offer.save()
#                     cpf.offer.remove(offer)
#                     # TODO: Разрыв связи не работает.


def notifications_after_creating_a_purchase(sender, instance, **kwargs):
    '''
    Уведомления после создания покупки
    :return:
    '''
    purchase_user = instance
    logger.info('PurchaseUser. Выдана покупка. Проверяю правомерность.')
    from cart.models import PurchaseUser
    purchase_user = PurchaseUser.objects.get(pk=instance.pk)
    if purchase_user.shop_offer.is_free:
        logger.info('Покупка бесплатная {}, товар:{} {}, цена товара: {}'.format(
            purchase_user,
            purchase_user.shop_offer.pk,
            purchase_user.shop_offer,
            purchase_user.shop_offer.price
        ))
    else:
        logger.info('Покупка платная {}, товар:{} {}, цена товара: {}, счет: {}'.format(
            purchase_user,
            purchase_user.shop_offer.pk,
            purchase_user.shop_offer,
            purchase_user.shop_offer.price,
            purchase_user.payment
        ))
        if purchase_user.payment is None and purchase_user.user is None:
            raise Exception('У покупки нет счета!!!')


post_save.connect(notifications_after_creating_a_purchase, sender='cart.PurchaseUser')
