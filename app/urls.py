# coding: utf-8
from django.conf import settings
from django.urls import include, path
from django.utils.translation import ugettext_lazy as _

from shop.models import Composer, Film, Performer
from shop.views import ShopLoadVideoPlayerView, autocomplete, buy, cpf, edit, moderator, tags, views
from shop.views.views import TempTestView

urlpatterns = [
    # системные ссылки, напрямуе не используемые пользователями
    path('test/', TempTestView.as_view(), name='test'),
    path('cpf-select/', autocomplete.CPFView.as_view(), name='cpf_select'),

    path('tag/', include([
        # path('', tags.TagOffersListView.as_view(), name='tag-list'),
        path('<str:tag>/', tags.TagOffersPageView.as_view(), name='tag-main'),
    ])),

    path('shop/', include([
        path('', views.ShopMainPageView.as_view(), name='shop-main'),
        path('moder/', moderator.ShopListForModerator.as_view(), name='shop-moder-list'),

        path('<int:pk>/', include([
            path('', views.ShopDetailView.as_view(), name='shop-detail'),
            path('edit/', edit.ShopUpdateView.as_view(), name='shop-edit'),  # @todo удалить
            path('load_player/', ShopLoadVideoPlayerView.as_view(), name='shop-load-player'),
            path('techniques/',
                 edit.ShopUpdateReactView.as_view(template_name='shop/forms/react-form.html',
                                                  title=_('Применяемые техники')),
                 name='shop-techniques'),
            path('related-offers/',
                 edit.ShopUpdateReactView.as_view(template_name='shop/forms/react-form.html',
                                                  title=_('Связанные товары')),
                 name='shop-related-offers'),
            path('song-text/', edit.ShopUpdateSongTextView.as_view(), name='shop-song-text'),
        ])),
    ])),

    path('{}-{}/'.format(settings.SITE_PROJECT, settings.SHOP_ARRANGEMENTS_TYPE_SLUG), include([
        # @todo удалить, когда форма на реакте будет всех устраивать (add)
        path('composer-select/', autocomplete.ComposerAutocompleteView.as_view(), name='composer_select'),
        path('performer-select/', autocomplete.PerformerAutocompleteView.as_view(), name='performer_select'),
        path('film-select/', autocomplete.FilmAutocompleteView.as_view(), name='film_select'),

        path('composer/', include([
            path('', cpf.CPFListView.as_view(model=Composer), name='composer_list'),
            path('<slug:slug>/', cpf.CPFPageView.as_view(cpf_model=Composer), name='composer_main'),
        ])),

        path('performer/', include([
            path('', cpf.CPFListView.as_view(model=Performer), name='performer_list'),
            path('<slug:slug>/', cpf.CPFPageView.as_view(cpf_model=Performer), name='performer_main'),
        ])),

        path('film/', include([
            path('', cpf.CPFListView.as_view(model=Film), name='film_list'),
            path('<slug:slug>/', cpf.CPFPageView.as_view(cpf_model=Film), name='film_main'),
        ])),
    ])),

    path('{}-<slug:shopoffers_type_slug>/'.format(settings.SITE_PROJECT), include([
        path('', views.ShopMainPageView.as_view(), name='shop-type-main'),

        path('tag/', include([
            path('', tags.TagOffersListView.as_view(), name='shop-tag-list'),
            path('<str:tag>/', tags.TagOffersPageView.as_view(), name='shop-tag-main'),
        ])),

        path('<slug:slug>/', include([
            path('', views.ShopDetailView.as_view(), name='shop-detail'),
            path('buy/', buy.ShopBuyView.as_view(), name='shop-buy'),
        ]))
    ])),
]
