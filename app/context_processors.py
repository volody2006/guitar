# encoding: utf-8
import logging
from django.conf import settings
from django.db.models import Q

from shop.models import ShopOffers

logger = logging.getLogger(__name__)


def lessons_count(request):
    return {'LESSONS_LANG_COUNT': ShopOffers.objects.filter(shopoffers_type__slug=settings.SHOP_LESSONS_TYPE_SLUG)
                                    .filter(Q(lang__code=request.LANGUAGE_CODE) | Q(lang__code__isnull=True)).count()}
