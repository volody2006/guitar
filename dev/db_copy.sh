#!/bin/sh
PATH=/etc:/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin

PGPASSWORD=
PGDATABASE=guitarsolo
PGUSER=guitarsolo
PGPORT=5434
export PGPASSWORD
export PGDATABASE
export PGUSER
export PGPORT
pathB=~/backup/

pg_dump -c -C --file=$pathB/pgsql_$(date "+%Y-%m-%d").sql -O -v --attribute-inserts --inserts

unset PGPASSWORD
unset PGDATABASE
unset PGUSER
unset PGPORT